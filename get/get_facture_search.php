<?php
ob_start();
session_start();
include '../connexion.php';



if(isset($_SESSION['user_einvoicetrack']))
{
	$datatable_data = array();
	$i = 0 ; 


$SQL="SELECT f.NUM_FACTURE, f.STATUS,s.NOM_STATUS,c.NOM_CLIENT, f.NUMERO_CLIENT, c.ADRESSE1, f.DATE_COMPTABILISATION, f.BC, f.BL,f.pdf
		FROM facture f , ( SELECT NOM_CLIENT,ADRESSE1,NUMERO_CLIENT,VILLE
							FROM client 
							UNION ALL 
							SELECT NOM_CLIENT,ADRESSE1,NUMERO_CLIENT,VILLE
							FROM client_history ) c, status s 
		WHERE 
			( f.NUMERO_CLIENT = c.NUMERO_CLIENT )
		AND f.STATUS = s.CODE_STATUS 
		
		
	
		";
					
	if (isset($_POST['NUM_FACTURE']))
	{
		$SQL .= " AND ( " ; 
		foreach ($_POST['NUM_FACTURE'] as $selectedOption)
		{
			$SQL .= " f.NUM_FACTURE LIKE \"%$selectedOption%\" OR " ; 
		}
		$SQL = substr($SQL, 0, -3);
		$SQL .= " )";
	}
	
					
	if (isset($_POST['NOM_CLIENT']))
	{
		$SQL .= " AND ( " ; 
		foreach ($_POST['NOM_CLIENT'] as $selectedOption)
		{
			$SQL .= " c.NOM_CLIENT LIKE \"%$selectedOption%\" OR " ; 
		}
		$SQL = substr($SQL, 0, -3);
		$SQL .= " )";
	}
					
	if (isset($_POST['NUMERO_CLIENT']))
	{
		$SQL .= " AND ( " ; 
		foreach ($_POST['NUMERO_CLIENT'] as $selectedOption)
		{
			$SQL .= " c.NUMERO_CLIENT LIKE \"%$selectedOption%\" OR " ; 
		}
		$SQL = substr($SQL, 0, -3);
		$SQL .= " )";
	}
	
					
	if (isset($_POST['BC']))
	{
		$SQL .= " AND ( " ; 
		foreach ($_POST['BC'] as $selectedOption)
		{
			$SQL .= " f.BC LIKE \"%$selectedOption%\" OR " ; 
		}
		$SQL = substr($SQL, 0, -3);
		$SQL .= " )";
	}				
	if (isset($_POST['BL']))
	{
		$SQL .= " AND ( " ; 
		foreach ($_POST['BL'] as $selectedOption)
		{
			$SQL .= " f.BL LIKE \"%$selectedOption%\" OR " ; 
		}
		$SQL = substr($SQL, 0, -3);
		$SQL .= " )";
	}
					
	if (isset($_POST['STATUS']))
	{
		$SQL .= " AND ( " ; 
		foreach ($_POST['STATUS'] as $selectedOption)
		{
			$SQL .= " f.STATUS = '".decode($selectedOption)."' OR " ; 
		}
		$SQL = substr($SQL, 0, -3);
		$SQL .= " )";
	}
	
					
	if (($_POST['DATE_COMPTABILISATION_START'] != '') &&  ($_POST['DATE_COMPTABILISATION_END'] != '' )  )
	{	 

		$SQL .= " AND  DATE_COMPTABILISATION between '".$_POST['DATE_COMPTABILISATION_START']."' and '".$_POST['DATE_COMPTABILISATION_END']."'    " ; 
	}
					
	if (($_POST['DATE_DISTRIBUTION_START'] != '') &&  ($_POST['DATE_DISTRIBUTION_END'] != '' )  )
	{	 
		$SQL .= " AND  f.NUM_FACTURE IN ( 
								SELECT fs.NUM_FACTURE
								FROM facture_status fs
								WHERE fs.DATE between '".$_POST['DATE_DISTRIBUTION_START']."' and '".$_POST['DATE_DISTRIBUTION_END']."'					
									)   
							" ; 

	}
					
					
	if (isset($_POST['VILLE_CLIENT']))
	{	 
		$SQL .= " AND ( " ; 
		foreach ($_POST['VILLE_CLIENT'] as $selectedOption)
		{
			$selectedOption = decode($selectedOption);
			$SQL .= " c.VILLE = '$selectedOption' OR " ; 
		}
		$SQL = substr($SQL, 0, -3);
		$SQL .= " )";
	}

	$SQL .= " GROUP BY f.NUM_FACTURE" ; 
	// $SQL .= " LIMIT 100" ; 
// echo $SQL ;
 
		
	$query=mysqli_query($ma_connexion,$SQL);
	while($row=mysqli_fetch_assoc($query))
	{		

				 $datatable_data[$i]["check_box"] = '';
				 $datatable_data[$i]["NUM_FACTURE"] = $row['NUM_FACTURE'];
				 $datatable_data[$i]["STATUS"] = $row['NOM_STATUS'];
				 $datatable_data[$i]["NOM_CLIENT"] = $row['NOM_CLIENT'];
				 $datatable_data[$i]["CODE_CLIENT"] = $row['NUMERO_CLIENT'];
				 $datatable_data[$i]["ADRESSE1"] = $row['ADRESSE1'];
				 $datatable_data[$i]["DATE_COMPTABILISATION"] = date('Y-m-d', strtotime($row['DATE_COMPTABILISATION']));
				 
				$datatable_data[$i]["MOIS_DISTRIBUTION"] = '';
				
				
				
				 $query2 = " SELECT fs.DATE
					FROM facture_status fs
					WHERE fs.NUM_FACTURE =  '".$row['NUM_FACTURE']."'
					AND fs.CODE_STATUS != 1
					ORDER BY fs.CODE_ DESC
					LIMIT 1
						 ";
						$result2 = mysqli_query($ma_connexion, $query2); 
					   while(($row2 = mysqli_fetch_array($result2)) == true )  
						{ 										
							
							$datatable_data[$i]["MOIS_DISTRIBUTION"] .= '<b>'.date('M Y', strtotime($row2['DATE'])).'</b> &nbsp; ';
							$datatable_data[$i]["MOIS_DISTRIBUTION"] .= ' <small> ('.date('Y-m-d', strtotime($row2['DATE'])).')</small> ';
							 
						}
				
				 $datatable_data[$i]["MOTIF"] = '';
				 $datatable_data[$i]["description"] = '';
				 
				 if($row['STATUS'] == 3)
				 {
					 $query2 = " SELECT m.NOM_MOTIF,fs.description
								FROM facture_status fs , motif m
								WHERE fs.motif =m.CODE_MOTIF
								AND fs.NUM_FACTURE = '".$row['NUM_FACTURE']."'
								ORDER BY fs.CODE_ DESC
								LIMIT 1
						 ";
						$result2 = mysqli_query($ma_connexion, $query2); 
					   while(($row2 = mysqli_fetch_array($result2)) == true )  
						{ 										
							
							$datatable_data[$i]["MOTIF"] = $row2['NOM_MOTIF'];
							$datatable_data[$i]["description"] = $row2['description'];
							 
						}
				 }

					
				 $datatable_data[$i]["BC"] = $row['BC'];
				 $datatable_data[$i]["BL"] = $row['BL'];
				 
				 
				 
				 $datatable_data[$i]["DETAILL"] = '
 
					<button type="button"  class="btn btn-primary btn-circle detail"  value="'.urlencode($row['NUM_FACTURE']).'">
						<i class="fa fa-list"></i>
					</button>  ' ; 
					
				if ($_SESSION['role'] ==  'superadmin' ||  $_SESSION['role'] ==  'admin'  )	
					 $datatable_data[$i]["DETAILL"] .= '
						<button type="button"  class="btn btn-danger btn-circle delete"  value="'.urlencode($row['NUM_FACTURE']).'">
							<i class="fa fa-trash"></i>
						</button> 
						';
						
						
				 if(	$row['STATUS'] != 2  && ( $_SESSION['role'] ==  'superadmin' ||  $_SESSION['role'] ==  'admin'  ) ) 
				 {
					 
					  $datatable_data[$i]["DETAILL"] .= '
						<button type="button"  class="btn btn-success btn-circle edit"  value="'.urlencode($row['NUM_FACTURE']).'">
							<i class="fa fa-edit"></i>
						</button> 
						';
				 }
				 
//				 if($row['pdf'] == 1 || (file_exists('../einvoicetrack.'.DIRECTORY_SEPARATOR.$row['NUM_FACTURE'].'.pdf')) )
				 if($row['pdf'] == 1 )

				 {
					 
					 $datatable_data[$i]["DETAILL"] .= '
						<button type="button"   class="btn btn-dark btn-circle pdf_dwn"    value="'.urlencode($row['NUM_FACTURE']).'">
							<i class="fa  fa-file-pdf-o"></i>
						</button> ';
				 }
				 
				 

			 $i++ ; 
	}
	
	echo json_encode($datatable_data);
}
ob_end_flush();
?>

