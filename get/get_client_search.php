<?php
ob_start();
session_start();
include '../connexion.php';



if(isset($_SESSION['user_einvoicetrack']))
{
	$result = array() ; 
	$datatable_data = array();
	$GPS_data = array();
	$i = 0 ; 


	$SQL="SELECT `NUMERO_CLIENT`, 
					`NOM_CLIENT`, `TELE1`, `TELE2`, 
					`ADRESSE1`, `SECTEUR1`, `ADRESSE2`, `SECTEUR2`,
					`VILLE`, `Longitude`, `Latitude`, `DESCRIPTION`
					, `CONTACT`, `TYPE_CLIENT`, `NATURE_CLIENT`
					FROM `client` WHERE ETAT = 1 ";
					
	if (isset($_POST['NOM_CLIENT']))
	{
		$SQL .= " AND ( " ; 
		foreach ($_POST['NOM_CLIENT'] as $selectedOption)
		{
			$SQL .= " NOM_CLIENT LIKE \"%$selectedOption%\" OR " ; 
		}
		$SQL = substr($SQL, 0, -3);
		$SQL .= " )";
	}
	
					
	if (isset($_POST['NUMERO_CLIENT']))
	{	 
		foreach ($_POST['NUMERO_CLIENT'] as $selectedOption)
		{
			$SQL .= " AND NUMERO_CLIENT LIKE \"%$selectedOption%\" " ; 
		}
	}
					
	if (isset($_POST['TELE_CLIENT']))
	{	 
		foreach ($_POST['TELE_CLIENT'] as $selectedOption)
		{
			$SQL .= " AND ( TELE1 LIKE \"%$selectedOption%\" OR TELE2  LIKE \"%$selectedOption%\"  )" ; 
		}
	}
					
	if (isset($_POST['ADRESSE_CLIENT']))
	{	 
		foreach ($_POST['ADRESSE_CLIENT'] as $selectedOption)
		{
			$SQL .= " AND ( ADRESSE1 LIKE \"%$selectedOption%\" OR ADRESSE2  LIKE \"%$selectedOption%\"  )" ; 
		}
	}
	
					
	if (isset($_POST['VILLE_CLIENT']))
	{

		$SQL .= " AND ( " ;
		foreach ($_POST['VILLE_CLIENT'] as $selectedOption)
		{
//			$selectedOption = decode($selectedOption);
			$SQL .= " VILLE LIKE LOWER('$selectedOption')  OR " ;
//			$SQL .= " VILLE COLLATE Latin1_general_CI_AI Like '$selectedOption' COLLATE Latin1_general_CI_AI  OR " ;

		}
		$SQL = substr($SQL, 0, -3);
		$SQL .= " )";


	}
	
					
	if (isset($_POST['SECTEUR_CLIENT']))
	{	 
		foreach ($_POST['SECTEUR_CLIENT'] as $selectedOption)
		{
			$SQL .= " AND ( SECTEUR1 LIKE \"%$selectedOption%\" OR SECTEUR2  LIKE \"%$selectedOption%\"  )" ; 
		}
	}


	if (isset($_POST['CONTACT']))
	{
		foreach ($_POST['CONTACT'] as $selectedOption)
		{
			$SQL .= " AND CONTACT LIKE \"%$selectedOption%\" " ;
		}
	}

	if (isset($_POST['TYPE_CLIENT']))
	{
		foreach ($_POST['TYPE_CLIENT'] as $selectedOption)
		{
			$SQL .= " AND TYPE_CLIENT LIKE \"%$selectedOption%\" " ;
		}
	}
		if (isset($_POST['NATURE_CLIENT']))
	{
		foreach ($_POST['NATURE_CLIENT'] as $selectedOption)
		{
			$SQL .= " AND NATURE_CLIENT LIKE \"%$selectedOption%\" " ;
		}
	}

	
	// $SQL .= " ORDER BY ETAT" ; 
	// $SQL .= " LIMIT 100" ; 
// echo $SQL ;
 
	


	
	$query=mysqli_query($ma_connexion,$SQL);
	while($row=mysqli_fetch_assoc($query))
	{
			$datatable_data[$i]["check_box"] = '';
			 $datatable_data[$i]["NOM"] = $row['NOM_CLIENT'];
			 $datatable_data[$i]["NUMERO"] = $row['NUMERO_CLIENT'];
			 $datatable_data[$i]["TEL1"] = $row['TELE1'];
			 $datatable_data[$i]["TEL2"] = $row['TELE2'];
			 $datatable_data[$i]["ADRESSE1"] = $row['ADRESSE1'];
			 $datatable_data[$i]["SECTEUR1"] = $row['SECTEUR1'];
			 $datatable_data[$i]["ADRESSE2"] = $row['ADRESSE2'];
			 $datatable_data[$i]["SECTEUR2"] = $row['SECTEUR2'];
			 $datatable_data[$i]["VILLE"] =  $row['VILLE'];
//			 $query2 = " SELECT v.NOM_VILLE
//						FROM ville v
//						WHERE v.CODE_VILLE =  '".$row['VILLE']."'
//				 ";
//				$result2 = mysqli_query($ma_connexion, $query2);
//			   while(($row2 = mysqli_fetch_array($result2)) == true )
//				{
//
//					$datatable_data[$i]["VILLE"] = $row2['NOM_VILLE'];
//
//				}
			 
			 $datatable_data[$i]["GPS"] = "(".$row['Latitude'].", ".$row['Longitude'].")";
			 $datatable_data[$i]["DESCRIPTION"] = $row['DESCRIPTION'];	
			 // if ( $row['ETAT'] ==  1 ) 
					 // $datatable_data[$i]["ETAT"] = '<span class="label label-primary">Actif</span>' ; 
				// else 
					// $datatable_data[$i]["ETAT"] = '<span class="label label-danger">supprimé</span>' ; 

		$datatable_data[$i]["CONTACT"] = $row['CONTACT'];
		$datatable_data[$i]["TYPE_CLIENT"] = $row['TYPE_CLIENT'];
		$datatable_data[$i]["NATURE_CLIENT"] = $row['NATURE_CLIENT'];


		if( $_SESSION['role'] ==  'superadmin' ||  $_SESSION['role'] ==  'admin' )
			{
			 $datatable_data[$i]["DETAILL"] = '
			 
									<button type="button"  class="btn btn-primary btn-circle detail"  value="'.urlencode($row['NUMERO_CLIENT']).'">
										<i class="fa fa-list"></i>
									</button> 
									<button type="button"  class="btn btn-success btn-circle edit"  value="'.urlencode($row['NUMERO_CLIENT']).'">
										<i class="fa fa-edit"></i>
									</button> 
									<button type="button"  class="btn btn-danger btn-circle delete"  value="'.urlencode($row['NUMERO_CLIENT']).'">
										<i class="fa fa-trash"></i>
									</button> 
									';
			}
			else 
			{
				 $datatable_data[$i]["DETAILL"] = '
			 
									<button type="button"  class="btn btn-primary btn-circle detail"  value="'.urlencode($row['NUMERO_CLIENT']).'">
										<i class="fa fa-list"></i>
									</button> 
									
									';
				
			}
									
									
				// if ( $row['ETAT'] ==  1 ) {		
					// }				
					$GPS_data[$i]["lat"]= $row['Latitude'];
					$GPS_data[$i]["lng"]= $row['Longitude'];
					$GPS_data[$i]["html"]= '<div><a>'.$row['NOM_CLIENT'].'</a>';
					$GPS_data[$i]["text"]= $row['NOM_CLIENT'];
					
		
		
			 
			 $i++ ; 
	}
	
	
	
	
	$result["datatable"] = json_encode($datatable_data) ;
	$result["gps_data"] = json_encode($GPS_data) ;

	echo json_encode($result);
}
ob_end_flush();
?>

