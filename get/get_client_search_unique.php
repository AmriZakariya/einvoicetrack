<?php
ob_start();
session_start();
include '../connexion.php';



if(isset($_SESSION['user_einvoicetrack']) && isset($_POST['current_client']) )
{
	
	$codeclient = urldecode($_POST['current_client']);
	
	$datatable_data = array();
	$i = 0 ; 


	$SQL="SELECT  `NUMERO_CLIENT`, 
					`NOM_CLIENT`, `TELE1`, `TELE2`, 
					`ADRESSE1`, `SECTEUR1`, `ADRESSE2`, `SECTEUR2`,
					`VILLE`, `Longitude`, `Latitude`, `DESCRIPTION` 
					, `CONTACT`, `TYPE_CLIENT`, `NATURE_CLIENT`

					FROM `client` WHERE NUMERO_CLIENT = '$codeclient' ";
	
	

	// echo $SQL ; 
	
	$query=mysqli_query($ma_connexion,$SQL);
	while($row=mysqli_fetch_assoc($query))
	{
			 $datatable_data[$i]["check_box"] = '';
			 $datatable_data[$i]["NOM"] = $row['NOM_CLIENT'];
			 $datatable_data[$i]["NUMERO"] = $row['NUMERO_CLIENT'];
			 $datatable_data[$i]["TEL1"] = $row['TELE1'];
			 $datatable_data[$i]["TEL2"] = $row['TELE2'];
			 $datatable_data[$i]["ADRESSE1"] = $row['ADRESSE1'];
			 $datatable_data[$i]["SECTEUR1"] = $row['SECTEUR1'];
			 $datatable_data[$i]["ADRESSE2"] = $row['ADRESSE2'];
			 $datatable_data[$i]["SECTEUR2"] = $row['SECTEUR2'];
			 $datatable_data[$i]["VILLE"] = $row['VILLE'];
//			 $query2 = " SELECT v.NOM_VILLE
//						FROM ville v
//						WHERE v.CODE_VILLE =  '".$row['VILLE']."'
//				 ";
//				$result2 = mysqli_query($ma_connexion, $query2);
//			   while(($row2 = mysqli_fetch_array($result2)) == true )
//				{
//
//					$datatable_data[$i]["VILLE"] = $row2['NOM_VILLE'];
//
//				}
			 
			 $datatable_data[$i]["GPS"] = "(".$row['Latitude'].", ".$row['Longitude'].")";
			 $datatable_data[$i]["DESCRIPTION"] = $row['DESCRIPTION'];	
			 // if ( $row['ETAT'] ==  1 ) 
					 // $datatable_data[$i]["ETAT"] = '<span class="label label-primary">Actif</span>' ; 
				// else 
					// $datatable_data[$i]["ETAT"] = '<span class="label label-danger">supprimé</span>' ; 


			$datatable_data[$i]["CONTACT"] = $row['CONTACT'];
			$datatable_data[$i]["TYPE_CLIENT"] = $row['TYPE_CLIENT'];
			$datatable_data[$i]["NATURE_CLIENT"] = $row['NATURE_CLIENT'];
			if( $_SESSION['role'] ==  'superadmin' ||  $_SESSION['role'] ==  'admin' )
			{
			 $datatable_data[$i]["DETAILL"] = '
			 
									<button type="button"  class="btn btn-primary btn-circle detail"  value="'.urlencode($row['NUMERO_CLIENT']).'">
										<i class="fa fa-list"></i>
									</button> 
									<button type="button"  class="btn btn-success btn-circle edit"  value="'.urlencode($row['NUMERO_CLIENT']).'">
										<i class="fa fa-edit"></i>
									</button> 
									<button type="button"  class="btn btn-danger btn-circle delete"  value="'.urlencode($row['NUMERO_CLIENT']).'">
										<i class="fa fa-trash"></i>
									</button> 
									';
			}
			else 
			{
				 $datatable_data[$i]["DETAILL"] = '
			 
									<button type="button"  class="btn btn-primary btn-circle detail"  value="'.urlencode($row['NUMERO_CLIENT']).'">
										<i class="fa fa-list"></i>
									</button> 
									
									';
				
			}
									
			
		
		
			 
			 $i++ ; 
	}
	
	
	
	

	echo json_encode($datatable_data);
}
ob_end_flush();
?>

