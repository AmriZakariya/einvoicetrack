<?php
ob_start();
session_start();
include '../connexion.php';



if(isset($_SESSION['user_einvoicetrack']) && ( $_SESSION['role'] ==  'superadmin' ||  $_SESSION['role'] ==  'admin'  ) )
{
	
	if(isset($_POST['__UI__']))
	{
		$__UI__= urldecode(mysqli_real_escape_string($ma_connexion,$_POST['__UI__']));
		
		$SQL="SELECT `NUMERO_CLIENT`, 
					`NOM_CLIENT`, `TELE1`, `TELE2`, 
					`ADRESSE1`, `SECTEUR1`, `ADRESSE2`, `SECTEUR2`,
					`VILLE`, `Longitude`, `Latitude`, `DESCRIPTION` 
					, `CONTACT`, `TYPE_CLIENT`, `NATURE_CLIENT`

					FROM `client` WHERE  NUMERO_CLIENT = '$__UI__'"
			;
		$query=mysqli_query($ma_connexion,$SQL);
		while($row=mysqli_fetch_assoc($query))
		{
			
?>

						
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">Code client</span>
										<input type="text" id="edit_numero" name="edit_numero" placeholder="NUMERO" class="form-control" value="<?php echo $row['NUMERO_CLIENT'] ; ?>" readonly >
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">Nom client</span>
										<input type="text" id="edit_nom" name="edit_nom" placeholder="NOM" class="form-control" value="<?php echo $row['NOM_CLIENT'] ; ?>">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">N° Tél1</span>
										<input type="text" id="edit_tel1" name="edit_tel1" placeholder="N° Tél1" value="<?php echo $row['TELE1'] ; ?>" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">N° Tél2</span>
										<input type="text" id="edit_tel2" name="edit_tel2" placeholder="N° Tél2" value="<?php echo $row['TELE2'] ; ?>" class="form-control">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<span class="badge badge-success">Adresse facturation</span>
										<textarea class="form-control" rows="2" id="edit_adresse1" name="edit_adresse1"><?php echo $row['ADRESSE1'] ; ?></textarea>

									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<span class="badge badge-success">Adresse supplémentaire</span>
										<textarea class="form-control" rows="2" id="edit_adresse2" name="edit_adresse2"><?php echo $row['ADRESSE2'] ; ?></textarea>

									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">Secteur facturation</span>
										<input type="text" id="edit_secteur1" name="edit_secteur1" value="<?php echo $row['SECTEUR1'] ; ?>" placeholder="Secteur facturation" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">Secteur supplémentaire</span>
										<input type="text" id="edit_secteur2" name="edit_secteur2" value="<?php echo $row['SECTEUR2'] ; ?>" placeholder="Secteur supplémentaire" class="form-control">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<span class="badge badge-success">Ville facturation</span>
										<select class="sc_select2_simple form-control required" name="edit_ville"  id="edit_ville" data-placeholder="Selectionner ville client" >
											<?php
                                            $query2 = " SELECT DISTINCT `VILLE` FROM `client` ORDER by `VILLE`
														 ";
													$result2 = mysqli_query($ma_connexion, $query2); 
												   while(($row2 = mysqli_fetch_array($result2)) == true )  
													{ 										
														
//														$CODE_VILLE = encode($row2['CODE_VILLE']) ;
														$NOM_VILLE = $row2['VILLE'] ;
														if($row2['VILLE'] == $row['VILLE'] )
															echo ' <option value="'.$NOM_VILLE.'" selected>'.$NOM_VILLE.'</option>' ;
														else
															echo ' <option value="'.$NOM_VILLE.'">'.$NOM_VILLE.'</option>' ;
														 
													}
												?>
										</select>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<span class="badge badge-success">GPS</span>
										<div class="input-group"> 
											<input placeholder="Latitude" name="edit_gps_lat" id="edit_gps_lat" value="<?php echo $row['Latitude'] ; ?>" type="text" class="form-control ">
											<input placeholder="Longitude" name="edit_gps_long" id="edit_gps_long" value="<?php echo $row['Longitude'] ; ?>" type="text" class="form-control ">
										</div>
									</div>
								</div>


								<div class="col-md-12">
									<div class="form-group">
										<span class="badge badge-success">Déscription</span>
										<textarea class="form-control" rows="3" id="edit_description" name="edit_description"><?php echo $row['DESCRIPTION'] ; ?></textarea>

									</div>
								</div>

								<div class="col-md-12">
									<div class="form-group">
										<span class="badge badge-success">CONTACT</span>
										<textarea class="form-control" rows="3" id="edit_CONTACT" name="edit_CONTACT"><?php echo $row['CONTACT'] ; ?></textarea>

									</div>
								</div>


								<div class="col-md-12">
									<div class="form-group">
										<span class="badge badge-success">Type client</span>
                                        <input type="text" id="edit_nom" name="edit_TYPE_CLIENT" placeholder="" class="form-control" value="<?php echo $row['TYPE_CLIENT'] ; ?>">

									</div>
								</div>

								<div class="col-md-12">
									<div class="form-group">
										<span class="badge badge-success">Nature client</span>
                                        <input type="text" id="edit_nom" name="edit_NATURE_CLIENT" placeholder="" class="form-control" value="<?php echo $row['NATURE_CLIENT'] ; ?>">

									</div>
								</div>



							</div>
							
							<br/>
								<div class="row">
									<div class="col-md-12">
										<button type="button" value="<?php echo $_POST['__UI__'] ; ?>"class="btn btn-success btn-rounded pull-right" id="enregistrer_edit" > <i class="fa fa-save"></i> Enregistrer</button>
										<button type="button" class="btn btn-danger btn-rounded pull-right" id="vider_edit" style="margin-right: 4px;"> <i class="fa fa-times"></i> Annuler</button>
									</div>
								</div>
						
					
					<?php			

		}
	}
	
}
ob_end_flush();
?>
