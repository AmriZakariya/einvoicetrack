<?php
ob_start();
session_start();
include '../connexion.php';



if(isset($_SESSION['user_einvoicetrack']) && isset($_POST['current_facture']) )
{
	
	$current_facture = urldecode($_POST['current_facture']);
	
	$datatable_data = array();
	$i = 0 ; 


	$SQL="SELECT f.NUM_FACTURE, f.STATUS,s.NOM_STATUS, c.NOM_CLIENT, f.NUMERO_CLIENT, c.ADRESSE1, f.DATE_COMPTABILISATION, f.BC, f.BL,f.pdf
			FROM facture f , ( SELECT NOM_CLIENT,ADRESSE1,NUMERO_CLIENT
							FROM client 
							UNION ALL 
							SELECT NOM_CLIENT,ADRESSE1,NUMERO_CLIENT
							FROM client_history ) c, status s 
			WHERE 
				( f.NUMERO_CLIENT = c.NUMERO_CLIENT )
			AND f.STATUS = s.CODE_STATUS 
			AND NUM_FACTURE = '$current_facture'
		";
	
		


	
	$query=mysqli_query($ma_connexion,$SQL);
	while($row=mysqli_fetch_assoc($query))
	{		

				 $datatable_data[$i]["check_box"] = '';
				 $datatable_data[$i]["NUM_FACTURE"] = $row['NUM_FACTURE'];
				 $datatable_data[$i]["STATUS"] = $row['NOM_STATUS'];
				 $datatable_data[$i]["NOM_CLIENT"] = $row['NOM_CLIENT'];
				 $datatable_data[$i]["CODE_CLIENT"] = $row['NUMERO_CLIENT'];
				 $datatable_data[$i]["ADRESSE1"] = $row['ADRESSE1'];
				 $datatable_data[$i]["DATE_COMPTABILISATION"] = date('Y-m-d', strtotime($row['DATE_COMPTABILISATION']));
				 
				$datatable_data[$i]["MOIS_DISTRIBUTION"] = '';
				
				
				
				 $query2 = " SELECT fs.DATE
					FROM facture_status fs
					WHERE fs.NUM_FACTURE =  '".$row['NUM_FACTURE']."'
					AND fs.CODE_STATUS != 1
					ORDER BY fs.CODE_ DESC
					LIMIT 1
						 ";
						$result2 = mysqli_query($ma_connexion, $query2); 
					   while(($row2 = mysqli_fetch_array($result2)) == true )  
						{ 										
							
							$datatable_data[$i]["MOIS_DISTRIBUTION"] .= '<b>'.date('M Y', strtotime($row2['DATE'])).'</b> &nbsp; ';
							$datatable_data[$i]["MOIS_DISTRIBUTION"] .= ' <small> ('.date('Y-m-d', strtotime($row2['DATE'])).')</small> ';
							 
						}
				
				 $datatable_data[$i]["MOTIF"] = '';
				 $datatable_data[$i]["description"] = '';
				 
				 if($row['STATUS'] == 3)
				 {
					 $query2 = " SELECT m.NOM_MOTIF,fs.description
								FROM facture_status fs , motif m
								WHERE fs.motif =m.CODE_MOTIF
								AND fs.NUM_FACTURE = '".$row['NUM_FACTURE']."'
								ORDER BY fs.CODE_ DESC
								LIMIT 1
						 ";
						$result2 = mysqli_query($ma_connexion, $query2); 
					   while(($row2 = mysqli_fetch_array($result2)) == true )  
						{ 										
							
							$datatable_data[$i]["MOTIF"] = $row2['NOM_MOTIF'];
							$datatable_data[$i]["description"] = $row2['description'];
							 
						}
				 }

					
				 $datatable_data[$i]["BC"] = $row['BC'];
				 $datatable_data[$i]["BL"] = $row['BL'];
				 
				 
				 
				 $datatable_data[$i]["DETAILL"] = '
 
					<button type="button"  class="btn btn-primary btn-circle detail"  value="'.urlencode($row['NUM_FACTURE']).'">
						<i class="fa fa-list"></i>
					</button> 
					<button type="button"  class="btn btn-danger btn-circle delete"  value="'.urlencode($row['NUM_FACTURE']).'">
						<i class="fa fa-trash"></i>
					</button> 
					';
						
						
				 if(	$row['STATUS'] != 2 ) 
				 {
					 
					  $datatable_data[$i]["DETAILL"] .= '
						<button type="button"  class="btn btn-success btn-circle edit"  value="'.urlencode($row['NUM_FACTURE']).'">
							<i class="fa fa-edit"></i>
						</button> 
						';
				 }
				 
				 if(	$row['pdf'] == 1 ) 
				 {
					 
					 $datatable_data[$i]["DETAILL"] .= '
						<button type="button"   class="btn btn-dark btn-circle pdf_dwn"   value="'.urlencode($row['NUM_FACTURE']).'">
							<i class="fa  fa-file-pdf-o"></i>
						</a> 
						
						
						
						';
				 }
				 
				 
				 

			 $i++ ; 
	}
	
	
	
	

	echo json_encode($datatable_data);
}
ob_end_flush();
?>

