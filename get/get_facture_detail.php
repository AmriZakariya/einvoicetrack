<?php
ob_start();
session_start();
include '../connexion.php';



if(isset($_SESSION['user_einvoicetrack']) && ( $_SESSION['role'] ==  'superadmin' ||  $_SESSION['role'] ==  'admin'  ) )
{
	
	if(isset($_POST['__UI__']))
	{
		
		$__UI__= urldecode(mysqli_real_escape_string($ma_connexion,$_POST['__UI__']));
		
		$SQL="SELECT  s.NOM_STATUS,s.CODE_STATUS,f.DATE_COMPTABILISATION, f.BC, f.BL
		FROM facture f ,  status s 
		WHERE f.STATUS = s.CODE_STATUS 
		AND  f.NUM_FACTURE = '$__UI__'
		AND f.STATUS != 2 
		
		"
		;
		$date_ditsrubution = '' ;
			
		// echo $SQL ; 
			
			// echo $SQL ; 
		$query=mysqli_query($ma_connexion,$SQL);
		while($row=mysqli_fetch_assoc($query))
		{
			
?>

						
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">N° de Facture</span>
										<input type="text" id="edit_numero" name="edit_numero" placeholder="NUMERO" class="form-control" value="<?php echo $__UI__ ; ?>" readonly disabled>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">Statut</span>
										<select class="sc_select2_simple form-control required" name="edit_statut"  id="edit_statut" data-placeholder="Selectionner status facture" <?php if($row['CODE_STATUS'] == 2 ) echo "readonly";  ?> required>
											<?php
													$query2 = " SELECT CODE_STATUS , NOM_STATUS 
																FROM status  
													 ";
													$result2 = mysqli_query($ma_connexion, $query2); 
												   while(($row2 = mysqli_fetch_array($result2)) == true )  
													{ 										
														
														$CODE_STATUS = $row2['CODE_STATUS'] ;
														$NOM_STATUS = $row2['NOM_STATUS'] ;
														if($row2['CODE_STATUS'] == $row['CODE_STATUS'] )
															echo ' <option value="'.$CODE_STATUS.'" selected>'.$NOM_STATUS.'</option>' ;
														else
															echo ' <option value="'.$CODE_STATUS.'">'.$NOM_STATUS.'</option>' ;
														 
													}
												?>
										</select>
									</div>

									<div class="form-group" id="edit_motif"   style="<?php if($row['CODE_STATUS'] == 1 || $row['CODE_STATUS'] == 4 ) echo 'display:none' ?>"  >
										<span class="badge badge-success">Motif de rejet</span>
										<select class="sc_select2_simple form-control required" name="edit_motif" data-placeholder="Selectionner status facture"  >
											<?php
											
												$motif_current = 0 ;
												$description_motif_current = '' ;
												 $query2 = " SELECT fs.motif,fs.description
															FROM facture_status fs 
															WHERE  fs.NUM_FACTURE =  '$__UI__'
															ORDER BY fs.CODE_ DESC
															LIMIT 1
													 ";
													$result2 = mysqli_query($ma_connexion, $query2); 
												   while(($row2 = mysqli_fetch_array($result2)) == true )  
													{ 										
														$motif_current = $row2['motif'];
														$description_motif_current = $row2['description'];
														 
													}
											 
													$query2 = " SELECT CODE_MOTIF , NOM_MOTIF 
																FROM motif  
													 ";
													$result2 = mysqli_query($ma_connexion, $query2); 
													while(($row2 = mysqli_fetch_array($result2)) == true )  
													{ 										
														
														$CODE_MOTIF = $row2['CODE_MOTIF'] ;
														$NOM_MOTIF = $row2['NOM_MOTIF'] ;
														
														if($row2['CODE_MOTIF'] == $motif_current )
															echo ' <option value="'.$CODE_MOTIF.'" selected>'.$NOM_MOTIF.'</option>' ;
														else
															echo ' <option value="'.$CODE_MOTIF.'">'.$NOM_MOTIF.'</option>' ;
														
															
														 
													}
												?>
										</select>
									</div>
									<div class="form-group" id="edit_description"   style="<?php if($row['CODE_STATUS'] == 1 || $row['CODE_STATUS'] == 4 ) echo 'display:none' ?>"  >
										<span class="badge badge-success">Déscription motif</span>
										<textarea class="form-control" rows="2" name="edit_description"><?php echo $description_motif_current ; ?></textarea>

									</div>
									<div class="form-group" id="edit_date_distribution" style="<?php if($row['CODE_STATUS'] == 1 ) echo 'display:none' ?>" >
											<span class="badge badge-success">Date de distribution </span><span style="color:red"></span> 
											<?php
											$query2 = " SELECT fs.DATE
												FROM facture_status fs
												WHERE fs.NUM_FACTURE =  '".$__UI__."'
												ORDER BY fs.CODE_ DESC
												LIMIT 1
													 ";
													 
													 // echo $query2 ; 
													$result2 = mysqli_query($ma_connexion, $query2); 
												   while(($row2 = mysqli_fetch_array($result2)) == true )  
													{ 	
														// echo $date_ditsrubution ; 												
														
														$date_ditsrubution = $row2['DATE'] ; 
												 
													}
											?>
											<input name="edit_date_distribution" type="date" class="form-control  required" value="<?php if ($date_ditsrubution != '') echo date('Y-m-d', strtotime($date_ditsrubution)) ; else echo  date('Y-m-d', strtotime($row['DATE_COMPTABILISATION'])) ; ?>" >
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">N° BC</span>
										<input type="text" id="edit_bc" name="edit_bc" placeholder="BC" value="<?php echo $row['BC'] ; ?>" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">N° BL</span>
										<input type="text" id="edit_bl" name="edit_bl" placeholder="BL" value="<?php echo $row['BL'] ; ?>" class="form-control">
									</div>
								</div>
								
								
								<div class="col-md-12">
									<div class="form-group" id="edit_date_comptabilisation"  >
											<span class="badge badge-success">Date de comptabilisation </span><span style="color:red"></span> 
											<input name="edit_date_comptabilisation" type="date" class="form-control  required" value="<?php echo $row['DATE_COMPTABILISATION'] ; ?>">
										</div>
								</div>
								<div class="col-md-12">
									<div class="form-group"  >
											<span class="badge badge-success">Fichier joint </span><span style="color:red"></span> 
											<input type="file" name="edit_pdf" class="form-control"  accept="application/pdf" id="edit_file_pdf" <?php if($row['CODE_STATUS'] == 2 || $row['CODE_STATUS'] == 3 ) echo 'required' ?> >
										</div>
								</div>
								
								
								
							</div>
							
							<br/>
								<div class="row">
									<div class="col-md-12">
										<button type="button" value="<?php echo $_POST['__UI__'] ; ?>"class="btn btn-success btn-rounded pull-right" id="enregistrer_edit" > <i class="fa fa-save"></i> Enregistrer</button>
										<button type="button" class="btn btn-danger btn-rounded pull-right" id="vider_edit" style="margin-right: 4px;"> <i class="fa fa-times"></i> Annuler</button>
									</div>
								</div>
						
					
					<?php			

		}
	}
	
}
ob_end_flush();
?>
