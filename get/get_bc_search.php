<?php
ob_start();
session_start();
include '../connexion.php';

if(isset($_SESSION['user_einvoicetrack']))
{
	$datatable_data = array();
	$i = 0 ; 

$SQL="SELECT b.id, b.BC,c.NOM_CLIENT, b.NUMERO_CLIENT, b.DATE_EDITION,b.DATE_NUMERISATION, b.pdf
		FROM bc b , ( SELECT NOM_CLIENT,ADRESSE1,NUMERO_CLIENT
							FROM client 
							UNION ALL 
							SELECT NOM_CLIENT,ADRESSE1,NUMERO_CLIENT
							FROM client_history ) c
		WHERE 
			( b.NUMERO_CLIENT = c.NUMERO_CLIENT )
		

		
	
		";
					
	if (isset($_POST['BC']))
	{
		$SQL .= " AND ( " ; 
		foreach ($_POST['BC'] as $selectedOption)
		{
			$SQL .= " b.BC LIKE \"%$selectedOption%\" OR " ; 
		}
		$SQL = substr($SQL, 0, -3);
		$SQL .= " )";
	}	
	
					
	if (isset($_POST['NOM_CLIENT']))
	{
		$SQL .= " AND ( " ; 
		foreach ($_POST['NOM_CLIENT'] as $selectedOption)
		{
			$SQL .= " c.NOM_CLIENT LIKE \"%$selectedOption%\" OR " ; 
		}
		$SQL = substr($SQL, 0, -3);
		$SQL .= " )";
	}
					
	if (isset($_POST['NUMERO_CLIENT']))
	{
		$SQL .= " AND ( " ; 
		foreach ($_POST['NUMERO_CLIENT'] as $selectedOption)
		{
			$SQL .= " c.NUMERO_CLIENT LIKE \"%$selectedOption%\" OR " ; 
		}
		$SQL = substr($SQL, 0, -3);
		$SQL .= " )";
	}
	
					
				
	
					
	if (($_POST['DATE_LIVRAISON_START'] != '') &&  ($_POST['DATE_LIVRAISON_END'] != '' )  )
	{	 

		$SQL .= " AND  b.DATE_EDITION between '".$_POST['DATE_LIVRAISON_START']."' and '".$_POST['DATE_LIVRAISON_END']."'    " ; 
	}
						
	if (($_POST['DATE_NUMERISATION_START'] != '') &&  ($_POST['DATE_NUMERISATION_END'] != '' )  )
	{	 

		$SQL .= " AND  b.DATE_NUMERISATION between '".$_POST['DATE_NUMERISATION_START']."' and '".$_POST['DATE_NUMERISATION_END']."'    " ; 
	}
					
	

	$SQL .= " GROUP BY b.id" ; 
	// $SQL .= " LIMIT 100" ; 
// echo $SQL ;
 
		
	$query=mysqli_query($ma_connexion,$SQL);
	while($row=mysqli_fetch_assoc($query))
	{		

				 $datatable_data[$i]["check_box"] = '';
				 $datatable_data[$i]["BC"] = $row['BC'];
				 $datatable_data[$i]["NOM_CLIENT"] = $row['NOM_CLIENT'];
				 $datatable_data[$i]["CODE_CLIENT"] = $row['NUMERO_CLIENT'];
				 $datatable_data[$i]["DATE_LIVRAISON"] = date('Y-m-d', strtotime($row['DATE_EDITION'])); 
				 $datatable_data[$i]["DATE_NUMERISATION"] = date('Y-m-d', strtotime($row['DATE_NUMERISATION'])); 
				 
				 
				 $datatable_data[$i]["DETAILL"] = '
 
					<button type="button"  class="btn btn-primary btn-circle detail"  hiddenvalue="'.$row['id'].'_'.$row['BC'].'" value="'.urlencode($row['id']).'">
						<i class="fa fa-list"></i>
					</button>  ' ; 
					
				if ($_SESSION['role'] ==  'superadmin' ||  $_SESSION['role'] ==  'admin'  )	
					 $datatable_data[$i]["DETAILL"] .= '
						<button type="button"  class="btn btn-danger btn-circle delete"  value="'.urlencode($row['id']).'">
							<i class="fa fa-trash"></i>
						</button> 
						';
						
						
				 if( ( $_SESSION['role'] ==  'superadmin' ||  $_SESSION['role'] ==  'admin'  ) ) 
				 {
					 
					  $datatable_data[$i]["DETAILL"] .= '
						<button type="button"  class="btn btn-success btn-circle edit"  value="'.urlencode($row['id']).'">
							<i class="fa fa-edit"></i>
						</button> 
						';
				 }
				 
				 if( (file_exists('../dossier_bc.'.DIRECTORY_SEPARATOR.$row['id'].'_'.$row['BC'].'.pdf')) )
				 {
					 $datatable_data[$i]["DETAILL"] .= '
						<button type="button"   class="btn btn-dark btn-circle pdf_dwn"    value="'.urlencode($row['id']).'">
							<i class="fa  fa-file-pdf-o"></i>
						</a> ';
				 }
				 
				 

			 $i++ ; 
	}
	
	echo json_encode($datatable_data);
}
ob_end_flush();
?>

