<?php
ob_start();
session_start();
include '../connexion.php';



if(isset($_SESSION['user_einvoicetrack'])  && ( $_SESSION['role'] ==  'superadmin' ||  $_SESSION['role'] ==  'admin'  ) ) 
{
								
	$date_end = date('Y-m-d H:i:s', strtotime('+1 hours'));
	$date_start = date('Y-m-d H:i:s', strtotime($date_end." -1 month"));
	$users = '' ; 
	
	if (($_POST['DATE_SEARCH_START'] != '') &&  ($_POST['DATE_SEARCH_END'] != '' )  )
	{	 
		$date_end =  $_POST['DATE_SEARCH_END'] ; 
		$date_start = $_POST['DATE_SEARCH_START'] ; 		
	}

	if (isset($_POST['USERS_SEARCH']))
	{
		foreach ($_POST['USERS_SEARCH'] as $selectedOption)
		{
			$users .= $selectedOption.',' ;
		}
		$users = substr($users, 0, -1);

	}
	
	// if( $_SESSION['role'] !=  'superadmin' ) 
	// {
		// $SQL="SELECT a.ID,a.TITRE, a.DESCRIPTION, a.DATE,u.NOM_USER,u.CIVILITE_USER
			// FROM action a,user u
			// WHERE u.ROLE != '0'
			// AND a.USER IN ($users) 
			// AND a.USER = u.CODE_USER
			// AND DATE(a.DATE) BETWEEN '$date_start' AND '$date_end'
			// ORDER BY a.ID DESC  ";
	// }
	// else 
	// {
			// $SQL="SELECT a.ID,a.TITRE, a.DESCRIPTION, a.DATE,u.NOM_USER,u.CIVILITE_USER
			// FROM action a,user u
			// WHERE a.USER IN ($users) 
			// AND a.USER = u.CODE_USER
			// AND DATE(a.DATE) BETWEEN '$date_start' AND '$date_end'
			// ORDER BY a.ID DESC  ";
	// }
	
	$SQL="SELECT a.ID,a.TITRE, a.DESCRIPTION, a.DATE,u.NOM_USER,u.CIVILITE_USER
			FROM action a,user u
			WHERE  a.USER IN ($users) 
			AND a.USER = u.CODE_USER
			AND DATE(a.DATE) BETWEEN '$date_start' AND '$date_end'
			ORDER BY a.ID DESC  ";
			// echo $SQL ; 
		$query=mysqli_query($ma_connexion,$SQL);
		while($row=mysqli_fetch_assoc($query))
		{	
		?>	

			 <div class="timeline-item" >
				<div class="row">
					<div class="col-3 date">
						<i class="fa fa-briefcase"></i>
						<?php echo '<label style=" font-size: 13px;" class="label label-success "> '.$row['CIVILITE_USER'].' '.$row['NOM_USER'].'</label>'; ?>
						<br/>
						<?php echo '<label style=" font-size: 13px;" class="label label-secondary "> '.$row['DATE'].'</label>'; ?>
					</div>
					<div class="col-8 content no-top-border">
						<p class="m-b-xs"><strong><?php echo '<label style=" font-size: 13px;"class="label label-info "> '.$row['TITRE'].'</label>';?></strong></p>

						<p><?php echo $row['DESCRIPTION'] ; ?>.</p>
					</div>
				</div>
			</div>
		<?php 
		}

}
ob_end_flush();
?>

