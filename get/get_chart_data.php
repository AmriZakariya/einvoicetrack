<?php
ob_start();
session_start();
include '../connexion.php';



if(isset($_SESSION['user_einvoicetrack']) && isset($_POST['key'])  )
{


	if ( $_POST['key'] == "factures_rejetes" )
	{
		// echo "factures_rejetes";
		$annee = $_POST['annee'] ;
		$result = array() ;
		$nomsDesMois = array("janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre") ;
		foreach ($nomsDesMois as $key => $value)
		{
			$result[$key]['mois']= $value ;
			$result[$key]['total']= "0" ;


			$SQL="select count(DISTINCT  fs.NUM_FACTURE) as total
				from facture_status fs 
				WHERE fs.CODE_STATUS = 3
				AND extract(year from fs.DATE) = '$annee'
				AND extract(month from fs.DATE) = '".($key+1)."'
				AND fs.NUM_FACTURE NOT IN ( 
						select DISTINCT  fs.NUM_FACTURE
						from facture_status fs 
						WHERE fs.CODE_STATUS = 3
						AND extract(year from fs.DATE) = '$annee'
						AND extract(month from fs.DATE) > '".($key+1)."'
						
						)
				";


			$query=mysqli_query($ma_connexion,$SQL);
			while($row=mysqli_fetch_assoc($query))
			{
				$result[$key]["total"] = $row['total'] ;
			}



		}

		// $SQL="select  extract(month from fs.DATE) as mon, count(DISTINCT  fs.NUM_FACTURE) as total
		// from facture_status fs
		// WHERE fs.CODE_STATUS = 3
		// AND extract(year from fs.DATE) = '$annee'
		// GROUP BY  extract(month from fs.DATE)
		// ";

		// $query=mysqli_query($ma_connexion,$SQL);
		// while($row=mysqli_fetch_assoc($query))
		// {					
		// $result[$row['mon']-1]["total"]= $row['total'] ;
		// }



		echo json_encode($result);

	}


	if ( $_POST['key'] == "factures_motifs" )
	{

		$annee = $_POST['annee'] ;
		$mois = $_POST['mois'] ;

		$result = array() ;
		$SQL="SELECT m.CODE_MOTIF,m.NOM_MOTIF
				FROM motif m
				
				";

		$query=mysqli_query($ma_connexion,$SQL);
		while($row=mysqli_fetch_assoc($query))
		{
			$result[$row['CODE_MOTIF']-1]['motif']= $row['NOM_MOTIF'] ;
			$result[$row['CODE_MOTIF']-1]['total']= "0" ;
		}


		if($mois != "0"){

			$SQL="select  fs.motif, count(fs.NUM_FACTURE) as total
			from facture_status fs 
			WHERE fs.CODE_STATUS = 3
			AND extract(year from fs.DATE) = '$annee'
			AND extract(month from fs.DATE) = '$mois'
			GROUP BY  fs.motif
			";
		}else{
			$SQL="select  fs.motif, count(fs.NUM_FACTURE) as total
			from facture_status fs 
			WHERE fs.CODE_STATUS = 3
			AND extract(year from fs.DATE) = '$annee'
			GROUP BY  fs.motif
			";

		}




		$query=mysqli_query($ma_connexion,$SQL);
		while($row=mysqli_fetch_assoc($query))
		{
			$result[$row['motif']-1]['total']= $row['total'] ;

		}

		//	echo $mois ;
		//	echo $SQL ;
		echo json_encode($result);

	}
	if ( $_POST['key'] == "factures_status" )
	{
		$mois = $_POST['mois'] ;
		$annee = $_POST['annee'] ;
		$result = array() ;
		$SQL="SELECT s.CODE_STATUS,s.NOM_STATUS
				FROM status  s	
				";

		$query=mysqli_query($ma_connexion,$SQL);
		while($row=mysqli_fetch_assoc($query))
		{
			$result[$row['CODE_STATUS']-1]['status']= $row['NOM_STATUS'] ;
			$result[$row['CODE_STATUS']-1]['total']= "0" ;


			$SQL2="select fs.CODE_STATUS,count(fs.NUM_FACTURE) as total
				from facture_status fs
				WHERE extract(year from fs.DATE) =  '$annee'
				AND extract(month from fs.DATE) =  '$mois'
                AND fs.CODE_STATUS = '".$row['CODE_STATUS']."'
				GROUP BY fs.CODE_STATUS

				";
			// echo $SQL2 ;
			$query2=mysqli_query($ma_connexion,$SQL2);
			while($row2=mysqli_fetch_assoc($query2))
			{
				$result[$row2['CODE_STATUS']-1]['total']= $row2['total'] ;
			}

		}


		// $SQL="select f.STATUS,count(f.NUM_FACTURE) as total
		// from facture f
		// WHERE extract(year from f.DATE_COMPTABILISATION) =  '$annee'
		// AND extract(month from f.DATE_COMPTABILISATION) =  '$mois'
		// GROUP BY f.STATUS
		// HAVING STATUS != 1
		// ";

		// $query=mysqli_query($ma_connexion,$SQL);
		// while($row=mysqli_fetch_assoc($query))
		// {
		// $result[$row['STATUS']-1]['total']= $row['total'] ;
		// }

		// echo $SQL ; 
		echo json_encode($result);

	}
	if ( $_POST['key'] == "factures_region" )
	{
		$mois = $_POST['mois'] ;
		$annee = $_POST['annee'] ;
		$i = 0 ;
		$result = array();


		$SQL="SELECT DISTINCT c.NOM_CLIENT, c.Longitude , c.Latitude, f.NUM_FACTURE,c.VILLE
						FROM client c , facture f , facture_status fs
                        WHERE c.ETAT = 1
						AND fs.NUM_FACTURE = f.NUM_FACTURE
						AND extract(year from fs.DATE) =  '$annee'
						AND extract(month from fs.DATE) =  '$mois'
                        AND c.NUMERO_CLIENT = f.NUMERO_CLIENT
                        GROUP BY c.NUMERO_CLIENT
                         ";



		$query=mysqli_query($ma_connexion,$SQL);
		while($row=mysqli_fetch_assoc($query))
		{
			$result[$i]["lat"]= $row['Latitude'];
			$result[$i]["lng"]= $row['Longitude'];
			$result[$i]["html"]= 'nom client : <a>'.$row['NOM_CLIENT'].'</a>  <br> ville :  <a>'.$row['VILLE'].'</a> ';
			$result[$i]["text"]= $row['NOM_CLIENT'];

			$i++ ;
		}

		// echo $SQL ; 
		echo json_encode($result);

	}



	if ( $_POST['key'] == "rejets_reccurent" )
	{

		$result_global = array() ;
		$result = array() ;
		$i = 0 ;



		$SQL="select c.NOM_CLIENT,count(fr.NUM_FACTURE) as total
			from facture_status fr, facture f,client c 
			WHERE fr.CODE_STATUS = 3
			AND extract(year from fr.DATE) = '2019'
            AND fr.NUM_FACTURE = f.NUM_FACTURE
            AND f.NUMERO_CLIENT = c.NUMERO_CLIENT
			GROUP BY f.NUMERO_CLIENT
            ORDER BY total desc
            LIMIT 10
			";

		$query=mysqli_query($ma_connexion,$SQL);
		while($row=mysqli_fetch_assoc($query))
		{

			$result[$i]["numero"]= $row['NOM_CLIENT'] ;
			$result[$i]["total"]= $row['total'] ;

			// $SQL2="SELECT m.CODE_MOTIF,m.NOM_MOTIF
			// FROM motif m
			// ";

			// $query2=mysqli_query($ma_connexion,$SQL2);
			// while($row2=mysqli_fetch_assoc($query2))
			// {

			// $result[$i][$row2['CODE_MOTIF']-1]= "0" ;
			// $result_global[$i]["total"][$row2['CODE_MOTIF']-1]= "0" ;

			// }


			// $SQL2="select fr.MOTIF,count(fr.NUM_FACTURE) as total
			// from facture_rejet fr, facture f
			// WHERE extract(year from fr.DATE_REJET) = '2019'
			// AND fr.NUM_FACTURE = f.NUM_FACTURE
			// AND f.NUMERO_CLIENT = '".$row['NUMERO_CLIENT']."'
			// GROUP BY fr.MOTIF
			// ";

			// $query2=mysqli_query($ma_connexion,$SQL2);
			// while($row2=mysqli_fetch_assoc($query2))
			// {
			// $result[$i]["total"][$row2['MOTIF']-1]= $row2['total'] ;



			// }

			$i++ ;


		}


		echo json_encode($result);

	}

	if ( $_POST['key'] == "rejets_reccurent_array" )
	{

		// $result_global = array() ;
		$result = array() ;
		$i = 0 ;



		$SQL="select c.NUMERO_CLIENT,c.NOM_CLIENT,count(fr.NUM_FACTURE) as total
				from facture_status fr, facture f,client c 
				WHERE fr.CODE_STATUS = 3
				AND extract(year from fr.DATE) = '2019'
				AND fr.NUM_FACTURE = f.NUM_FACTURE
				AND f.NUMERO_CLIENT = c.NUMERO_CLIENT
				GROUP BY f.NUMERO_CLIENT
				ORDER BY total desc
			";

		$query=mysqli_query($ma_connexion,$SQL);
		while($row=mysqli_fetch_assoc($query))
		{

			$result[$i]["NUMERO_CLIENT"]= $row['NUMERO_CLIENT'] ;
			$result[$i]["NOM_CLIENT"]= $row['NOM_CLIENT'] ;
			$result[$i]["total"]= $row['total'] ;

			// $SQL2="SELECT m.CODE_MOTIF,m.NOM_MOTIF
			// FROM motif m
			// ";

			// $query2=mysqli_query($ma_connexion,$SQL2);
			// while($row2=mysqli_fetch_assoc($query2))
			// {

			// $result[$i][$row2['CODE_MOTIF']-1]= "0" ;
			// $result_global[$i]["total"][$row2['CODE_MOTIF']-1]= "0" ;

			// }


			// $SQL2="select fr.MOTIF,count(fr.NUM_FACTURE) as total
			// from facture_rejet fr, facture f
			// WHERE extract(year from fr.DATE_REJET) = '2019'
			// AND fr.NUM_FACTURE = f.NUM_FACTURE
			// AND f.NUMERO_CLIENT = '".$row['NUMERO_CLIENT']."'
			// GROUP BY fr.MOTIF
			// ";

			// $query2=mysqli_query($ma_connexion,$SQL2);
			// while($row2=mysqli_fetch_assoc($query2))
			// {
			// $result[$i]["total"][$row2['MOTIF']-1]= $row2['total'] ;



			// }

			$i++ ;


		}


		echo json_encode($result);

	}

	if ( $_POST['key'] == "rejets_reccurent_array_detail" )
	{

		$NUMERO_CLIENT = $_POST['NUMERO_CLIENT'] ;;
		// $result = array() ;
		// $i = 0 ; 

		echo '
		<table id="child_details" class=" child_details table " cellpadding="5" cellspacing="0" border="0"  style="width : 100% ; background-color:gray">
			<thead><tr ><th>N° facture</th><th>Date rejet</th><th>Motif rejet</th><th>Description rejet</th><th>PDF</th></tr></thead><tbody>
		';

		$SQL="SELECT fs.NUM_FACTURE,fs.DATE, m.NOM_MOTIF,fs.description
				FROM facture_status fs, motif m , facture f
				WHERE fs.motif = m.CODE_MOTIF
				AND fs.NUM_FACTURE = f.NUM_FACTURE
				AND f.NUMERO_CLIENT = '$NUMERO_CLIENT'
			";

		$query=mysqli_query($ma_connexion,$SQL);
		while($row=mysqli_fetch_assoc($query))
		{

			// $result[$i]["NUM_FACTURE"]= $row['NUM_FACTURE'] ;
			// $result[$i]["DATE"]= $row['DATE'] ;
			// $result[$i]["NOM_MOTIF"]= $row['NOM_MOTIF'] ;
			// $result[$i]["description"]= $row['description'] ;
			// $result[$i]["DETAILL"] = '
			// <a href="einvoicetrack/'.$row['NUM_FACTURE'].'.pdf'.'" download style="text-decoration: underline;">
			// <i class="fa fa-file-pdf-o "></i> Télécharger le fichier .pdf
			// </a> ';

			// $i++ ;
			echo '
					<tr>
								<td>'.$row['NUM_FACTURE'].'</td>
								<td><b>'.date('M Y', strtotime($row['DATE'])).'</b> &nbsp; <small> ('.date('Y-m-d', strtotime($row['DATE'])).')</small></td>
								<td>'.$row['NOM_MOTIF'].'</td>
								<td>'.$row['description'].'</td>
								<td>
									<a href="einvoicetrack/'.$row['NUM_FACTURE'].'.pdf'.'" download style="text-decoration: underline;">
									 <i class="fa fa-file-pdf-o "></i> Télécharger le fichier .pdf
									</a> 
								</td>
								
							</tr>
							
							';



		}
		echo '
		</tbody></table>
		
		';


		// echo json_encode($result);

	}

}








// $SQL="select m.NOM_MOTIF,count(f.NUM_FACTURE) as total
// from motif m
// LEFT JOIN facture_rejet f
// ON  f.MOTIF =m.CODE_MOTIF
// WHERE extract(year from f.DATE_REJET) = '2019'
// GROUP BY m.CODE_MOTIF
// UNION
// select m.NOM_MOTIF,count(f.NUM_FACTURE) as total
// from motif m
// RIGHT JOIN facture_rejet f
// ON  f.MOTIF =m.CODE_MOTIF
// WHERE extract(year from f.DATE_REJET) = '2019'
// GROUP BY m.CODE_MOTIF
// ";
// $SQL="select extract(month from DATE_COMPTABILISATION) as mon, count(*) as total
// from facture
// WHERE extract(year from `DATE_COMPTABILISATION`) = '2019'
// GROUP BY  extract(month from DATE_COMPTABILISATION)
// ";
ob_end_flush();
?>

