 <?php
ob_start();
session_start();
include '../connexion.php';

?>
  <form id="add_form" method="POST" class="wizard-big">
 <h1>Informations générales</h1>
                                <fieldset >
                                    <div class="row">
										 
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                                <span class="badge badge-success">Code client</span></span><span style="color:red">*</span> 
                                                <input name="add_numero" id="add_numero" type="text" class="form-control required " required>
                                            </div>
                                            <div class="form-group">
                                                <span class="badge badge-success">Nom client</span></span><span style="color:red">*</span> 
                                                <input name="add_nom" type="text" class="form-control  "
                                            </div>
										
                                            <div class="form-group">
                                                <span class="badge badge-success">N° Tél1 </span>
                                                <input  name="add_tel1" type="text" class="form-control ">
                                            </div>
                                            <div class="form-group">
                                                <span class="badge badge-success">N° Tél2</span>
                                                <input  name="add_tel2" type="text" class="form-control ">
                                            </div>
                                            <div class="form-group">
                                                <span class="badge badge-success">Contact</span>
                                                <input  name="add_contact" type="text" class="form-control ">
                                            </div>
                                            <div class="form-group">
                                                <span class="badge badge-success">Type client</span>
                                                <input  name="add_type_client" type="text" class="form-control ">
                                            </div>
                                            <div class="form-group">
                                                <span class="badge badge-success">Nature client</span>
                                                <input  name="add_nature_client" type="text" class="form-control ">
                                            </div>
                                        </div>
										<div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 50px">
                                                    <i class="fa fa-user-plus" style="font-size: 140px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div>

                                </fieldset>
                                <h1>Adresse facturation</h1>
                                <fieldset >
                                    <h2>Adresse facturation</h2>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <span class="badge badge-success">Adresse facturation</span></span><span style="color:red">*</span> 
												<textarea class="form-control required" rows="2" id="add_adresse1" name="add_adresse1"></textarea>

                                            </div>
                                            <div class="form-group">
                                                <span class="badge badge-success">Secteur 1</span>
												<input type="text" placeholder="Secteur 1" id="add_secteur1" name="add_secteur1" class="form-control ">
                                            </div>
											<div class="form-group">
                                               <span class="badge badge-success">Ville</span> 
												<select class="sc_select2_simple form-control required" name="add_ville"  id="add_ville" data-placeholder="Selectionner ville client" >
													<?php
															$query = " SELECT CODE_VILLE , NOM_VILLE 
																		FROM ville ORDER BY NOM_VILLE
															 ";
															$result = mysqli_query($ma_connexion, $query); 
														   while(($row = mysqli_fetch_array($result)) == true )  
															{ 										
																
//																$CODE_VILLE = encode($row['CODE_VILLE']) ;
																$NOM_VILLE = $row['NOM_VILLE'] ;
																echo ' <option value="'.$NOM_VILLE.'">'.$NOM_VILLE.'</option>' ;
																 
															}
														?>
												</select>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            
                                            <div class="form-group">
												<div class="input-group"> 
													<span class="badge badge-success"> GPS</span>
													<input placeholder="Latitude" name="add_gps_lat" id="add_gps_lat" type="text" class="form-control ">
													 <input placeholder="Longitude" name="add_gps_long" id="add_gps_long" type="text" class="form-control ">

												</div>
												 
												 <div id="map" style="width: 100%; height: 350px; background: grey" />
    
												
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <h1>Informations supplémentaires</h1>
                                <fieldset >
                                    <h2>Adresse facturation</h2>
                                    <div class="row">
											
											<div class="col-lg-6">
												  
													<div class="form-group">
													   <span class="badge badge-success"> Adresse supplémentaire</span>
														<textarea class="form-control" rows="2" id="add_adresse2" name="add_adresse2"></textarea>

													</div>
													<div class="form-group">
														<span class="badge badge-success">Secteur 2</span>
														<input type="text" placeholder="Secteur 2" id="add_secteur2" name="add_secteur2" class="form-control">
													</div>
													
													<div class="form-group">
													   <span class="badge badge-success">Ville</span> 
														<select class="sc_select2_simple form-control required" name="add_ville2"  id="add_ville2" data-placeholder="Selectionner ville client" >
															<?php
																	$query = " SELECT CODE_VILLE , NOM_VILLE 
																				FROM ville ORDER BY NOM_VILLE
																	 ";
																	$result = mysqli_query($ma_connexion, $query); 
																   while(($row = mysqli_fetch_array($result)) == true )  
																	{ 										
																		
//																		$CODE_VILLE = encode($row['CODE_VILLE']) ;
																		$NOM_VILLE = $row['NOM_VILLE'] ;
																		echo ' <option value="'.$NOM_VILLE.'">'.$NOM_VILLE.'</option>' ;
																		 
																	}
																?>
														</select>
													</div>
												
											</div>
											


											<div class="col-lg-6">
                                            
													<div class="form-group">
														<div class="input-group"> 
															<span class="badge badge-success"> GPS</span>
															<input placeholder="Latitude" name="add_gps_lat2" id="add_gps_lat2" type="text" class="form-control ">
															<input placeholder="Longitude" name="add_gps_long2" id="add_gps_long2" type="text" class="form-control ">

														</div>
														 <!--
														 <div id="map2" style="width: 100%; height: 350px; background: grey" />
			-->
														
													</div>
											</div>
                                       
                                    </div>
                                </fieldset>

                                
                                <h1>Déscription</h1>
                                <fieldset>
                                    <div  style="margin-top: 20px">
                                         <span class="badge badge-success">Déscription</span>
										<textarea class="form-control" rows="5" id="add_description" name="add_description"></textarea>

                                    </div>
                                    
                                </fieldset>
								
								</form>
								
 <?php								
ob_end_flush();
?>
