<?php
ob_start();
session_start();
include '../connexion.php';



if(isset($_SESSION['user_einvoicetrack']) && ( $_SESSION['role'] ==  'superadmin' ||  $_SESSION['role'] ==  'admin'  ) )
{
	
	if(isset($_POST['__UI__']))
	{
		$__UI__= urldecode(mysqli_real_escape_string($ma_connexion,$_POST['__UI__']));
		
		$SQL="SELECT `BC`, `NUMERO_CLIENT`, `NOM_CLIENT`, `DATE_EDITION`, `DATE_NUMERISATION`, `pdf` FROM `bc` WHERE id = '$__UI__'
		
		"
		
			;
			
		// echo $SQL ; 
			
			// echo $SQL ; 
		$query=mysqli_query($ma_connexion,$SQL);
		while($row=mysqli_fetch_assoc($query))
		{
			
?>

						
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">N° BC</span>
										<input type="text" id="edit_numero" name="edit_numero" placeholder="NUMERO" class="form-control" value="<?php echo $row['BC'] ; ?>" readonly disabled>
									</div>
								</div>
								
								<div class="col-md-6">
									<div class="form-group"   >
										<span class="badge badge-success">Date d'édition </span><span style="color:red"></span> 
										<input name="edit_date_livraison" type="date" class="form-control  required" value="<?php echo date('Y-m-d', strtotime($row['DATE_EDITION'])); ?>" >
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group"  >
										<span class="badge badge-success">Date de numérisation </span><span style="color:red"></span> 
										<input name="edit_date_numerisation" type="date" class="form-control  required" value="<?php echo date('Y-m-d', strtotime($row['DATE_NUMERISATION'])); ?>">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group"  >
											<span class="badge badge-success">Fichier joint </span><span style="color:red"></span> 
											<input type="file" name="edit_pdf" class="form-control"  accept="application/pdf" id="edit_file_pdf">
										</div>
								</div>
								
								
								
							</div>
							
							<br/>
								<div class="row">
									<div class="col-md-12">
										<button type="button" value="<?php echo $_POST['__UI__'] ; ?>"class="btn btn-success btn-rounded pull-right" id="enregistrer_edit" > <i class="fa fa-save"></i> Enregistrer</button>
										<button type="button" class="btn btn-danger btn-rounded pull-right" id="vider_edit" style="margin-right: 4px;"> <i class="fa fa-times"></i> Annuler</button>
									</div>
								</div>
						
					
					<?php			

		}
	}
	
}
ob_end_flush();
?>
