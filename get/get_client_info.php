<?php
ob_start();
session_start();
include '../connexion.php';



if(isset($_SESSION['user_einvoicetrack'])  )
{
	
	if(isset($_POST['__UI__']))
	{
		
		$__UI__= urldecode(mysqli_real_escape_string($ma_connexion,$_POST['__UI__']));
		
		$SQL="SELECT DISTINCT `NUMERO_CLIENT`, 
					`NOM_CLIENT`, `TELE1`, `TELE2`, 
					`ADRESSE1`, `SECTEUR1`, `ADRESSE2`, `SECTEUR2`,
					`VILLE`, `Longitude`, `Latitude`, `DESCRIPTION` 
					, `CONTACT`, `TYPE_CLIENT`, `NATURE_CLIENT`

					FROM `client` WHERE  NUMERO_CLIENT = '$__UI__'"
			;
		$query=mysqli_query($ma_connexion,$SQL);
		while($row=mysqli_fetch_assoc($query))
		{
			
?>

						
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">Code client</span>
										<div class="alert alert-primary">
											<?php echo $row['NUMERO_CLIENT'] ; ?>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">Nom client</span>
										<div class="alert alert-primary">
											<?php echo $row['NOM_CLIENT'] ; ?>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">N° Tél1</span>
										<div class="alert alert-primary">
											<?php echo $row['TELE1'] ; ?>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">N° Tél2</span>
										<div class="alert alert-primary">
											<?php echo $row['TELE2'] ; ?>
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<span class="badge badge-success">Adresse facturation</span>
										<div class="alert alert-primary">
											<?php echo $row['ADRESSE1'] ; ?>
										</div>

									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<span class="badge badge-success">Adresse supplémentaire</span>
										<div class="alert alert-primary">
											<?php echo $row['ADRESSE2'] ; ?>
										</div>

									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">Secteur facturation</span>
										<div class="alert alert-primary">
											<?php echo $row['SECTEUR1'] ; ?>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">Secteur supplémentaire</span>
										<div class="alert alert-primary">
											<?php echo $row['SECTEUR2'] ; ?>
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<span class="badge badge-success">Ville facturation</span>
										<div class="alert alert-primary">
											<?php
//													$query2 = " SELECT  NOM_VILLE
//																FROM ville WHERE CODE_VILLE = ".$row['VILLE']."
//													 ";
//													$result2 = mysqli_query($ma_connexion, $query2);
//												   while(($row2 = mysqli_fetch_array($result2)) == true )
//													{
//														 echo $row2['NOM_VILLE'] ;
//
//													}

                                                 echo $row['VILLE'] ;
												?>
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<span class="badge badge-success">GPS</span>
										<div class="alert alert-primary">
											<?php echo "( ".$row['Latitude']." ,".$row['Longitude'].")" ; ?>
										</div>
									</div>
								</div>


								<div class="col-md-12">
									<div class="form-group">
										<span class="badge badge-success">Déscription</span>
										<div class="alert alert-primary">
											<?php echo $row['DESCRIPTION'] ; ?>
										</div>

									</div>
								</div>

								<div class="col-md-12">
									<div class="form-group">
										<span class="badge badge-success">Contact</span>
										<div class="alert alert-primary">
											<?php echo $row['CONTACT'] ; ?>
										</div>

									</div>
								</div>



                                <div class="col-md-6">
                                    <div class="form-group">
                                        <span class="badge badge-success">Type client</span>
                                        <div class="alert alert-primary">
                                            <?php echo $row['TYPE_CLIENT'] ; ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <span class="badge badge-success">Nature client</span>
                                        <div class="alert alert-primary">
                                            <?php echo $row['NATURE_CLIENT'] ; ?>
                                        </div>
                                    </div>
                                </div>

							</div>
					<?php			

		}
	}
	
}
ob_end_flush();
?>
