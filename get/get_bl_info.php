<?php
ob_start();
session_start();
include '../connexion.php';



if(isset($_SESSION['user_einvoicetrack'])  )
{
	
	if(isset($_POST['__UI__']))
	{
		
		$__UI__= urldecode(mysqli_real_escape_string($ma_connexion,$_POST['__UI__']));
		
		$SQL="SELECT b.BL,c.NOM_CLIENT, b.NUMERO_CLIENT,c.ADRESSE1,b.BC, b.DATE_LIVRAISON,b.DATE_NUMERISATION, b.pdf
        , `TYPE_VENTE`, `TYPE_CLIENT`, `BL_MANUEL`, `DATE_DOCUMENT`

		FROM bl b , ( SELECT NOM_CLIENT,ADRESSE1,NUMERO_CLIENT
							FROM client 
							UNION ALL 
							SELECT NOM_CLIENT,ADRESSE1,NUMERO_CLIENT
							FROM client_history ) c
		WHERE 
			( b.NUMERO_CLIENT = c.NUMERO_CLIENT ) AND b.BL = '$__UI__'
		GROUP BY b.BL
	
		
			";
		$query=mysqli_query($ma_connexion,$SQL);
		while($row=mysqli_fetch_assoc($query))
		{

            $row['DATE_LIVRAISON'] = date('Y-m-d', strtotime($row['DATE_LIVRAISON']));
            if (strpos($row['DATE_LIVRAISON'], '1970') !== false) {
                $row['DATE_LIVRAISON'] = "";
            }

            $row['DATE_NUMERISATION'] = date('Y-m-d', strtotime($row['DATE_NUMERISATION']));
            if (strpos($row['DATE_NUMERISATION'], '1970') !== false) {
                $row['DATE_NUMERISATION'] = "";
            }

?>

						
							<div class="row">
								
								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">N° BL</span>
										<div class="alert alert-primary">
											<?php echo $__UI__ ; ?>
										</div>
									</div>
								</div>
								
								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">Nom client</span>
										<div class="alert alert-primary">
											<?php echo $row['NOM_CLIENT'] ; ?>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">Code client</span>
										<div class="alert alert-primary">
											<?php echo $row['NUMERO_CLIENT'] ; ?>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">Adresse</span>
										<div class="alert alert-primary">
											<?php echo $row['ADRESSE1'] ; ?>
										</div>
									</div>
								</div>
								
								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">Responsable facturation</span>
										<div class="alert alert-primary">
											<?php echo $row['BC'] ; ?>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">Date livraison</span>
										<div class="alert alert-primary">
											<?php echo $row['DATE_LIVRAISON'] ; ?>
										</div>
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">Date numérisation</span>
										<div class="alert alert-primary">
											<?php echo $row['DATE_NUMERISATION'] ; ?>
										</div>
									</div>
								</div>
                                , ``, ``, ``, ``

								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">Type de vente</span>
										<div class="alert alert-primary">
											<?php echo $row['TYPE_VENTE'] ; ?>
										</div>
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">Type client</span>
										<div class="alert alert-primary">
											<?php echo $row['TYPE_CLIENT'] ; ?>
										</div>
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">N° BL Manuel</span>
										<div class="alert alert-primary">
											<?php echo $row['BL_MANUEL'] ; ?>
										</div>
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">Date document</span>
										<div class="alert alert-primary">
											<?php echo $row['DATE_DOCUMENT'] ; ?>
										</div>
									</div>
								</div>



								
							</div>			

													
						
					<?php			

		}
	}
	
}
ob_end_flush();
?>
