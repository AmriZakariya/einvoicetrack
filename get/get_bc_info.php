<?php
ob_start();
session_start();
include '../connexion.php';



if(isset($_SESSION['user_einvoicetrack'])  )
{
	
	if(isset($_POST['__UI__']))
	{
		
		$__UI__= urldecode(mysqli_real_escape_string($ma_connexion,$_POST['__UI__']));
		
		$SQL="SELECT c.NOM_CLIENT, b.NUMERO_CLIENT,c.ADRESSE1,b.BC, b.DATE_EDITION,b.DATE_NUMERISATION, b.pdf
		FROM bc b , ( SELECT NOM_CLIENT,ADRESSE1,NUMERO_CLIENT
							FROM client 
							UNION ALL 
							SELECT NOM_CLIENT,ADRESSE1,NUMERO_CLIENT
							FROM client_history ) c
		WHERE 
			( b.NUMERO_CLIENT = c.NUMERO_CLIENT ) AND b.id = '$__UI__'
		GROUP BY b.id
	
		
			";
			
			
		$query=mysqli_query($ma_connexion,$SQL);
		while($row=mysqli_fetch_assoc($query))
		{
			
?>

						
							<div class="row">
								
								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">N° BC</span>
										<div class="alert alert-primary">
											<?php echo $__UI__ ; ?>
										</div>
									</div>
								</div>
								
								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">Nom client</span>
										<div class="alert alert-primary">
											<?php echo $row['NOM_CLIENT'] ; ?>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">Code client</span>
										<div class="alert alert-primary">
											<?php echo $row['NUMERO_CLIENT'] ; ?>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">Adresse</span>
										<div class="alert alert-primary">
											<?php echo $row['ADRESSE1'] ; ?>
										</div>
									</div>
								</div>
								
								
								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">Date d'édition</span>
										<div class="alert alert-primary">
											<?php echo $row['DATE_EDITION'] ; ?>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">Date numérisation</span>
										<div class="alert alert-primary">
											<?php echo $row['DATE_NUMERISATION'] ; ?>
										</div>
									</div>
								</div>
								
							</div>			

													
						
					<?php			

		}
	}
	
}
ob_end_flush();
?>
