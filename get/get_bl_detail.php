<?php
ob_start();
session_start();
include '../connexion.php';



if(isset($_SESSION['user_einvoicetrack']) && ( $_SESSION['role'] ==  'superadmin' ||  $_SESSION['role'] ==  'admin'  ) )
{
	
	if(isset($_POST['__UI__']))
	{
		$__UI__= urldecode(mysqli_real_escape_string($ma_connexion,$_POST['__UI__']));
		
		$SQL="SELECT 
       `BL`, `NUMERO_CLIENT`, `NOM_CLIENT`, `BC`, `DATE_LIVRAISON`, `DATE_NUMERISATION`, `pdf` 
        , `TYPE_VENTE`, `TYPE_CLIENT`, `BL_MANUEL`, `DATE_DOCUMENT`
        FROM `bl` WHERE BL = '$__UI__'
		
		"
		
			;
			
//		 echo $SQL ;
			
			// echo $SQL ; 
		$query=mysqli_query($ma_connexion,$SQL);
		while($row=mysqli_fetch_assoc($query))
		{

            $row['DATE_LIVRAISON'] = date('Y-m-d', strtotime($row['DATE_LIVRAISON']));
            if (strpos($row['DATE_LIVRAISON'], '1970') !== false) {
                $row['DATE_LIVRAISON'] = "";
            }

            $row['DATE_NUMERISATION'] = date('Y-m-d', strtotime($row['DATE_NUMERISATION']));
            if (strpos($row['DATE_NUMERISATION'], '1970') !== false) {
                $row['DATE_NUMERISATION'] = "";
            }

//            echo "DATE_LIVRAISON: ".$row['DATE_LIVRAISON'];
//            echo "DATE_NUMERISATION: ".$row['DATE_NUMERISATION'];

?>

						
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">N° BL</span>
										<input type="text" id="edit_numero" name="edit_numero" placeholder="NUMERO" class="form-control" value="<?php echo $__UI__ ; ?>" readonly disabled>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">N° BC</span>
										<input type="text" id="edit_bc" name="edit_bc" placeholder="BC" value="<?php echo $row['BC'] ; ?>" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group"   >
										<span class="badge badge-success">Date de livraison </span><span style="color:red"></span> 
										<input name="edit_date_livraison" type="date" class="form-control  required"  value="<?php echo $row['DATE_LIVRAISON']; ?>">
									</div>
								</div>



                                <div class="col-md-6">
                                    <div class="form-group" id="edit_description"    >
                                        <span class="badge badge-success">Date de numérisation </span><span style="color:red"></span>
                                        <input name="edit_date_numerisation" type="date" class="form-control  required" value="<?php echo $row['DATE_NUMERISATION']; ?>">
                                    </div>
                                </div>



                                <div class="col-md-6">
									<div class="form-group" id="edit_description"    >
										<span class="badge badge-success">Type de vente </span><span style="color:red"></span>
										<input id="edit_TYPE_VENTE"  name="edit_TYPE_VENTE" type="text" class="form-control  required" value="<?php echo $row['TYPE_VENTE']; ?>">
									</div>
								</div>



								<div class="col-md-6">
									<div class="form-group" id="edit_description"    >
										<span class="badge badge-success">Type client </span><span style="color:red"></span>
										<input id="edit_TYPE_CLIENT" name="edit_TYPE_CLIENT" type="text" class="form-control  required" value="<?php echo $row['TYPE_CLIENT']; ?>">
									</div>
								</div>


								<div class="col-md-6">
									<div class="form-group" id="edit_description"    >
										<span class="badge badge-success">N° BL Manuel </span><span style="color:red"></span>
										<input id="edit_BL_MANUEL" name="edit_BL_MANUEL" type="text" class="form-control  required" value="<?php echo $row['BL_MANUEL']; ?>">
									</div>
								</div>


								<div class="col-md-6">
									<div class="form-group" id="edit_description" >
										<span class="badge badge-success">Date document </span><span style="color:red"></span>
										<input id="edit_BL_MANUEL" name="edit_BL_MANUEL" type="text" class="form-control  required" value="<?php echo $row['DATE_DOCUMENT']; ?>">
									</div>
								</div>






								<div class="col-md-12">
									<div class="form-group"  >
											<span class="badge badge-success">Fichier joint </span><span style="color:red"></span> 
											<input type="file" name="edit_pdf" class="form-control"  accept="application/pdf" id="edit_file_pdf">
										</div>
								</div>
								
								
								
							</div>
							
							<br/>
								<div class="row">
									<div class="col-md-12">
										<button type="button" value="<?php echo $_POST['__UI__'] ; ?>"class="btn btn-success btn-rounded pull-right" id="enregistrer_edit" > <i class="fa fa-save"></i> Enregistrer</button>
										<button type="button" class="btn btn-danger btn-rounded pull-right" id="vider_edit" style="margin-right: 4px;"> <i class="fa fa-times"></i> Annuler</button>
									</div>
								</div>
						
					
					<?php			

		}
	}
	
}
ob_end_flush();
?>
