<?php
ob_start();
session_start();
include '../connexion.php';



if(isset($_SESSION['user_einvoicetrack'])  )
{
	?>
<div class="row">
		<div class="col-lg-6">
			<div class="form-group">
				<span class="badge badge-success">NUMERO</span><span style="color:red">*</span> 
				<input name="add_numero[]"  type="text" class="form-control required" required>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-lg-6">
						<span class="badge badge-success"> BC</span>
						<input placeholder="BC" name="add_bc[]"  type="text" class="form-control ">
					</div>
					<div class="col-lg-6">
						<span class="badge badge-success"> BL</span>
						<input placeholder="BL" name="add_bl[]"  type="text" class="form-control ">
					</div>
				</div>
			</div>
			
		</div>
		<div class="col-lg-6">
			<div class="form-group">
				<span class="badge badge-success">Client</span><span style="color:red">*</span> 
				<select class="sc_select2_simple form-control required" name="add_client[]"  data-placeholder="Selectionner client" required>
					<option > </option>
					<?php
							$query = " SELECT CODE_CLIENT , NUMERO_CLIENT,NOM_CLIENT 
										FROM client WHERE ETAT = 1 
							 ";
							$result = mysqli_query($ma_connexion, $query); 
						   while(($row = mysqli_fetch_array($result)) == true )  
							{ 										
								
								$CODE_CLIENT = encode($row['CODE_CLIENT']) ;
								$NUMERO_CLIENT = $row['NUMERO_CLIENT'] ;
								$NOM_CLIENT = $row['NOM_CLIENT'] ;
								echo ' <option value="'.$CODE_CLIENT.'">'.$NUMERO_CLIENT.'--'.$NOM_CLIENT.'</option>' ;
								 
							}
						?>
				</select>
			</div>
			<div class="form-group">
				<span class="badge badge-success">Date de comptabilisation </span><span style="color:red">*</span> 
				<input name="add_date_comptabilisation[]" type="date" class="form-control  required" required>
			</div>

		</div>                                        
	</div>
	
<?php			

		
	
	
}
ob_end_flush();
?>
