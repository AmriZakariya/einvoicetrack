<?php
ob_start();
session_start();
include '../connexion.php';



if(isset($_SESSION['user_einvoicetrack'])  )
{
	
	if(isset($_POST['__UI__']))
	{
		
		$__UI__= urldecode(mysqli_real_escape_string($ma_connexion,$_POST['__UI__']));
		
		$SQL="SELECT DISTINCT s.NOM_STATUS,s.CODE_STATUS, c.NOM_CLIENT, f.NUMERO_CLIENT, c.ADRESSE1, f.DATE_COMPTABILISATION, f.BC, f.BL
		FROM facture f , ( SELECT NOM_CLIENT,ADRESSE1,NUMERO_CLIENT
							FROM client 
							UNION ALL 
							SELECT NOM_CLIENT,ADRESSE1,NUMERO_CLIENT
							FROM client_history ) c, status s 
		WHERE f.NUMERO_CLIENT = c.NUMERO_CLIENT
		AND f.STATUS = s.CODE_STATUS AND  f.NUM_FACTURE = '$__UI__'
		GROUP BY f.NUM_FACTURE  
		
			";
		$query=mysqli_query($ma_connexion,$SQL);
		while($row=mysqli_fetch_assoc($query))
		{
			
?>


<div class="text-center pdf-toolbar">

										<div class="btn-group">
											<button id="prev" class="btn btn-white"><i class="fa fa-long-arrow-left"></i> <span class="d-none d-sm-inline">Previous</span></button>
											<button id="next" class="btn btn-white"><i class="fa fa-long-arrow-right"></i> <span class="d-none d-sm-inline">Next</span></button>
											<button id="zoomin" class="btn btn-white"><i class="fa fa-search-minus"></i> <span class="d-none d-sm-inline">Zoom In</span></button>
											<button id="zoomout" class="btn btn-white"><i class="fa fa-search-plus"></i> <span class="d-none d-sm-inline">Zoom Out</span> </button>
											<button id="zoomfit" class="btn btn-white"> 100%</button>
											<span class="btn btn-white hidden-xs">Page: </span>

										<div class="input-group">
											<input type="text" class="form-control" id="page_num">

											<div class="input-group-append">
												<button type="button" class="btn btn-white" id="page_count">/ 22</button>
											</div>
										</div>

											</div>
									</div>

									<div class="text-center m-t-md">
										<canvas id="the-canvas" class="pdfcanvas border-left-right border-top-bottom b-r-md"></canvas>
									</div>	
									
						
							<div class="row">
								<div class="col-md-12">
									<div class="pdf-container" data-href="tracemonkey.pdf"></div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">N° Facture</span>
										<div class="alert alert-primary">
											<?php echo $__UI__ ; ?>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">Statut</span>
										<div class="alert alert-primary">
											<?php echo $row['NOM_STATUS'] ; ?>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">Nom client</span>
										<div class="alert alert-primary">
											<?php echo $row['NOM_CLIENT'] ; ?>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">Code client</span>
										<div class="alert alert-primary">
											<?php echo $row['NUMERO_CLIENT'] ; ?>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">Adresse</span>
										<div class="alert alert-primary">
											<?php echo $row['ADRESSE1'] ; ?>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">Date comptabilisation</span>
										<div class="alert alert-primary">
											<?php echo $row['DATE_COMPTABILISATION'] ; ?>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">N° BC</span>
										<div class="alert alert-primary">
											<?php echo $row['BC'] ; ?>
												<?php
												if($row['BC'] != '' )
												{
													$files = preg_grep('~\.(pdf)$~', scandir("../dossier_bc/"));
													foreach($files as $file){
														$reg = '/.*\_'.$row['BC'].'.pdf/i';
														if(preg_match($reg,$file)) {
															echo'
															<a download="'.$row['BC'].'.pdf'.'" href="dossier_bc/'.$file.'" title="'.$row['BC'].'.pdf">
																<img src="dw.png" class="img-fluid pull-right " style="width:20px ;" alt="'.$row['BC'].'.pdf">
															</a>
															
															';   
														}
													}
												}
												
												
										
									
								    	?>
										</div>
										
									</div>
									
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<span class="badge badge-success">N° BL</span>
										<div class="alert alert-primary">
											<?php echo $row['BL'] ; ?>
												<?php
												if($row['BL'] != '' )
												{
													$files = preg_grep('~\.(pdf)$~', scandir("../dossier_bl/"));
													foreach($files as $file){
														$reg = '/'.$row['BL'].'.pdf/i';
														if(preg_match($reg,$file)) {
															echo'
															<a download="'.$row['BL'].'.pdf'.'" href="dossier_bl/'.$file.'" title="'.$row['BL'].'.pdf">
																<img src="dw.png" class="img-fluid pull-right " style="width:20px ;" alt="'.$row['BL'].'.pdf">
															</a>
															
															';   
														}
													}
												}
												
												
										
									
								    	?>
										</div>
									</div>
									
								</div>
								
							</div>			

							<div id="vertical-timeline" class="vertical-container dark-timeline center-orientation">
								
								
								
								
								
								
								<?php 
									$colors = [
											"1" => "navy",
											"2" => "blue",
											"3" => "lazur",
											"4" => "yellow"];
									$query2 = " SELECT s.NOM_STATUS,fs.DATE,fs.CODE_STATUS,fs.description, fs.is_file_edited
											FROM facture_status fs , status s
											WHERE fs.CODE_STATUS = s.CODE_STATUS
                                            AND fs.NUM_FACTURE = '$__UI__'
											ORDER BY fs.CODE_ 
											
									 ";
									$result2 = mysqli_query($ma_connexion, $query2); 
								   while(($row2 = mysqli_fetch_array($result2)) == true )  
									{ 										
										$is_file_edited = $row2['is_file_edited'];
										if($row2['CODE_STATUS'] == 3 ) 
										{
											$motif_timeline = '' ; 
											$query3 = " SELECT m.NOM_MOTIF,fs.DATE
														FROM facture_status fs , motif m
														WHERE fs.motif =m.CODE_MOTIF
														AND fs.NUM_FACTURE = '$__UI__'
														ORDER BY  fs.CODE_ DESC
														LIMIT 1
												 ";
												$result3 = mysqli_query($ma_connexion, $query3); 
											   while(($row3 = mysqli_fetch_array($result3)) == true )  
												{ 										
													
													$motif_timeline = $row3['NOM_MOTIF'];
													 
												}
											echo '
												<div class="vertical-timeline-block">
													<div class="vertical-timeline-icon '.$colors[$row2['CODE_STATUS']].'-bg">
														<i class="fa fa-file-text"></i>
													</div>

													<div class="vertical-timeline-content">
														<h2>Facture '.$row2['NOM_STATUS'].' </h2>
														<p>
															<b> Motif: </b>'.$motif_timeline.'<br/>
															<b> Description: </b>'.$row2['description'].'';

															if(file_exists('../einvoicetrack_backup/'.$__UI__.'_'.date('Y-m-d', strtotime($row2['DATE'])).'.pdf'))
															{
																echo '
																	<b> PDF: </b><a href="einvoicetrack_backup/'.$__UI__.'_'.date('Y-m-d', strtotime($row2['DATE'])).'.pdf" download style="text-decoration: underline;">
																	 <i class="fa fa-file-pdf-o "></i> Télécharger le fichier .pdf
																	</a>	
																	';
															}
															else if(file_exists('../einvoicetrack/'.$__UI__.'.pdf'))
															{
																echo '
																	<b> PDF: </b><a href="einvoicetrack/'.$__UI__.'.pdf" download style="text-decoration: underline;">
																	 <i class="fa fa-file-pdf-o "></i> Télécharger le fichier .pdf
																	</a>
																	';
															}																
														echo'
														</p>
														<span class="vertical-date">
															'.$row2['NOM_STATUS'].' <br/>
															<small>'.date('Y-m-d', strtotime($row2['DATE'])).' </small>
														</span>
													</div>
												</div>
												
												
												';
										}
										else 
										{
//											echo $query2;
											echo '
												<div class="vertical-timeline-block">
													<div class="vertical-timeline-icon '.$colors[$row2['CODE_STATUS']].'-bg">
														<i class="fa fa-file-text"></i>
													</div>

													<div class="vertical-timeline-content">
														<h2>Facture '.$row2['NOM_STATUS'].' </h2>
														<p>';
											                if($is_file_edited == 1){
                                                                if(file_exists('../einvoicetrack_backup/'.$__UI__.'_'.date('Y-m-d', strtotime($row2['DATE'])).'.pdf'))
                                                                {
                                                                    echo '
																	<b> PDF: </b><a href="einvoicetrack_backup/'.$__UI__.'_'.date('Y-m-d', strtotime($row2['DATE'])).'.pdf" download style="text-decoration: underline;">
																	 <i class="fa fa-file-pdf-o "></i> Télécharger le fichier .pdf
																	</a>	
																	';
                                                                }
                                                                else if(file_exists('../einvoicetrack/'.$__UI__.'.pdf'))
                                                                {
                                                                    echo '
																	<b> PDF: </b><a href="einvoicetrack/'.$__UI__.'.pdf" download style="text-decoration: underline;">
																	 <i class="fa fa-file-pdf-o "></i> Télécharger le fichier .pdf
																	</a>
																	';
                                                                }
                                                            }

														echo'
														</p>
														<span class="vertical-date">
															'.$row2['NOM_STATUS'].' <br/>
															<small>'.date('Y-m-d', strtotime($row2['DATE'])).' </small>
														</span>
													</div>
												</div>
												
												
												';
											
										} 
									}
											
								
								
								?>

								
							</div>							
						
					<?php			

		}
	}
	
}
ob_end_flush();
?>
