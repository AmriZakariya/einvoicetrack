<?php
ob_start();
session_start();
include '../connexion.php';


if (isset($_SESSION['user_einvoicetrack'])) {
    $datatable_data = array();
    $i = 0;


    $SQL = "SELECT b.BL, b.NUMERO_CLIENT,b.BC, b.DATE_LIVRAISON,b.DATE_NUMERISATION, b.pdf
                    , `TYPE_VENTE`, `TYPE_CLIENT`, `BL_MANUEL`, `DATE_DOCUMENT`
		    FROM bl b  

		";

//    $SQL = "SELECT b.BL,c.NOM_CLIENT, b.NUMERO_CLIENT,b.BC, b.DATE_LIVRAISON,b.DATE_NUMERISATION, b.pdf
//                    , `TYPE_VENTE`, `TYPE_CLIENT`, `BL_MANUEL`, `DATE_DOCUMENT`
//		FROM bl b , ( SELECT NOM_CLIENT,ADRESSE1,NUMERO_CLIENT
//							FROM client
//							UNION ALL
//							SELECT NOM_CLIENT,ADRESSE1,NUMERO_CLIENT
//							FROM client_history ) c
//		WHERE
//			( b.NUMERO_CLIENT = c.NUMERO_CLIENT )
//
//
//
//		";


    if (isset($_POST['NUM_BL'])) {
        $SQL .= " AND ( ";
        foreach ($_POST['NUM_BL'] as $selectedOption) {
            $SQL .= " b.BL LIKE \"%$selectedOption%\" OR ";
        }
        $SQL = substr($SQL, 0, -3);
        $SQL .= " )";
    }


//    if (isset($_POST['NOM_CLIENT'])) {
//        $SQL .= " AND ( ";
//        foreach ($_POST['NOM_CLIENT'] as $selectedOption) {
//            $SQL .= " c.NOM_CLIENT LIKE \"%$selectedOption%\" OR ";
//        }
//        $SQL = substr($SQL, 0, -3);
//        $SQL .= " )";
//    }

    if (isset($_POST['NUMERO_CLIENT'])) {
        $SQL .= " AND ( ";
        foreach ($_POST['NUMERO_CLIENT'] as $selectedOption) {
            $SQL .= " c.NUMERO_CLIENT LIKE \"%$selectedOption%\" OR ";
        }
        $SQL = substr($SQL, 0, -3);
        $SQL .= " )";
    }


    if (isset($_POST['BC'])) {
        $SQL .= " AND ( ";
        foreach ($_POST['BC'] as $selectedOption) {
            $SQL .= " b.BC LIKE \"%$selectedOption%\" OR ";
        }
        $SQL = substr($SQL, 0, -3);
        $SQL .= " )";
    }


    if (isset($_POST['TYPE_VENTE'])) {
        $SQL .= " AND ( ";
        foreach ($_POST['TYPE_VENTE'] as $selectedOption) {
            $SQL .= " b.TYPE_VENTE LIKE \"%$selectedOption%\" OR ";
        }
        $SQL = substr($SQL, 0, -3);
        $SQL .= " )";
    }


    if (isset($_POST['TYPE_CLIENT'])) {
        $SQL .= " AND ( ";
        foreach ($_POST['TYPE_CLIENT'] as $selectedOption) {
            $SQL .= " b.TYPE_CLIENT LIKE \"%$selectedOption%\" OR ";
        }
        $SQL = substr($SQL, 0, -3);
        $SQL .= " )";
    }


    if (isset($_POST['BL_MANUEL'])) {
        $SQL .= " AND ( ";
        foreach ($_POST['BL_MANUEL'] as $selectedOption) {
            $SQL .= " b.BL_MANUEL LIKE \"%$selectedOption%\" OR ";
        }
        $SQL = substr($SQL, 0, -3);
        $SQL .= " )";
    }


    if (isset($_POST['DATE_DOCUMENT'])) {
        $SQL .= " AND ( ";
        foreach ($_POST['DATE_DOCUMENT'] as $selectedOption) {
            $SQL .= " b.DATE_DOCUMENT LIKE \"%$selectedOption%\" OR ";
        }
        $SQL = substr($SQL, 0, -3);
        $SQL .= " )";
    }




    if (($_POST['DATE_LIVRAISON_START'] != '') && ($_POST['DATE_LIVRAISON_END'] != '')) {

        $SQL .= " AND  b.DATE_LIVRAISON between '" . $_POST['DATE_LIVRAISON_START'] . "' and '" . $_POST['DATE_LIVRAISON_END'] . "'    ";
    }

    if (($_POST['DATE_NUMERISATION_START'] != '') && ($_POST['DATE_NUMERISATION_END'] != '')) {

        $SQL .= " AND  b.DATE_NUMERISATION between '" . $_POST['DATE_NUMERISATION_START'] . "' and '" . $_POST['DATE_NUMERISATION_END'] . "'    ";
    }


    // $SQL .= " ORDER BY ETAT" ;
//	 $SQL .= " LIMIT 100" ;
    $SQL .= " 		GROUP BY b.BL";
// echo $SQL ;


    $query = mysqli_query($ma_connexion, $SQL);
    while ($row = mysqli_fetch_assoc($query)) {

        $row_id_client =  $row['NUMERO_CLIENT'] ;
        $client_name = "" ;

        $SQL_search = "SELECT NOM_CLIENT,ADRESSE1,NUMERO_CLIENT
							FROM client
                            WHERE NUMERO_CLIENT  = '$row_id_client'
						";


//        echo $SQL_search;
        $query_search = mysqli_query($ma_connexion, $SQL_search);
        if (mysqli_num_rows($query_search) == 1) {
            while ($row_search = mysqli_fetch_assoc($query_search)) {
                $client_name = $row_search['NOM_CLIENT'];
            }
        }





        $datatable_data[$i]["check_box"] = '';
        $datatable_data[$i]["BL"] = $row['BL'];
        $datatable_data[$i]["NOM_CLIENT"] = $client_name;
        $datatable_data[$i]["CODE_CLIENT"] = $row['NUMERO_CLIENT'];
        $datatable_data[$i]["BC"] = $row['BC'];
        $datatable_data[$i]["DATE_LIVRAISON"] = date('Y-m-d', strtotime($row['DATE_LIVRAISON']));
        $datatable_data[$i]["DATE_NUMERISATION"] = date('Y-m-d', strtotime($row['DATE_NUMERISATION']));


        if (strpos($datatable_data[$i]["DATE_LIVRAISON"], '1970') !== false) {
            $datatable_data[$i]["DATE_LIVRAISON"] = "";
        }

        if (strpos($datatable_data[$i]["DATE_NUMERISATION"], '1970') !== false) {
            $datatable_data[$i]["DATE_NUMERISATION"] = "";
        }

        $datatable_data[$i]["TYPE_VENTE"] = $row['TYPE_VENTE'];
        $datatable_data[$i]["TYPE_CLIENT"] = $row['TYPE_CLIENT'];
        $datatable_data[$i]["BL_MANUEL"] = $row['BL_MANUEL'];
        $datatable_data[$i]["DATE_DOCUMENT"] = $row['DATE_DOCUMENT'];


        $datatable_data[$i]["DETAILL"] = '
 
					<button type="button"  class="btn btn-primary btn-circle detail"  value="' . urlencode($row['BL']) . '">
						<i class="fa fa-list"></i>
					</button>  ';

        if ($_SESSION['role'] == 'superadmin' || $_SESSION['role'] == 'admin')
            $datatable_data[$i]["DETAILL"] .= '
						<button type="button"  class="btn btn-danger btn-circle delete"  value="' . urlencode($row['BL']) . '">
							<i class="fa fa-trash"></i>
						</button> 
						';


        if (($_SESSION['role'] == 'superadmin' || $_SESSION['role'] == 'admin')) {

            $datatable_data[$i]["DETAILL"] .= '
						<button type="button"  class="btn btn-success btn-circle edit"  value="' . urlencode($row['BL']) . '">
							<i class="fa fa-edit"></i>
						</button> 
						';
        }

//				 if(	$row['pdf'] == 1 || (file_exists($_SERVER['DOCUMENT_ROOT'].'/dossier_bl.'.DIRECTORY_SEPARATOR.$row['BL'].'.pdf')) )

        $thumb_name = "../dossier_bl/".$row['BL'] . ".pdf";

        if (file_exists($thumb_name) == true) {

            $datatable_data[$i]["DETAILL"] .= '
						<button type="button"   class="btn btn-dark btn-circle pdf_dwn"    value="' . urlencode($row['BL']) . '">
							<i class="fa  fa-file-pdf-o"></i>
						</a> ';
        }

//        if ($row['BL'] == "BL910") {
//
//            $datatable_data[$i]["DETAILL"] .= $thumb_name ;
//        }


        $i++;
    }

    echo json_encode($datatable_data);
}
ob_end_flush();
?>

