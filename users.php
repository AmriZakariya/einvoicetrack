<?php
ob_start();
session_start();
include 'connexion.php';


if(isset($_SESSION['user_einvoicetrack']))
{
	
	$current_user = decode($_SESSION['user_einvoicetrack']) ; 					
	$SQL="SELECT  `NOM_USER`, `PRENOM_USER`, `EMAIL_USER`,`CIVILITE_USER`,
			`CODE_ENTREPRISE`, `ROLE_USER`
		  FROM `user` 
		  WHERE CODE_USER = $current_user
		  AND actif= 1";
	$query=mysqli_query($ma_connexion,$SQL);
	if(mysqli_num_rows($query) == 1)
	{
		while($row=mysqli_fetch_assoc($query))
		{	
				$NOM_USER = $row['CIVILITE_USER'].' ' .$row['NOM_USER'];
		}
	}
	else 
	{
		header('Location: login');
	}
	
	
		// $i = 0 ; 
												

		// $SQL="SHOW COLUMNS FROM client";
		// $query=mysqli_query($ma_connexion,$SQL);
		// while($row=mysqli_fetch_assoc($query))
		// {	
				// echo '
					// <a class="toggle-vis" data-column="'.$i++.'">'.$row['Field'].'</a> -
					// ';
		// }
}
else 
{
	header('Location: login');
}
?>
<!DOCTYPE html>
<html lang="fr">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>eInvoiceTrack</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/plugins/dropzone/basic.css" rel="stylesheet">
    <link href="css/plugins/dropzone/dropzone.css" rel="stylesheet">
    <link href="css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
	
	 <link href="css/plugins/dataTables/datatables.min.css" rel="stylesheet">

	
	<link href="css/plugins/select2/select2.min.css" rel="stylesheet">
	
	
	<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
	
	
	
	
	
	
	
	
	
	

<style>

.select2 {
	width:100%!important;
	}
	
	.DETAILL
	{
		text-align: center; 
		vertical-align: middle;
	}
	.NOM
	{
		font-weight: bold;
		text-decoration: underline;
		cursor : pointer ; 
	}
	#liste_table tr
	{
		 cursor : pointer ; 
	}
	
	
	</style>

</head>

<body class="">

    <div id="wrapper">

    
		<?php
			include 'includes/nav.php';
		?>	

        <div id="page-wrapper" class="gray-bg">
		   <?php
				include 'includes/header.php';
			?>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>List des utilisateurs</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="index"> Accueil </a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong></strong>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-8">
                    <div class="title-action">
                    </div>
                </div>
            </div>

           <div class="wrapper wrapper-content  animated fadeInRight">
            <div class="row" id="sortable-view">
			<div class="col-md-12 dragme" >
				<div class="ibox">
					
					
					
				</div>
			</div >
			
			
				
			<div class="col-md-12 dragme">
                    <div class="ibox ">
					 <div class="ibox-title" style="background-color: #24c6c8; color: white;">
							<h5> <i class="fa fa-search"></i> Recherche </h5>
							 <div class="ibox-tools" >
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up" style="color: white;"></i>
                                </a>
                                
                               
                            </div>
                        </div>
                        <div class="ibox-content">
                        <form id="search_form" method="POST">
									<div class="row">
										<div class="col-md-3">
											<span class="badge badge-success">Nom</span>
											<select class="sc_select2 form-control"  name="NOM_USER[]" id="NOM_USER" data-placeholder="Entrer noms "  multiple="multiple">
												
											</select>
										</div>
										
										<div class="col-md-3">
											<span class="badge badge-success">Email</span> 
											<select class="sc_select2 form-control" name="EMAIL_USER[]"  id="EMAIL_USER" data-placeholder="Entrer Emails " multiple="multiple">
												
											</select>
										</div>
										
										<div class="col-md-3">
											<span class="badge badge-success">Civilité</span> 
											<select class="sc_select2 form-control" name="CIVILITE_USER[]"  id="CIVILITE_USER" data-placeholder="Selectionner civilités" multiple="multiple">
												<option value="Mr"  >Homme</option>
												<option value="Mme" >Femme</option>
											</select>
										</div>
										
										<div class="col-md-3">
											<span class="badge badge-success">Rôle</span> 
											<select class="sc_select2 form-control" name="ROLE_USER[]"  id="ROLE_USER" data-placeholder="Selectionner rôles " multiple="multiple">
												<option value="1" >Utilisateur</option>
												<option value="2" >Admin</option>
											</select>
										</div>
										
										<div class="col-md-3">
											<span class="badge badge-success">Status</span> 
											<select class="sc_select2 form-control" name="STATUS_USER[]"  id="STATUS_USER" data-placeholder="Selectionner rôles " multiple="multiple">
												<option value="0" >Non activé</option>
											<option value="1" > Activé</option>
											</select>
										</div>
										
									</div>
									<br/>
									<div class="row">
										<div class="col-md-12">
											<button type="button" class="btn btn-success btn-rounded pull-right" id="rechercher" > <i class="fa fa-search"></i> Rechercher</button>
											<button type="button" class="btn btn-danger btn-rounded pull-right" id="vider" style="margin-right: 4px;"> <i class="fa fa-eraser"></i> Vider</button>
										</div>
									</div>
								</form>
	
						</div>
                       
					</div>
			</div>
				
			
			 <div class="col-lg-12 dragme" id="res_search">
                    <div class="ibox">
                        <div class="ibox-title" style="background-color: #24c6c8; color: white;">
						<h5> <i class="fa fa-list"></i> Resultat Recherche</h5>
						 <div class="ibox-tools" >
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up" style="color: white;"></i>
                                </a>
							
                               
                            </div>
                        </div>
                        <div class="ibox-content">
						

							
							<hr/>
								<?php
				if( $_SESSION['role'] ==  'superadmin' )
				{
					
														
					?>
							<button class="btn btn-danger delete_multiple" ><i class="glyphicon glyphicon-trash"></i>  <i class="fa fa-delete "></i> Supprimer</button>	
<?php
				}
														
					?>			
							<div class="table-responsive">
								<div class="sk-spinner sk-spinner-wave" style="display: none">
                                    <div class="sk-rect1"></div>
                                    <div class="sk-rect2"></div>
                                    <div class="sk-rect3"></div>
                                    <div class="sk-rect4"></div>
                                    <div class="sk-rect5"></div>
                                </div>
								<table class="table table-hover table-bordered table-striped" style="width : 100%" id="liste_table">
									<thead>
									  <tr>
										<th style="width:50px"></th>
										<th style="width:30px">Civilité</th>
										<th >Nom </th>
										<th >Email</th>
										<th style="width:50px">Rôle</th>
										<th style="width:30px">Status</th>
										<th <?php if( $_SESSION['role'] !=  'superadmin')echo 'style="width:5px"' ;  ?>></th>
									  </tr>
									</thead>
									<tbody>
									
									</tbody>
								</table>
								
								
							</div>

                        </div>
                    </div>
                </div>
				
				
				
				<div class="col-lg-4 dragme"  style="display:none" id="drag_edit">
					<form id="edit_form" method="POST"  >				
						<div class="ibox">
							<div class="ibox-title">
								<h5>Modifier <span class="label label-primary"></span> </h5>
							</div>

							<div class="ibox-content" id="detail_list">
							</div>
						</div>
					<form>
				</div>
				
				<div class="col-lg-4 dragme"  style="display:none" id="drag_detail">
					<div class="ibox">
						<div class="ibox-title">
							<h5>DETAILLE <span class="label label-primary"></span> </h5>
						</div>
						<div class="ibox-content" id="detail_list_info">
						
							
						</div>
						
						
					</div>
				</div>
				
				
				
				
			
				
				
				
               
				
                
                
            

            </div>
        </div>
           <?php
			include 'includes/footer.php';
			
		?>	

        </div>
        </div>





	
    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>

    <!-- jquery UI -->
    <script src="js/plugins/jquery-ui/jquery-ui.min.js"></script>

    <!-- Touch Punch - Touch Event Support for jQuery UI -->
    <script src="js/plugins/touchpunch/jquery.ui.touch-punch.min.js"></script>

	
    <!-- Select2 -->
    <script src="js/plugins/select2/select2.full.min.js"></script>
	<script src="js/i18n/fr.js"></script>
	
	
	<!-- datatable -->
	<script src="js/plugins/dataTables/datatables.min.js"></script>
    <script src="js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>


	<!-- Steps -->
	<link href="css/plugins/steps/jquery.steps.css" rel="stylesheet">
    <script src="js/plugins/steps/jquery.steps.min.js"></script>
    <script src="js/plugins/steps/jquery.steps.fix.js"></script>

    <!-- Jquery Validate -->
    <script src="js/plugins/validate/jquery.validate.min.js"></script>
    <script src="js/plugins/validate/messages_fr.js"></script>
	
	
	 <!-- Maps -->
	<link rel="stylesheet" type="text/css" href="https://js.api.here.com/v3/3.0/mapsjs-ui.css?dp-version=1549984893" />
	<script type="text/javascript" src="https://js.api.here.com/v3/3.0/mapsjs-core.js"></script>
	<script type="text/javascript" src="https://js.api.here.com/v3/3.0/mapsjs-service.js"></script>
	<script type="text/javascript" src="https://js.api.here.com/v3/3.0/mapsjs-ui.js"></script>
	<script type="text/javascript" src="https://js.api.here.com/v3/3.0/mapsjs-mapevents.js"></script>
	
	 <!-- Sweet alert -->
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	
	
	 <!-- DROPZONE -->
    <script src="js/plugins/dropzone/dropzone.js"></script>
	
	<!-- datatables checkboxes	  -->
	<link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/css/dataTables.checkboxes.css" rel="stylesheet" />
	<script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/js/dataTables.checkboxes.min.js"></script>

	<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
	
	  <!-- Password meter -->
    <script src="js/plugins/pwstrength/pwstrength-bootstrap.min.js"></script>
	

    <script src="js/plugins/pwstrength/zxcvbn.js"></script>
	
	
	
	
 <div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
	<div class="modal-content animated bounceInRight">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<i class="fa fa-key modal-icon"></i>
				<h4 class="modal-title">Modifier Mot de passe</h4>
			</div>
			<div class="modal-body" >
				
				<div id="edit_mdp_panel">
					
				</div>
				<div class="row" id="pwd-container1">
					<div class="col-sm-12">
						<div class="form-group">
							<span class="label label-info float-left">Nouveau de passe  </span>  <span class="required">* </span>
							<input type="password" class="form-control example1" id="password1" name="password1" placeholder="**********">
						</div>
						<div class="form-group">
							<div class="pwstrength_viewport_progress"></div>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<span class="label label-info float-left">Confirmer mot de passe  </span>  <span class="required">* </span>
					<input type="password" class="form-control" id="password1_confirm" name="password1_confirm" placeholder="**********">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" data-dismiss="modal">Annuler</button>
				<button type="button" id="enregistrer_edit_mdp" class="btn btn-primary">Modifer</button>
			</div>
		</div>
	</div>
</div>

 <script>
        $(document).ready(function(){
            var options1 = {};
				options1.ui = {
					container: "#pwd-container1",
					showVerdictsInsideProgressBar: true,
					viewports: {
						progress: ".pwstrength_viewport_progress"
					}
				};
				options1.common = {
					debug: false,
				};
				$('.example1').pwstrength(options1);
        });
		
    </script>
<script>

$(document).ready(function(){
	
		WinMove();
	
		$.fn.select2.defaults.set('language', 'fr');
	
		$(".sc_select2").select2({
			allowClear: true,
			tags: true,
			language: "fr",
			 width: '100%',
			 
		});
		
		var table =   $('#liste_table').DataTable({	
				responsive: true,
				"language":{
						"sProcessing":     "Traitement en cours...",
						"sSearch":         "Rechercher un utilisateur &nbsp;:",
						"sLengthMenu":     "Afficher _MENU_  utilisateur",
						"sInfo":           "Affichage des utilisateur _START_ &agrave; _END_ sur _TOTAL_ utilisateurs",
						"sInfoEmpty":      "Affichage du utilisateur 0 &agrave; 0 sur 0 utilisateurs",
						"sInfoFiltered":   "(filtr&eacute; de _MAX_ utilisateurs au total)",
						"sInfoPostFix":    "",
						"sLoadingRecords": "Chargement en cours...",
						"sZeroRecords":    "Aucuns virements &agrave; afficher",
						"sEmptyTable":     "Aucunes donn&eacute;e disponible dans le tableau",
						"oPaginate": {
							"sFirst":      "Premier",
							"sPrevious":   "Pr&eacute;c&eacute;dent",
							"sNext":       "Suivant",
							"sLast":       "Dernier"
						},
						"oAria": {
							"sSortAscending":  ": activer pour trier la colonne par ordre croissant",
							"sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
						}
				},
				dom: '<"html5buttons"B>lTfgitp',
				buttons: [
					{ 
						extend: 'copy',
						title: 'eInvoiceTrack_users',
						exportOptions: {
							columns:  ':visible:not(:last-child)'
						}
					},
					{
						extend: 'csv',
						title: 'eInvoiceTrack_users',
						exportOptions: {
							columns:  ':visible:not(:last-child)'
						}
					},
					{
						extend: 'excel',
						title: 'eInvoiceTrack_users',
						exportOptions: {
							columns:  ':visible:not(:last-child)'
						}
					},

					{extend: 'print',
					 customize: function (win){
							$(win.document.body).addClass('white-bg');
							$(win.document.body).css('font-size', '10px');

							$(win.document.body).find('table')
									.addClass('compact')
									.css('font-size', 'inherit');
						}
					}
				],
				"columns": [
					{ "data": "check_box"},
					{ "data": "CIVILITE",className: "CIVILITE"},
					{ "data": "NOM",className: "NOM"},
					{ "data": "EMAIL",className: "EMAIL"},
					{ "data": "ROLE",className: "ROLE"},
					{ "data": "STATUS",className: "STATUS"},
					{ "data": "DETAILL",className: "DETAILL"}
				 ],
			    'columnDefs': [
				 {
					'targets': 0,
					'checkboxes': {
					   'selectRow': true,
					   'selectCallback': function(nodes, selected){
								// console.log(nodes);
								
						        
					   }
					},
				 }
			  ],
			  'select': 'multi',
			  'order': [[1, 'asc']]
			});	
			
		  
			
			
			
			$(document).on('click', '#rechercher', function(){ 	
				$.ajax(
				{
					type : 'post',
					url : 'get/get_user_search.php',
					data: { 
							"NOM_USER" : $('#NOM_USER').val(),
							"EMAIL_USER" : $('#EMAIL_USER').val(),
							"CIVILITE_USER" : $('#CIVILITE_USER').val(),
							"ROLE_USER" : $('#ROLE_USER').val(),
							"STATUS_USER" : $('#STATUS_USER').val()
							},
					success : function(data)
					{
						console.log(data);
						info_html = JSON.parse(data);
						table.clear();
						table.rows.add(info_html).draw();

					},
					complete : function(data)
					{
						$(".sk-spinner").css("display","none");
						//$("#res_search").attr("class","col-lg-8");
					},
					beforeSend : function(data)
					{
						$(".sk-spinner").css("display","block ");
						// alert(data);
						// do something, not critical.
					},
					error: function (jqXHR, exception) {
						var msg = '';
						if (jqXHR.status === 0) {
							msg = 'Not connect.\n Verify Network.';
						} else if (jqXHR.status == 404) {
							msg = 'Requested page not found. [404]';
						} else if (jqXHR.status == 500) {
							msg = 'Internal Server Error [500].';
						} else if (exception === 'parsererror') {
							msg = 'Requested JSON parse failed.';
						} else if (exception === 'timeout') {
							msg = 'Time out error.';
						} else if (exception === 'abort') {
							msg = 'Ajax request aborted.';
						} else {
							msg = 'Uncaught Error.\n' + jqXHR.responseText;
						}
						// alert(msg);
					}
				});

			});
			
			
			
			 $('a.toggle-vis').on( 'click', function (e) {
				e.preventDefault();
		 
				var column = table.column( $(this).attr('data-column') );
		 
				column.visible( ! column.visible() );
				
				$(this).toggleClass( "badge-success text-light" );
			} );
			
			
			
			$(document).on('click', '.delete', function(e)
			{ 
				e.stopPropagation();
					
				var this_ = $(this) ; 
						Swal.fire({
						  title: 'Supprimer utilisateur',
						  text: "Êtes-vous sûr de continuer?!",
						  type: 'warning',
						  showCancelButton: true,
						  confirmButtonColor: '#3085d6',
						  cancelButtonColor: '#d33',
						  confirmButtonText: 'Oui, Supprimer!',
						  cancelButtonText: "Non, Annuler!",  
						}).then((result) => {
						  if (result.value) {
									var dataString = "current_delete="+ this_.val(); 
									$.ajax({
									type: "POST",
									url: "set/edit_user.php",
									data: dataString,
									cache: true,
									success: function(html){
											// this_.parents("tr").find('.label-primary').parent().html('<span class="label label-danger">supprimé </span>');
											// this_.parents("tr").find('.prixhistoryedit').html('<span class="label label-danger">');
											// this_.parent().html('<span class="label label-danger">');
											console.log(html);
											table.row(this_.parents('tr')).remove().draw(false);
											 $("#detail_list").html('')
												Swal.fire(
												  'Client supprimé !',
												  'Les informations sont changées!.',
												  'success'
												)
												
												// $("#res_search").attr("class","col-lg-12");
												$("#drag_gps").hide();
												$("#drag_detail").hide();
												$("#drag_edit").hide();
													
											// console.log(html);
											} 
									
											
									});
							  }else 
							  {
								  Swal.fire(
									  'annulé!',
									  'Action annulée!!',
									  'error'
									)
								  
							  }
							})
				

			});
	
			$(document).on('click', '.delete_multiple', function(e)
			{ 
				e.stopPropagation();
				
				let rows = $( table.$('input[type="checkbox"]').map(function () {
						  return $(this).prop("checked") ? $(this).closest('tr') : null ;					  
				}));
				
				// keys.each(function(k,v){
					// console.log(v);
				// })

					
				var this_ = $(this) ; 
						Swal.fire({
						  title: 'Supprimer clients',
						  text: "Êtes-vous sûr de continuer?!",
						  type: 'warning',
						  showCancelButton: true,
						  confirmButtonColor: '#3085d6',
						  cancelButtonColor: '#d33',
						  confirmButtonText: 'Oui, Supprimer!',
						  cancelButtonText: "Non, Annuler!",  
						}).then((result) => {
						  if (result.value) {
							  

								$.ajax(
								{
									type : 'POST',
									url : 'set/edit_user.php',
									data: { 
											"delete_code" : table.$('input[type="checkbox"]').map(function () {
																	  return $(this).prop("checked") ? $(this).closest('tr').find('.delete').val() : null;
															}).get(),
											},
									success : function(data)
									{
										console.log(data);
										// $(".logs_man").html(data);
										rows.each(function(k,v){
											table.row(v).remove().draw(false);
										})
										

									},
									complete : function(data)
									{
										$("#res_search").attr("class","col-lg-12");
										$("#drag_gps").hide();
										$("#drag_detail").hide();
										$("#drag_edit").hide();
										// $(".sk-spinner").css("display","none");
										// $("#res_search").attr("class","col-lg-8");
										// $('#ibox_result').children('.ibox-content').toggleClass('sk-loading');
									},
									beforeSend : function(data)
									{
										// $('#ibox_result').children('.ibox-content').toggleClass('sk-loading');
										// $(".sk-spinner").css("display","block ");
										// alert(data);
										// do something, not critical.
									},
									error: function (jqXHR, exception) {
										var msg = '';
										if (jqXHR.status === 0) {
											msg = 'Not connect.\n Verify Network.';
										} else if (jqXHR.status == 404) {
											msg = 'Requested page not found. [404]';
										} else if (jqXHR.status == 500) {
											msg = 'Internal Server Error [500].';
										} else if (exception === 'parsererror') {
											msg = 'Requested JSON parse failed.';
										} else if (exception === 'timeout') {
											msg = 'Time out error.';
										} else if (exception === 'abort') {
											msg = 'Ajax request aborted.';
										} else {
											msg = 'Uncaught Error.\n' + jqXHR.responseText;
										}
										// alert(msg);
									}
								});
							  
								// var rows = $( table.$('input[type="checkbox"]').map(function () {
								  // return $(this).prop("checked") ? $(this).closest('tr') : null;
								// } ) );
								
								
								// function show(values) {
									// console.log(values);  
								 // }
								 // rows.forEach(show[0].cells[4].find('.delete').val()); 
								
								// console.log(rows[0].cells[4].find('.delete').val());
									// var dataString = "current_delete="+ this_.val(); 
									// $.ajax({
										// type: "POST",
										// url: "set/edit_client.php",
										// data: dataString,
										// cache: true,
										// success: function(html){
											
											// console.log(html);
										// }
											
									// });
							  }else 
							  {
								  Swal.fire(
									  'annulé!',
									  'Action annulée!!',
									  'error'
									)
								  
							  }
							})
				

			});

			
			var current_tr = null ; 
			$(document).on('click', '.edit', function(e){
				current_tr = $(this);
					e.stopPropagation();
					
					var dataString = '__UI__='+$(this).val();
					$.ajax({
						type: "POST",
						url: "get/get_user_detail.php",
						data: dataString,
						cache: true,
						success: function(html){
								$("#res_search").attr("class","col-lg-8");
								
								$("#detail_list").html(html);
								$("#drag_edit").show();
								trigers_checked();
								$('#edit_statuts').bootstrapToggle({
									on: 'Activé',
									off: 'Non Activé',
									size: 'large',
									onstyle: 'primary',
									offstyle: 'warning'
								});
							}
						});	
					
			});
			
			$(document).on('click', '.edit_mdp', function(e){
				current_tr = $(this);
					e.stopPropagation();
					
					$( "#password1" ).val('') ;
					$( "#password1_confirm" ).val('') ;
					// $('.example1').pwstrength(nul);
					// $('#example1').pwstrength(options);
					$("#myModal").modal();
					
			});
			
			
			
			
			
			
			

			$(document).on('click', '#enregistrer_edit', function(){ 	
			var this_ = $(this) ; 
				var dataString = $( "#edit_form" ).serialize()+"&current_list="+ $(this).val(); 
				console.log(dataString);
				$.ajax
					({
						type: "POST",
						url: "set/edit_user.php",
						data: dataString ,
						cache: false,
						success: function(html)
						{
							console.log(html);
							if (html.trim() == 1 )
								{
							
									Swal.fire(
									  'Succès!',
									  'Utilisateur modifié!',
									  'success'
									)
									$("#res_search").attr("class","col-lg-12");
									$("#drag_edit").hide();
									$("#rechercher").trigger("click");
									 // table.row(current_tr.parents('tr')).remove().draw(false);
									
									// $.ajax(
									// {
										// type : 'post',
										// url : 'get/get_client_search_unique.php',
										// data: 'current_client=' +  this_.val(),
										// success : function(data)
										// {
											// info_html = JSON.parse(data);
											// table.rows.add(info_html).draw();

										// }
										
									// });
									
									
									
								
								}
								else 
								{
									Swal.fire(
									  'Erreur!',
									  'Utilisateur --!',
									  'error'
									)
									
								}


						}
					});

			});
			
	
			

			$(document).on('click', '#enregistrer_edit_mdp', function(){ 
				$("#edit_mdp_panel").html('');
				if(	 $( "#password1" ).val().length  < 6 )
				{
					$("#edit_mdp_panel").prepend('<div class="alert alert-warning">  Le mot de passe doit contenir au moins 6 caractères! </div>');
					
				}
				else 
				{
					if(	$( "#password1" ).val() != $( "#password1_confirm" ).val() )
					{
						$("#edit_mdp_panel").prepend('<div class="alert alert-warning"> Les mots de passes saisis ne sont pas identiques </div>');
						
					}
					else 
					{
						var dataString = "token=set_mdp&current="+current_tr.val()+"&mdp="+$( "#password1" ).val()  ; 
						// alert(dataString);
						$.ajax
						({
							type: "POST",
							url: "set/edit_user.php",
							data: dataString ,
							cache: false,
							success: function(html)
							{
								// console.log(html);
								if (html.trim() == 1 )
									{
								
										Swal.fire(
										  'Succès!',
										  'Utilisateur modifié!',
										  'success'
										)
										$("#myModal").modal("hide");
										$( "#password1" ).val('') ;
										$( "#password1_confirm" ).val('') ;
									
									}
									else 
									{
										
										$("#edit_mdp_panel").prepend('<div class="alert alert-danger"> '+html+' </div>');
						
									}


							}
						});

					}	
					
				}
						
			// var this_ = $(this) ; 
				// var dataString = $( "#edit_form" ).serialize()+"&current_list="+ $(this).val(); 
				// console.log(dataString);
				// $.ajax
					// ({
						// type: "POST",
						// url: "set/edit_user.php",
						// data: dataString ,
						// cache: false,
						// success: function(html)
						// {
							// console.log(html);
							// if (html.trim() == 1 )
								// {
							
									// Swal.fire(
									  // 'Succès!',
									  // 'Utilisateur modifié!',
									  // 'success'
									// )
									// $("#res_search").attr("class","col-lg-12");
									// $("#drag_edit").hide();
									// $("#rechercher").trigger("click");
									 
									
								
								// }
								// else 
								// {
									// Swal.fire(
									  // 'Erreur!',
									  // 'Utilisateur --!',
									  // 'error'
									// )
									
								// }


						// }
					// });

			});
			
	
		
			$(document).on('click', '#vider', function(){ 	
				$('.sc_select2').val(null).trigger('change');
			});
			
			$(document).on('click', '#vider_edit', function(){ 	
				// $("#res_search").attr("class","col-lg-12");
				$("#detail_list").html('');
				$("#drag_edit").hide();
			});
			
			$("#drag_gps").on( "sortupdate", function( event, ui ) {
				showmapGPS();
			});
		

	
	
});
</script>

<script >
		
function trigers_checked()
{
var client = document.getElementById('checkbox_1');
var facture = document.getElementById('checkbox_2');
var tableauboard = document.getElementById('checkbox_6');
var rapport = document.getElementById('checkbox_7');


client.onchange = function() 
{
    if (facture.checked==true) 
    {
        client.checked = true;
    }
};

facture.onchange = function() 
{
    if (facture.checked==true) 
    {
        client.checked = true;
    }
	
    if (tableauboard.checked==true || rapport.checked==true ) 
    {
        facture.checked = true;
    }
	
};

tableauboard.onchange = function() 
{
    if (tableauboard.checked==true) 
    {
        facture.checked = true;
        client.checked = true;
    }
};

rapport.onchange = function() 
{
    if (rapport.checked==true) 
    {
        facture.checked = true;
        client.checked = true;
    }
};
}


	
	</script>

	
</body>

</html>
