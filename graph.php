<?php
ob_start();
session_start();
include 'connexion.php';


if(isset($_SESSION['user_einvoicetrack']))
{
	
	$current_user = decode($_SESSION['user_einvoicetrack']) ; 					
	$SQL="SELECT  `NOM_USER`, `PRENOM_USER`, `EMAIL_USER`,`CIVILITE_USER`,
			`CODE_ENTREPRISE`, `ROLE_USER`
		  FROM `user` 
		  WHERE CODE_USER = $current_user
		  AND actif= 1";
	$query=mysqli_query($ma_connexion,$SQL);
	if(mysqli_num_rows($query) == 1)
	{
		while($row=mysqli_fetch_assoc($query))
		{	
				$NOM_USER = $row['CIVILITE_USER'].' ' .$row['NOM_USER'];
		}
	}
	else 
	{
		header('Location: login');
	}
	if( $_SESSION['role'] != 'superadmin' )
	{
		$SQL="SELECT 1
				FROM  user_module um
				WHERE  um.CODE_USER ='".decode($_SESSION['user_einvoicetrack'])."' 
				AND um.CODE_MODULE = 7"
		;
		$query=mysqli_query($ma_connexion,$SQL);
			
		if(mysqli_num_rows($query) == 0)
		{
			
			header('Location: users');
		}
	}
}
else 
{
	header('Location: login');
}
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>eInvoiceTrack</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- c3 Charts -->
    <link href="css/plugins/c3/c3.min.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
	 <link href="css/plugins/dataTables/datatables.min.css" rel="stylesheet">
	 
	 
	
	<link href="css/plugins/select2/select2.min.css" rel="stylesheet">
	
	
    <link href="css/plugins/datapicker/datepicker3.css" rel="stylesheet">
	
	<!-- Date range picker -->
     <link href="css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
	 
 <!-- Bootstrap Tour -->
    <link href="css/plugins/bootstrapTour/bootstrap-tour.min.css" rel="stylesheet">



    <style>
	

td.details-control {
    background: url('https://www.datatables.net/examples/resources/details_open.png') no-repeat center center;
    cursor: pointer;
}
tr.shown td.details-control {
    background: url('https://www.datatables.net/examples/resources/details_close.png') no-repeat center center;
}


    </style>


</head>

<body>
    

    <div id="wrapper">

    
		<?php
			include 'includes/nav.php';
		?>	

        <div id="page-wrapper" class="gray-bg dashbard-1">
		
				<?php
					include 'includes/header.php';
				?>
        <div class="row wrapper border-bottom white-bg page-heading">
			<div class="col-sm-4">
				<h2>Rapport</h2> 
				<ol class="breadcrumb">
					<li class="breadcrumb-item">
						<a href="index"> Accueil </a>
					</li>
					<li class="breadcrumb-item active">
						<strong></strong>
					</li>
				</ol>
			</div>
			<div class="col-sm-8">
				<div class="title-action">
				 <a href="#" class="btn btn-primary startTour"><i class="fa fa-play"></i> Démo</a>
				</div>
			</div>
		</div>
        <div class="wrapper wrapper-content animated fadeInRight">
		 <div class="row">
                <div class="col-lg-12">
                    <div class="ibox " id="ibox_result">
                           <div class="ibox-title  ">
                           

							 <label class="h6 font-weight-bold">Volume des factures rejetées  par client / [Période en cours ]</label>
							<div id="rejets_reccurent_date" class="form-control" style="max-width:40%; cursor:pointer">
								<i class="fa fa-calendar"></i>
								<span></span> <b class="caret"></b>
							</div>
							
							
							<button class="btn btn-success btn-rounded pull-right " type="button" onclick="printDiv('rejets_reccurent')"  ><i class="fa fa-print"></i>&nbsp;&nbsp;<span class="bold">Imprimer</span></button>

                        </div>
                        <div class="ibox-content">
							<div id="rejets_reccurent"></div>
						
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<hr>
								<br>
								<div class="table-responsive">
								<table class="table table-borderless " style="width : 100%: margin-top: 350px" id="liste_table">
									<thead>
									  <tr style="    background-color: skyblue;">
										<th ></th>
										<th >Code client</th>
										<th >Nom Client</th>
										<th >Nbr facture</th>
									  </tr>
									</thead>
									
									<tfoot>
									  <tr>
										<th ></th>
										<th >Code client</th>
										<th >Nom Client</th>
										<th >Nbr fature</th>
									  </tr>
									</tfoot>
								</table>
								</div>
								
								
								
                          
                        </div>
                    </div>
                </div>
                
				
		</div>
		
		
		
         
        </div>
       <?php
				include 'includes/footer.php';
			?>	

        </div>
        </div>



    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>

    <!-- d3 and c3 charts -->
    <script src="js/plugins/d3/d3.min.js"></script>
    <script src="js/plugins/c3/c3.min.js"></script>
	
	
	 <link href="https://nightly.datatables.net/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />
    <script src="https://nightly.datatables.net/js/jquery.dataTables.js"></script>


 <!-- Data picker -->
   <script src="js/plugins/datapicker/bootstrap-datepicker.js"></script>

	  
	 <!-- Date range use moment.js same as full calendar plugin -->
    <script src="js/plugins/fullcalendar/moment.min.js"></script>
	

	 <!-- Date range picker -->
    <script src="js/plugins/daterangepicker/daterangepicker.js"></script>
	
	
	
    <!-- Select2 -->
    <script src="js/plugins/select2/select2.full.min.js"></script>
	<script src="js/i18n/fr.js"></script>
	
	<script src="canvasjs.min.js"></script>
	
	
	
	  <!-- Bootstrap Tour -->
    <script src="js/plugins/bootstrapTour/bootstrap-tour.min.js"></script>

	
    <script>
	
	function printDiv(divName) {
		 var printContents = document.getElementById(divName).innerHTML;
		 var originalContents = document.body.innerHTML;

		 document.body.innerHTML = printContents;

		 window.print();

		 document.body.innerHTML = originalContents;
	}
	
	function getRandomColor() {
	  var letters = '0123456789ABCDEF';
	  var color = '#';
	  for (var i = 0; i < 6; i++) {
		color += letters[Math.floor(Math.random() * 16)];
	  }
	  return color;
	}
	
	function toggleDataSeries(e) {
		if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
			e.dataSeries.visible = false;
		}
		else {
			e.dataSeries.visible = true;
		}
		chart.render();
	}


        $(document).ready(function () {
			
			var tour = new Tour({
            steps: [
                {
                    element: "#ibox_result",
                    title: "Ajouter client",
                    content: "Vous pouvez ajouter un client manuellement <br/> Les champs marqués par ( <span style='color:red'>*</span> ) sont obligatoires",
                    placement: "top",
                    backdrop: true,
                    backdropContainer: '#wrapper',
                    onShown: function (tour){
                        $('body').addClass('tour-open')
                    },
                    onHidden: function (tour){
                        $('body').removeClass('tour-close')
                    }
                }
                
            ],
			smartPlacement: true,
			  keyboard: true,
			  storage: false,
			  debug: true,
			  labels: {
				end: 'Terminer',
				next: 'Suivant &raquo;',
				prev: '&laquo; Prev'
			  },
			  });

        // Initialize the tour
        tour.init();

         $('.startTour').click(function(){
            tour.restart();

            // Start the tour
            // tour.start();
        })
			
			// var table =   $('#liste_table').DataTable({	
				// responsive: true,
				// "language":{
						// "sProcessing":     "Traitement en cours...",
						// "sSearch":         "Rechercher un facture &nbsp;:",
						// "sLengthMenu":     "Afficher _MENU_  facture",
						// "sInfo":           "Affichage des facture _START_ &agrave; _END_ sur _TOTAL_ factures",
						// "sInfoEmpty":      "Affichage du facture 0 &agrave; 0 sur 0 factures",
						// "sInfoFiltered":   "(filtr&eacute; de _MAX_ factures au total)",
						// "sInfoPostFix":    "",
						// "sLoadingRecords": "Chargement en cours...",
						// "sZeroRecords":    "Aucuns virements &agrave; afficher",
						// "sEmptyTable":     "Aucunes donn&eacute;e disponible dans le tableau",
						// "oPaginate": {
							// "sFirst":      "Premier",
							// "sPrevious":   "Pr&eacute;c&eacute;dent",
							// "sNext":       "Suivant",
							// "sLast":       "Dernier"
						// },
						// "oAria": {
							// "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
							// "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
						// }
				// },
				// dom: '<"html5buttons"B>lTfgitp',
				// buttons: [
					// { 
						// extend: 'copy',
						// title: 'eInvoiceTrack',
						// exportOptions: {
							// columns:  ':visible:not(:last-child)'
						// }
					// },
					// {
						// extend: 'csv',
						// title: 'eInvoiceTrack',
						// exportOptions: {
							// columns:  ':visible:not(:last-child)'
						// }
					// },
					// {
						// extend: 'excel',
						// title: 'eInvoiceTrack',
						// exportOptions: {
							// columns:  ':visible:not(:last-child)'
						// }
					// },

					// {extend: 'print',
					 // customize: function (win){
							// $(win.document.body).addClass('white-bg');
							// $(win.document.body).css('font-size', '10px');

							// $(win.document.body).find('table')
									// .addClass('compact')
									// .css('font-size', 'inherit');
						// }
					// }
				// ],
				// "columns": [
					// { "data": "NUM_FACTURE",className: "NUM_FACTURE"},
					// { "data": "STATUS",className: "STATUS"},
					// { "data": "NOM_CLIENT",className: "NOM_CLIENT"},
					// { "data": "CODE_CLIENT",className: "CODE_CLIENT"},
					// { "data": "ADRESSE1",className: "ADRESSE1", "visible": false },
					// { "data": "DATE_COMPTABILISATION",className: "DATE_COMPTABILISATION"},
					// { "data": "MOIS_DISTRIBUTION",className: "MOIS_DISTRIBUTION"},
					// { "data": "MOTIF",className: "MOTIF"},
					// { "data": "BC",className: "BC", "visible": false},
					// { "data": "BL",className: "BL", "visible": false}
				 // ]
			// });	
					
			rejets_reccurent();
			function rejets_reccurent()
			{
				$.ajax(
				{
					type : 'POST',
					url : 'get/get_chart_data.php',
					data: "key=rejets_reccurent",
					success : function(data)
					{
						console.log(data);
						info_html = JSON.parse(data);
						// var chart = c3.generate({
							// bindto: '#rejets_reccurent',
							 // data: {
								// json: info_html,
								// keys: {
								  // x: 'numero',
								  // value: ['total']
								// },
								
								// type: 'bar',
								// groups: [
									// ['total']
								  // ]
								
								
							  // },
							  // colors: {
									// total: "red"
									
								// },
							  // axis: {
									// x:{
										// type: 'category',
										// tick: {
										  // culling: false,
										  // rotate: 0
										// },
										// label: {
											// text: 'mois',
											// position: 'inner-bottom'
										// }
									// },
									// y:{
										// label: {
											// text: 'volume',
											// position: 'outer-middle'
										// }
									// },
							// },
							
							
						  // });
						  var dataPoints = [];
							for (var i = 0; i < info_html.length; i++) {
								dataPoints.push({
									label: info_html[i].numero,
									y: Number(info_html[i].total)
								});
							}
						  
						var chart = new CanvasJS.Chart("rejets_reccurent", {
							animationEnabled: true,
							theme: "light2",
							title: {
								text: "Les 1O rejets récurrents"
							},
							axisY: {
								title: "Volume",
								titleFontSize: 24
							},
							axisX: {
								title: "Code client",
								titleFontSize: 24
							},
							data: [
								{
									type: "column",
									yValueFormatString: "#,### rejets",
									dataPoints: dataPoints,
									click: function(e){
										alert(  e.dataSeries.type+ ", dataPoint { x:" + e.dataPoint.x + ", y: "+ e.dataPoint.y + " }" );
									   },
								}
							]
						});
						chart.render();
					}
				});
				
				
				
				// $.ajax(
				// {
					// type : 'POST',
					// url : 'get/get_chart_data.php',
					// data: "key=rejets_reccurent_array",
					// success : function(data)
					// {
						
						// datatable = JSON.parse(data);
						// table.clear();
						// table.rows.add(datatable).draw();
						// console.log(data); 
					// },
					// complete : function(data)
					// {
						// $('#ibox_result').children('.ibox-content').toggleClass('sk-loading');
					// },
					// beforeSend : function(data)
					// {
						// $('#ibox_result').children('.ibox-content').toggleClass('sk-loading');
					// },
					// error: function (jqXHR, exception) {
						// var msg = '';
						// if (jqXHR.status === 0) {
							// msg = 'Not connect.\n Verify Network.';
						// } else if (jqXHR.status == 404) {
							// msg = 'Requested page not found. [404]';
						// } else if (jqXHR.status == 500) {
							// msg = 'Internal Server Error [500].';
						// } else if (exception === 'parsererror') {
							// msg = 'Requested JSON parse failed.';
						// } else if (exception === 'timeout') {
							// msg = 'Time out error.';
						// } else if (exception === 'abort') {
							// msg = 'Ajax request aborted.';
						// } else {
							// msg = 'Uncaught Error.\n' + jqXHR.responseText;
						// }
					// }
				// });
			}
			
			
			
		$('#rejets_reccurent_date span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

		$('#rejets_reccurent_date').daterangepicker({
			language:'fr',
			format: 'YYYY-MM-DD',
			startDate: moment().subtract(29, 'days'),
			dateLimit: { days: 360 },
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			ranges: {
				'Les 30 derniers jours': [moment().subtract(29, 'days'), moment()],
				'Ce mois-ci': [moment().startOf('month'), moment().endOf('month')],
				'Le mois dernier': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			opens: 'right',
			drops: 'down',
			buttonClasses: ['btn', 'btn-sm'],
			applyClass: 'btn-primary',
			cancelClass: 'btn-default',
			separator: ' to ',
			locale: {
				applyLabel: 'Enregistrer',
				cancelLabel: 'Annuler',
				fromLabel: 'À partir',
				toLabel: 'jusqu\'à',
				firstDay: 1,
				customRangeLabel: 'Personnalisé',
			}
		}, function(start, end, label) {
			start_date_1 = start.format('YYYY-MM-DD');
			end_date_1 = end.format('YYYY-MM-DD');
			$('#rejets_reccurent_date span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
		});
		
				

			
			
			  
        });
		
		
		
		

    </script>
    <script>
	function get_date()
	{
		
		
	}
	// var data = null  ; 
	
	
	
 
$(document).ready(function() {
    
	var table = null ; 
    $.ajax(
		{
			async: false,
			type : 'POST',
			url : 'get/get_chart_data.php',
			data: "key=rejets_reccurent_array",
			success : function(data)
			{
				// console.log(data);
				data = JSON.parse(data);
				// table.clear();
				// table.rows.add(datatable).draw();
				// console.log(data); 
				
				
				table = $('#liste_table').DataTable( {
					data: data,
							"language":{
									"sProcessing":     "Traitement en cours...",
									"sSearch":         "Rechercher un client &nbsp;:",
									"sLengthMenu":     "Afficher _MENU_  client",
									"sInfo":           "Affichage des client _START_ &agrave; _END_ sur _TOTAL_ clients",
									"sInfoEmpty":      "Affichage du client 0 &agrave; 0 sur 0 clients",
									"sInfoFiltered":   "(filtr&eacute; de _MAX_ clients au total)",
									"sInfoPostFix":    "",
									"sLoadingRecords": "Chargement en cours...",
									"sZeroRecords":    "Aucuns virements &agrave; afficher",
									"sEmptyTable":     "Aucunes donn&eacute;e disponible dans le tableau",
									"oPaginate": {
										"sFirst":      "Premier",
										"sPrevious":   "Pr&eacute;c&eacute;dent",
										"sNext":       "Suivant",
										"sLast":       "Dernier"
									},
									"oAria": {
										"sSortAscending":  ": activer pour trier la colonne par ordre croissant",
										"sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
									}
							},
							dom: '<"html5buttons"B>lTfgitp',
							buttons: [
								{ 
									extend: 'copy',
									title: 'eInvoiceTrack',
									exportOptions: {
										columns:  ':visible:not(:last-child)'
									}
								},
								{
									extend: 'csv',
									title: 'eInvoiceTrack',
									exportOptions: {
										columns:  ':visible:not(:last-child)'
									}
								},
								{
									extend: 'excel',
									title: 'eInvoiceTrack',
									exportOptions: {
										columns:  ':visible:not(:last-child)'
									}
								},

								{extend: 'print',
								 customize: function (win){
										$(win.document.body).addClass('white-bg');
										$(win.document.body).css('font-size', '10px');

										$(win.document.body).find('table')
												.addClass('compact')
												.css('font-size', 'inherit');
									}
								}
							],
					columns: [
						{
							"className":      'details-control',
							"orderable":      false,
							"data":           null,
							"defaultContent": ''
						},
						{ "data": "NUMERO_CLIENT" },
						{ "data": "NOM_CLIENT" },
						{ "data": "total" }
					],
					"order": [[1, 'asc']],
					  createdRow: function ( row, data, index ) {
						if (data.extn === '') {
						  var td = $(row).find("td:first");
						  td.removeClass( 'details-control' );
						}
					   },
					  rowCallback: function ( row, data, index ) {
						//console.log('rowCallback');
					   }
				} );
			}
		});
		
		
		
		
		
		
		$('#liste_table tbody').on('click', 'td.details-control', function () {
			// alert('ok');
			var tr = $(this).closest('tr');
			tr.css('background-color','#f3f3f4');
			var row = table.row( tr );
	 
			if ( row.child.isShown() ) {
				// This row is already open - close it
				row.child.hide();
				tr.removeClass('shown');
				tr.css('background-color','#ffffff');
				
			}
			else {
				// alert('ok');
				// Open this row
				var NUMERO_CLIENT = row.data().NUMERO_CLIENT;
				$.ajax(
				{
					async: false,
					type : 'POST',
					url : 'get/get_chart_data.php',
					data: "key=rejets_reccurent_array_detail&NUMERO_CLIENT="+NUMERO_CLIENT,
					success : function(data_child)
					{
						// console.log(data_child);
						// data_child = JSON.parse(data_child);
						// data_child = data_child[0];
						
						// row.child( 
						 // '<table id="child_details" class=" child_details table " cellpadding="5" cellspacing="0" border="0"  style="width : 100% ; background-color:gray">'+
							// '<thead><tr ><th>N° facture</th><th>Date rejet</th><th>Motif rejet</th><th>Description rejet</th><th>PDF</th></tr></thead><tbody>' +
								// '<tr>'+
								// '<td>'+data_child.NUM_FACTURE+'</td>'+
								// '<td>'+data_child.DATE+'</td>'+
								// '<td>'+data_child.NOM_MOTIF+'</td>'+
								// '<td>'+data_child.description+'</td>'+
								// '<td>'+data_child.DETAILL+'</td>'+
							// '</tr>'+
						// '</tbody></table>').show();						
						row.child( data_child).show();
						
					}
				});
				
			  
			  $('#child_details').DataTable({
					"language":{
							"sProcessing":     "Traitement en cours...",
							"sSearch":         "Rechercher un facture &nbsp;:",
							"sLengthMenu":     "Afficher _MENU_  facture",
							"sInfo":           "Affichage des facture _START_ &agrave; _END_ sur _TOTAL_ factures",
							"sInfoEmpty":      "Affichage du facture 0 &agrave; 0 sur 0 factures",
							"sInfoFiltered":   "(filtr&eacute; de _MAX_ factures au total)",
							"sInfoPostFix":    "",
							"sLoadingRecords": "Chargement en cours...",
							"sZeroRecords":    "Aucuns virements &agrave; afficher",
							"sEmptyTable":     "Aucunes donn&eacute;e disponible dans le tableau",
							"oPaginate": {
								"sFirst":      "Premier",
								"sPrevious":   "Pr&eacute;c&eacute;dent",
								"sNext":       "Suivant",
								"sLast":       "Dernier"
							},
							"oAria": {
								"sSortAscending":  ": activer pour trier la colonne par ordre croissant",
								"sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
							}
					},
					dom: '<"html5buttons"B>lTfgitp',
					buttons: [
						{ 
							extend: 'copy',
							title: 'eInvoiceTrack',
							exportOptions: {
								columns:  ':visible:not(:last-child)'
							}
						},
						{
							extend: 'csv',
							title: 'eInvoiceTrack',
							exportOptions: {
								columns:  ':visible:not(:last-child)'
							}
						},
						{
							extend: 'excel',
							title: 'eInvoiceTrack',
							exportOptions: {
								columns:  ':visible:not(:last-child)'
							}
						},

						{extend: 'print',
						 customize: function (win){
								$(win.document.body).addClass('white-bg');
								$(win.document.body).css('font-size', '10px');

								$(win.document.body).find('table')
										.addClass('compact')
										.css('font-size', 'inherit');
							}
						}
					],
				destroy: true,
				scrollY: '100px'
			  });
				tr.addClass('shown');
			}
		} );
   
} );
    </script>

</body>

</html>
