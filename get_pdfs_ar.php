<?php 
ob_start();
session_start();
include 'connexion.php';
ini_set('max_execution_time', 10000);


$start 	= date_create(); // Current time and date

require 'PhpSpreadsheet/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;



	require_once dirname(__FILE__).'/html2pdf/vendor/autoload.php';
									use Spipu\Html2Pdf\Html2Pdf;
									use Spipu\Html2Pdf\Exception\Html2PdfException;
									use Spipu\Html2Pdf\Exception\ExceptionFormatter;



require 'dompdf/vendor/autoload.php';
use Dompdf\Dompdf;

// $res_html = '' ; 


if(isset($_SESSION['user_einvoicetrack']))
{
	
	$current_user = decode($_SESSION['user_einvoicetrack']) ; 					
	$SQL_SESSION="SELECT  `NOM_USER`, `PRENOM_USER`, `EMAIL_USER`,`CIVILITE_USER`,
			`CODE_ENTREPRISE`, `ROLE_USER`
		  FROM `user` 
		  WHERE CODE_USER = $current_user
		  AND actif= 1";
	$query_SESSION=mysqli_query($ma_connexion,$SQL_SESSION);
	if(mysqli_num_rows($query_SESSION) == 1)
	{
		while($row_SESSION=mysqli_fetch_assoc($query_SESSION))
		{	
				$NOM_USER_SESSION = $row_SESSION['CIVILITE_USER'].' ' .$row_SESSION['NOM_USER'];
				$EMAIL_USER_SESSION = $row_SESSION['EMAIL_USER'];
				
				
				$folder_name = 'upload/';

				$files = scandir('upload');
				if(false !== $files)
				{
					foreach($files as $file)
					{
						if('.' !=  $file && '..' != $file)
						{
							unlink($folder_name.DIRECTORY_SEPARATOR.$file);
						}
				   
					}
				}


				if(!empty($_FILES))
				{
					 $temp_file = $_FILES['file']['tmp_name'];
					 $location = $folder_name . $_FILES['file']['name'];
					 move_uploaded_file($temp_file, $location);
				}
				

				$header_nom = array("Num facture");
				$header_num = array("Code client");
				$header_client = array("Nom client");
				$header_date = array("Date  comptabilisation");
				$header_adresse1 = array("Adresse");
				$header_ville = array("Ville");
				


				$files = scandir('upload');
				if(false !== $files)
				{
					
					foreach($files as $file)
					{
						if('.' !=  $file && '..' != $file)
						{
							
							$content = '' ; 
							$ok = 0 ; 
							$nook = 0 ; 
							
							$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($folder_name.'/'.$file);
							$worksheet = $spreadsheet->getActiveSheet();
							
							if (   in_array($worksheet->getCellByColumnAndRow(1, 1)->getCalculatedValue(), $header_nom) 
								&& in_array($worksheet->getCellByColumnAndRow(2, 1)->getCalculatedValue(), $header_num) 
								&& in_array($worksheet->getCellByColumnAndRow(3, 1)->getCalculatedValue(), $header_client) 
								&& in_array($worksheet->getCellByColumnAndRow(4, 1)->getCalculatedValue(), $header_date) 
								&& in_array($worksheet->getCellByColumnAndRow(5, 1)->getCalculatedValue(), $header_adresse1) 
								&& in_array($worksheet->getCellByColumnAndRow(6, 1)->getCalculatedValue(), $header_ville) 
								
								)
							{
								

								$highestRow = $worksheet->getHighestRow(); // e.g. 10
								for ($row = 2; $row <= 2; ++$row) 
								{
									
									$num_facture = mysqli_real_escape_string($ma_connexion,$worksheet->getCellByColumnAndRow(1, $row)->getFormattedValue());		
									$code_client = mysqli_real_escape_string($ma_connexion,$worksheet->getCellByColumnAndRow(2, $row)->getFormattedValue());		
									$nom_client = mysqli_real_escape_string($ma_connexion,$worksheet->getCellByColumnAndRow(3, $row)->getFormattedValue());		
									$date_comptabilisationt = mysqli_real_escape_string($ma_connexion,$worksheet->getCellByColumnAndRow(4, $row)->getFormattedValue());		
									$adresse = mysqli_real_escape_string($ma_connexion,$worksheet->getCellByColumnAndRow(5, $row)->getFormattedValue());		
									$ville = mysqli_real_escape_string($ma_connexion,$worksheet->getCellByColumnAndRow(6, $row)->getFormattedValue());		
										

$res_html =  $_POST['html'];
// echo $res_html.'  OK' ; 

 $res_html= str_replace("[Code client]",$code_client,$res_html);
 $res_html= str_replace("[Nom client]",$nom_client,$res_html);
 $res_html= str_replace("[Adresse]",$adresse,$res_html);
 $res_html= str_replace("[Ville]",$ville,$res_html);
 $res_html= str_replace("[Num facture]",$num_facture,$res_html);
 $res_html= str_replace("[Date  comptabilisation]",$date_comptabilisationt,$res_html);


// echo $res_html ; 
// $res_html = '


// <body style="padding: .2in;">
	// <br>	
	// <br>
	// <br>
	// <br>
	// <div style="width: 25%; text-align: left;">	
		// <img style="width: 100%;" src="logo.png" alt="Logo">
	// </div>
	// <div style="width: 100%; text-align: right; margin-top:-90px">	
		// <b>  Témara </b> le  <b>'.date("d/m/Y").'</b> <br> <br>
		// <b>'.$code_client.'<br><br>
		// <b>'.$nom_client.'  </b><br><br>
		// <b>'.$adresse.'  </b><br><br>
		// <b>'.$ville.'</b><br><br>
	// </div>

	
	// <br>	
	// <br>	
	// <b><u>Objet</u>: Accusé réception des factures</b><br>
	// <br>
	// <br>
	// Messieurs,<br>
	 // <br>
	// Par la présente, vous accusez de réception des factures :<br>
	 // <br>
	// N° : <b>'.$num_facture.' </b> <br>
	 // <br>
	// Date comptabilisation : <b> '.$date_comptabilisationt.' </b> <br>
	 // <br>
	// Date réception : <b> __/__/____ </b> <br>
	 // <br>
	// Motif de rejet :   <br>
	// <br>
	
	// <table cellspacing="0" style="width: 100%; text-align: center;margin-left:10px" font-size: 14px" class="motifs">
		// <tr style=" height : 10px;">
			// <td style="width: 50%; text-align: center; ">
				// <table cellspacing="0" style="width: 100%; text-align: right; font-size: 11pt;" border-collapse="collapse">
					// <tr >
						// <td style="width:20%;text-align: center;">
							// <div style="width:10px;height:10px;border:2px solid #000;"></div> 
						// </td>
						// <td style="width:80%;text-align:left; ">
							// <b>Manque bon de commande   </b>  
						// </td>
					// </tr>
				// </table>
			// </td>
			// <td style="width: 50%; text-align: center; ">
				// <table cellspacing="0" style="width: 100%; text-align: right; font-size: 11pt;" border-collapse="collapse">
					// <tr >
						// <td style="width:20%;text-align: center;">
							// <div style="width:10px;height:10px;border:2px solid #000;"></div> 
						// </td>
						// <td style="width:80%;text-align:left; ">
							// <b>Facture erronée( en double. date …)    </b>  
						// </td>
					// </tr>
				// </table>
			// </td>
		// </tr>
		// <tr><td colspan="2"></td></tr>
				// <tr><td colspan="2"></td></tr>
				// <tr><td colspan="2"></td></tr>
				// <tr><td colspan="2"></td></tr>
				// <tr><td colspan="2"></td></tr>
		
		// <tr>
			
			// <td style="width: 50%;">
				// <table cellspacing="0" style="width: 100%; text-align: right; font-size: 11pt;" border-collapse="collapse">
					// <tr >
						// <td style="width:20%;text-align: center;">
							// <div style="width:10px;height:10px;border:2px solid #000;"></div> 
						// </td>
						// <td style="width:80%;text-align:left; ">
							// <b>Liasse incomplète     </b>  
						// </td>
					// </tr>
				// </table>
			// </td>
			// <td style="width: 50%;">
				// <table cellspacing="0" style="width: 100%; text-align: right; font-size: 11pt;" cellspacing="0">
					// <tr>
						// <td style="width:20%;text-align: center;">
							// <div style="width:10px;height:10px;border:2px solid #000;"></div> 
						// </td>
						// <td style="width:80%;text-align:left; ">
							// <b>Litige    </b>  
						// </td>
					// </tr>
				// </table>
			// </td>
		// </tr>
		// <tr><td colspan="2"></td></tr>
				// <tr><td colspan="2"></td></tr>
				// <tr><td colspan="2"></td></tr>
				// <tr><td colspan="2"></td></tr>
				// <tr><td colspan="2"></td></tr>
		// <tr>	
			// <td style="width: 50%;">
				// <table cellspacing="0" style="width: 100%; text-align: right; font-size: 11pt;" border-collapse="collapse">
					// <tr >
						// <td style="width:20%;text-align: center;">
							// <div style="width:10px;height:10px;border:2px solid #000;"></div> 
						// </td>
						// <td style="width:80%;text-align:left; ">
							// <b>Manque bon de livraison     </b>  
						// </td>
					// </tr>
				// </table>
			// </td>
			// <td style="width: 50%;">
				// <table cellspacing="0" style="width: 100%; text-align: right; font-size: 11pt;" cellspacing="0">
					// <tr>
						// <td style="width:20%;text-align: center;">
							// <div style="width:10px;height:10px;border:2px solid #000;"></div> 
						// </td>
						// <td style="width:80%;text-align:left; ">
							// <b>Refus    </b>  
						// </td>
					// </tr>
				// </table>
			// </td>
		// </tr>
		// <tr><td colspan="2"></td></tr>
				// <tr><td colspan="2"></td></tr>
				// <tr><td colspan="2"></td></tr>
				// <tr><td colspan="2"></td></tr>
				// <tr><td colspan="2"></td></tr>
		
		// <tr>	
			// <td style="width: 50%;">
				// <table cellspacing="0" style="width: 100%; text-align: right; font-size: 11pt;" border-collapse="collapse">
					// <tr >
						// <td style="width:20%;text-align: center;">
							// <div style="width:10px;height:10px;border:2px solid #000;"></div> 
						// </td>
						// <td style="width:80%;text-align:left; ">
							// <b>Montant Incorrect (HT, TVA et TTC)     </b>  
						// </td>
					// </tr>
				// </table>
			// </td>
			// <td style="width: 50%;">
				// <table cellspacing="0" style="width: 100%; text-align: right; font-size: 11pt;" cellspacing="0">
					// <tr>
						// <td style="width:20%;text-align: center;">
							// <div style="width:10px;height:10px;border:2px solid #000;"></div> 
						// </td>
						// <td style="width:80%;text-align:left; ">
							// <b>Mentions obligatoires (ICE,RC, CNSS, Patente, IF. Raison sociale)   </b>  
						// </td>
					// </tr>
				// </table>
			// </td>
		// </tr>
		
		
	// </table>
	
	
	// <br>
	// <br>
	// <br>
	// <br>
	// <br>
	// <p >
		// Nous vous en remercions et vous prions d’agréer, messieurs, l’assurance de nos saluations distinguées.	
	// </p>
		// <br>
	// Cachet et signature 
// </body>
// ';


								$options = new Options();
								$options->set('isRemoteEnabled',true);     	
								$dompdf = new Dompdf($options);
								
								$dompdf->loadHtml('<body style="padding: .2in;">'.$res_html.'</body>');
								$dompdf->setPaper('A4', 'portrait');
								$dompdf->render();
								$output = $dompdf->output();
								file_put_contents('ar_gene/dompdf'.$num_facture.'_'.strtotime(date('Y-m-d H:i:s')).'.pdf', $output);

								echo  '
								<div class="alert alert-success" role="alert">
									  <strong>Line'.($row-1).' :</strong> La facture '.$num_facture.' est bien génerer..
									</div>
								';
								
								}
							}
							else 
							{
								echo '
								<div class="alert alert-danger" role="alert">
									le  fichier   <strong>  '.$file.' </strong> est incompatible 
									</div>
								';
							}
							
						}
					}
					
					
				}
		}
	}
	else{
		echo "session request err" ;
		
	}
}
else{
	echo "session err" ;
	
}


$end 	= date_create(); 
$diff  	= date_diff( $start, $end );


// $content = ob_get_clean();
// try
// {
	// ob_end_clean();
	// $output_file = 'ar_gene/ar'.strtotime(date('Y-m-d H:i:s')).'.pdf';
	// $html2pdf = new HTML2PDF('P', 'A4', 'fr', true, 'UTF-8',array(18, 10, 18, 10));
	// $html2pdf->setDefaultFont("Arial");
	// $html2pdf->pdf->SetProtection(array('print','copy'));
	// $html2pdf->writeHTML($content);
	// $html2pdf->Output(__DIR__ . '/'.$output_file, 'F');
	
	// echo $output_file ; 

// }
// catch(HTML2PDF_exception $e) {
	// echo $e;
	// exit;
// }

    
?>