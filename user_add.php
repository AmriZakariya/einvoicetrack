<?php
ob_start();
session_start();
include 'connexion.php';


if(isset($_SESSION['user_einvoicetrack']) && $_SESSION['role'] ==  'superadmin')
{

    $current_user = decode($_SESSION['user_einvoicetrack']) ;
    $SQL="SELECT  `NOM_USER`, `PRENOM_USER`, `EMAIL_USER`,`CIVILITE_USER`,
			`CODE_ENTREPRISE`, `ROLE_USER`
		  FROM `user` 
		  WHERE CODE_USER = $current_user
		  AND actif= 1";
    $query=mysqli_query($ma_connexion,$SQL);
    if(mysqli_num_rows($query) == 1)
    {
        while($row=mysqli_fetch_assoc($query))
        {
            $NOM_USER = $row['CIVILITE_USER'].' ' .$row['NOM_USER'];
        }
    }
    else
    {
        header('Location: login');
    }
}
else
{
    header('Location: login');
}




use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require 'vendor/autoload.php';



$nomErr = $emailErr = $civiliteErr =$roleErr = "";
$NOM_USER = $EMAIL_USER = $CIVILITE_USER = $ROLE_USER = "";
$BigErr = "" ;
$flag = 1;


function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>eInvoiceTrack</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">


    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <!-- iCheck -->
    <script src="js/plugins/iCheck/icheck.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <link href="css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">


    <script>
        $(document).ready(function(){
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });

    </script>


    <style>
        .required{
            color : red ;
            float: left;
        }
    </style>

</head>

<body>




<?php


if( isset($_POST['submit']) )
{
    if (isset($_POST['nom']) && isset($_POST['email']) && isset($_POST['civilite']) && isset($_POST['role']))
    {
        $NOM_USER=mysqli_real_escape_string($ma_connexion,$_POST['nom']);
        if (empty($NOM_USER)) {
            $nomErr = "Le nom est obligatoire";
            $flag = 0 ;
        } else {
            $NOM_USER = test_input($NOM_USER);
            // check if name only contains letters and whitespace
            if (!preg_match("/^[a-zA-Z ]*$/",$NOM_USER)) {
                $nomErr = "le nom n'est pas valide";
                $flag = 0 ;
            }
        }


        //$prenom=mysqli_real_escape_string($ma_connexion,$_POST['prenom']);
        $PRENOM_USER='';


        $EMAIL_USER=mysqli_real_escape_string($ma_connexion,$_POST['email']);
        if (empty($EMAIL_USER)) {
            $emailErr = "l'email est obligatoire";
            $flag = 0 ;
        } else {
            $EMAIL_USER = test_input($EMAIL_USER);
            if (!filter_var($EMAIL_USER, FILTER_VALIDATE_EMAIL)) {
                $emailErr = "Email invalide";
                $flag = 0 ;
            }
        }

        // $MDP_USER = str_replace('-', '', date("s-m-y-d")).str_replace('-', '', date("s-m-y-d")) ;
        $MDP_USER = substr(md5(microtime()),rand(0,26),9);
        $CIVILITE_USER=mysqli_real_escape_string($ma_connexion,$_POST['civilite']);
        $CIVILITE_USER = test_input($CIVILITE_USER);
        if ($CIVILITE_USER != 'Mr' && $CIVILITE_USER != 'Mme' ) {
            $civiliteErr = "Merci de saisir la civilite rr";
            $flag = 0 ;
        }

        $CODE_ENTREPRISE=1;

        $ROLE_USER=1;
        $ROLE_USER=mysqli_real_escape_string($ma_connexion,$_POST['role']);
        $ROLE_USER = test_input($ROLE_USER);
        if ($ROLE_USER != '1' && $ROLE_USER != '2' ) {
            $roleErr = "Merci de choisir le role";
            $flag = 0 ;
        }




        if( $flag == 1 )
        {
            $sql= "INSERT INTO `user`(`NOM_USER`, `PRENOM_USER`, `EMAIL_USER`, `MDP_USER`, `CIVILITE_USER`, `CODE_ENTREPRISE`, `ROLE_USER`) VALUES 
									('$NOM_USER','$PRENOM_USER','$EMAIL_USER','$MDP_USER','$CIVILITE_USER','$CODE_ENTREPRISE','$ROLE_USER') ; ";
            if (mysqli_query($ma_connexion, $sql)) {


                $user_current = mysqli_insert_id($ma_connexion);


                if ( isset($_POST['modules']))
                {
                    foreach($_POST['modules'] as $key3 => $val3)
                    {
                        // $val3 =
                        $sql5 = "INSERT INTO `user_module`(`CODE_USER`, `CODE_MODULE`) VALUES 
									('$user_current','$val3');";
                        if (mysqli_query($ma_connexion, $sql5))
                        {

                        }
                        else {
                            echo '<br/> sous sectt '. mysqli_error($ma_connexion);
                        }


                    }
                }
                $user_current = encode2($user_current);






                $name = $NOM_USER;
                $mdp = $MDP_USER;
                $to = $EMAIL_USER;
                $user = $user_current;



                //Create an instance; passing `true` enables exceptions
                $mail = new PHPMailer(true);



                try {
                    //Server settings
                    //$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
                    $mail->isSMTP();                                            //Send using SMTP
                    $mail->Host       = 'mail.einvoicetrack.net';                     //Set the SMTP server to send through
                    $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
                    $mail->Username   = 'support@einvoicetrack.net';                     //SMTP username
                    $mail->Password   = 'D~1tW5RO!+uN';                               //SMTP password
                    $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
                    $mail->Port       = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

                    //    $mail->SMTPAuth   = true;
                    //    $mail->SMTPSecure = "tls";

                    //    $mail->SMTPOptions = array(
                    //        'ssl' => array(
                    //            'verify_peer' => false,
                    //            'verify_peer_name' => false,
                    //            'allow_self_signed' => true
                    //        )
                    //    );
                    //Recipients
                    $mail->setFrom('support@einvoicetrack.net', 'eInvoiceTrack');

                    $mail->addAddress($to);



                    $mail->addAttachment('einvoicetrack.png');



                    $mail->isHTML(true);                                  // Set email format to HTML
                    $mail->Subject = 'Inscription eInvoiceTrack';
                    $mail->Body = '
    		 <html>
    		   <head>
    			  <style>
    				 .banner-color {
    				 background-color: #eb681f;
    				 }
    				 .title-color {
    				 color: #1eb092;
    				 }
    				 .button-color {
    				     background-color: #1eb092;
    				     color: #ececec;
    				 }
    				 @media screen and (min-width: 500px) {
    				 .banner-color {
    				 background-color: #1eb092;
    				 }
    				 .title-color {
    				 color: #eb681f;
    				 }
    			  </style>
    		   </head>
    		   <body>
    			  <div style="background-color:#ececec;padding:0;margin:0 auto;font-weight:200;width:100%!important">
    				 <table align="center" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
    					<tbody>
    					   <tr>
    						  <td align="center">
    							 <center style="width:100%">
    								<table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" style="margin:0 auto;max-width:512px;font-weight:200;width:inherit;font-family:Helvetica,Arial,sans-serif" width="512">
    								   <tbody>
    									  <tr>
    										 <td bgcolor="#F3F3F3" width="100%" style="background-color:#f3f3f3;padding:12px;border-bottom:1px solid #ececec">
    											<table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;width:100%!important;font-family:Helvetica,Arial,sans-serif;min-width:100%!important" width="100%">
    											   <tbody>
    												  <tr>
    													 <td align="left" valign="middle" width="50%"><span style="margin:0;color:#4c4c4c;white-space:normal;display:inline-block;text-decoration:none;font-size:12px;line-height:20px">eInvoiceTrack</span></td>
    													 <td valign="middle" width="50%" align="right" style="padding:0 0 0 10px"><span style="margin:0;color:#4c4c4c;white-space:normal;display:inline-block;text-decoration:none;font-size:12px;line-height:20px">'.date('Y-m-d H:i:s').'</span></td>
    													 <td width="1">&nbsp;</td>
    												  </tr>
    											   </tbody>
    											</table>
    										 </td>
    									  </tr>
    									  <tr>
    										 <td align="left">
    											<table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
    											   <tbody>
    												  <tr>
    													 <td width="100%">
    														<table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
    														   <tbody>
    															  <tr>
    																 <td align="center" bgcolor="#8BC34A" style="padding:20px 48px;color:#ffffff" class="banner-color">
    																	<table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
    																	   <tbody>
    																		  <tr>
    																			 <td align="center" width="100%">
    																				<h1 style="padding:0;margin:0;color:#ffffff;font-weight:500;font-size:20px;line-height:24px">Bienvenue chez eInvoiceTrack</h1>
    																			 </td>
    																		  </tr>
    																	   </tbody>
    																	</table>
    																 </td>
    															  </tr>
    									
    															<tr>
    																 <td align="center" style="padding:20px 0 10px 0">
    																	<table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
    																	   <tbody>
    																		  <tr>
    																			 <td align="center" width="100%" style="padding: 0 15px;text-align: justify;color: rgb(76, 76, 76);font-size: 12px;line-height: 18px;">
    																				<h3 style="font-weight: 600; padding: 0px; margin: 0px; font-size: 16px; line-height: 24px; text-align: justify ;" class="title-color">Bonjour '.$name.',</h3>
    																				<p style="margin: 20px 0 30px 0;font-size: 15px;text-align: justify ;">Bienvenue sur la plateforme eInvoiceTrack, où vous pouvez suivre en temps réel la distribution de vos factures clients ainsi gérer électroniquement tous vos liasses clients.
     
    																				Vos  informations de connexion temporaires sont ci-dessous:<br><br>  Identifiant: '.$to.' <br>  Votre mot de passe est :  <b> '.$mdp.'</b><br><br>  <br>&nbsp;&nbsp;&nbsp; Valider votre inscription sur le lien suivant : https://einvoicetrack.net/validation?token='.$user.' <br><br> Note: Nous vous recommandons d\'ajouter le lien d\'accès de eInvoicetrack en raccourci afin de faciliter l\'accès à votre compte.Nos meilleures salutation</p>
    																				<div style="font-weight: 200; text-align: justify ; margin: 25px;"><a  style="background-color: #1eb092; color : #ececec ; padding:0.6em 1em;border-radius:600px;font-size:14px;text-decoration:none;font-weight:bold; cursor:pointer" class="button-color" href="https://einvoicetrack.net/login" target="_blanc" >eInvoiceTrack</a></div>
    																			 </td>
    																		  </tr>
    																	   </tbody>
    																	</table>
    																 </td>
    															  </tr>
    															  <tr>
    															  </tr>
    															  <tr>
    															  </tr>
    														   </tbody>
    														</table>
    													 </td>
    												  </tr>
    											   </tbody>
    											</table>
    										 </td>
    									  </tr>
    									  <tr>
    										 <td align="left">
    											<table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" style="padding:0 24px;color:#999999;font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
    											   <tbody>
    												  <tr>
    													 <td align="center" width="100%">
    														<table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
    														   <tbody>
    															  <tr>
    																 <td align="center" valign="middle" width="100%" style="border-top:1px solid #d9d9d9;padding:12px 0px 20px 0px;text-align:center;color:#4c4c4c;font-weight:200;font-size:12px;line-height:18px">Merci,
    																	<br><b> Service client eInvoiceTrack </b>
    																 </td>
    															  </tr>
    														   </tbody>
    														</table>
    													 </td>
    												  </tr>
    												  <tr>
    													 <td align="center" width="100%">
    														<table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
    														   <tbody>
    															  <tr>
    																 <td align="center" style="padding:0 0 8px 0" width="100%"></td>
    															  </tr>
    														   </tbody>
    														</table>
    													 </td>
    												  </tr>
    											   </tbody>
    											</table>
    										 </td>
    									  </tr>
    								   </tbody>
    								</table>
    							 </center>
    						  </td>
    					   </tr>
    					</tbody>
    				 </table>
    			  </div>
    		   </body>
    		</html>';
                    $mail->AltBody = 'Bienvenue chez eInvoiceTrack Votre mot de pass :'.$mdp;


                    $mail->send();
                    // echo 'Message has been sent';
                } catch (Exception $e) {
                    //echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
                }


                echo '
				
				<script>
					
					Swal.fire({
					  type: "success",
					  title: "Ajout a été effectué avec succès ",
					  showConfirmButton: false,
					  timer: 3000
					})
					
				</script>
				
				
				';

                // echo '
                // $.ajax({
                // url : "https://www.einvoicetrack.com/phpMailer/index.php",
                // type : "POST",
                // data : {"name":"'.$NOM_USER.'", "mdp" : "'.$MDP_USER.'", "to" : "'.$EMAIL_USER.'","user" : "'.$user_current.'"},
                // success : function(code_html){
                // console.log(code_html);
                // },
                // });

                // ';
                // header( "refresh:3;url=login" );


            }
            else
            {
                echo '
				<script>
									
					
					Swal.fire({
					  type: "error",
					  title: "Informations erronés ou Utilisateur déjà existant! ",
					  showConfirmButton: false,
					  timer: 2000
					})
					
				</script> ';
                // echo '<br/>'. mysqli_error($ma_connexion);
                $BigErr = "Informations erronés ou Utilisateur déjà existant!";
            }
        }
    }
    else
    {
        $BigErr = "Informations erronés ou Utilisateur déjà existant!";
    }
}


ob_end_flush();
?>


<div id="wrapper">


    <?php
    include 'includes/nav.php';
    ?>

    <div id="page-wrapper" class="gray-bg dashbard-1">

        <?php
        include 'includes/header.php';
        ?>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">

            </div>
        </div>
        <div class="wrapper wrapper-content">


            <div class="col-md-4 dragme" > </div>
            <div class="col-md-8 dragme" id="import_client_panel">
                <div class="ibox" id="ibox_import">
                    <div class="ibox-title" style="background-color: #24c6c8; color: white;">
                        <h5> <i class="fa fa-file"></i> Importer fichier client </h5>

                    </div>
                    <div class="ibox-content" >
                        <div class="sk-spinner sk-spinner-wave " >
                            <div class="sk-rect1"></div>
                            <div class="sk-rect2"></div>
                            <div class="sk-rect3"></div>
                            <div class="sk-rect4"></div>
                            <div class="sk-rect5"></div>
                        </div>
                        <div class="alert alert-warning">
                            Les champs indiqués par une * sont obligatoires
                        </div>
                        <span class="required font-weight-bold"><?php echo $BigErr;?></span>

                        <div style="background-color:#eeeeee">
                            <br/>
                            <form class="m-t" method="POST" id="myForm" >
                                <div class="form-group">
                                    <span class="label label-info float-left">Nom  </span> <span class="required">* <?php echo $nomErr;?></span>
                                    <input type="text" class="form-control" value="<?php echo $NOM_USER;?>" name="nom" id="nom" required="">
                                </div>
                                <div class="form-group">
                                    <span class="label label-info float-left">Email  </span>  <span class="required">* <?php echo $emailErr;?> </span>
                                    <input type="email" class="form-control <?php echo ($emailErr!='')?'is-invalid':'' ?>" value="<?php echo $EMAIL_USER;?>" name="email" id="email" required="">
                                </div>

                                <div class="form-group">
                                    <span class="label label-info float-left">Civilité </span> <span class="required"><?php echo $civiliteErr;?> </span>
                                    <select name="civilite" id="civilite" class="form-control" required>
                                        <option value="Mr" <?php echo ($CIVILITE_USER=='Mr')?'selected':'' ?> >Homme</option>
                                        <option value="Mme" <?php echo ($CIVILITE_USER=='Mme')?'selected':'' ?>>Femme</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <span class="label label-info float-left">Rôle  </span> <span class="required"><?php echo $roleErr;?> </span>
                                    <select name="role" id="role" class="form-control" required>
                                        <option value="1" <?php echo ($ROLE_USER=='1')?'selected':'' ?> >Utilisateur</option>
                                        <option value="2" <?php echo ($ROLE_USER=='2')?'selected':'' ?>>Admin</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <span class="label label-info float-left">Modules  </span> <span class="required"><?php echo $roleErr;?> </span>
                                    <br/>
                                    <?php

                                    $SQL="SELECT `CODE`, `NOM` FROM `module` WHERE 1"
                                    ;
                                    $query=mysqli_query($ma_connexion,$SQL);
                                    while($row=mysqli_fetch_assoc($query))
                                    {

                                        ?>
                                        <div class="checkbox m-r-xs form-control">
                                            <input type="checkbox" name="modules[]" id="checkbox_<?php echo $row['CODE'] ?>" value="<?php echo $row['CODE'] ?>">
                                            <label class="" for="checkbox_<?php echo $row['CODE'] ?>">
                                                <?php echo $row['NOM'] ?>
                                            </label>
                                        </div>


                                        <?php
                                    }
                                    ?>
                                </div>




                                <button type="submit" name="submit" class="btn btn-primary block full-width m-b">Ajouter</button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 dragme" > </div>
        </div>










        <?php
        include 'includes/footer.php';
        ?>
    </div>
</div>




<!-- Mainly scripts -->
<script src="js/jquery-3.1.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="js/inspinia.js"></script>
<script src="js/plugins/pace/pace.min.js"></script>
<script >


    var client = document.getElementById('checkbox_1');
    var facture = document.getElementById('checkbox_2');
    var tableauboard = document.getElementById('checkbox_6');
    var rapport = document.getElementById('checkbox_7');


    client.onchange = function()
    {
        if (facture.checked==true)
        {
            client.checked = true;
        }
    };

    facture.onchange = function()
    {
        if (facture.checked==true)
        {
            client.checked = true;
        }

        if (tableauboard.checked==true || rapport.checked==true )
        {
            facture.checked = true;
        }

    };

    tableauboard.onchange = function()
    {
        if (tableauboard.checked==true)
        {
            facture.checked = true;
            client.checked = true;
        }
    };

    rapport.onchange = function()
    {
        if (rapport.checked==true)
        {
            facture.checked = true;
            client.checked = true;
        }
    };



</script>





</body>

</html>
		