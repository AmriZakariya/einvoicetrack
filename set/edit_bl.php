<?php
ob_start();
session_start();
include '../connexion.php';




use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require '../vendor/autoload.php';



if(isset($_SESSION['user_einvoicetrack']) && ( $_SESSION['role'] ==  'superadmin' ||  $_SESSION['role'] ==  'admin'  ) )
{

		$current_user = decode($_SESSION['user_einvoicetrack']) ;
		$SQL_SESSION="SELECT  `NOM_USER`, `PRENOM_USER`, `EMAIL_USER`,`CIVILITE_USER`,
					`CODE_ENTREPRISE`, `ROLE_USER`
				  FROM `user` 
				  WHERE CODE_USER = $current_user
				  AND actif= 1";
		$query_SESSION=mysqli_query($ma_connexion,$SQL_SESSION);
		if(mysqli_num_rows($query_SESSION) == 1) {
			while ($row_SESSION = mysqli_fetch_assoc($query_SESSION)) {
				$NOM_USER_SESSION = $row_SESSION['CIVILITE_USER'] . ' ' . $row_SESSION['NOM_USER'];
				$EMAIL_USER_SESSION = $row_SESSION['EMAIL_USER'];


			}
		}


	if(isset($_POST['current_list']))
	{
		
		$current_list= urldecode(mysqli_real_escape_string($ma_connexion,$_POST['current_list']));



		// $edit_numero=mysqli_real_escape_string($ma_connexion,$_POST['edit_numero']);
		$edit_bc=mysqli_real_escape_string($ma_connexion,$_POST['edit_bc']);
		$edit_date_livraison=mysqli_real_escape_string($ma_connexion,$_POST['edit_date_livraison']);
		$edit_date_numerisation=mysqli_real_escape_string($ma_connexion,$_POST['edit_date_numerisation']);

		$edit_TYPE_VENTE=mysqli_real_escape_string($ma_connexion,$_POST['edit_TYPE_VENTE']);
		$edit_TYPE_CLIENT=mysqli_real_escape_string($ma_connexion,$_POST['edit_TYPE_CLIENT']);
		$edit_BL_MANUEL=mysqli_real_escape_string($ma_connexion,$_POST['edit_BL_MANUEL']);
		$edit_DATE_DOCUMENT=mysqli_real_escape_string($ma_connexion,$_POST['edit_DATE_DOCUMENT']);

		$sql=" UPDATE `bl` SET 
									`BC` = '$edit_bc',
									`DATE_LIVRAISON` = '$edit_date_livraison',
									`DATE_NUMERISATION` = '$edit_date_numerisation',
									`TYPE_VENTE` = '$edit_TYPE_VENTE',
									`TYPE_CLIENT` = '$edit_TYPE_CLIENT',
									`BL_MANUEL` = '$edit_BL_MANUEL',
									`DATE_DOCUMENT` = '$edit_DATE_DOCUMENT'
							WHERE `BL`= '$current_list'  " ;

					if (mysqli_query($ma_connexion, $sql)) {
						echo '1';
						$user_einvoicetrack = decode($_SESSION['user_einvoicetrack']);
						$sql=" INSERT INTO `action`(`USER`, `TITRE`, `DESCRIPTION`) VALUES
											('$user_einvoicetrack','Gestion des bons de livraison','Modification du bon de livraison numéro : <b> $current_list </b>')" ;
						mysqli_query($ma_connexion, $sql);
						if($_FILES)
						{
							if ( 0 < $_FILES['file']['error'] ) {
								// echo 'Error: ' . $_FILES['file']['error'] . '<br>';
								switch ($_FILES['file']['error']) {
									case UPLOAD_ERR_INI_SIZE:
										$message = "The uploaded file exceeds the upload_max_filesize directive in php.ini";
										break;
									case UPLOAD_ERR_FORM_SIZE:
										$message = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form";
										break;
									case UPLOAD_ERR_PARTIAL:
										$message = "The uploaded file was only partially uploaded";
										break;
									case UPLOAD_ERR_NO_FILE:
										$message = "No file was uploaded";
										break;
									case UPLOAD_ERR_NO_TMP_DIR:
										$message = "Missing a temporary folder";
										break;
									case UPLOAD_ERR_CANT_WRITE:
										$message = "Failed to write file to disk";
										break;
									case UPLOAD_ERR_EXTENSION:
										$message = "File upload stopped by extension";
										break;

									default:
										$message = "Unknown upload error";
										break;
								}

								echo $message;				
							}
							else {
								move_uploaded_file($_FILES['file']['tmp_name'], '../dossier_bl/' . $current_list .'.pdf');
								
								
								// $myimage = $_FILES['file']['name'] ; 
								// echo $myimage;
								
								
								$sql=" Update bl set pdf = 1 WHERE BL = '$current_list';" ;
						
								if (mysqli_query($ma_connexion, $sql)) {
									
									
										
								} else {
											echo "\n  : " . mysqli_error($ma_connexion);
								
								}	
								
								
								
							}
						}
						
						
					} else {
						echo $sql;

						echo "Error updating record: " . mysqli_error($ma_connexion);
					
					}

		

		
	}
	
	if(isset($_POST['current_delete']))
	{
		$current_delete= urldecode(mysqli_real_escape_string($ma_connexion,$_POST['current_delete']));
		
		
		$sql=" DELETE FROM  bl WHERE `BL`= '$current_delete'  " ;

		
				if (mysqli_query($ma_connexion, $sql)) {
						echo '1';
						$user_einvoicetrack = decode($_SESSION['user_einvoicetrack']);
						$sql=" INSERT INTO `action`(`USER`, `TITRE`, `DESCRIPTION`) VALUES
											('$user_einvoicetrack','Gestion des bons de livraison','Suppression du bon de livraison numéro : <b> $current_delete </b>')" ;
						mysqli_query($ma_connexion, $sql);
				} else {
					echo "Error updating record: " . mysqli_error($ma_connexion);
				
				}	
	}
	
	
	if(isset($_POST['delete_code']))
	{
		
		foreach ($_POST['delete_code'] as $key => $delete_code)
		{
			
			$current_delete= urldecode(mysqli_real_escape_string($ma_connexion,$delete_code));
		
			
			$sql=" DELETE FROM  bl WHERE `BL`= '$current_delete'  " ;

		
				if (mysqli_query($ma_connexion, $sql)) {
						echo '1';
				} else {
					echo "Error updating record: " . mysqli_error($ma_connexion);
				
				}	
			
				
		}
		$elements = implode( ',', $_POST['delete_code'] );
		$user_einvoicetrack = decode($_SESSION['user_einvoicetrack']);
		$sql=" INSERT INTO `action`(`USER`, `TITRE`, `DESCRIPTION`) VALUES
							('$user_einvoicetrack','Gestion des bons de livraison','Suppression des BLS numéros :  [  <b> $elements  </b> ]  ')" ;
		mysqli_query($ma_connexion, $sql);
		

	}
	
	if(isset($_POST['pdf_dwn']))
	{
		
		$current_list= urldecode(mysqli_real_escape_string($ma_connexion,$_POST['pdf_dwn']));
	
		if(file_exists('../dossier_bl/'.$current_list.'.pdf'))
		{
//			$SQL_check="SELECT 1
//					FROM  bl b
//					WHERE b.BL = '$current_list'
//			";
//			$query=mysqli_query($ma_connexion,$SQL_check);
//			if(mysqli_num_rows($query) == 1)
//			{
//				echo "dossier_bl/$current_list.pdf" ;
//
//			}

			echo "dossier_bl/$current_list.pdf" ;

		}
		
	}


	if(isset($_POST['pdf_code']))
	{
		$urls = array() ;
		foreach ($_POST['pdf_code'] as $key => $pdf_code)
		{
			$current_list= urldecode(mysqli_real_escape_string($ma_connexion,$pdf_code));

			if(file_exists('../dossier_bl/'.$current_list.'.pdf'))
			{
				$urls[] = "../dossier_bl/$current_list.pdf" ;
				// $SQL_check="SELECT 1
				// FROM  facture f
				// WHERE f.NUM_FACTURE = '$current_list'
				// AND f.pdf = 1
				// ";
				// $query=mysqli_query($ma_connexion,$SQL_check);
				// if(mysqli_num_rows($query) == 1)
				// {
				// $urls[] = "../einvoicetrack/$current_list.pdf" ;

				// }
				break;
			}
		}
		if(file_exists('export/export.zip'))
			unlink('export/export.zip');


		$zip = new ZipArchive();
		$filename = "export/export_BL_".encode(strtotime(date('Y-m-d H:i:s'))).".zip";


		if ($zip->open($filename, ZipArchive::CREATE)!==TRUE) {
			echo("cannot open <$filename>\n");
		}



		$zip->addFile('einvoicetrack.txt');
		foreach ($urls as $file) {
			$new_filename = substr($file,strrpos($file,'/') + 1);
			$zip->addFile($file,$new_filename);


		}

		$zip->close();

		echo 'set/'.$filename;


		// echo json_encode($urls);


	}



	if(isset($_POST['synchronisation'])) {

		$content = '' ;
		$total = 0;

		$files = scandir("../dossier_bl");
		if(false !== $files)
		{

			foreach($files as $file)
			{

				if('.' !=  $file && '..' != $file && pathinfo($file)['extension'] == 'pdf' )
				{
					$NUM_BL_FROM_FILE = pathinfo($file)['filename'] ;

					//TODO CHECK FOR EXISTING BLs
					$SQL_check="SELECT 1
					FROM  bl b
					WHERE b.BL = '$NUM_BL_FROM_FILE'
						";
					$query_check=mysqli_query($ma_connexion,$SQL_check);
					if(mysqli_num_rows($query_check) == 1)
					{
						echo "<br>  Already ==>". $NUM_BL_FROM_FILE ;

					}else{
						$sql_insert = "INSERT INTO `bl`(`BL`,`src`)VALUES
							('$NUM_BL_FROM_FILE','2'); ";

						// echo $sql ;
						if (mysqli_query($ma_connexion, $sql_insert)) {
							echo "<br> here we go found added ==>". $NUM_BL_FROM_FILE ;

							$total++ ;
							$content .= ' <br/> <span style="color:green">Le bon de livraison << '.$NUM_BL_FROM_FILE.' >> est bien ajouté.</span>' ;

						} else {
							 echo '<br/>update  : '. mysqli_error($ma_connexion);

						}

					}


				}
			}
		}


		/*
* START SENDING EMAIL
*/
		$content = "<h3> $total lignes traitées </h3> $content" ;


		$to = $EMAIL_USER_SESSION;
		$name = $NOM_USER_SESSION;
		$html = $content;


		//Create an instance; passing `true` enables exceptions
		$mail = new PHPMailer(true);


		try {

//                        echo "stat sending mail" ;
			//Server settings
			//$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
			$mail->isSMTP();                                            //Send using SMTP
			$mail->Host = 'mail.einvoicetrack.net';                     //Set the SMTP server to send through
			$mail->SMTPAuth = true;                                   //Enable SMTP authentication
			$mail->Username = 'support@einvoicetrack.net';                     //SMTP username
			$mail->Password = 'D~1tW5RO!+uN';                               //SMTP password
			$mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
			$mail->Port = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

			//    $mail->SMTPAuth   = true;
			//    $mail->SMTPSecure = "tls";

			//    $mail->SMTPOptions = array(
			//        'ssl' => array(
			//            'verify_peer' => false,
			//            'verify_peer_name' => false,
			//            'allow_self_signed' => true
			//        )
			//    );
			//Recipients
			$mail->setFrom('support@einvoicetrack.net', 'eInvoiceTrack');

			$mail->addAddress($to);


//                        $mail->addAttachment('einvoicetrack.png');


			$mail->isHTML(true);                                  // Set email format to HTML
			$mail->Subject = 'RAPPORT OPERATION SYNCHRONISATION FICHIER  eInvoieTrack';
			$mail->Body = '
		 <html>
		   <head>
			  <style>
				 .banner-color {
				 background-color: #eb681f;
				 }
				 .title-color {
				 color: #33bbbb;
				 }
				 
				 @media screen and (min-width: 500px) {
				 .banner-color {
				 background-color: #33bbbb;
				 }
				 .title-color {
				 color: #eb681f;
				 }
				 .button-color {
				 background-color: #33bbbb;
				 }
				 }
			  </style>
		   </head>
		   <body>
			  <div style="background-color:#ececec;padding:0;margin:0 auto;font-weight:200;width:100%!important">
				 <table align="center" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
					<tbody>
					   <tr>
						  <td align="center">
							 <center style="width:100%">
								<table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" style="margin:0 auto;max-width:512px;font-weight:200;width:inherit;font-family:Helvetica,Arial,sans-serif" width="512">
								   <tbody>
									  <tr>
										 <td bgcolor="#F3F3F3" width="100%" style="background-color:#f3f3f3;padding:12px;border-bottom:1px solid #ececec">
											<table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;width:100%!important;font-family:Helvetica,Arial,sans-serif;min-width:100%!important" width="100%">
											   <tbody>
												  <tr>
													 <td align="left" valign="middle" width="50%"><span style="margin:0;color:#4c4c4c;white-space:normal;display:inline-block;text-decoration:none;font-size:12px;line-height:20px">eInvoiceTrack</span></td>
													 <td valign="middle" width="50%" align="right" style="padding:0 0 0 10px"><span style="margin:0;color:#4c4c4c;white-space:normal;display:inline-block;text-decoration:none;font-size:12px;line-height:20px">' . date('Y-m-d H:i:s') . '</span></td>
													 <td width="1">&nbsp;</td>
												  </tr>
											   </tbody>
											</table>
										 </td>
									  </tr>
									  <tr>
										 <td align="left">
											<table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
											   <tbody>
												  <tr>
													 <td width="100%">
														<table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
														   <tbody>
															  <tr>
																 <td align="center" bgcolor="#33bbbb" style="padding:20px 48px;color:#ffffff" class="banner-color">
																	<table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
																	   <tbody>
																		  <tr>
																			 <td align="center" width="100%">
																				<h1 style="padding:0;margin:0;color:#ffffff;font-weight:500;font-size:20px;line-height:24px">eInvoiceTrack</h1>
																			 </td>
																		  </tr>
																	   </tbody>
																	</table>
																 </td>
															  </tr>
									
															<tr>
																 <td align="center" style="padding:20px 0 10px 0">
																	<table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
																	   <tbody>
																		  <tr>
																			 <td align="center" width="100%" style="padding: 0 15px;text-align: justify;color: rgb(76, 76, 76);font-size: 12px;line-height: 18px;">
																				<h3 style="font-weight: 600; padding: 0px; margin: 0px; font-size: 16px; line-height: 24px; text-align: justify ;" class="title-color">Bonjour ' . $name . ',</h3>
																				<p style="margin: 20px 0 30px 0;font-size: 15px;text-align: justify ;">Opération d\'importation initiée.
 
																				 <br> <br>Voici le rapport de la synchronisation :  <br> ' . $html . '
																				<div style="font-weight: 200; text-align: justify ; margin: 25px;"><a  style="padding:0.6em 1em;border-radius:600px;font-size:14px;text-decoration:none;font-weight:bold; cursor:pointer ; color : #ffffff" class="button-color" href="https://skcconseil.einvoicetrack.com/login" target="_blanc" >eInvoiceTrack</a></div>
																			 </td>
																		  </tr>
																	   </tbody>
																	</table>
																 </td>
															  </tr>
															  <tr>
															  </tr>
															  <tr>
															  </tr>
														   </tbody>
														</table>
													 </td>
												  </tr>
											   </tbody>
											</table>
										 </td>
									  </tr>
									  <tr>
										 <td align="left">
											<table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" style="padding:0 24px;color:#999999;font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
											   <tbody>
												  <tr>
													 <td align="center" width="100%">
														<table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
														   <tbody>
															  <tr>
																 <td align="center" valign="middle" width="100%" style="border-top:1px solid #d9d9d9;padding:12px 0px 20px 0px;text-align:center;color:#4c4c4c;font-weight:200;font-size:12px;line-height:18px">Merci,
																	<br><b> Service client eInvoiceTrack </b>
																 </td>
															  </tr>
														   </tbody>
														</table>
													 </td>
												  </tr>
												  <tr>
													 <td align="center" width="100%">
														<table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
														   <tbody>
															  <tr>
																 <td align="center" style="padding:0 0 8px 0" width="100%"></td>
															  </tr>
														   </tbody>
														</table>
													 </td>
												  </tr>
											   </tbody>
											</table>
										 </td>
									  </tr>
								   </tbody>
								</table>
							 </center>
						  </td>
					   </tr>
					</tbody>
				 </table>
			  </div>
		   </body>
		</html>';
			$mail->AltBody = strip_tags($mail->Body);

			$mail->send();
//                         echo 'Message has been sent';
		} catch (Exception $e) {
			echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
		}

		/*
         * END SENDING EMAIL
         */








	}
}

ob_end_flush();
?>


