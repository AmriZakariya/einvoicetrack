<?php
ob_start();
session_start();
include '../connexion.php';

require 'PhpSpreadsheet/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

if(isset($_SESSION['user_einvoicetrack']) && ( $_SESSION['role'] ==  'superadmin' ||  $_SESSION['role'] ==  'admin'  ) )
{
	
	$current_user = decode($_SESSION['user_einvoicetrack']) ; 					
	$SQL_SESSION="SELECT  `NOM_USER`, `PRENOM_USER`, `EMAIL_USER`,`CIVILITE_USER`,
			`CODE_ENTREPRISE`, `ROLE_USER`
		  FROM `user` 
		  WHERE CODE_USER = $current_user
		  AND actif= 1";
	$query_SESSION=mysqli_query($ma_connexion,$SQL_SESSION);
	if(mysqli_num_rows($query_SESSION) == 1)
	{
		while($row_SESSION=mysqli_fetch_assoc($query_SESSION))
		{	
				$NOM_USER_SESSION = $row_SESSION['CIVILITE_USER'].' ' .$row_SESSION['NOM_USER'];
				$EMAIL_USER_SESSION = $row_SESSION['EMAIL_USER'];
				
				
				$folder_name = 'upload/';
				
				


			if(!empty($_FILES))
			{
					 $temp_file = $_FILES['file']['tmp_name'];
					$temp_name = $_FILES['file']['name'];
			

				$header_num = array("N° facture");
				$header_nom = array("Nom client");
				$header_code = array("Code client");
				$header_date = array("Date comptabilisation");
				$header_bc = array("N° BC");
				$header_bl = array("N° BL");
				
				
				
							$content = '' ; 
							$ok = 0 ; 
							$okmaj = 0 ; 
							$nook = 0 ; 
							
							
							$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($temp_file);
							$worksheet = $spreadsheet->getActiveSheet();
							
							if (   in_array($worksheet->getCellByColumnAndRow(1, 1)->getCalculatedValue(), $header_num) 
								&& in_array($worksheet->getCellByColumnAndRow(2, 1)->getCalculatedValue(), $header_nom) 
								&& in_array($worksheet->getCellByColumnAndRow(3, 1)->getCalculatedValue(), $header_code) 
								&& in_array($worksheet->getCellByColumnAndRow(4, 1)->getCalculatedValue(), $header_date) 
								&& in_array($worksheet->getCellByColumnAndRow(5, 1)->getCalculatedValue(), $header_bc) 
								&& in_array($worksheet->getCellByColumnAndRow(6, 1)->getCalculatedValue(), $header_bl)
								
								)
							{

								$elements = '' ; 
								$highestRow = $worksheet->getHighestRow(); // e.g. 10
								for ($row = 2; $row <= $highestRow; ++$row) 
								{
									if ( ($NUM_FACTURE = mysqli_real_escape_string($ma_connexion,$worksheet->getCellByColumnAndRow(1, $row)->getFormattedValue())) != '' 
										  && ($NUM_CLIENT = mysqli_real_escape_string($ma_connexion,$worksheet->getCellByColumnAndRow(3, $row)->getFormattedValue())) != '' 
										  && ($DATE_COMPTABILISATION = mysqli_real_escape_string($ma_connexion,$worksheet->getCellByColumnAndRow(4, $row)->getFormattedValue())) != '' 
												)
									{
										
										$DATE_COMPTABILISATION =  date("Y-m-d", strtotime($DATE_COMPTABILISATION));
										$BC = mysqli_real_escape_string($ma_connexion,$worksheet->getCellByColumnAndRow(5, $row)->getFormattedValue());
										$BL = mysqli_real_escape_string($ma_connexion,$worksheet->getCellByColumnAndRow(6, $row)->getFormattedValue());		
										$STATUS = mysqli_real_escape_string($ma_connexion,$worksheet->getCellByColumnAndRow(7, $row)->getFormattedValue());		
										
										
										
										$sql_test= "SELECT NUMERO_CLIENT,STATUS
										FROM facture
										WHERE NUM_FACTURE = '$NUM_FACTURE' " ;  
										$query_test=mysqli_query($ma_connexion,$sql_test) ;
										if(mysqli_num_rows($query_test) == 1)
										{
											while($row_test=mysqli_fetch_assoc($query_test))
											{
												
												if( $row_test['STATUS'] == 2 )
												{
													$nook++ ; 
													$content .= ' <br/> <span style="color:orange"><strong> Line'.($row-1).' :</strong> La facture N° '.$NUM_FACTURE.' est déjà distribuée </span>' ; 
													echo '
													<div class="alert alert-warning" role="alert">
													  <strong>Line'.($row-1).' :</strong> La facture N° '.$NUM_FACTURE.' est déjà distribuée.
													</div>';
												}
												else 
												{
													
													if( $row_test['NUMERO_CLIENT'] == $NUM_CLIENT )
													{
														
														$sql= "UPDATE `facture` SET DATE_COMPTABILISATION = '$DATE_COMPTABILISATION',
																										BC = '$BC',
																										BL = '$BL',
																										STATUS = '1'
																				WHERE NUM_FACTURE = '$NUM_FACTURE' "; 
																											
														if (mysqli_query($ma_connexion, $sql)) {
															$elements .= $NUM_FACTURE.',' ;
															$okmaj++ ; 
															$content .= ' <br/> <span style="color:blue"> <strong>Line'.($row-1).' :</strong> La facture N° '.$NUM_FACTURE.' est mise à jour. </span>';
															echo  '
																<div class="alert alert-primary" role="alert">
																	  <strong>Line'.($row-1).' :</strong> La facture N° '.$NUM_FACTURE.' est mise à jour.
																</div>';
																
																
																$sql= "INSERT INTO `facture_status`(`NUM_FACTURE`, `CODE_STATUS`, `DATE`) VALUES
																						('$NUM_FACTURE','1','$DATE_COMPTABILISATION'); "; 
																														
																		if (mysqli_query($ma_connexion, $sql)) {


																			
																		}
																		else 
																		{
																			
																		}
																
																
																
																
																
															
														}
														else 
														{
															$nook++ ; 
															$content .= '  <br/> <span style="color:red"> <strong>Line'.($row-1).' :</strong> Les données de la facture '.$NUM_FACTURE.' sont erronnées</span>';
															echo '
															<div class="alert alert-danger" role="alert">
																  <strong>Line'.($row-1).' :</strong> Les données de la facture '.$NUM_FACTURE.' sont erronnées.
																</div>';
															
														}
														
														
													}
													else 
													{
														
														$nook++ ; 
														$content .= '  <br/><span style="color:red">  <strong>Line'.($row-1).' :</strong> La facture '.$NUM_FACTURE.' est enregistrée avec un code de client différent  </span>';
														echo '
														<div class="alert alert-danger" role="alert">
															  <strong>Line'.($row-1).' :</strong> La facture '.$NUM_FACTURE.' est enregistrée avec un code de client différent.
															</div>';
														
													}
													
												}
												
											}
											
										}
										else 
										{
											$sql_test2= "SELECT 1
											FROM client
											WHERE NUMERO_CLIENT = '$NUM_CLIENT' " ;  
											$query_test2=mysqli_query($ma_connexion,$sql_test2) ;
											if(mysqli_num_rows($query_test2) != 1)
											{
												
												$nook++ ; 
												$content .= '  <br/> <span style="color:red"> <strong>Line'.($row-1).' :</strong> Aucun client avec le numero : << '.$NUM_CLIENT.' >> n\'existe dans la base des données</span>';
												echo '
												<div class="alert alert-danger" role="alert">
													  <strong>Line'.($row-1).' :</strong> Aucun client avec le numero : << '.$NUM_CLIENT.' >>  n\'existe dans la base des données
													</div>';
											}
											else 
											{
												
											
												$sql= "INSERT INTO `facture`(`NUM_FACTURE`, `NUMERO_CLIENT`, `DATE_COMPTABILISATION`, `BC`, `BL`) VALUES
																			('$NUM_FACTURE','$NUM_CLIENT','$DATE_COMPTABILISATION','$BC','$BL') ; "; 
																			
												 if (mysqli_query($ma_connexion, $sql)) {
													$elements .= $NUM_FACTURE.',' ;
													$ok++ ; 
													$content .= ' <br/> <span style="color:green"> <strong>Line'.($row-1).' :</strong> La facture '.$NUM_FACTURE.' est bien ajoutée.</span>';
													echo  '
													<div class="alert alert-success" role="alert">
														  <strong>Line'.($row-1).' :</strong> La facture '.$NUM_FACTURE.' est bien ajoutée.
														</div>
													';
													
													
													
													$sql= "INSERT INTO `facture_status`(`NUM_FACTURE`, `CODE_STATUS`, `DATE`) VALUES
																						('$NUM_FACTURE','1','$DATE_COMPTABILISATION'); "; 
																										
														if (mysqli_query($ma_connexion, $sql)) {


															
														}
														else 
														{
															
														}
													
												}
												else 
												{
													$nook++ ; 
													$content .= '  <br/> <span style="color:red"> <strong>Line'.($row-1).' :</strong> Les données de la facture '.$NUM_FACTURE.' sont erronnées</span>';
													echo '
													<div class="alert alert-danger" role="alert">
														  <strong>Line'.($row-1).' :</strong> Les données de la facture '.$NUM_FACTURE.' sont erronnées.
														</div>';
													
												}
											}
											
										}		
									}
									else
									{
										$nook++ ; 
										$content .= ' <br/><span style="color:red"> <strong>Line'.($row-1).' :</strong> champs manquants.</span>';
										echo '
										<div class="alert alert-danger" role="alert">
											  <strong>Line'.($row-1).' :</strong> champs manquants.
											</div>';
										
									}
								}
								
								$total = $highestRow - 1 ; 
								$res_text = "$total lignes traitées : $ok / $total Nouvelles factures, $okmaj / $total mise a jour,  $nook / $total Erreurs";
								$user_einvoicetrack = decode($_SESSION['user_einvoicetrack']);
								$sql=" INSERT INTO `action`(`USER`, `TITRE`, `DESCRIPTION`) VALUES
										('$user_einvoicetrack','Gestion des factures','Importation des factures - <b> $res_text </b>')" ;
								mysqli_query($ma_connexion, $sql);
								if($total>0)
								{
									$content = "<h3> $total lignes traitées </h3>  <h4 style='color:green'>  - $ok / $total Nouvelles factures </h4>   <h4 style='color:blue'>  - $okmaj / $total mise a jour  </h4>  <h4 style='color:red'> - $nook / $total Erreurs </h4>  $content" ; 

									$url = 'https://www.einvoicetrack.com/phpMailer/log.php';
									$data = array('to' => $EMAIL_USER_SESSION, 'name' => $NOM_USER_SESSION, 'html' => $content);

									$options = array(
										'http' => array(
											'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
											'method'  => 'POST',
											'content' => http_build_query($data)
										)
									);
									$context  = stream_context_create($options);
									$result = file_get_contents($url, false, $context);
									if ($result === FALSE) { 
											echo '
											<script>
													$.ajax({
													url : "https://www.einvoicetrack.com/phpMailer/index.php",
													type : "POST",
													data : {"to" : "'.$EMAIL_USER_SESSION.'","name":"'.$NOM_USER_SESSION.'", "html" : "'.$content.'"},
													success : function(code_html, statut){
														alert("ok");
													}
												});
												
											</script>';

									}
								}
								else 
								{
									echo '
									<div class="alert alert-warning" role="alert">
										le  fichier   <strong>  '.$temp_name.' </strong> est vide 
										</div>
									';
								}
							}
							else 
							{
								echo '
								<div class="alert alert-danger" role="alert">
									le  fichier   <strong>  '.$temp_name.' </strong> est incompatible 
									</div>
								';
							}
						
			}

				
				// echo $result ; 
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
		}
	}
	else 
	{
		// header('Location: login');
	}
	
	
		// $i = 0 ; 
												

		// $SQL="SHOW COLUMNS FROM client";
		// $query=mysqli_query($ma_connexion,$SQL);
		// while($row=mysqli_fetch_assoc($query))
		// {	
				// echo '
					// <a class="toggle-vis" data-column="'.$i++.'">'.$row['Field'].'</a> -
					// ';
		// }
}
else 
{
	// header('Location: login');
}
 

// $file = 'log/'.date('Y-m-d H-i-s').'.txt' ; 
// $fp = fopen($file,"wb");
// fwrite($fp,$content);
// fclose($fp);



/*



*/

ob_end_flush();
?>


