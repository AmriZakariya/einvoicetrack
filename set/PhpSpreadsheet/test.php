<?php
require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;



$streamedResponse = new StreamedResponse();
$streamedResponse->setCallback(function () {
      
	$spreadsheet = new Spreadsheet();
	$sheet = $spreadsheet->getActiveSheet();

	for($i=1;$i<10;$i++)
		$sheet->setCellValue('A'.$i,$i);

	$writer = new Xlsx($spreadsheet);
      $writer =  new Xlsx($spreadsheet);
      $writer->save('php://output');
});
$streamedResponse->setStatusCode(Response::HTTP_OK);
$streamedResponse->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
$streamedResponse->headers->set('Content-Disposition', 'attachment; filename="your_file.xlsx"');
return $streamedResponse->send();