<?php
ob_start();
session_start();
include '../connexion.php';


if(isset($_SESSION['user_einvoicetrack']) && ( $_SESSION['role'] ==  'superadmin' ||  $_SESSION['role'] ==  'admin'  ) )
{
if (isset($_POST['add_numero']) && 
					isset($_POST['add_client']) && 
					isset($_POST['add_date_comptabilisation']) && 
					isset($_POST['add_statut']) 
			 )
	{
		
		
		
		
						
		$add_numero = mysqli_real_escape_string($ma_connexion,$_POST['add_numero']) ;
		$add_client = urldecode(mysqli_real_escape_string($ma_connexion,$_POST['add_client'])) ;
		$add_date_comptabilisation = mysqli_real_escape_string($ma_connexion,$_POST['add_date_comptabilisation']) ;
		$add_statut = mysqli_real_escape_string($ma_connexion,$_POST['add_statut']) ;
		$add_date_distribution = mysqli_real_escape_string($ma_connexion,$_POST['add_date_distribution']) ;
		$add_motif = mysqli_real_escape_string($ma_connexion,$_POST['add_motif']) ;
		$add_description = mysqli_real_escape_string($ma_connexion,$_POST['add_description']) ;
		$add_bc = '' ;
		$add_bl = '' ;
		if (isset($_POST['add_bc']) && isset($_POST['add_bl']) )
		{
			if ( !empty($_POST['add_bc']) && !empty($_POST['add_bl']) )
			{
				$add_bc = mysqli_real_escape_string($ma_connexion,$_POST['add_bc']) ;
				$add_bl = mysqli_real_escape_string($ma_connexion,$_POST['add_bl']) ;
			}
		}
		
		$flag = 1 ; 
		
		
		if($add_statut == 2 || $add_statut == 3 ) 
		{
			if(!$_FILES)
			{
				$flag = 0 ; 
				echo '
					<div class="alert alert-danger" role="alert">
						  <strong> </strong> Le fichier est obligatoire 
					</div>
				';
				
			}
			else 
			{
				if ( 0 < $_FILES['file']['error'] ) {
					// echo 'Error: ' . $_FILES['file']['error'] . '<br>';
					switch ($_FILES['file']['error']) {
						case UPLOAD_ERR_INI_SIZE:
							$message = "The uploaded file exceeds the upload_max_filesize directive in php.ini";
							break;
						case UPLOAD_ERR_FORM_SIZE:
							$message = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form";
							break;
						case UPLOAD_ERR_PARTIAL:
							$message = "The uploaded file was only partially uploaded";
							break;
						case UPLOAD_ERR_NO_FILE:
							$message = "No file was uploaded";
							break;
						case UPLOAD_ERR_NO_TMP_DIR:
							$message = "Missing a temporary folder";
							break;
						case UPLOAD_ERR_CANT_WRITE:
							$message = "Failed to write file to disk";
							break;
						case UPLOAD_ERR_EXTENSION:
							$message = "File upload stopped by extension";
							break;

						default:
							$message = "Unknown upload error";
							break;
					}
					$flag = 0 ; 
					echo '
						<div class="alert alert-danger" role="alert">
							  <strong> </strong> '.$message.'
						</div>
					';
				}
				
			}
			
		}
		
		
		
		
		if($flag)
		{
			$sql= "INSERT INTO `facture`(`NUM_FACTURE`, `NUMERO_CLIENT`, `DATE_COMPTABILISATION`, `STATUS`, `BC`, `BL`) VALUES
										('$add_numero','$add_client','$add_date_comptabilisation','$add_statut','$add_bc','$add_bl') ; "; 
			// echo $sql ;
			if (mysqli_query($ma_connexion, $sql)) {
				
				echo '
				<div class="alert alert-success" role="alert">
					  <strong/> La facture << '.$add_numero.' >> est bien ajoutée.
					</div>
				';
				
				$user_einvoicetrack = decode($_SESSION['user_einvoicetrack']);
				$sql=" INSERT INTO `action`(`USER`, `TITRE`, `DESCRIPTION`) VALUES
									('$user_einvoicetrack','Gestion des factures','Ajout de la facture numéro : <b> $add_numero </b>')" ;
				mysqli_query($ma_connexion, $sql);

				$is_file_edited = 0 ;
				if($_FILES)
				{
					if ( 0 < $_FILES['file']['error'] ) {
						// echo 'Error: ' . $_FILES['file']['error'] . '<br>';
						switch ($_FILES['file']['error']) {
							case UPLOAD_ERR_INI_SIZE:
								$message = "The uploaded file exceeds the upload_max_filesize directive in php.ini";
								break;
							case UPLOAD_ERR_FORM_SIZE:
								$message = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form";
								break;
							case UPLOAD_ERR_PARTIAL:
								$message = "The uploaded file was only partially uploaded";
								break;
							case UPLOAD_ERR_NO_FILE:
								$message = "No file was uploaded";
								break;
							case UPLOAD_ERR_NO_TMP_DIR:
								$message = "Missing a temporary folder";
								break;
							case UPLOAD_ERR_CANT_WRITE:
								$message = "Failed to write file to disk";
								break;
							case UPLOAD_ERR_EXTENSION:
								$message = "File upload stopped by extension";
								break;

							default:
								$message = "Unknown upload error";
								break;
						}

						echo $message;				
					}
					else {
						move_uploaded_file($_FILES['file']['tmp_name'], '../einvoicetrack/' . $add_numero .'.pdf');
						
						
						$myimage = $_FILES['file']['name'] ; 
						// echo $myimage;
						
						
						$sql=" Update facture set pdf = 1 WHERE NUM_FACTURE = '$add_numero';" ;
				
						if (mysqli_query($ma_connexion, $sql)) {

							$is_file_edited = 1 ;
								
						} else {
									echo "\n  : " . mysqli_error($ma_connexion);
						
						}	
						
						
						
					}
				}
				
				if($add_statut == 1 ) 
					$add_date_distribution = $add_date_comptabilisation ; 
						
				$sql= "INSERT INTO `facture_status`(`NUM_FACTURE`, `CODE_STATUS`, `DATE`, `motif`, `description`, `is_file_edited`) VALUES
									('$add_numero','$add_statut','$add_date_distribution','$add_motif','$add_description','$is_file_edited'); ";
																	
					if (mysqli_query($ma_connexion, $sql)) {

						// if($add_statut == 3 ) 
						// {
							// $sql= "INSERT INTO `facture_rejet`(`DATE_REJET`, `NUM_FACTURE`, `MOTIF`)VALUES
												// ('$add_date_distribution','$add_numero'); "; 
																	
								// if (mysqli_query($ma_connexion, $sql)) {

									
								// }
								// else 
								// {
									// echo $sql ; 
									// echo "\n  : " . mysqli_error($ma_connexion);
								// }
							
							
						// }
						
					}
					else 
					{
						
					}
				
			}
			else 
			{
				echo '
					<div class="alert alert-danger" role="alert">
						  <strong> </strong> La facture << '.$add_numero.' >> existe déja.
					</div>
				';
			}
		}
	}
	else echo "error";
			
	
}


ob_end_flush();
?>

