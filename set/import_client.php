<?php
ob_start();
session_start();
include '../connexion.php';

require 'PhpSpreadsheet/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;



use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require '../vendor/autoload.php';




if (isset($_SESSION['user_einvoicetrack']) && ($_SESSION['role'] == 'superadmin' || $_SESSION['role'] == 'admin')) {


    $current_user = decode($_SESSION['user_einvoicetrack']);
    $SQL_SESSION = "SELECT  `NOM_USER`, `PRENOM_USER`, `EMAIL_USER`,`CIVILITE_USER`,
			`CODE_ENTREPRISE`, `ROLE_USER`
		  FROM `user` 
		  WHERE CODE_USER = $current_user
		  AND actif= 1";
    $query_SESSION = mysqli_query($ma_connexion, $SQL_SESSION);
    if (mysqli_num_rows($query_SESSION) == 1) {
        while ($row_SESSION = mysqli_fetch_assoc($query_SESSION)) {
            $NOM_USER_SESSION = $row_SESSION['CIVILITE_USER'] . ' ' . $row_SESSION['NOM_USER'];
            $EMAIL_USER_SESSION = $row_SESSION['EMAIL_USER'];


            $folder_name = 'upload/';


            if (!empty($_FILES)) {
                $temp_file = $_FILES['file']['tmp_name'];
                $temp_name = $_FILES['file']['name'];


                $header_nom = array("Nom client");
                $header_num = array("Code client");
                $header_tel1 = array("N° Tél1");
                $header_tel2 = array("N° Tél2");
                $header_adresse1 = array("Adresse de facturation ", "ADRESSE 1");
                $header_secteur1 = array("Secteur", "SECTEUR 1");
                $header_ville1 = array("Ville", "VILLE 1");
                $header_gps1_lat = array("GPS1 - Latitude");
                $header_gps1_long = array("GPS1 - Longitude");
                $header_adresse2 = array("Adresse 2", "ADRESSE2");
                $header_secteur2 = array("Secteur 2", "SECTEUR2");
                $header_ville2 = array("Ville 2", "VILLE 2");
                $header_gps2_lat = array("GPS2 - Latitude");
                $header_gps2_long = array("GPS2 - Longitude");
                $header_description = array("Déscription");

                $header_CONTACT = array("Contact");
                $header_TYPE_CLIENT = array("Type client");
                $header_NATURE_CLIENT = array("Nature client");


                $content = '';
                $ok = 0;
                $nook = 0;

                $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($temp_file);
                $worksheet = $spreadsheet->getActiveSheet();

                if (in_array($worksheet->getCellByColumnAndRow(1, 1)->getCalculatedValue(), $header_nom)
                    && in_array($worksheet->getCellByColumnAndRow(2, 1)->getCalculatedValue(), $header_num)
                    && in_array($worksheet->getCellByColumnAndRow(3, 1)->getCalculatedValue(), $header_tel1)
                    && in_array($worksheet->getCellByColumnAndRow(4, 1)->getCalculatedValue(), $header_tel2)
                    && in_array($worksheet->getCellByColumnAndRow(5, 1)->getCalculatedValue(), $header_adresse1)
                    && in_array($worksheet->getCellByColumnAndRow(6, 1)->getCalculatedValue(), $header_secteur1)
                    && in_array($worksheet->getCellByColumnAndRow(7, 1)->getCalculatedValue(), $header_ville1)
                    && in_array($worksheet->getCellByColumnAndRow(8, 1)->getCalculatedValue(), $header_gps1_lat)
                    && in_array($worksheet->getCellByColumnAndRow(9, 1)->getCalculatedValue(), $header_gps1_long)
                    && in_array($worksheet->getCellByColumnAndRow(10, 1)->getCalculatedValue(), $header_adresse2)
                    && in_array($worksheet->getCellByColumnAndRow(11, 1)->getCalculatedValue(), $header_secteur2)
                    && in_array($worksheet->getCellByColumnAndRow(12, 1)->getCalculatedValue(), $header_ville2)
                    && in_array($worksheet->getCellByColumnAndRow(13, 1)->getCalculatedValue(), $header_gps2_lat)
                    && in_array($worksheet->getCellByColumnAndRow(14, 1)->getCalculatedValue(), $header_gps2_long)
                    && in_array($worksheet->getCellByColumnAndRow(15, 1)->getCalculatedValue(), $header_description)


                    && in_array($worksheet->getCellByColumnAndRow(16, 1)->getCalculatedValue(), $header_CONTACT)
                    && in_array($worksheet->getCellByColumnAndRow(17, 1)->getCalculatedValue(), $header_TYPE_CLIENT)
                    && in_array($worksheet->getCellByColumnAndRow(18, 1)->getCalculatedValue(), $header_NATURE_CLIENT)
                ) {

                    $elements = '';
                    $highestRow = $worksheet->getHighestRow(); // e.g. 10
                    for ($row = 2; $row <= $highestRow; ++$row) {
                        if (($NOM_CLIENT = mysqli_real_escape_string($ma_connexion, $worksheet->getCellByColumnAndRow(1, $row)->getValue())) != '' &&
                            ($NUMERO_CLIENT = mysqli_real_escape_string($ma_connexion, $worksheet->getCellByColumnAndRow(2, $row)->getValue())) != ''
                        ) {

                            $TELE1 = mysqli_real_escape_string($ma_connexion, $worksheet->getCellByColumnAndRow(3, $row)->getValue());
                            $TELE2 = mysqli_real_escape_string($ma_connexion, $worksheet->getCellByColumnAndRow(4, $row)->getValue());

                            $ADRESSE1 = mysqli_real_escape_string($ma_connexion, $worksheet->getCellByColumnAndRow(5, $row)->getValue());
                            $SECTEUR1 = mysqli_real_escape_string($ma_connexion, $worksheet->getCellByColumnAndRow(6, $row)->getValue());
                            $VILLE = trim(mysqli_real_escape_string($ma_connexion, $worksheet->getCellByColumnAndRow(7, $row)->getValue()));

                            //TODO Need optimisation -- fetch all cities before enter here
//										if(empty($VILLE)){
//											$VILLE = NULL ;
//										}else{
//											$VILLE =  trim($VILLE) ;
//											$oldValue = $VILLE;
//											$queryVille = " SELECT v.CODE_VILLE
//													FROM ville v
//													WHERE v.CODE_VILLE =  '".$VILLE."'
//													OR v.NOM_VILLE LIKE  '".$VILLE."'
//													LIMIT 1
//											 ";
//											$resultVille = mysqli_query($ma_connexion, $queryVille);
//											while(($rowVille = mysqli_fetch_array($resultVille)) == true )
//											{
//												$VILLE = $rowVille['CODE_VILLE'];
//											}
//
//											if($VILLE == $oldValue && !is_numeric($VILLE)) {
//
//												$sqlInsertVille = " INSERT INTO `ville`(`NOM_VILLE`) VALUES
//																	('$VILLE')";
//												echo "inssssssssssssset ".$sqlInsertVille;
//
//												if (mysqli_query($ma_connexion, $sqlInsertVille)) {
//													$VILLE = mysqli_insert_id($ma_connexion);
//												}
//											}
//
//										}

                            $Latitude = mysqli_real_escape_string($ma_connexion, $worksheet->getCellByColumnAndRow(8, $row)->getCalculatedValue());
                            $Longitude = mysqli_real_escape_string($ma_connexion, $worksheet->getCellByColumnAndRow(9, $row)->getCalculatedValue());

                            $ADRESSE2 = mysqli_real_escape_string($ma_connexion, $worksheet->getCellByColumnAndRow(10, $row)->getValue());
                            $SECTEUR2 = mysqli_real_escape_string($ma_connexion, $worksheet->getCellByColumnAndRow(11, $row)->getValue());
                            $VILLE2 = trim(mysqli_real_escape_string($ma_connexion, $worksheet->getCellByColumnAndRow(12, $row)->getValue()));

                            //TODO Need optimisation -- fetch all cities before enter here
//										if(empty($VILLE2)){
//											$VILLE2 = NULL ;
//										}else{
//											$VILLE2 =  trim($VILLE2) ;
//											$oldValue = $VILLE2;
//											$queryVille = " SELECT v.CODE_VILLE
//													FROM ville v
//													WHERE v.CODE_VILLE =  '".$VILLE2."'
//													OR v.NOM_VILLE LIKE  '".$VILLE2."'
//													LIMIT 1
//											 ";
//											$resultVille = mysqli_query($ma_connexion, $queryVille);
//											while(($rowVille = mysqli_fetch_array($resultVille)) == true )
//											{
//												$VILLE2 = $rowVille['CODE_VILLE'];
//											}
//
//											if($VILLE2 == $oldValue && !is_numeric($VILLE2)) {
//												$sqlInsertVille = " INSERT INTO `action`(`NOM_VILLE`) VALUES
//																	('$VILLE2')";
//
//												if (mysqli_query($ma_connexion, $sqlInsertVille)) {
//													$VILLE2 = mysqli_insert_id($ma_connexion);
//												}
//											}
//
//										}


                            $Latitude2 = mysqli_real_escape_string($ma_connexion, $worksheet->getCellByColumnAndRow(13, $row)->getValue());
                            $Longitude2 = mysqli_real_escape_string($ma_connexion, $worksheet->getCellByColumnAndRow(14, $row)->getValue());


                            $DESCRIPTION = mysqli_real_escape_string($ma_connexion, $worksheet->getCellByColumnAndRow(15, $row)->getValue());


                            $CONTACT = mysqli_real_escape_string($ma_connexion, $worksheet->getCellByColumnAndRow(16, $row)->getValue());
                            $TYPE_CLIENT = mysqli_real_escape_string($ma_connexion, $worksheet->getCellByColumnAndRow(17, $row)->getValue());
                            $NATURE_CLIENT = mysqli_real_escape_string($ma_connexion, $worksheet->getCellByColumnAndRow(18, $row)->getValue());

                            $flag = 1;


                            if ($flag == 1) {

//											if($VILLE != NULL && $VILLE2 != NULL){
//												$sql= "INSERT INTO `client`(`NUMERO_CLIENT`, `NOM_CLIENT`, `TELE1`, `TELE2`, `ADRESSE1`, `SECTEUR1`, `VILLE`, `Latitude`, `Longitude`, `ADRESSE2`, `SECTEUR2`, `VILLE2`, `Latitude2`, `Longitude2`, `DESCRIPTION`) VALUES
//																		('$NUMERO_CLIENT','$NOM_CLIENT','$TELE1','$TELE2','$ADRESSE1','$SECTEUR1','$VILLE','$Latitude','$Longitude','$ADRESSE2','$SECTEUR2','$VILLE2','$Latitude2','$Longitude2','$DESCRIPTION') ; ";
//
//											}elseif ($VILLE != NULL && $VILLE2 == NULL){
//												$sql= "INSERT INTO `client`(`NUMERO_CLIENT`, `NOM_CLIENT`, `TELE1`, `TELE2`, `ADRESSE1`, `SECTEUR1`, `VILLE`, `Latitude`, `Longitude`, `ADRESSE2`, `SECTEUR2`, `VILLE2`, `Latitude2`, `Longitude2`, `DESCRIPTION`) VALUES
//																		('$NUMERO_CLIENT','$NOM_CLIENT','$TELE1','$TELE2','$ADRESSE1','$SECTEUR1','$VILLE','$Latitude','$Longitude','$ADRESSE2','$SECTEUR2',NULL,'$Latitude2','$Longitude2','$DESCRIPTION') ; ";
//
//											}elseif ($VILLE == NULL && $VILLE2 != NULL){
//												$sql= "INSERT INTO `client`(`NUMERO_CLIENT`, `NOM_CLIENT`, `TELE1`, `TELE2`, `ADRESSE1`, `SECTEUR1`, `VILLE`, `Latitude`, `Longitude`, `ADRESSE2`, `SECTEUR2`, `VILLE2`, `Latitude2`, `Longitude2`, `DESCRIPTION`) VALUES
//																		('$NUMERO_CLIENT','$NOM_CLIENT','$TELE1','$TELE2','$ADRESSE1','$SECTEUR1',NULL,'$Latitude','$Longitude','$ADRESSE2','$SECTEUR2','$VILLE2','$Latitude2','$Longitude2','$DESCRIPTION') ; ";
//
//											}elseif ($VILLE == NULL && $VILLE2 == NULL){
//												$sql= "INSERT INTO `client`(`NUMERO_CLIENT`, `NOM_CLIENT`, `TELE1`, `TELE2`, `ADRESSE1`, `SECTEUR1`, `VILLE`, `Latitude`, `Longitude`, `ADRESSE2`, `SECTEUR2`, `VILLE2`, `Latitude2`, `Longitude2`, `DESCRIPTION`) VALUES
//																		('$NUMERO_CLIENT','$NOM_CLIENT','$TELE1','$TELE2','$ADRESSE1','$SECTEUR1',NULL,'$Latitude','$Longitude','$ADRESSE2','$SECTEUR2',NULL,'$Latitude2','$Longitude2','$DESCRIPTION') ; ";
//
//											}
                                $sql = "INSERT INTO `client`(`NUMERO_CLIENT`, `NOM_CLIENT`, `TELE1`, `TELE2`, `ADRESSE1`, `SECTEUR1`, `VILLE`, `Latitude`, `Longitude`, `ADRESSE2`, `SECTEUR2`, `VILLE2`, `Latitude2`, `Longitude2`, `DESCRIPTION`, `CONTACT`, `TYPE_CLIENT`, `NATURE_CLIENT`) VALUES 
																		('$NUMERO_CLIENT','$NOM_CLIENT','$TELE1','$TELE2','$ADRESSE1','$SECTEUR1','$VILLE','$Latitude','$Longitude','$ADRESSE2','$SECTEUR2','$VILLE2','$Latitude2','$Longitude2','$DESCRIPTION','$CONTACT','$TYPE_CLIENT','$NATURE_CLIENT') ;";


                                if (mysqli_query($ma_connexion, $sql)) {

                                    $ok++;
                                    $content .= ' <br/> <span style="color:green"><strong>Line' . ($row - 1) . ' :</strong> Le client ' . $NUMERO_CLIENT . '--' . $NOM_CLIENT . ' est bien ajouté.</span>';
                                    echo '
													<div class="alert alert-success" role="alert">
														  <strong>Line' . ($row - 1) . ' :</strong> Le client ' . $NUMERO_CLIENT . '--' . $NOM_CLIENT . ' est bien ajouté.
														</div>
													';

                                    $elements .= $NUMERO_CLIENT . ',';

                                } else {
                                    echo '<br/>error insert client  : 	' . $sql . '<br/>' . mysqli_error($ma_connexion);
                                    $nook++;
                                    $content .= ' <br/> <span style="color:red"><strong>Line' . ($row - 1) . ' :</strong> Le client ' . $NUMERO_CLIENT . '--' . $NOM_CLIENT . ' existe déjà </span>';
                                    echo '
													<div class="alert alert-danger" role="alert">
														  <strong>Line' . ($row - 1) . ' :</strong> Le client ' . $NUMERO_CLIENT . '--' . $NOM_CLIENT . ' existe déjà.
														</div>
													';
                                }

                            }
                        } else {
                            $nook++;
                            $content .= ' <br/><span style="color:red"> <strong>Line' . ($row - 1) . ' :</strong> champs manquants.</span>';
                            echo '
										<div class="alert alert-danger" role="alert">
											  <strong>Line' . ($row - 1) . ' :</strong> champs manquants.
											</div>';

                        }
                    }

                    $total = $highestRow - 1;

                    $res_text = " $total lignes traitées : <br/>
								 $ok Nouveaux clients , <br/>
								 $nook  Erreurs";

                    $user_einvoicetrack = decode($_SESSION['user_einvoicetrack']);
                    $sql = " INSERT INTO `action`(`USER`, `TITRE`, `DESCRIPTION`) VALUES
										('$user_einvoicetrack','Gestion des clients','Importation des clients - <b> $res_text </b>')";
                    mysqli_query($ma_connexion, $sql);


                    $content = "<h3> $total lignes traitées </h3>  <h4 style='color:green'>  - $ok / $total Nouveaux clients </h4>  <h4 style='color:red'> - $nook / $total Erreurs </h4>  $content";


                    /*
                     * START SENDING EMAIL
                     */
                    $to = $EMAIL_USER_SESSION;
                    $name = $NOM_USER_SESSION;
                    $html = $content;


                    //Create an instance; passing `true` enables exceptions
                    $mail = new PHPMailer(true);


                    try {

//                        echo "stat sending mail" ;
                        //Server settings
                        //$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
                        $mail->isSMTP();                                            //Send using SMTP
                        $mail->Host = 'mail.einvoicetrack.net';                     //Set the SMTP server to send through
                        $mail->SMTPAuth = true;                                   //Enable SMTP authentication
                        $mail->Username = 'support@einvoicetrack.net';                     //SMTP username
                        $mail->Password = 'D~1tW5RO!+uN';                               //SMTP password
                        $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
                        $mail->Port = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

                        //    $mail->SMTPAuth   = true;
                        //    $mail->SMTPSecure = "tls";

                        //    $mail->SMTPOptions = array(
                        //        'ssl' => array(
                        //            'verify_peer' => false,
                        //            'verify_peer_name' => false,
                        //            'allow_self_signed' => true
                        //        )
                        //    );
                        //Recipients
                        $mail->setFrom('support@einvoicetrack.net', 'eInvoiceTrack');

                        $mail->addAddress($to);


//                        $mail->addAttachment('einvoicetrack.png');


                        $mail->isHTML(true);                                  // Set email format to HTML
                        $mail->Subject = 'RAPPORT OPERATION IMPORT FICHIER  eInvoieTrack';
                        $mail->Body = '
		 <html>
		   <head>
			  <style>
				 .banner-color {
				 background-color: #eb681f;
				 }
				 .title-color {
				 color: #33bbbb;
				 }
				 
				 @media screen and (min-width: 500px) {
				 .banner-color {
				 background-color: #33bbbb;
				 }
				 .title-color {
				 color: #eb681f;
				 }
				 .button-color {
				 background-color: #33bbbb;
				 }
				 }
			  </style>
		   </head>
		   <body>
			  <div style="background-color:#ececec;padding:0;margin:0 auto;font-weight:200;width:100%!important">
				 <table align="center" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
					<tbody>
					   <tr>
						  <td align="center">
							 <center style="width:100%">
								<table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" style="margin:0 auto;max-width:512px;font-weight:200;width:inherit;font-family:Helvetica,Arial,sans-serif" width="512">
								   <tbody>
									  <tr>
										 <td bgcolor="#F3F3F3" width="100%" style="background-color:#f3f3f3;padding:12px;border-bottom:1px solid #ececec">
											<table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;width:100%!important;font-family:Helvetica,Arial,sans-serif;min-width:100%!important" width="100%">
											   <tbody>
												  <tr>
													 <td align="left" valign="middle" width="50%"><span style="margin:0;color:#4c4c4c;white-space:normal;display:inline-block;text-decoration:none;font-size:12px;line-height:20px">eInvoiceTrack</span></td>
													 <td valign="middle" width="50%" align="right" style="padding:0 0 0 10px"><span style="margin:0;color:#4c4c4c;white-space:normal;display:inline-block;text-decoration:none;font-size:12px;line-height:20px">' . date('Y-m-d H:i:s') . '</span></td>
													 <td width="1">&nbsp;</td>
												  </tr>
											   </tbody>
											</table>
										 </td>
									  </tr>
									  <tr>
										 <td align="left">
											<table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
											   <tbody>
												  <tr>
													 <td width="100%">
														<table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
														   <tbody>
															  <tr>
																 <td align="center" bgcolor="#33bbbb" style="padding:20px 48px;color:#ffffff" class="banner-color">
																	<table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
																	   <tbody>
																		  <tr>
																			 <td align="center" width="100%">
																				<h1 style="padding:0;margin:0;color:#ffffff;font-weight:500;font-size:20px;line-height:24px">eInvoiceTrack</h1>
																			 </td>
																		  </tr>
																	   </tbody>
																	</table>
																 </td>
															  </tr>
									
															<tr>
																 <td align="center" style="padding:20px 0 10px 0">
																	<table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
																	   <tbody>
																		  <tr>
																			 <td align="center" width="100%" style="padding: 0 15px;text-align: justify;color: rgb(76, 76, 76);font-size: 12px;line-height: 18px;">
																				<h3 style="font-weight: 600; padding: 0px; margin: 0px; font-size: 16px; line-height: 24px; text-align: justify ;" class="title-color">Bonjour ' . $name . ',</h3>
																				<p style="margin: 20px 0 30px 0;font-size: 15px;text-align: justify ;">Opération d\'importation initiée.
 
																				 <br> <br>Voici le rapport d\'importation:  <br> ' . $html . '
																				<div style="font-weight: 200; text-align: justify ; margin: 25px;"><a  style="padding:0.6em 1em;border-radius:600px;font-size:14px;text-decoration:none;font-weight:bold; cursor:pointer ; color : #ffffff" class="button-color" href="https://skcconseil.einvoicetrack.com/login" target="_blanc" >eInvoiceTrack</a></div>
																			 </td>
																		  </tr>
																	   </tbody>
																	</table>
																 </td>
															  </tr>
															  <tr>
															  </tr>
															  <tr>
															  </tr>
														   </tbody>
														</table>
													 </td>
												  </tr>
											   </tbody>
											</table>
										 </td>
									  </tr>
									  <tr>
										 <td align="left">
											<table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" style="padding:0 24px;color:#999999;font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
											   <tbody>
												  <tr>
													 <td align="center" width="100%">
														<table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
														   <tbody>
															  <tr>
																 <td align="center" valign="middle" width="100%" style="border-top:1px solid #d9d9d9;padding:12px 0px 20px 0px;text-align:center;color:#4c4c4c;font-weight:200;font-size:12px;line-height:18px">Merci,
																	<br><b> Service client eInvoiceTrack </b>
																 </td>
															  </tr>
														   </tbody>
														</table>
													 </td>
												  </tr>
												  <tr>
													 <td align="center" width="100%">
														<table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
														   <tbody>
															  <tr>
																 <td align="center" style="padding:0 0 8px 0" width="100%"></td>
															  </tr>
														   </tbody>
														</table>
													 </td>
												  </tr>
											   </tbody>
											</table>
										 </td>
									  </tr>
								   </tbody>
								</table>
							 </center>
						  </td>
					   </tr>
					</tbody>
				 </table>
			  </div>
		   </body>
		</html>';
                        $mail->AltBody = strip_tags($mail->Body);

                        $mail->send();
//                         echo 'Message has been sent';
                    } catch (Exception $e) {
                        echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
                    }

                    /*
                     * END SENDING EMAIL
                     */




                } else {
                    echo '
								<div class="alert alert-danger" role="alert">
									le  fichier   <strong>  ' . $temp_name . ' </strong> est incompatible 
									</div>
								';
                }


            }
        }
    } else {
        echo "session request err";

    }
} else {
    echo "session err";

}


// $file = 'log/'.date('Y-m-d H-i-s').'.txt' ;
// $fp = fopen($file,"wb");
// fwrite($fp,$content);
// fclose($fp);


/*



*/

ob_end_flush();
?>


