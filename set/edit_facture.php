<?php
ob_start();
session_start();
include '../connexion.php';


if(isset($_SESSION['user_einvoicetrack']) && ( $_SESSION['role'] ==  'superadmin' ||  $_SESSION['role'] ==  'admin'  ) )
{
	// echo "ok" ;	
	// echo "current ".$_POST['current_list'] ; 
	// var_dump($_POST);
	if(isset($_POST['current_list']))
	{
		// echo "ok" ;	
		$current_list= urldecode(mysqli_real_escape_string($ma_connexion,$_POST['current_list']));



		// $edit_numero=mysqli_real_escape_string($ma_connexion,$_POST['edit_numero']);
		$edit_statut=mysqli_real_escape_string($ma_connexion,$_POST['edit_statut']);
		$edit_bc=mysqli_real_escape_string($ma_connexion,$_POST['edit_bc']);
		$edit_bl=mysqli_real_escape_string($ma_connexion,$_POST['edit_bl']);
		$edit_motif=$_POST['edit_motif'];
		$edit_date_distribution=$_POST['edit_date_distribution'];
		$edit_date_comptabilisation=$_POST['edit_date_comptabilisation'];
		$edit_description=$_POST['edit_description'];
		// echo "ok" ;	
		$SQL_check="SELECT `DATE_COMPTABILISATION`, `BC`, `BL`, `STATUS`, `DATE_CREATION`, `SOURCE`, `pdf` 
				FROM  facture
				WHERE NUM_FACTURE = '$current_list'
				AND STATUS != 2 
		";
		$query=mysqli_query($ma_connexion,$SQL_check);
		if(mysqli_num_rows($query) == 1)
		{
			while($row=mysqli_fetch_assoc($query))
			{

				$flag = 1 ;
				if($edit_date_distribution == '' )
				{
					echo "date distribution erronée " ; 
					$flag = 0 ; 
				}
				if($edit_statut == 2 || $edit_statut == 3 ) 
				{
					if(!$_FILES)
					{
						$flag = 0 ; 
						echo '
							<div class="alert alert-danger" role="alert">
								  <strong> </strong> Le fichier est obligatoire 
							</div>
						';
						
					}
					else 
					{
						if ( 0 < $_FILES['file']['error'] ) {
							// echo 'Error: ' . $_FILES['file']['error'] . '<br>';
							switch ($_FILES['file']['error']) {
								case UPLOAD_ERR_INI_SIZE:
									$message = "The uploaded file exceeds the upload_max_filesize directive in php.ini";
									break;
								case UPLOAD_ERR_FORM_SIZE:
									$message = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form";
									break;
								case UPLOAD_ERR_PARTIAL:
									$message = "The uploaded file was only partially uploaded";
									break;
								case UPLOAD_ERR_NO_FILE:
									$message = "No file was uploaded";
									break;
								case UPLOAD_ERR_NO_TMP_DIR:
									$message = "Missing a temporary folder";
									break;
								case UPLOAD_ERR_CANT_WRITE:
									$message = "Failed to write file to disk";
									break;
								case UPLOAD_ERR_EXTENSION:
									$message = "File upload stopped by extension";
									break;

								default:
									$message = "Unknown upload error";
									break;
							}
							$flag = 0 ; 
							echo '
								<div class="alert alert-danger" role="alert">
									  <strong> </strong> '.$message.'
								</div>
							';
						}
						
					}
					
				}
				if($flag)
				{		
// echo "ok" ;			
					$sql=" UPDATE `facture` SET 
									`STATUS` = '$edit_statut',
									`BC` = '$edit_bc',
									`BL` = '$edit_bl',
									`DATE_COMPTABILISATION` = '$edit_date_comptabilisation'
							WHERE `NUM_FACTURE`= '$current_list'  " ;

					if (mysqli_query($ma_connexion, $sql)) {
						
						$user_einvoicetrack = decode($_SESSION['user_einvoicetrack']);
						$sql=" INSERT INTO `action`(`USER`, `TITRE`, `DESCRIPTION`) VALUES
											('$user_einvoicetrack','Gestion des factures','Modification de la facture numéro : <b> $current_list </b>')" ;
						mysqli_query($ma_connexion, $sql);

						$is_file_edited = 0 ;
						if($_FILES)
						{
							if ( 0 < $_FILES['file']['error'] ) {
								// echo 'Error: ' . $_FILES['file']['error'] . '<br>';
								switch ($_FILES['file']['error']) {
									case UPLOAD_ERR_INI_SIZE:
										$message = "The uploaded file exceeds the upload_max_filesize directive in php.ini";
										break;
									case UPLOAD_ERR_FORM_SIZE:
										$message = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form";
										break;
									case UPLOAD_ERR_PARTIAL:
										$message = "The uploaded file was only partially uploaded";
										break;
									case UPLOAD_ERR_NO_FILE:
										$message = "No file was uploaded";
										break;
									case UPLOAD_ERR_NO_TMP_DIR:
										$message = "Missing a temporary folder";
										break;
									case UPLOAD_ERR_CANT_WRITE:
										$message = "Failed to write file to disk";
										break;
									case UPLOAD_ERR_EXTENSION:
										$message = "File upload stopped by extension";
										break;

									default:
										$message = "Unknown upload error";
										break;
								}

								echo $message;				
							}
							else {
								
								if(file_exists('../einvoicetrack/'.$current_list.'.pdf'))
								{
									if( rename('../einvoicetrack/'.$current_list.'.pdf', '../einvoicetrack_backup/'.$current_list.'_'.date('Y-m-d', strtotime($edit_date_distribution)).'.pdf') ) 	
									{
										// echo "rename" ;  
									}
								}
								move_uploaded_file($_FILES['file']['tmp_name'], '../einvoicetrack/' . $current_list .'.pdf');
								
								
								// $myimage = $_FILES['file']['name'] ; 
								// echo $myimage;
								
								
								$sql=" Update facture set pdf = 1 WHERE NUM_FACTURE = '$current_list';" ;
						
								if (mysqli_query($ma_connexion, $sql)) {

									$is_file_edited = 1 ;


								} else {
											echo "\n  : " . mysqli_error($ma_connexion);
								
								}	
								
								
								
							}
						}
						// echo "dat ".$edit_date_distribution ; 
						if($edit_statut == 1 ) {
							$edit_date_distribution = $edit_date_comptabilisation ;
						}



//						if($current_facture_status != $edit_statut){
							$sql2= "INSERT INTO `facture_status`(`NUM_FACTURE`, `CODE_STATUS`, `DATE`, `motif`, `description`, `is_file_edited` ) VALUES
															('$current_list','$edit_statut','$edit_date_distribution','$edit_motif','$edit_description','$is_file_edited'); ";

// echo $sql2 ;
							if (mysqli_query($ma_connexion, $sql2)) {
								// echo "hello " ;
								echo '1';

							}
							else
							{
								echo $sql2 ;
								echo '<br/>update probleme   : '. mysqli_error($ma_connexion);
							}
//						}else{
//							echo '1';
//						}



					} else {
						// echo "Error updating record: " . mysqli_error($ma_connexion);
					
					}
				}
					
					
				
			}
		}
		else 
		{
			echo " la facture $current_list n'existe pas ou elle est disribuée ";
			
		}

		

		
	}
	
	if(isset($_POST['current_delete']))
	{
		$current_delete= urldecode(mysqli_real_escape_string($ma_connexion,$_POST['current_delete']));
		
		
		$sql=" DELETE FROM  facture WHERE `NUM_FACTURE`= '$current_delete'  " ;

		
				if (mysqli_query($ma_connexion, $sql)) {
						echo '1';
						$user_einvoicetrack = decode($_SESSION['user_einvoicetrack']);
						$sql=" INSERT INTO `action`(`USER`, `TITRE`, `DESCRIPTION`) VALUES
											('$user_einvoicetrack','Gestion des factures','Suppression de la facture numero : <b> $current_delete </b>')" ;
						mysqli_query($ma_connexion, $sql);
				} else {
					echo "Error updating record: " . mysqli_error($ma_connexion);
				
				}	
	}
	
	
	if(isset($_POST['delete_code']))
	{
		
		foreach ($_POST['delete_code'] as $key => $delete_code)
		{
			
			$current_delete= urldecode(mysqli_real_escape_string($ma_connexion,$delete_code));
		
			
			$sql=" DELETE FROM  facture WHERE `NUM_FACTURE`= '$current_delete'  " ;

		
			if (mysqli_query($ma_connexion, $sql)) {
					echo '1';
			} else {
				echo "Error updating record: " . mysqli_error($ma_connexion);
			
			}	
			
				
		}
		$elements = implode( ',', $_POST['delete_code'] );
		$user_einvoicetrack = decode($_SESSION['user_einvoicetrack']);
		$sql=" INSERT INTO `action`(`USER`, `TITRE`, `DESCRIPTION`) VALUES
							('$user_einvoicetrack','Gestion des factures','Suppression des factures numéros :  [  <b> $elements ] </b>')" ;
		mysqli_query($ma_connexion, $sql);
		

	}
	
	if(isset($_POST['pdf_dwn']))
	{
		
		$current_list= urldecode(mysqli_real_escape_string($ma_connexion,$_POST['pdf_dwn']));
	
		if(file_exists('../einvoicetrack/'.$current_list.'.pdf'))
		{
			echo "einvoicetrack/$current_list.pdf" ; 
			// $SQL_check="SELECT 1 
					// FROM  facture f 
					// WHERE f.NUM_FACTURE = '$current_list'
					// AND f.pdf = 1 
			// ";
			// $query=mysqli_query($ma_connexion,$SQL_check);
			// if(mysqli_num_rows($query) == 1)
			// {
				// echo "einvoicetrack/$current_list.pdf" ; 
					
			// }
		}
		
	}
	
	
	
	
	if(isset($_POST['pdf_code']))
	{
		$urls = array() ;
		foreach ($_POST['pdf_code'] as $key => $pdf_code)
		{
			$current_list= urldecode(mysqli_real_escape_string($ma_connexion,$pdf_code));
		
			if(file_exists('../einvoicetrack/'.$current_list.'.pdf'))
			{
				$urls[] = "../einvoicetrack/$current_list.pdf" ; 
				// $SQL_check="SELECT 1 
						// FROM  facture f 
						// WHERE f.NUM_FACTURE = '$current_list'
						// AND f.pdf = 1 
				// ";
				// $query=mysqli_query($ma_connexion,$SQL_check);
				// if(mysqli_num_rows($query) == 1)
				// {
					// $urls[] = "../einvoicetrack/$current_list.pdf" ; 
						
				// }
			}
		}
		if(file_exists('export/export.zip'))
			unlink('export/export.zip');
		
		
		$zip = new ZipArchive();
		$filename = "export/export.zip";
		

		if ($zip->open($filename, ZipArchive::CREATE)!==TRUE) {
		  echo("cannot open <$filename>\n");
		}
		
		

		$zip->addFile('einvoicetrack.txt');
		foreach ($urls as $file) {
		  $zip->addFile($file);

		}

		$zip->close();

		echo 'set/'.$filename;

		
		// echo json_encode($urls);
		

	}
}

ob_end_flush();
?>


