<?php
ob_start();
session_start();
include '../connexion.php';

require 'PhpSpreadsheet/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;



if(isset($_SESSION['user_einvoicetrack']) && ( $_SESSION['role'] ==  'superadmin' ||  $_SESSION['role'] ==  'admin'  ) )
{
	
	$current_user = decode($_SESSION['user_einvoicetrack']) ; 					
	$SQL_SESSION="SELECT  `NOM_USER`, `PRENOM_USER`, `EMAIL_USER`,`CIVILITE_USER`,
			`CODE_ENTREPRISE`, `ROLE_USER`
		  FROM `user` 
		  WHERE CODE_USER = $current_user
		  AND actif= 1";
	$query_SESSION=mysqli_query($ma_connexion,$SQL_SESSION);
	if(mysqli_num_rows($query_SESSION) == 1)
	{
		while($row_SESSION=mysqli_fetch_assoc($query_SESSION))
		{	
				$NOM_USER_SESSION = $row_SESSION['CIVILITE_USER'].' ' .$row_SESSION['NOM_USER'];
				$EMAIL_USER_SESSION = $row_SESSION['EMAIL_USER'];
				
				
				$folder_name = 'upload/';

				

			if(!empty($_FILES))
			{
			 $temp_file = $_FILES['file']['tmp_name'];
			 $temp_name = $_FILES['file']['name'];
					
				

				$header_bc = array("N° BC");
				$header_client = array("Code client");
				$header_nomclient = array("Nom client");
				$header_dateEdit = array("date d'édition");
				$header_datenum = array("Date numérisation");
				


				
							$content = '' ; 
							$ok = 0 ; 
							$nook = 0 ; 
							
							$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($temp_file);
							$worksheet = $spreadsheet->getActiveSheet();
							
							if (   in_array($worksheet->getCellByColumnAndRow(1, 1)->getCalculatedValue(), $header_bc) 
								&& in_array($worksheet->getCellByColumnAndRow(2, 1)->getCalculatedValue(), $header_client) 
								&& in_array($worksheet->getCellByColumnAndRow(3, 1)->getCalculatedValue(), $header_nomclient) 
								&& in_array($worksheet->getCellByColumnAndRow(4, 1)->getCalculatedValue(), $header_dateEdit) 
								&& in_array($worksheet->getCellByColumnAndRow(5, 1)->getCalculatedValue(), $header_datenum) 
					
								)
							{
								

								$highestRow = $worksheet->getHighestRow(); // e.g. 10
								// echo $highestRow ; 
								for ($row = 2; $row <= $highestRow; ++$row) 
								{
									if ( ($add_bc = mysqli_real_escape_string($ma_connexion,$worksheet->getCellByColumnAndRow(1, $row)->getFormattedValue())) != '' 
										   && ($add_client = mysqli_real_escape_string($ma_connexion,$worksheet->getCellByColumnAndRow(2, $row)->getFormattedValue())) != ''  
										   && ($add_date_livraison = mysqli_real_escape_string($ma_connexion,$worksheet->getCellByColumnAndRow(4, $row)->getFormattedValue())) != ''  
										   && ($add_date_numerisation = mysqli_real_escape_string($ma_connexion,$worksheet->getCellByColumnAndRow(5, $row)->getFormattedValue())) != ''  
												)
									{
										$flag = 1 ; 
										$SQL_check="SELECT b.NUMERO_CLIENT
													FROM  bc b 
													WHERE b.BC = '$add_bc'
													
										";
										$query=mysqli_query($ma_connexion,$SQL_check);
										while($rowTest=mysqli_fetch_assoc($query))
										{
											if ( $rowTest['NUMERO_CLIENT']  == $add_client )
											{
												$nook++ ; 
												$content .= '  <br/> <span style="color:red"> <strong>Line'.($row-1).' :</strong> Le bon de commande << '.$add_bc.' >> est déja enregistré pour le meme client </span>';
												echo '
												<div class="alert alert-danger" role="alert">
													  <strong>Line'.($row-1).' :</strong> Le bon de commande << '.$add_bc.' >> est déja enregistré pour le même client '.$add_client.'.
													</div>';
												// echo "err " ;
												$flag = 0 ; 
											}
										}
										
										if(	$flag)
										{
											$sql_test2= "SELECT 1
											FROM client
											WHERE NUMERO_CLIENT = '$add_client' " ;  
											$query_test2=mysqli_query($ma_connexion,$sql_test2) ;
											if(mysqli_num_rows($query_test2) != 1)
											{
												
												$nook++ ; 
												$content .= '  <br/> <span style="color:red"> <strong>Line'.($row-1).' :</strong> Aucun client avec le numero : << '.$add_client.' >> n\'existe dans la base des données</span>';
												echo '
												<div class="alert alert-danger" role="alert">
													  <strong>Line'.($row-1).' :</strong> Aucun client avec le numero : << '.$add_client.' >>  n\'existe dans la base des données
													</div>';
											}
											else 
											{
												$add_date_livraison =  date("Y-m-d", strtotime($add_date_livraison));
												$add_date_numerisation =  date("Y-m-d", strtotime($add_date_numerisation));
												$sql= "INSERT INTO `bc`(`BC`, `NUMERO_CLIENT`,`DATE_EDITION`, `DATE_NUMERISATION`) VALUES
																		('$add_bc','$add_client','$add_date_livraison','$add_date_numerisation') ; "; 
												// echo $sql ;
												if (mysqli_query($ma_connexion, $sql)) {
													
													$ok++ ; 
													$content .= ' <br/> <span style="color:green"><strong>Line'.($row-1).' :</strong> Le bon de commande << '.$add_bc.' >> est bien ajouté.</span>' ; 
													echo '
													<div class="alert alert-success" role="alert">
														  <strong>Line'.($row-1).' :</strong> Le bon de commande << '.$add_bc.' >> est bien ajouté.
														</div>
													';
													
													if(file_exists('../dossier_bl/'.$add_bc.'.pdf'))
													{
														
														$sql=" Update bl set pdf = 1 WHERE bl = '$add_bc';" ;
					
															if (mysqli_query($ma_connexion, $sql)) {
																

																	
															} else {
																		echo "\n  : " . mysqli_error($ma_connexion);
															
															}
													}
													
												}
												else 
												{
													echo '<br/>update fournisseur : '. mysqli_error($ma_connexion); 
													$nook++ ; 
													$content .= ' <br/> <span style="color:orange"><strong>Line'.($row-1).' :</strong>Le bon de commande << '.$add_bc.' >> existe déjà . </span>' ; 
													echo '
													<div class="alert alert-danger" role="alert">
														  <strong>Line'.($row-1).' :</strong> Le bon de commande << '.$add_bc.' >> existe déjà .
														</div>
													';
												}
											}
										}
												
										
									}
									else
									{
										$nook++ ; 
										$content .= ' <br/><span style="color:red"> <strong>Line'.($row-1).' :</strong> champs manquants.</span>';
										echo '
										<div class="alert alert-danger" role="alert">
											  <strong>Line'.($row-1).' :</strong> champs manquants.
											</div>';
										
									}
								}
								$total = $highestRow - 1 ; 
								$res_text = "$total lignes traitées : $ok / $total Nouveaux bons de commande,  $nook / $total Erreurs ";
								$user_einvoicetrack = decode($_SESSION['user_einvoicetrack']);
								$sql=" INSERT INTO `action`(`USER`, `TITRE`, `DESCRIPTION`) VALUES
													('$user_einvoicetrack','Gestion des bons de commande','Imporrtation des bons de commande - <b> $res_text </b>')" ;
								mysqli_query($ma_connexion, $sql);
								
								$content = "<h3> $total lignes traitées </h3>  <h4 style='color:green'>  - $ok / $total Nouveaux bons de commande </h4>  <h4 style='color:red'> - $nook / $total Erreurs </h4>  $content" ; 

								
								$url = 'https://www.einvoicetrack.com/phpMailer/log.php';
								$data = array('to' => $EMAIL_USER_SESSION, 'name' => $NOM_USER_SESSION, 'html' => $content);

								$options = array(
									'http' => array(
										'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
										'method'  => 'POST',
										'content' => http_build_query($data)
									)
								);
								$context  = stream_context_create($options);
								$result = file_get_contents($url, false, $context);
								if ($result === FALSE) { 
										echo '
										<script>
												$.ajax({
												url : "https://www.einvoicetrack.com/phpMailer/index.php",
												type : "POST",
												data : {"to" : "'.$EMAIL_USER_SESSION.'","name":"'.$NOM_USER_SESSION.'", "html" : "'.$content.'"},
												success : function(code_html, statut){
													alert("ok");
												}
											});
											
										</script>';

								}
								
								
								
								
							}
							else 
							{
								echo '
								<div class="alert alert-danger" role="alert">
									le  fichier   <strong>  '.$temp_name.' </strong> est incompatible 
									</div>
								';
							}
							
		
			}
		}
	}
	else{
		echo "session request err" ;
		
	}
}
else{
	echo "session err" ;
	
}




// $file = 'log/'.date('Y-m-d H-i-s').'.txt' ; 
// $fp = fopen($file,"wb");
// fwrite($fp,$content);
// fclose($fp);



/*



*/

ob_end_flush();
?>


