<?php
ob_start();
session_start();
include '../connexion.php';


if(isset($_SESSION['user_einvoicetrack']) && ( $_SESSION['role'] ==  'superadmin' ||  $_SESSION['role'] ==  'admin'  ) )
{
	
	if(isset($_POST['current_list']))
	{
		
		$current_list= urldecode(mysqli_real_escape_string($ma_connexion,$_POST['current_list']));

// echo $current_list;

		// $edit_numero=mysqli_real_escape_string($ma_connexion,$_POST['edit_numero']);
		// $edit_bc=mysqli_real_escape_string($ma_connexion,$_POST['edit_bc']);
		$edit_date_livraison=mysqli_real_escape_string($ma_connexion,$_POST['edit_date_livraison']);
		$edit_date_numerisation=mysqli_real_escape_string($ma_connexion,$_POST['edit_date_numerisation']);
		$edit_bc = '' ; 
		$SQL_check="SELECT b.BC
					FROM  bc b 
					WHERE b.id = '$current_list'
					
		";
		$query=mysqli_query($ma_connexion,$SQL_check);
		while($row=mysqli_fetch_assoc($query))
		{
			$edit_bc = $row['BC'] ; 
		}
		
		$sql=" UPDATE `bc` SET 
									`DATE_EDITION` = '$edit_date_livraison',
									`DATE_NUMERISATION` = '$edit_date_numerisation'
							WHERE `id`= '$current_list'  " ;

					if (mysqli_query($ma_connexion, $sql)) {
						echo '1';
						$user_einvoicetrack = decode($_SESSION['user_einvoicetrack']);
						$sql=" INSERT INTO `action`(`USER`, `TITRE`, `DESCRIPTION`) VALUES
											('$user_einvoicetrack','Gestion des bons de commande','Modification du bon de commande numéro : <b> $current_list </b>')" ;
						mysqli_query($ma_connexion, $sql);
						if($_FILES)
						{
							if ( 0 < $_FILES['file']['error'] ) {
								// echo 'Error: ' . $_FILES['file']['error'] . '<br>';
								switch ($_FILES['file']['error']) {
									case UPLOAD_ERR_INI_SIZE:
										$message = "The uploaded file exceeds the upload_max_filesize directive in php.ini";
										break;
									case UPLOAD_ERR_FORM_SIZE:
										$message = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form";
										break;
									case UPLOAD_ERR_PARTIAL:
										$message = "The uploaded file was only partially uploaded";
										break;
									case UPLOAD_ERR_NO_FILE:
										$message = "No file was uploaded";
										break;
									case UPLOAD_ERR_NO_TMP_DIR:
										$message = "Missing a temporary folder";
										break;
									case UPLOAD_ERR_CANT_WRITE:
										$message = "Failed to write file to disk";
										break;
									case UPLOAD_ERR_EXTENSION:
										$message = "File upload stopped by extension";
										break;


									default:
										$message = "Unknown upload error";
										break;
								}

								echo $message;				
							}
							else {
								// move_uploaded_file($_FILES['file']['tmp_name'], '../dossier_bc/' . $current_list .'.pdf');
								
								move_uploaded_file($_FILES['file']['tmp_name'], '../dossier_bc/'.$current_list.'_'.$edit_bc.'.pdf');
						
								// $myimage = $_FILES['file']['name'] ; 
								// echo $myimage;
								
								
								$sql=" Update bc set pdf = 1 WHERE id = '$current_list';" ;
				
									if (mysqli_query($ma_connexion, $sql)) {
										
										// echo $sql ; 
											
									} else {
												echo "\n  : " . mysqli_error($ma_connexion);
									
									}	
								
								
								
								
							}
						}
						
						
					} else {
						echo "Error updating record: " . mysqli_error($ma_connexion);
					
					}

		

		
	}
	
	if(isset($_POST['current_delete']))
	{
		$current_delete= urldecode(mysqli_real_escape_string($ma_connexion,$_POST['current_delete']));
		
		
		$sql=" DELETE FROM  bc WHERE `id`= '$current_delete'  " ;
				// echo $sql ; 
		
				if (mysqli_query($ma_connexion, $sql)) {
						echo '1';
						$user_einvoicetrack = decode($_SESSION['user_einvoicetrack']);
						$sql=" INSERT INTO `action`(`USER`, `TITRE`, `DESCRIPTION`) VALUES
											('$user_einvoicetrack','Gestion des bons de commande','Suppression du bon de commande numéro : <b> $current_delete </b>')" ;
						mysqli_query($ma_connexion, $sql);
				} else {
					echo "Error updating record: " . mysqli_error($ma_connexion);
				
				}	
	}
	
	
	if(isset($_POST['delete_code']))
	{
		
		foreach ($_POST['delete_code'] as $key => $delete_code)
		{
			
			$current_delete= urldecode(mysqli_real_escape_string($ma_connexion,$delete_code));
		
			
			$sql=" DELETE FROM  bc WHERE `id`= '$current_delete'  " ;

		
				if (mysqli_query($ma_connexion, $sql)) {
						echo '1';
				} else {
					echo "Error updating record: " . mysqli_error($ma_connexion);
				
				}	
			
				
		}
		$elements = implode( ',', $_POST['delete_code'] );
		$user_einvoicetrack = decode($_SESSION['user_einvoicetrack']);
		$sql=" INSERT INTO `action`(`USER`, `TITRE`, `DESCRIPTION`) VALUES
							('$user_einvoicetrack','Gestion des bons de commandes','Suppression BCs numéros :  [  <b> $elements </b> ] ')" ;
		mysqli_query($ma_connexion, $sql);
		

	}
	
	if(isset($_POST['pdf_dwn']))
	{
		
		$current_list= urldecode(mysqli_real_escape_string($ma_connexion,$_POST['pdf_dwn']));
	
			if(file_exists('../dossier_bc/'.$current_list.'_'.$row['BC'].'.pdf'))
				{
						echo 'dossier_bc/'.$current_list.'_'.$row['BC'].'.pdf' ; 
				}
			// $SQL_check="SELECT b.BC
					// FROM  bc b 
					// WHERE b.id = '$current_list'
					// AND b.pdf = 1 
			// ";
			// $query=mysqli_query($ma_connexion,$SQL_check);
			// while($row=mysqli_fetch_assoc($query))
			// {
				// if(file_exists('../dossier_bc/'.$current_list.'_'.$row['BC'].'.pdf'))
				// {
						// echo 'dossier_bc/'.$current_list.'_'.$row['BC'].'.pdf' ; 
				// }
			// }
				
					
			
		
		
	}
	
	
	
	
	if(isset($_POST['pdf_code']))
	{
		$urls = array() ;
		foreach ($_POST['pdf_code'] as $key => $pdf_code)
		{
			$current_list= urldecode(mysqli_real_escape_string($ma_connexion,$pdf_code));
			if(file_exists('../dossier_bc/'.$current_list.'_'.$row['BC'].'.pdf'))
			{
					$urls[] = '../dossier_bc/'.$current_list.'_'.$row['BC'].'.pdf' ; 
			}
			
			// $SQL_check="SELECT b.BC
					// FROM  bc b 
					// WHERE b.id = '$current_list'
					// AND b.pdf = 1 
			// ";
			// $query=mysqli_query($ma_connexion,$SQL_check);
			// while($row=mysqli_fetch_assoc($query))
			// {
				// if(file_exists('../dossier_bc/'.$current_list.'_'.$row['BC'].'.pdf'))
				// {
						// $urls[] = '../dossier_bc/'.$current_list.'_'.$row['BC'].'.pdf' ; 
				// }
			// }
		
			
		}

		
		$zip = new ZipArchive();
		$filename = "export/export".encode(strtotime(date('Y-m-d H:i:s'))).".zip";
		

		if ($zip->open($filename, ZipArchive::CREATE)!==TRUE) {
		  echo("cannot open <$filename>\n");
		}
		
		

		$zip->addFile('einvoicetrack.txt');
		foreach ($urls as $file) {
		  $zip->addFile($file);

		}

		$zip->close();

		echo 'set/'.$filename;

		
		// echo json_encode($urls);
	}

}


ob_end_flush();
?>


