<?php
ob_start();
session_start();
include '../connexion.php';

require 'PhpSpreadsheet/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

if (isset($_SESSION['user_einvoicetrack']) && ($_SESSION['role'] == 'superadmin' || $_SESSION['role'] == 'admin')) {
    $current_user = decode($_SESSION['user_einvoicetrack']);
    $SQL_SESSION = "SELECT  `NOM_USER`, `PRENOM_USER`, `EMAIL_USER`,`CIVILITE_USER`,
			`CODE_ENTREPRISE`, `ROLE_USER`
		  FROM `user` 
		  WHERE CODE_USER = $current_user
		  AND actif= 1";
    $query_SESSION = mysqli_query($ma_connexion, $SQL_SESSION);
    if (mysqli_num_rows($query_SESSION) == 1) {
        while ($row_SESSION = mysqli_fetch_assoc($query_SESSION)) {
            $NOM_USER_SESSION = $row_SESSION['CIVILITE_USER'] . ' ' . $row_SESSION['NOM_USER'];
            $EMAIL_USER_SESSION = $row_SESSION['EMAIL_USER'];


            /*
             *  Folder contains ocr results file
             *
             */
            $foder_src = '../tmp/';


            if (!empty($_FILES)) {
                $temp_file = $_FILES['file']['tmp_name'];
                $temp_name = $_FILES['file']['name'];


                $header_num = array("Num facture");
                $header_code = array("Code client");
                $header_statut = array("STATUT");
                $header_date = array("Date distribution");
                $header_motif = array("motif rejet");
                $header_description = array("description motif");


                $content = '';
                $ok = 0;
                $okmaj = 0;
                $nook = 0;


                $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($temp_file);
                $worksheet = $spreadsheet->getActiveSheet();

                if (in_array($worksheet->getCellByColumnAndRow(1, 1)->getCalculatedValue(), $header_num)
                    && in_array($worksheet->getCellByColumnAndRow(2, 1)->getCalculatedValue(), $header_code)
                    && in_array($worksheet->getCellByColumnAndRow(3, 1)->getCalculatedValue(), $header_statut)
                    && in_array($worksheet->getCellByColumnAndRow(4, 1)->getCalculatedValue(), $header_date)
                    && in_array($worksheet->getCellByColumnAndRow(5, 1)->getCalculatedValue(), $header_motif)
                    && in_array($worksheet->getCellByColumnAndRow(6, 1)->getCalculatedValue(), $header_description)
                ) {

                    $elements = '';
                    $highestRow = $worksheet->getHighestRow(); // e.g. 10
                    for ($row = 2; $row <= $highestRow; ++$row) {
                        if (($NUM_FACTURE = mysqli_real_escape_string($ma_connexion, $worksheet->getCellByColumnAndRow(1, $row)->getFormattedValue())) != ''
                            && ($NUM_CLIENT = mysqli_real_escape_string($ma_connexion, $worksheet->getCellByColumnAndRow(2, $row)->getFormattedValue())) != ''
                            && ($STATUS = mysqli_real_escape_string($ma_connexion, $worksheet->getCellByColumnAndRow(3, $row)->getFormattedValue())) != ''
                            && ($DATE_DISTRIBUTION = mysqli_real_escape_string($ma_connexion, $worksheet->getCellByColumnAndRow(4, $row)->getFormattedValue())) != ''
                        ) {

//                            $DATE_COMPTABILISATION = NULL;
                            $DATE_DISTRIBUTION = date("Y-m-d", strtotime($DATE_DISTRIBUTION));
                            $MOTIF = mysqli_real_escape_string($ma_connexion, $worksheet->getCellByColumnAndRow(5, $row)->getFormattedValue());

                            $flag_motif_glob = 1;
                            if ($STATUS == 3) {
                                if ($MOTIF < 1 || $MOTIF > 9) {
                                    $nook++;
                                    $content .= ' <br/><span style="color:red"> <strong>Line' . ($row - 1) . ' :</strong> motif manquant pour la facture   ' . $NUM_FACTURE . '.</span>';
                                    echo '
													<div class="alert alert-danger" role="alert">
														  <strong>Line' . ($row - 1) . ' :</strong> motif manquant pour la facture   ' . $NUM_FACTURE . '.
														</div>';

                                    $flag_motif_glob = 0;


                                }


                            } else {
                                $MOTIF = '';
                            }

                            if ($flag_motif_glob) {

                                $DESCRIPTION = mysqli_real_escape_string($ma_connexion, $worksheet->getCellByColumnAndRow(6, $row)->getFormattedValue());


                                $sql_test = "SELECT NUMERO_CLIENT,STATUS
												FROM facture
												WHERE NUM_FACTURE = '$NUM_FACTURE' ";
                                $query_test = mysqli_query($ma_connexion, $sql_test);
                                // si la facture existe déjà
                                if (mysqli_num_rows($query_test) == 1) {
                                    while ($row_test = mysqli_fetch_assoc($query_test)) {
                                        // si la facture  est deja distribuée
                                        if ($row_test['STATUS'] == 2) {
                                            $nook++;
                                            $content .= ' <br/> <span style="color:orange"><strong> Line' . ($row - 1) . ' :</strong> La facture N° ' . $NUM_FACTURE . ' est déjà distribuée </span>';
                                            echo '
															<div class="alert alert-warning" role="alert">
															  <strong>Line' . ($row - 1) . ' :</strong> La facture N° ' . $NUM_FACTURE . ' est déjà distribuée.
															</div>';
                                        } else {
                                            // si la facture est non distribuée et les informations sonr corrects

                                            if ($row_test['NUMERO_CLIENT'] == $NUM_CLIENT) {
                                                // obligation des pdfs pour les facture distribuée et rejetes
                                                if ($STATUS == 2 || $STATUS == 3) {
                                                    $flag_pdf = 0;
                                                    if (file_exists($foder_src . $NUM_FACTURE . '.pdf')) {
                                                        if (rename($foder_src . DIRECTORY_SEPARATOR . $NUM_FACTURE . '.pdf', '../einvoicetrack/' . $NUM_FACTURE . '.pdf')) {
                                                            $flag_pdf = 1;
                                                            $sql = "UPDATE `facture` SET  STATUS = '$STATUS',pdf = 1 
																								WHERE NUM_FACTURE = '$NUM_FACTURE' ";

                                                            if (mysqli_query($ma_connexion, $sql)) {
                                                                $elements .= $NUM_FACTURE . ',';
                                                                $okmaj++;
                                                                $content .= ' <br/> <span style="color:blue"> <strong>Line' . ($row - 1) . ' :</strong> La facture N° ' . $NUM_FACTURE . ' est mise à jour. ( +PDF). </span>';
                                                                echo '
																						<div class="alert alert-primary" role="alert">
																							  <strong>Line' . ($row - 1) . ' :</strong> La facture N° ' . $NUM_FACTURE . ' est mise à jour. ( +PDF).
																						</div>';


                                                                $sql = "INSERT INTO `facture_status`(`NUM_FACTURE`, `CODE_STATUS`, `DATE`, `motif`, `description`, `is_file_edited`) VALUES
																																('$NUM_FACTURE','$STATUS','$DATE_DISTRIBUTION','$MOTIF','$DESCRIPTION','$flag_pdf'); ";

                                                                // echo $sql ;
                                                                if (mysqli_query($ma_connexion, $sql)) {


                                                                } else {
                                                                    // echo '<br/>update  : '. mysqli_error($ma_connexion);

                                                                }
                                                            } else {
                                                                $nook++;
                                                                $content .= '  <br/> <span style="color:red"> <strong>Line' . ($row - 1) . ' :</strong> Les données de la facture ' . $NUM_FACTURE . ' sont erronées.</span>';
                                                                echo '
																					<div class="alert alert-danger" role="alert">
																						  <strong>Line' . ($row - 1) . ' :</strong> Les données de la facture ' . $NUM_FACTURE . ' sont erronées..
																						</div>';

                                                            }

                                                        }

                                                    }
                                                    if (!$flag_pdf) {
                                                        $nook++;
                                                        $content .= '  <br/> <span style="color:red"> <strong>Line' . ($row - 1) . ' :</strong> Aucun PDF ne correspond à la  facture ' . $NUM_FACTURE . ' </span>';
                                                        echo '
																			<div class="alert alert-danger" role="alert">
																				  <strong>Line' . ($row - 1) . ' :</strong> Aucun PDF ne correspond à la  facture ' . $NUM_FACTURE . ' .
																				</div>';

                                                    }

                                                } // NON OBLIGATION DES PDFs pour les factures en cours et recus
                                                else {
                                                    $flag_pdf = 0;
                                                    if (file_exists($foder_src . $NUM_FACTURE . '.pdf')) {
                                                        if (rename($foder_src . DIRECTORY_SEPARATOR . $NUM_FACTURE . '.pdf', '../einvoicetrack/' . $NUM_FACTURE . '.pdf')) {
                                                            $flag_pdf = 1;
                                                            $sql = "UPDATE `facture` SET  STATUS = '$STATUS',pdf=1
																								WHERE NUM_FACTURE = '$NUM_FACTURE' ";

                                                            if (mysqli_query($ma_connexion, $sql)) {
                                                                $elements .= $NUM_FACTURE . ',';
                                                                $okmaj++;
                                                                $content .= ' <br/> <span style="color:blue"> <strong>Line' . ($row - 1) . ' :</strong> La facture N° ' . $NUM_FACTURE . ' est mise à jour. ( +PDF).</span>';
                                                                echo '
																						<div class="alert alert-primary" role="alert">
																							  <strong>Line' . ($row - 1) . ' :</strong> La facture N° ' . $NUM_FACTURE . ' est mise à jour. ( +PDF).
																						</div>';


                                                                $sql = "INSERT INTO `facture_status`(`NUM_FACTURE`, `CODE_STATUS`, `DATE`, `motif`, `description`, `is_file_edited`) VALUES
                                                                                                    ('$NUM_FACTURE','$STATUS','$DATE_DISTRIBUTION','$MOTIF','$DESCRIPTION','$flag_pdf'); ";

                                                                // echo $sql ;
                                                                if (mysqli_query($ma_connexion, $sql)) {


                                                                } else {
                                                                    // echo '<br/>update  : '. mysqli_error($ma_connexion);

                                                                }
                                                            } else {
                                                                $nook++;
                                                                $content .= '  <br/> <span style="color:red"> <strong>Line' . ($row - 1) . ' :</strong> Les données de la facture ' . $NUM_FACTURE . ' sont erronées.</span>';
                                                                echo '
																					<div class="alert alert-danger" role="alert">
																						  <strong>Line' . ($row - 1) . ' :</strong> Les données de la facture ' . $NUM_FACTURE . ' sont erronées..
																						</div>';

                                                            }
                                                        }

                                                    }

                                                    if (!$flag_pdf) {

                                                        $sql = "UPDATE `facture` SET  STATUS = '$STATUS'
																								WHERE NUM_FACTURE = '$NUM_FACTURE' ";

                                                        if (mysqli_query($ma_connexion, $sql)) {
                                                            $elements .= $NUM_FACTURE . ',';
                                                            $okmaj++;
                                                            $content .= ' <br/> <span style="color:blue"> <strong>Line' . ($row - 1) . ' :</strong> La facture N° ' . $NUM_FACTURE . ' est mise à jour. </span>';
                                                            echo '
																						<div class="alert alert-primary" role="alert">
																							  <strong>Line' . ($row - 1) . ' :</strong> La facture N° ' . $NUM_FACTURE . ' est mise à jour.
																						</div>';


                                                            $sql = "INSERT INTO `facture_status`(`NUM_FACTURE`, `CODE_STATUS`, `DATE`, `motif`, `description`) VALUES
																																('$NUM_FACTURE','$STATUS','$DATE_DISTRIBUTION','$MOTIF','$DESCRIPTION'); ";

                                                            // echo $sql ;
                                                            if (mysqli_query($ma_connexion, $sql)) {


                                                            } else {
                                                                echo '<br/>update  : ' . mysqli_error($ma_connexion);

                                                            }
                                                        } else {
                                                            $nook++;
                                                            $content .= '  <br/> <span style="color:red"> <strong>Line' . ($row - 1) . ' :</strong> Les données de la facture ' . $NUM_FACTURE . ' sont erronées.</span>';
                                                            echo '
																					<div class="alert alert-danger" role="alert">
																						  <strong>Line' . ($row - 1) . ' :</strong> Les données de la facture ' . $NUM_FACTURE . ' sont erronées..
																						</div>';

                                                        }


                                                    }

                                                }


                                            } // si la facture est non distribuée mais le client n'est pas identique
                                            else {

                                                $nook++;
                                                $content .= '  <br/><span style="color:red">  <strong>Line' . ($row - 1) . ' :</strong> La facture ' . $NUM_FACTURE . ' est enregistrée avec un code de client different  </span>';
                                                echo '
																<div class="alert alert-danger" role="alert">
																	  <strong>Line' . ($row - 1) . ' :</strong> La facture ' . $NUM_FACTURE . ' est enregistrée avec un code de client différent.
																	</div>';

                                            }

                                        }

                                    }

                                } // Une nouvelle facture
                                else {
                                    // echo "Une nouvelle facture";
                                    //test si client existe
                                    $sql_test2 = "SELECT 1
													FROM client
													WHERE NUMERO_CLIENT = '$NUM_CLIENT' ";
                                    $query_test2 = mysqli_query($ma_connexion, $sql_test2);
                                    if (mysqli_num_rows($query_test2) != 1) {

                                        $nook++;
                                        $content .= '  <br/> <span style="color:red"> <strong>Line' . ($row - 1) . ' :</strong> Aucun client avec le numero : << ' . $NUM_CLIENT . ' >> n\'existe dans la BD</span>';
                                        echo '
														<div class="alert alert-danger" role="alert">
															<strong>Line' . ($row - 1) . ' :</strong> Aucun client avec le numero : << ' . $NUM_CLIENT . ' >>  n\'existe dans la BD
														</div>';
                                    }// cas client existe
                                    else {
                                        // obligation des pdfs pour les facture distribuée et rejetes
                                        if ($STATUS == 2 || $STATUS == 3) {
                                            $flag_pdf = 0;
                                            if (file_exists($foder_src . $NUM_FACTURE . '.pdf')) {
                                                if (rename($foder_src . DIRECTORY_SEPARATOR . $NUM_FACTURE . '.pdf', '../einvoicetrack/' . $NUM_FACTURE . '.pdf')) {
                                                    $flag_pdf = 1;
                                                    $sql = "INSERT INTO `facture`(`NUM_FACTURE`, `NUMERO_CLIENT`,`STATUS`, `SOURCE`, `pdf`) VALUES
																							('$NUM_FACTURE','$NUM_CLIENT','$STATUS',2,1) ; ";
                                                    // echo $sql ;
                                                    if (mysqli_query($ma_connexion, $sql)) {
                                                        $elements .= $NUM_FACTURE . ',';
                                                        $ok++;
                                                        $content .= ' <br/> <span style="color:green"> <strong>Line' . ($row - 1) . ' :</strong> La facture ' . $NUM_FACTURE . ' est bien ajoutée. ( +PDF).</span>';
                                                        echo '
																		<div class="alert alert-success" role="alert">
																			  <strong>Line' . ($row - 1) . ' :</strong> La facture ' . $NUM_FACTURE . ' est bien ajoutée. ( +PDF).
																			</div>
																		';


                                                        $sql = "INSERT INTO `facture_status`(`NUM_FACTURE`, `CODE_STATUS`, `DATE`, `motif`, `description`, `is_file_edited`) VALUES
																											('$NUM_FACTURE','$STATUS','$DATE_DISTRIBUTION','$MOTIF','$DESCRIPTION','$flag_pdf'); ";
                                                        if (mysqli_query($ma_connexion, $sql)) {


                                                        } else {
                                                            echo '<br/>update  : ' . mysqli_error($ma_connexion);

                                                        }


                                                    } else {
                                                        $nook++;
                                                        $content .= '  <br/> <span style="color:red"> <strong>Line' . ($row - 1) . ' :</strong> Les données de la facture ' . $NUM_FACTURE . ' sont erronées.</span>';
                                                        echo '
																		<div class="alert alert-danger" role="alert">
																			  <strong>Line' . ($row - 1) . ' :</strong> Les données de la facture ' . $NUM_FACTURE . ' sont erronées..
																			</div>';

                                                    }
                                                }

                                            }
                                            // echo "ok" ;
                                            if (!$flag_pdf) {
                                                $nook++;
                                                $content .= '  <br/> <span style="color:red"> <strong>Line' . ($row - 1) . ' :</strong> Aucun PDF ne correspond à la  facture ' . $NUM_FACTURE . ' </span>';
                                                echo '
																<div class="alert alert-danger" role="alert">
																	  <strong>Line' . ($row - 1) . ' :</strong> Aucun PDF ne correspond à la facture ' . $NUM_FACTURE . ' .
																	</div>';


                                            }

                                        }// cas de facture recu ou en cours
                                        else {
                                            // echo "recu ou encour" ;
                                            $flag_pdf = 0;
                                            if (file_exists($foder_src . $NUM_FACTURE . '.pdf')) {
                                                if (rename($foder_src . DIRECTORY_SEPARATOR . $NUM_FACTURE . '.pdf', '../einvoicetrack/' . $NUM_FACTURE . '.pdf')) {
                                                    $flag_pdf = 1;
                                                    $sql = "INSERT INTO `facture`(`NUM_FACTURE`, `NUMERO_CLIENT`, `STATUS`, `SOURCE`, `pdf`) VALUES
																							('$NUM_FACTURE','$NUM_CLIENT','$STATUS',2,1) ; ";

                                                    if (mysqli_query($ma_connexion, $sql)) {
                                                        $elements .= $NUM_FACTURE . ',';
                                                        $ok++;
                                                        $content .= ' <br/> <span style="color:green"> <strong>Line' . ($row - 1) . ' :</strong> La facture ' . $NUM_FACTURE . ' est bien ajoutée. ( +PDF).</span>';
                                                        echo '
																		<div class="alert alert-success" role="alert">
																			  <strong>Line' . ($row - 1) . ' :</strong> La facture ' . $NUM_FACTURE . ' est bien ajoutée. ( +PDF).
																			</div>
																		';


                                                        $sql = "INSERT INTO `facture_status`(`NUM_FACTURE`, `CODE_STATUS`, `DATE`, `motif`, `description`, `is_file_edited`) VALUES
																											('$NUM_FACTURE','$STATUS','$DATE_DISTRIBUTION','$MOTIF','$DESCRIPTION','$flag_pdf'); ";
                                                        // echo $sql ;
                                                        if (mysqli_query($ma_connexion, $sql)) {


                                                        } else {

                                                        }


                                                    } else {
                                                        $nook++;
                                                        $content .= '  <br/> <span style="color:red"> <strong>Line' . ($row - 1) . ' :</strong> Les données de la facture ' . $NUM_FACTURE . ' sont erronées.</span>';
                                                        echo '
																		<div class="alert alert-danger" role="alert">
																			  <strong>Line' . ($row - 1) . ' :</strong> Les données de la facture ' . $NUM_FACTURE . ' sont erronées..
																			</div>';

                                                    }
                                                }

                                            }

                                            if (!$flag_pdf) {
                                                // echo "we are here" ;

                                                $sql = "INSERT INTO `facture`(`NUM_FACTURE`, `NUMERO_CLIENT`, `STATUS`, `SOURCE`, `pdf`) VALUES
																							('$NUM_FACTURE','$NUM_CLIENT','$STATUS',2,0) ; ";
                                                // echo $DATE_COMPTABILISATION ;
                                                // echo $sql ;
                                                if (mysqli_query($ma_connexion, $sql)) {
                                                    $elements .= $NUM_FACTURE . ',';
                                                    $ok++;
                                                    $content .= ' <br/> <span style="color:green"> <strong>Line' . ($row - 1) . ' :</strong> La facture ' . $NUM_FACTURE . ' est bien ajoutée.</span>';
                                                    echo '
																		<div class="alert alert-success" role="alert">
																			  <strong>Line' . ($row - 1) . ' :</strong> La facture ' . $NUM_FACTURE . ' est bien ajoutée. 
																			</div>
																		';


                                                    $sql = "INSERT INTO `facture_status`(`NUM_FACTURE`, `CODE_STATUS`, `DATE`, `motif`, `description`) VALUES
																											('$NUM_FACTURE','$STATUS','$DATE_DISTRIBUTION','$MOTIF','$DESCRIPTION'); ";
                                                    // echo $sql ;
                                                    if (mysqli_query($ma_connexion, $sql)) {


                                                    } else {

                                                    }


                                                } else {
                                                    $nook++;
                                                    $content .= '  <br/> <span style="color:red"> <strong>Line' . ($row - 1) . ' :</strong> Les données de la facture ' . $NUM_FACTURE . ' sont erronées.</span>';
                                                    echo '
																		<div class="alert alert-danger" role="alert">
																			  <strong>Line' . ($row - 1) . ' :</strong> Les données de la facture ' . $NUM_FACTURE . ' sont erronées..
																			</div>';

                                                }


                                            }


                                        }
                                    }


                                }
                            }
                        } else {
                            $nook++;
                            $content .= ' <br/><span style="color:red"> <strong>Line' . ($row - 1) . ' :</strong> champs manquants.</span>';
                            echo '
										<div class="alert alert-danger" role="alert">
											  <strong>Line' . ($row - 1) . ' :</strong> champs manquants.
											</div>';

                        }

                    }
                    $total = $highestRow - 1;
                    $res_text = "$total lignes traitées: $ok / $total Nouvelles factures  ,$okmaj / $total mise a jour, $nook / $total Erreurs ";
                    $user_einvoicetrack = decode($_SESSION['user_einvoicetrack']);
                    $sql = " INSERT INTO `action`(`USER`, `TITRE`, `DESCRIPTION`) VALUES
										('$user_einvoicetrack','Gestion des factures','Importation (Resultat distibution) des factures -  <b> $res_text </b>')";
                    mysqli_query($ma_connexion, $sql);

                    $content = "<h3> $total lignes traitées </h3>  <h4 style='color:green'>  - $ok / $total Nouvelles factures </h4>   <h4 style='color:blue'>  - $okmaj / $total Factures mise à jour  </h4>  <h4 style='color:red'> - $nook / $total Erreurs 	 </h4>  $content";

                    $url = 'https://www.einvoicetrack.com/phpMailer/log.php';
                    $data = array('to' => $EMAIL_USER_SESSION, 'name' => $NOM_USER_SESSION, 'html' => $content);

                    $options = array(
                        'http' => array(
                            'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                            'method' => 'POST',
                            'content' => http_build_query($data)
                        )
                    );
                    $context = stream_context_create($options);
                    $result = file_get_contents($url, false, $context);
                    if ($result === FALSE) {
                        echo '
										<script>
												$.ajax({
												url : "https://www.einvoicetrack.com/phpMailer/index.php",
												type : "POST",
												data : {"to" : "' . $EMAIL_USER_SESSION . '","name":"' . $NOM_USER_SESSION . '", "html" : "' . $content . '"},
												success : function(code_html, statut){
													alert("ok");
												}
											});
											
										</script>';

                    }
                } else {
                    echo '
								<div class="alert alert-danger" role="alert">
									le  fichier   <strong>  ' . $temp_name . ' </strong> est incompatible 
									</div>
								';
                }


                // echo $result ;


            }


        }
    } else {
        // header('Location: login');
    }


    // $i = 0 ;


    // $SQL="SHOW COLUMNS FROM client";
    // $query=mysqli_query($ma_connexion,$SQL);
    // while($row=mysqli_fetch_assoc($query))
    // {
    // echo '
    // <a class="toggle-vis" data-column="'.$i++.'">'.$row['Field'].'</a> -
    // ';
    // }
} else {
    // header('Location: login');
}


// $file = 'log/'.date('Y-m-d H-i-s').'.txt' ; 
// $fp = fopen($file,"wb");
// fwrite($fp,$content);
// fclose($fp);


/*



*/

ob_end_flush();
?>


