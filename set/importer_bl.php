<?php
ob_start();
session_start();
include '../connexion.php';

require 'PhpSpreadsheet/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;



use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require '../vendor/autoload.php';




if(isset($_SESSION['user_einvoicetrack']) && ( $_SESSION['role'] ==  'superadmin' ||  $_SESSION['role'] ==  'admin'  ) )
{
	
	$current_user = decode($_SESSION['user_einvoicetrack']) ; 					
	$SQL_SESSION="SELECT  `NOM_USER`, `PRENOM_USER`, `EMAIL_USER`,`CIVILITE_USER`,
			`CODE_ENTREPRISE`, `ROLE_USER`
		  FROM `user` 
		  WHERE CODE_USER = $current_user
		  AND actif= 1";
	$query_SESSION=mysqli_query($ma_connexion,$SQL_SESSION);
	if(mysqli_num_rows($query_SESSION) == 1)
	{
		while($row_SESSION=mysqli_fetch_assoc($query_SESSION))
		{	
				$NOM_USER_SESSION = $row_SESSION['CIVILITE_USER'].' ' .$row_SESSION['NOM_USER'];
				$EMAIL_USER_SESSION = $row_SESSION['EMAIL_USER'];
				
				
				$folder_name = 'upload/';

				

			if(!empty($_FILES))
			{
			 $temp_file = $_FILES['file']['tmp_name'];
			$temp_name = $_FILES['file']['name'];		
				

				$header_bl = array("N° BL");
				$header_client = array("Code client");
				$header_nomclient = array("Nom client");
				$header_bc = array("Responsable facturation");
				$header_dateliv = array("date livraison");
				$header_datenum = array("Date numérisation");

				$header_TYPE_VENTE = array("Type de vente");
				$header_TYPE_CLIENT = array("Type client");
				$header_BL_MANUEL = array("BL Manuel");
				$header_DATE_DOCUMENT = array("Date document");



				
							$content = '' ; 
							$ok = 0 ; 
							$nook = 0 ; 
							
							$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($temp_file);
							$worksheet = $spreadsheet->getActiveSheet();
							
							// echo $worksheet->getCellByColumnAndRow(1, 1)->getCalculatedValue().'<br>';
							// echo $worksheet->getCellByColumnAndRow(2, 1)->getCalculatedValue().'<br>';
							// echo $worksheet->getCellByColumnAndRow(3, 1)->getCalculatedValue().'<br>';
							// echo $worksheet->getCellByColumnAndRow(4, 1)->getCalculatedValue().'<br>';
							// echo $worksheet->getCellByColumnAndRow(5, 1)->getCalculatedValue().'<br>';
							// echo $worksheet->getCellByColumnAndRow(6, 1)->getCalculatedValue().'<br>';
							
							if (   in_array($worksheet->getCellByColumnAndRow(1, 1)->getCalculatedValue(), $header_bl) 
								&& in_array($worksheet->getCellByColumnAndRow(2, 1)->getCalculatedValue(), $header_client) 
								&& in_array($worksheet->getCellByColumnAndRow(3, 1)->getCalculatedValue(), $header_nomclient) 
								&& in_array($worksheet->getCellByColumnAndRow(4, 1)->getCalculatedValue(), $header_bc) 
								&& in_array($worksheet->getCellByColumnAndRow(5, 1)->getCalculatedValue(), $header_dateliv) 
								&& in_array($worksheet->getCellByColumnAndRow(6, 1)->getCalculatedValue(), $header_datenum)

								&& in_array($worksheet->getCellByColumnAndRow(7, 1)->getCalculatedValue(), $header_TYPE_VENTE)
								&& in_array($worksheet->getCellByColumnAndRow(8, 1)->getCalculatedValue(), $header_TYPE_CLIENT)
								&& in_array($worksheet->getCellByColumnAndRow(9, 1)->getCalculatedValue(), $header_BL_MANUEL)
								&& in_array($worksheet->getCellByColumnAndRow(10, 1)->getCalculatedValue(), $header_DATE_DOCUMENT)

								)
							{
								

								$highestRow = $worksheet->getHighestRow(); // e.g. 10
								for ($row = 2; $row <= $highestRow; ++$row) 
								{
									$add_bl = mysqli_real_escape_string($ma_connexion,$worksheet->getCellByColumnAndRow(1, $row)->getFormattedValue()) ;
									$add_client = mysqli_real_escape_string($ma_connexion,$worksheet->getCellByColumnAndRow(2, $row)->getFormattedValue())    ;
									$add_bc = mysqli_real_escape_string($ma_connexion,$worksheet->getCellByColumnAndRow(4, $row)->getFormattedValue())  ;
									$add_date_livraison = mysqli_real_escape_string($ma_connexion,$worksheet->getCellByColumnAndRow(5, $row)->getFormattedValue());
									$add_date_numerisation = mysqli_real_escape_string($ma_connexion,$worksheet->getCellByColumnAndRow(6, $row)->getFormattedValue()) ;


									$add_TYPE_VENTE = mysqli_real_escape_string($ma_connexion,$worksheet->getCellByColumnAndRow(7, $row)->getFormattedValue()) ;
									$add_TYPE_CLIENT = mysqli_real_escape_string($ma_connexion,$worksheet->getCellByColumnAndRow(8, $row)->getFormattedValue()) ;
									$add_BL_MANUEL = mysqli_real_escape_string($ma_connexion,$worksheet->getCellByColumnAndRow(9, $row)->getFormattedValue()) ;
									$add_DATE_DOCUMENT = mysqli_real_escape_string($ma_connexion,$worksheet->getCellByColumnAndRow(10, $row)->getFormattedValue()) ;

									if ( $add_bl != ''
										&& $add_client != ''
										&& $add_date_livraison != ''
									)
									{
										$sql_test2= "SELECT 1
										FROM client
										WHERE NUMERO_CLIENT = '$add_client' " ;  
										$query_test2=mysqli_query($ma_connexion,$sql_test2) ;
										if(mysqli_num_rows($query_test2) != 1)
										{

											$nook++ ;
											$content .= '  <br/> <span style="color:red"> <strong>Line'.($row-1).' :</strong> Aucun client avec le numero : << '.$add_client.' >> n\'existe dans la base des données</span>';
											echo '
											<div class="alert alert-danger" role="alert">
												  <strong>Line'.($row-1).' :</strong> Aucun client avec le numero : << '.$add_client.' >>  n\'existe dans la base des données
												</div>';
										}
										else
										{
											$add_date_livraison =  date("Y-m-d", strtotime($add_date_livraison));
											$add_date_numerisation =  date("Y-m-d", strtotime($add_date_numerisation));
											$sql= "INSERT INTO `bl`(`BL`, `NUMERO_CLIENT`,`BC`, `DATE_LIVRAISON`, `DATE_NUMERISATION`, `TYPE_VENTE`, `TYPE_CLIENT`, `BL_MANUEL`, `DATE_DOCUMENT`) VALUES
																	('$add_bl','$add_client','$add_bc','$add_date_livraison','$add_date_numerisation','$add_TYPE_VENTE','$add_TYPE_CLIENT','$add_BL_MANUEL','$add_DATE_DOCUMENT') ; ";


//											$sql= "INSERT INTO `bl`
//    													set `NUMERO_CLIENT` = `NUMERO_CLIENT`,
//    													set `NUMERO_CLIENT` = `NUMERO_CLIENT`,
//    													set `NUMERO_CLIENT` = `NUMERO_CLIENT`,
//    													set `NUMERO_CLIENT` = `NUMERO_CLIENT`,
//
//
//    (`BL`, `NUMERO_CLIENT`,`BC`, `DATE_LIVRAISON`, `DATE_NUMERISATION`, `TYPE_VENTE`, `TYPE_CLIENT`, `BL_MANUEL`, `DATE_DOCUMENT`) VALUES
//																	('$add_bl','$add_client','$add_bc','$add_date_livraison','$add_date_numerisation','$add_TYPE_VENTE','$add_TYPE_CLIENT','$add_BL_MANUEL','$add_DATE_DOCUMENT')
//																	  ON DUPLICATE KEY UPDATE
//																		 `NUMERO_CLIENT` = values(`NUMERO_CLIENT`),
//																		 `BC` = values(`BC`),
//																		 `DATE_LIVRAISON` = values(`DATE_LIVRAISON`),
//																		 `DATE_NUMERISATION` = values(`DATE_NUMERISATION`),
//																		 `NUMERO_CLIENT` = values(`NUMERO_CLIENT`),
//																		 `NUMERO_CLIENT` = values(`NUMERO_CLIENT`),
//																		 `NUMERO_CLIENT` = values(`NUMERO_CLIENT`),
//
//
//																		                   ; ";


											// echo $sql ;
											if (mysqli_query($ma_connexion, $sql)) {

												
												$ok++ ; 
												$content .= ' <br/> <span style="color:green"><strong>Line'.($row-1).' :</strong> Le bon de livraison << '.$add_bl.' >> est bien ajouté.</span>' ;
												echo '
												<div class="alert alert-success" role="alert">
													  <strong>Line'.($row-1).' :</strong> Le bon de livraison << '.$add_bl.' >> est bien ajouté.
													</div>
												';
												
												if(file_exists('../dossier_bl/'.$add_bl.'.pdf'))
												{
													
													$sql=" Update bl set pdf = 1 WHERE bl = '$add_bl';" ;
				
														if (mysqli_query($ma_connexion, $sql)) {
															

																
														} else {
																	echo "\n  : " . mysqli_error($ma_connexion);
														
														}
												}
												
											}
											else 
											{
												$sql_update=" Update bl 
 															set `NUMERO_CLIENT` = '$add_client', 
 																`BC` = '$add_bc', 
 																`DATE_LIVRAISON` = '$add_date_livraison', 
 																`DATE_NUMERISATION` = '$add_date_numerisation', 
 																`TYPE_VENTE` = '$add_TYPE_VENTE', 
 																`TYPE_CLIENT` = '$add_TYPE_CLIENT', 
 																`BL_MANUEL` = '$add_BL_MANUEL', 
 																`DATE_DOCUMENT` = '$add_DATE_DOCUMENT'
 																WHERE bl = '$add_bl';" ;

												if (mysqli_query($ma_connexion, $sql_update)) {
													$ok++ ;
													$content .= ' <br/> <span style="color:green"><strong>Line'.($row-1).' :</strong> Le bon de livraison << '.$add_bl.' >> est bien mise a jour.</span>' ;
													echo '
														<div class="alert alert-success" role="alert">
															  <strong>Line'.($row-1).' :</strong> Le bon de livraison << '.$add_bl.' >> est bien mise a jour.
															</div>
														';

												}else{
													echo "\n  : " . mysqli_error($ma_connexion);


													$nook++ ;
													$content .= ' <br/> <span style="color:orange"><strong>Line'.($row-1).' :</strong>Le bon de livraison << '.$add_bl.' >> existe déjà Erreur . </span>' ;
													echo '
												<div class="alert alert-danger" role="alert">
													  <strong>Line'.($row-1).' :</strong> Le bon de livraison << '.$add_bl.' >> existe déjà Erreur .
													</div>
												';
												}



											}
										}
												
										
									}
									else
									{
										$nook++ ; 
										$content .= ' <br/><span style="color:red"> <strong>Line'.($row-1).' :</strong> champs manquants.</span>';
										echo '
										<div class="alert alert-danger" role="alert">
											  <strong>Line'.($row-1).' :</strong> champs manquants.
											</div>';
										
									}
								}
								
								
								$total = $highestRow - 1 ; 
								
								$res_text = "$total lignes traitées : $ok / $total Nouveaux bons de livraison,  $nook / $total ERREURS ";
								$user_einvoicetrack = decode($_SESSION['user_einvoicetrack']);
								$sql=" INSERT INTO `action`(`USER`, `TITRE`, `DESCRIPTION`) VALUES
													('$user_einvoicetrack','Gestion des bons de livraison','Imporrtation des bons de livraison - <b> $res_text </b>')" ;
								mysqli_query($ma_connexion, $sql);
								
								$content = "<h3> $total lignes traitées </h3>  <h4 style='color:green'>  - $ok / $total Nouveaux bon de livraison </h4>  <h4 style='color:red'> - $nook / $total ERREURS </h4>  $content" ; 

								
//								$url = 'https://www.einvoicetrack.com/phpMailer/log.php';
//								$data = array('to' => $EMAIL_USER_SESSION, 'name' => $NOM_USER_SESSION, 'html' => $content);

								/*
                   * START SENDING EMAIL
                   */
								$to = $EMAIL_USER_SESSION;
								$name = $NOM_USER_SESSION;
								$html = $content;


								//Create an instance; passing `true` enables exceptions
								$mail = new PHPMailer(true);


								try {

//                        echo "stat sending mail" ;
									//Server settings
									//$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
									$mail->isSMTP();                                            //Send using SMTP
									$mail->Host = 'mail.einvoicetrack.net';                     //Set the SMTP server to send through
									$mail->SMTPAuth = true;                                   //Enable SMTP authentication
									$mail->Username = 'support@einvoicetrack.net';                     //SMTP username
									$mail->Password = 'D~1tW5RO!+uN';                               //SMTP password
									$mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
									$mail->Port = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

									//    $mail->SMTPAuth   = true;
									//    $mail->SMTPSecure = "tls";

									//    $mail->SMTPOptions = array(
									//        'ssl' => array(
									//            'verify_peer' => false,
									//            'verify_peer_name' => false,
									//            'allow_self_signed' => true
									//        )
									//    );
									//Recipients
									$mail->setFrom('support@einvoicetrack.net', 'eInvoiceTrack');

									$mail->addAddress($to);


//                        $mail->addAttachment('einvoicetrack.png');


									$mail->isHTML(true);                                  // Set email format to HTML
									$mail->Subject = 'RAPPORT OPERATION IMPORT FICHIER  eInvoieTrack';
									$mail->Body = '
		 <html>
		   <head>
			  <style>
				 .banner-color {
				 background-color: #eb681f;
				 }
				 .title-color {
				 color: #33bbbb;
				 }
				 
				 @media screen and (min-width: 500px) {
				 .banner-color {
				 background-color: #33bbbb;
				 }
				 .title-color {
				 color: #eb681f;
				 }
				 .button-color {
				 background-color: #33bbbb;
				 }
				 }
			  </style>
		   </head>
		   <body>
			  <div style="background-color:#ececec;padding:0;margin:0 auto;font-weight:200;width:100%!important">
				 <table align="center" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
					<tbody>
					   <tr>
						  <td align="center">
							 <center style="width:100%">
								<table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" style="margin:0 auto;max-width:512px;font-weight:200;width:inherit;font-family:Helvetica,Arial,sans-serif" width="512">
								   <tbody>
									  <tr>
										 <td bgcolor="#F3F3F3" width="100%" style="background-color:#f3f3f3;padding:12px;border-bottom:1px solid #ececec">
											<table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;width:100%!important;font-family:Helvetica,Arial,sans-serif;min-width:100%!important" width="100%">
											   <tbody>
												  <tr>
													 <td align="left" valign="middle" width="50%"><span style="margin:0;color:#4c4c4c;white-space:normal;display:inline-block;text-decoration:none;font-size:12px;line-height:20px">eInvoiceTrack</span></td>
													 <td valign="middle" width="50%" align="right" style="padding:0 0 0 10px"><span style="margin:0;color:#4c4c4c;white-space:normal;display:inline-block;text-decoration:none;font-size:12px;line-height:20px">' . date('Y-m-d H:i:s') . '</span></td>
													 <td width="1">&nbsp;</td>
												  </tr>
											   </tbody>
											</table>
										 </td>
									  </tr>
									  <tr>
										 <td align="left">
											<table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
											   <tbody>
												  <tr>
													 <td width="100%">
														<table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
														   <tbody>
															  <tr>
																 <td align="center" bgcolor="#33bbbb" style="padding:20px 48px;color:#ffffff" class="banner-color">
																	<table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
																	   <tbody>
																		  <tr>
																			 <td align="center" width="100%">
																				<h1 style="padding:0;margin:0;color:#ffffff;font-weight:500;font-size:20px;line-height:24px">eInvoiceTrack</h1>
																			 </td>
																		  </tr>
																	   </tbody>
																	</table>
																 </td>
															  </tr>
									
															<tr>
																 <td align="center" style="padding:20px 0 10px 0">
																	<table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
																	   <tbody>
																		  <tr>
																			 <td align="center" width="100%" style="padding: 0 15px;text-align: justify;color: rgb(76, 76, 76);font-size: 12px;line-height: 18px;">
																				<h3 style="font-weight: 600; padding: 0px; margin: 0px; font-size: 16px; line-height: 24px; text-align: justify ;" class="title-color">Bonjour ' . $name . ',</h3>
																				<p style="margin: 20px 0 30px 0;font-size: 15px;text-align: justify ;">Opération d\'importation initiée.
 
																				 <br> <br>Voici le rapport d\'importation:  <br> ' . $html . '
																				<div style="font-weight: 200; text-align: justify ; margin: 25px;"><a  style="padding:0.6em 1em;border-radius:600px;font-size:14px;text-decoration:none;font-weight:bold; cursor:pointer ; color : #ffffff" class="button-color" href="https://skcconseil.einvoicetrack.com/login" target="_blanc" >eInvoiceTrack</a></div>
																			 </td>
																		  </tr>
																	   </tbody>
																	</table>
																 </td>
															  </tr>
															  <tr>
															  </tr>
															  <tr>
															  </tr>
														   </tbody>
														</table>
													 </td>
												  </tr>
											   </tbody>
											</table>
										 </td>
									  </tr>
									  <tr>
										 <td align="left">
											<table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" style="padding:0 24px;color:#999999;font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
											   <tbody>
												  <tr>
													 <td align="center" width="100%">
														<table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
														   <tbody>
															  <tr>
																 <td align="center" valign="middle" width="100%" style="border-top:1px solid #d9d9d9;padding:12px 0px 20px 0px;text-align:center;color:#4c4c4c;font-weight:200;font-size:12px;line-height:18px">Merci,
																	<br><b> Service client eInvoiceTrack </b>
																 </td>
															  </tr>
														   </tbody>
														</table>
													 </td>
												  </tr>
												  <tr>
													 <td align="center" width="100%">
														<table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
														   <tbody>
															  <tr>
																 <td align="center" style="padding:0 0 8px 0" width="100%"></td>
															  </tr>
														   </tbody>
														</table>
													 </td>
												  </tr>
											   </tbody>
											</table>
										 </td>
									  </tr>
								   </tbody>
								</table>
							 </center>
						  </td>
					   </tr>
					</tbody>
				 </table>
			  </div>
		   </body>
		</html>';
									$mail->AltBody = strip_tags($mail->Body);

									$mail->send();
//                         echo 'Message has been sent';
								} catch (Exception $e) {
									echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
								}

								/*
                                 * END SENDING EMAIL
                                 */
								
								
								
								
							}
							else 
							{
								echo '
								<div class="alert alert-danger" role="alert">
									le  fichier   <strong>  '.$temp_name.' </strong> est incompatible 
									</div>
								';
							}
							
		
			}
		}
	}
	else{
		echo "session request err" ;
		
	}
}
else{
	echo "session err" ;
	
}




// $file = 'log/'.date('Y-m-d H-i-s').'.txt' ; 
// $fp = fopen($file,"wb");
// fwrite($fp,$content);
// fclose($fp);



/*



*/

ob_end_flush();
?>


