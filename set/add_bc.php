<?php
ob_start();
session_start();
include '../connexion.php';


if(isset($_SESSION['user_einvoicetrack']) && ( $_SESSION['role'] ==  'superadmin' ||  $_SESSION['role'] ==  'admin'  ) )
{
if (isset($_POST['add_bc']) && 
					isset($_POST['add_client']) && 
					isset($_POST['add_date_livraison']) && 
					isset($_POST['add_date_numerisation']) 
			 )
	{
		$flag = 1 ; 
		$add_bc = mysqli_real_escape_string($ma_connexion,$_POST['add_bc']) ;
		$add_client = urldecode(mysqli_real_escape_string($ma_connexion,$_POST['add_client'])) ;
		$add_date_livraison = mysqli_real_escape_string($ma_connexion,$_POST['add_date_livraison']) ;
		$add_date_numerisation = mysqli_real_escape_string($ma_connexion,$_POST['add_date_numerisation']) ;
		
		$SQL_check="SELECT b.NUMERO_CLIENT
					FROM  bc b 
					WHERE b.BC = '$add_bc'
					
		";
		$query=mysqli_query($ma_connexion,$SQL_check);
		while($row=mysqli_fetch_assoc($query))
		{
			if ( $row['NUMERO_CLIENT']  == $_POST['add_client'] )
			{
				echo '
				<div class="alert alert-danger" role="alert">
					  <strong/> Le bon de commande << '.$add_bc.' >> est déja enregistré pour le meme client '.$row['NUMERO_CLIENT'] .'.
					</div>
				';
				$flag = 0 ; 
			}
		}
		
		if(	$flag)
		{			
		
		
			$sql= "INSERT INTO `bc`(`BC`, `NUMERO_CLIENT`, `DATE_EDITION`, `DATE_NUMERISATION`) VALUES
									('$add_bc','$add_client','$add_date_livraison','$add_date_numerisation') ; "; 
			// echo $sql ;
			if (mysqli_query($ma_connexion, $sql)) {
				
				$bc_current = mysqli_insert_id($ma_connexion);
				
				
				
				$user_einvoicetrack = decode($_SESSION['user_einvoicetrack']);
				$sql=" INSERT INTO `action`(`USER`, `TITRE`, `DESCRIPTION`) VALUES
									('$user_einvoicetrack','Gestion des bons de commande','Ajout du bon de commande numéro : <b> $$add_bc </b>')" ;
				mysqli_query($ma_connexion, $sql);
				echo '
				<div class="alert alert-success" role="alert">
					  <strong/> Le bon de commande << '.$add_bc.' >> est bien ajouté.
					</div>
				';
				
				if($_FILES)
				{
					if ( 0 < $_FILES['file']['error'] ) {
						// echo 'Error: ' . $_FILES['file']['error'] . '<br>';
						switch ($_FILES['file']['error']) {
							case UPLOAD_ERR_INI_SIZE:
								$message = "The uploaded file exceeds the upload_max_filesize directive in php.ini";
								break;
							case UPLOAD_ERR_FORM_SIZE:
								$message = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form";
								break;
							case UPLOAD_ERR_PARTIAL:
								$message = "The uploaded file was only partially uploaded";
								break;
							case UPLOAD_ERR_NO_FILE:
								$message = "No file was uploaded";
								break;
							case UPLOAD_ERR_NO_TMP_DIR:
								$message = "Missing a temporary folder";
								break;
							case UPLOAD_ERR_CANT_WRITE:
								$message = "Failed to write file to disk";
								break;
							case UPLOAD_ERR_EXTENSION:
								$message = "File upload stopped by extension";
								break;

							default:
								$message = "Unknown upload error";
								break;
						}

						echo $message;				
					}
					else {
						move_uploaded_file($_FILES['file']['tmp_name'], '../dossier_bc/'.$bc_current.'_'.$add_bc.'.pdf');
						
						
						$myimage = $_FILES['file']['name'] ; 
						// echo $myimage;
						
						
						$sql=" Update bc set pdf = 1 WHERE id = '$bc_current';" ;
				
						if (mysqli_query($ma_connexion, $sql)) {
							

								
						} else {
									echo "\n  : " . mysqli_error($ma_connexion);
						
						}	
						
						
						
					}
				}

			}
			else 
			{
				echo "\n  : " . mysqli_error($ma_connexion);
				echo '
					<div class="alert alert-danger" role="alert">
						  <strong> </strong> Le bon de commande << '.$add_bc.' >> existe déja.
					</div>
				';
			}
		}
		
	}
	else echo "error";
			
	
}


ob_end_flush();
?>

