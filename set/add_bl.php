<?php
ob_start();
session_start();
include '../connexion.php';


if(isset($_SESSION['user_einvoicetrack']) && ( $_SESSION['role'] ==  'superadmin' ||  $_SESSION['role'] ==  'admin'  ) )
{
if (isset($_POST['add_bl']) && 
					isset($_POST['add_client']) && 
					isset($_POST['add_bc']) && 
					isset($_POST['add_date_livraison']) && 
					isset($_POST['add_date_numerisation']) 
			 )
	{
		
		
		
		
						
		$add_bl = mysqli_real_escape_string($ma_connexion,$_POST['add_bl']) ;
		$add_client = urldecode(mysqli_real_escape_string($ma_connexion,$_POST['add_client'])) ;
		$add_bc = mysqli_real_escape_string($ma_connexion,$_POST['add_bc']) ;
		$add_date_livraison = mysqli_real_escape_string($ma_connexion,$_POST['add_date_livraison']) ;
		$add_date_numerisation = mysqli_real_escape_string($ma_connexion,$_POST['add_date_numerisation']) ;

		$add_TYPE_VENTE = mysqli_real_escape_string($ma_connexion,$_POST['add_TYPE_VENTE']) ;
		$add_TYPE_CLIENT = mysqli_real_escape_string($ma_connexion,$_POST['add_TYPE_CLIENT']) ;
		$add_BL_MANUEL = mysqli_real_escape_string($ma_connexion,$_POST['add_BL_MANUEL']) ;
		$add_DATE_DOCUMENT = mysqli_real_escape_string($ma_connexion,$_POST['add_DATE_DOCUMENT']) ;

		
			$sql= "INSERT INTO `bl`(`BL`, `NUMERO_CLIENT`,`BC`, `DATE_LIVRAISON`, `DATE_NUMERISATION` , `TYPE_VENTE`, `TYPE_CLIENT`, `BL_MANUEL`, `DATE_DOCUMENT`) VALUES
									('$add_bl','$add_client','$add_bc','$add_date_livraison','$add_date_numerisation','$add_TYPE_VENTE','$add_TYPE_CLIENT','$add_BL_MANUEL','$add_DATE_DOCUMENT') ; ";
			// echo $sql ;
			if (mysqli_query($ma_connexion, $sql)) {
				$user_einvoicetrack = decode($_SESSION['user_einvoicetrack']);
				$sql=" INSERT INTO `action`(`USER`, `TITRE`, `DESCRIPTION`) VALUES
									('$user_einvoicetrack','Gestion des bons de livraison','Ajout du bon de livraison numéro : <b> $add_bl </b>')" ;
				mysqli_query($ma_connexion, $sql);
				echo '
				<div class="alert alert-success" role="alert">
					  <strong/> Le bon de livraison << '.$add_bl.' >> est bien ajouté.
					</div>
				';
				
				if($_FILES)
				{
					if ( 0 < $_FILES['file']['error'] ) {
						// echo 'Error: ' . $_FILES['file']['error'] . '<br>';
						switch ($_FILES['file']['error']) {
							case UPLOAD_ERR_INI_SIZE:
								$message = "The uploaded file exceeds the upload_max_filesize directive in php.ini";
								break;
							case UPLOAD_ERR_FORM_SIZE:
								$message = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form";
								break;
							case UPLOAD_ERR_PARTIAL:
								$message = "The uploaded file was only partially uploaded";
								break;
							case UPLOAD_ERR_NO_FILE:
								$message = "No file was uploaded";
								break;
							case UPLOAD_ERR_NO_TMP_DIR:
								$message = "Missing a temporary folder";
								break;
							case UPLOAD_ERR_CANT_WRITE:
								$message = "Failed to write file to disk";
								break;
							case UPLOAD_ERR_EXTENSION:
								$message = "File upload stopped by extension";
								break;

							default:
								$message = "Unknown upload error";
								break;
						}

						echo $message;				
					}
					else {
						move_uploaded_file($_FILES['file']['tmp_name'], '../dossier_bl/' . $add_bl .'.pdf');
						
						
						$myimage = $_FILES['file']['name'] ; 
						// echo $myimage;
						
						
						$sql=" Update bl set pdf = 1 WHERE BL = '$add_bl';" ;
				
						if (mysqli_query($ma_connexion, $sql)) {
							

								
						} else {
									echo "\n  : " . mysqli_error($ma_connexion);
						
						}	
						
						
						
					}
				}

			}
			else 
			{
				echo "\n  : " . mysqli_error($ma_connexion);
				echo '
					<div class="alert alert-danger" role="alert">
						  <strong> </strong> Le bon de livraison << '.$add_bl.' >> existe déja.
					</div>
				';
			}
		
	}
	else echo "error";
			
	
}


ob_end_flush();
?>

