<?php
ob_start();
session_start();
include '../connexion.php';

if(isset($_SESSION['user_einvoicetrack']) && ( $_SESSION['role'] ==  'superadmin' ||  $_SESSION['role'] ==  'admin'  ) )
{
	
	if(isset($_POST['current_list']))
	{
		// $edit_numero=mysqli_real_escape_string($ma_connexion,$_POST['edit_numero']);
		$edit_nom=mysqli_real_escape_string($ma_connexion,$_POST['edit_nom']);
		$edit_tel1=mysqli_real_escape_string($ma_connexion,$_POST['edit_tel1']);
		$edit_tel2=mysqli_real_escape_string($ma_connexion,$_POST['edit_tel2']);
		$edit_adresse1=mysqli_real_escape_string($ma_connexion,$_POST['edit_adresse1']);
		$edit_secteur1=mysqli_real_escape_string($ma_connexion,$_POST['edit_secteur1']);
		$edit_adresse2=mysqli_real_escape_string($ma_connexion,$_POST['edit_adresse2']);
		$edit_secteur2=mysqli_real_escape_string($ma_connexion,$_POST['edit_secteur2']);
		$edit_ville=mysqli_real_escape_string($ma_connexion,$_POST['edit_ville']);
		$edit_description=mysqli_real_escape_string($ma_connexion,$_POST['edit_description']);
		$edit_gps_lat=mysqli_real_escape_string($ma_connexion,$_POST['edit_gps_lat']);
		$edit_gps_long=mysqli_real_escape_string($ma_connexion,$_POST['edit_gps_long']);


		$edit_CONTACT=mysqli_real_escape_string($ma_connexion,$_POST['edit_CONTACT']);
		$edit_TYPE_CLIENT=mysqli_real_escape_string($ma_connexion,$_POST['edit_TYPE_CLIENT']);
		$edit_NATURE_CLIENT=mysqli_real_escape_string($ma_connexion,$_POST['edit_NATURE_CLIENT']);



		
		$current_list= urldecode(mysqli_real_escape_string($ma_connexion,$_POST['current_list']));

		$SQL="SELECT  `ETAT` FROM `client` WHERE NUMERO_CLIENT = '$current_list' ";
		$query=mysqli_query($ma_connexion,$SQL);
		while($row=mysqli_fetch_assoc($query))
		{
			if ( $row['ETAT'] != 1 ) 
				echo 'etat';
			else 
			{
				
				$sql=" UPDATE `client` SET 
									
									`NOM_CLIENT` = '$edit_nom',
									`TELE1` = '$edit_tel1',
									`TELE2` = '$edit_tel2',
									`ADRESSE1` = '$edit_adresse1',
									`SECTEUR1` = '$edit_secteur1',
									`ADRESSE2` = '$edit_adresse2',
									`SECTEUR2` = '$edit_secteur2',
									`VILLE` = '$edit_ville',
									`Longitude` = '$edit_gps_long',
									`Latitude` = '$edit_gps_lat',
									`DESCRIPTION` = '$edit_description',
									`CONTACT` = '$edit_CONTACT',
									`TYPE_CLIENT` = '$edit_TYPE_CLIENT',
									`NATURE_CLIENT` = '$edit_NATURE_CLIENT'
							WHERE `NUMERO_CLIENT`= '$current_list'  " ;

				if (mysqli_query($ma_connexion, $sql)) {
						
						echo '1';
						$user_einvoicetrack = decode($_SESSION['user_einvoicetrack']);
						$sql=" INSERT INTO `action`(`USER`, `TITRE`, `DESCRIPTION`) VALUES
											('$user_einvoicetrack','Gestion des clients','Modification de client numéro : <b> $current_list </b>')" ;
						mysqli_query($ma_connexion, $sql);
				} else {
//					echo $sql;
					echo "Error updating record: " . mysqli_error($ma_connexion);
				
				}

			}
		}
	}
	
	if(isset($_POST['current_delete']))
	{
		$current_delete= urldecode(mysqli_real_escape_string($ma_connexion,$_POST['current_delete']));


		$sql=" DELETE FROM  client 	WHERE `NUMERO_CLIENT`= '$current_delete'  " ;


		if (mysqli_query($ma_connexion, $sql)) {
			echo '1';


			$user_einvoicetrack = decode($_SESSION['user_einvoicetrack']);
			$sql=" INSERT INTO `action`(`USER`, `TITRE`, `DESCRIPTION`) VALUES
											('$user_einvoicetrack','Gestion des clients','Suppression de client numéro : <b> $current_delete </b>')" ;
			mysqli_query($ma_connexion, $sql);
		} else {
			echo "Error updating record: " . mysqli_error($ma_connexion);

		}


//		$sql1=" INSERT INTO `client_history`(`NUMERO_CLIENT`, `NOM_CLIENT`, `TELE1`, `TELE2`, `ADRESSE1`, `SECTEUR1`, `VILLE`, `Latitude`, `Longitude`, `ADRESSE2`, `SECTEUR2`, `VILLE2`, `Latitude2`, `Longitude2`, `DESCRIPTION`, `CODE_ENTREPRISE`)
//						SELECT
//						   `NUMERO_CLIENT`, `NOM_CLIENT`, `TELE1`, `TELE2`, `ADRESSE1`, `SECTEUR1`, `VILLE`, `Latitude`, `Longitude`, `ADRESSE2`, `SECTEUR2`, `VILLE2`, `Latitude2`, `Longitude2`, `DESCRIPTION`, `CODE_ENTREPRISE`
//						FROM
//						   client
//						WHERE `NUMERO_CLIENT`= '$current_delete'
//						; " ;
//
//
//
//		if (mysqli_query($ma_connexion, $sql1)) {
//				$sql=" DELETE FROM  client 	WHERE `NUMERO_CLIENT`= '$current_delete'  " ;
//
//
//				if (mysqli_query($ma_connexion, $sql)) {
//						echo '1';
//
//
//						$user_einvoicetrack = decode($_SESSION['user_einvoicetrack']);
//						$sql=" INSERT INTO `action`(`USER`, `TITRE`, `DESCRIPTION`) VALUES
//											('$user_einvoicetrack','Gestion des clients','Suppression de client numéro : <b> $current_delete </b>')" ;
//						mysqli_query($ma_connexion, $sql);
//				} else {
//					echo "Error updating record: " . mysqli_error($ma_connexion);
//
//				}
//		} else {
//			// echo "Error updating record: " . mysqli_error($ma_connexion);
//
//					$sqlX1=" delete from client_history WHERE NUMERO_CLIENT =  '$current_delete' " ;
//					if (mysqli_query($ma_connexion, $sqlX1)) {
//
//							$sql1=" INSERT INTO `client_history`(`NUMERO_CLIENT`, `NOM_CLIENT`, `TELE1`, `TELE2`, `ADRESSE1`, `SECTEUR1`, `VILLE`, `Latitude`, `Longitude`, `ADRESSE2`, `SECTEUR2`, `VILLE2`, `Latitude2`, `Longitude2`, `DESCRIPTION`, `CODE_ENTREPRISE`)
//											SELECT
//											   `NUMERO_CLIENT`, `NOM_CLIENT`, `TELE1`, `TELE2`, `ADRESSE1`, `SECTEUR1`, `VILLE`, `Latitude`, `Longitude`, `ADRESSE2`, `SECTEUR2`, `VILLE2`, `Latitude2`, `Longitude2`, `DESCRIPTION`, `CODE_ENTREPRISE`
//											FROM
//											   client
//											WHERE `NUMERO_CLIENT`= '$current_delete'
//											; " ;
//
//
//
//							if (mysqli_query($ma_connexion, $sql1)) {
//									$sql=" DELETE FROM  client 	WHERE `NUMERO_CLIENT`= '$current_delete'  " ;
//
//
//									if (mysqli_query($ma_connexion, $sql)) {
//											echo '1';
//											$user_einvoicetrack = decode($_SESSION['user_einvoicetrack']);
//											$sql=" INSERT INTO `action`(`USER`, `TITRE`, `DESCRIPTION`) VALUES
//											('$user_einvoicetrack','Gestion des clients','Suppression de client numero : <b> $current_delete </b>')" ;
//											mysqli_query($ma_connexion, $sql);
//
//									} else {
//										echo "Error updating record: " . mysqli_error($ma_connexion);
//
//									}
//							} else {
//								// echo "Error updating record: " . mysqli_error($ma_connexion);
//
//										$sqlX1=" delete from client_history WHERE NUMERO_CLIENT =  '$current_delete' " ;
//										if (mysqli_query($ma_connexion, $sqlX1)) {
//
//
//
//										} else {
//											echo "Error updating record: " . mysqli_error($ma_connexion);
//
//										}
//
//							}
//
//					} else {
//						echo "Error updating record: " . mysqli_error($ma_connexion);
//
//					}
//
//		}
	}
	
	
	if(isset($_POST['delete_code']))
	{
		foreach ($_POST['delete_code'] as $key => $delete_code)
		{
			
			$current_delete= urldecode(mysqli_real_escape_string($ma_connexion,$delete_code));
		
			
//			$sql1=" INSERT INTO `client_history`(`NUMERO_CLIENT`, `NOM_CLIENT`, `TELE1`, `TELE2`, `ADRESSE1`, `SECTEUR1`, `VILLE`, `Latitude`, `Longitude`, `ADRESSE2`, `SECTEUR2`, `VILLE2`, `Latitude2`, `Longitude2`, `DESCRIPTION`, `CODE_ENTREPRISE`)
//							SELECT
//							   `NUMERO_CLIENT`, `NOM_CLIENT`, `TELE1`, `TELE2`, `ADRESSE1`, `SECTEUR1`, `VILLE`, `Latitude`, `Longitude`, `ADRESSE2`, `SECTEUR2`, `VILLE2`, `Latitude2`, `Longitude2`, `DESCRIPTION`, `CODE_ENTREPRISE`
//							FROM
//							   client
//							WHERE `NUMERO_CLIENT`= '$current_delete'; " ;
//
//
//
//			if (mysqli_query($ma_connexion, $sql1)) {
//					$sql=" DELETE FROM  client 	WHERE `NUMERO_CLIENT`= '$current_delete'  " ;
//
//
//					if (mysqli_query($ma_connexion, $sql)) {
//							echo '1';
//					} else {
//						echo "Error updating record: " . mysqli_error($ma_connexion);
//
//					}
//			} else {
//				echo "Error updating record: " . mysqli_error($ma_connexion);
//
//			}

			$sql=" DELETE FROM  client 	WHERE `NUMERO_CLIENT`= '$current_delete'  " ;


			if (mysqli_query($ma_connexion, $sql)) {
				echo '1';
			} else {
				echo "Error updating record: " . mysqli_error($ma_connexion);

			}


			
				
		}
		
		$elements = implode( ',', $_POST['delete_code'] );
		$user_einvoicetrack = decode($_SESSION['user_einvoicetrack']);
		$sql=" INSERT INTO `action`(`USER`, `TITRE`, `DESCRIPTION`) VALUES
				('$user_einvoicetrack','Gestion des clients','Suppression des clients numéros: [  <b> $elements ] </b>')" ;
		mysqli_query($ma_connexion, $sql);

	}
}

ob_end_flush();
?>


