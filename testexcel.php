<?php
ob_start();
include '../connexion.php';

require 'PhpSpreadsheet/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load("X3.xlsx");
$worksheet = $spreadsheet->getActiveSheet();


$highestRow = $worksheet->getHighestRow(); // e.g. 10
// $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
// $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn); // e.g. 5
// $NUM_FACTURE = mysqli_real_escape_string($ma_connexion,$worksheet->getCellByColumnAndRow(1, $row)->getValue());
for ($row = 2; $row <= $highestRow; ++$row) 
{
	if ( ($NUM_FACTURE = mysqli_real_escape_string($ma_connexion,$worksheet->getCellByColumnAndRow(1, $row)->getValue())) != '' &&
		  ($NUM_CLIENT = mysqli_real_escape_string($ma_connexion,$worksheet->getCellByColumnAndRow(3, $row)->getValue())) != '' &&
			$worksheet->getCellByColumnAndRow(4, $row)->getValue() != ''   )
	{
		$flag = 1 ;
		$CODE_CLIENT= null ;
		
		
		$sql_test= "SELECT 1
		FROM facture
		WHERE NUM_FACTURE = '$NUM_FACTURE'" ;  
		$query_test=mysqli_query($ma_connexion,$sql_test) ;
		if(mysqli_num_rows($query_test) == 1)
		{
			echo '
			<div class="alert alert-danger" role="alert">
				  <strong>Line'.($row-1).' :</strong> Le numero de facture '.$NUM_FACTURE.' existe déjà.
				</div>
			';
			$flag = 0 ; 
		}
		
		$sql_test= "SELECT CODE_CLIENT
		FROM client
		WHERE NUMERO_CLIENT = '$NUM_CLIENT'" ;  
		$query_test=mysqli_query($ma_connexion,$sql_test) ;
		if(mysqli_num_rows($query_test) == 1)
		{
			while($rowtest = mysqli_fetch_assoc($query_test))
			{
				$CODE_CLIENT = $rowtest['CODE_CLIENT'];		
				
			}
		}else{
			
			echo '
			
			<div class="alert alert-danger" role="alert">
				  <strong>Line'.($row-1).' :</strong> Aucun client avec ce numero '.$NUM_CLIENT.' n\'existe dans la BD .
				</div>
			';
			
			$flag = 0 ; 
		}

		$value =  $worksheet->getCellByColumnAndRow(4, $row)->getValue() ; 
		$date = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value);
		$DATE_COMPTABILISATION = $date->format('Y-m-d H:i:s') ;
		$BC = mysqli_real_escape_string($ma_connexion,$worksheet->getCellByColumnAndRow(5, $row)->getValue());
		$BL = mysqli_real_escape_string($ma_connexion,$worksheet->getCellByColumnAndRow(6, $row)->getValue());		
		
		
		
		if ( $flag == 1 )
		{								
			$sql= "INSERT INTO `facture`(`NUM_FACTURE`, `CODE_CLIENT`, `DATE_COMPTABILISATION`, `BC`, `BL`) VALUES
										('$NUM_FACTURE','$CODE_CLIENT','$DATE_COMPTABILISATION','$BC','$BL') ; "; 
				if (mysqli_query($ma_connexion, $sql)) {
					
					echo '
				
					<div class="alert alert-success" role="alert">
						  <strong>Line'.($row-1).' :</strong> La facture '.$NUM_FACTURE.' est bien ajoutée.
						</div>
					';
					
				}
				else 
				{
					
				}
				
		}
	}
}

ob_end_flush();
?>


