<?php
ob_start();
session_start();
include 'connexion.php';
require_once('vendor/autoload.php');

ini_set('max_execution_time', 10000);
set_time_limit(10000);


$start = date_create(); // Current time and date

require 'PhpSpreadsheet/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


require_once dirname(__FILE__) . '/html2pdf/vendor/autoload.php';

use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;


require 'dompdf/vendor/autoload.php';

use Dompdf\Dompdf;

use Dompdf\Options;


// $res_html = '' ;


class ConcatPdf extends TCPDI
{
    public $files = array();

    public function setFiles($files)
    {
        $this->files = $files;
    }

    public function concat()
    {
        foreach ($this->files as $file) {
            $pageCount = $this->setSourceFile($file);
            for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
                $pageId = $this->ImportPage($pageNo);
                $s = $this->getTemplatesize($pageId);
                $this->AddPage();
                $this->useTemplate($pageId);
            }
        }
    }
}

if (isset($_SESSION['user_einvoicetrack'])) {

    $current_user = decode($_SESSION['user_einvoicetrack']);
    $SQL_SESSION = "SELECT  `NOM_USER`, `PRENOM_USER`, `EMAIL_USER`,`CIVILITE_USER`,
			`CODE_ENTREPRISE`, `ROLE_USER`
		  FROM `user` 
		  WHERE CODE_USER = $current_user
		  AND actif= 1";
    $query_SESSION = mysqli_query($ma_connexion, $SQL_SESSION);
    if (mysqli_num_rows($query_SESSION) == 1) {
        while ($row_SESSION = mysqli_fetch_assoc($query_SESSION)) {
            $NOM_USER_SESSION = $row_SESSION['CIVILITE_USER'] . ' ' . $row_SESSION['NOM_USER'];
            $EMAIL_USER_SESSION = $row_SESSION['EMAIL_USER'];


//            $folder_name = 'upload/';


            if (!empty($_FILES)) {
                $temp_file = $_FILES['file']['tmp_name'];
                $temp_name = $_FILES['file']['name'];


                $header_numFacture = array("N° facture");
                $header_nomClient = array("Nom client");
                $header_codeClient = array("Code client");
                $header_dateComptabilisation = array("Date comptabilisation");
                $header_numBC = array("N° BC");
                $header_numBL = array("N° BL");


                $content = '';
                $contentHTML = '';
                $ok = 0;
                $nook = 0;

                $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($temp_file);
                $worksheet = $spreadsheet->getActiveSheet();

                if (in_array($worksheet->getCellByColumnAndRow(1, 1)->getCalculatedValue(), $header_numFacture)
                    && in_array($worksheet->getCellByColumnAndRow(2, 1)->getCalculatedValue(), $header_nomClient)
                    && in_array($worksheet->getCellByColumnAndRow(3, 1)->getCalculatedValue(), $header_codeClient)
                    && in_array($worksheet->getCellByColumnAndRow(4, 1)->getCalculatedValue(), $header_dateComptabilisation)
                    && in_array($worksheet->getCellByColumnAndRow(5, 1)->getCalculatedValue(), $header_numBC)
                    && in_array($worksheet->getCellByColumnAndRow(6, 1)->getCalculatedValue(), $header_numBL)

                ) {

                    $rand_value = rand();
                    $foler_zip = 'MSE ' . date('Y-m-d H-i-s') . ' ' . $rand_value;
                    $dossier_job = 'mse_gene/' . $foler_zip;
                    mkdir($dossier_job, 0777, true);

                    $foler_zip_ar = 'AR ' . date('Y-m-d H-i-s') . ' ' . $rand_value;
                    $dossier_job_ar = 'ar_gene/' . $foler_zip_ar;
                    mkdir($dossier_job_ar, 0777, true);

//                    $total_files = 0;
                    $start = microtime(true);

                    $highestRow = $worksheet->getHighestRow(); // e.g. 10
                    for ($row = 2; $row <= $highestRow; ++$row) {

                        $value_numFacture = mysqli_real_escape_string($ma_connexion, $worksheet->getCellByColumnAndRow(1, $row)->getFormattedValue());
                        $value_nomClient = mysqli_real_escape_string($ma_connexion, $worksheet->getCellByColumnAndRow(2, $row)->getFormattedValue());
                        $value_codeClient = mysqli_real_escape_string($ma_connexion, $worksheet->getCellByColumnAndRow(3, $row)->getFormattedValue());
                        $value_dateComptabilisation = mysqli_real_escape_string($ma_connexion, $worksheet->getCellByColumnAndRow(4, $row)->getFormattedValue());
                        $value_numBC = mysqli_real_escape_string($ma_connexion, $worksheet->getCellByColumnAndRow(5, $row)->getFormattedValue());
                        $value_numBL = mysqli_real_escape_string($ma_connexion, $worksheet->getCellByColumnAndRow(6, $row)->getFormattedValue());


                        /*
                         * AR GENERATION


                        if ($_SESSION['role'] == 'superadmin' || $_SESSION['role'] == 'admin') {
                            $res_html = $_POST['html'];
                        } else {

                            $SQL = "SELECT  HTML
                              FROM `ar_template` 
                              ORDER BY CODE DESC LIMIT 1 ";
                            $query = mysqli_query($ma_connexion, $SQL);

                            while ($row2 = mysqli_fetch_assoc($query)) {
                                $res_html = $row2['HTML'];
                            }

                        }

                        $res_html = str_replace("[Code client]", $value_nomClient, $res_html);
                        $res_html = str_replace("[Nom client]", $value_codeClient, $res_html);
                        $res_html = str_replace("[Adresse]", $value_numBC, $res_html);
                        $res_html = str_replace("[Ville]", $value_numBL, $res_html);
                        $res_html = str_replace("[Num facture]", $value_numFacture, $res_html);
                        $res_html = str_replace("[Date  comptabilisation]", $value_dateComptabilisation, $res_html);
                        $res_html = str_replace("[Date aujourd'hui]", date("d/m/y"), $res_html);

                        $options = new Options();
                        $options->set('isRemoteEnabled', true);
                        $dompdf = new Dompdf($options);
                        // $dompdf = new Dompdf();
                        $dompdf->loadHtml('<body style="padding: .2in;">' . $res_html . '</body>');
                        $dompdf->setPaper('A4', 'portrait');
                        $dompdf->render();
                        $output = $dompdf->output();

                        $pdf_ar_fileName = $dossier_job_ar . '/' . ($row - 1) . '_' . $value_numFacture . '.pdf';
                        file_put_contents($pdf_ar_fileName, $output);

                        /*
                         * END AR GENERATION
                         */


                        $pdf = new ConcatPdf();
                        $files_array = array();

                        $warning_bl_not_found = "";
                        $warning_bl_not_found_content = "";

                        if (file_exists('mse_src/' . $value_numFacture . '.pdf')) {
                            array_push($files_array, 'mse_src/' . $value_numFacture . '.pdf');
                        }

                        if (!empty($value_numBL)) {
                            if (file_exists('dossier_bl/' . $value_numBL . '.pdf')) {
                                array_push($files_array, 'dossier_bl/' . $value_numBL . '.pdf');
                            } else {
//                                $warning_bl_not_found = true;
                                $warning_bl_not_found = '
                                <div class="alert alert-warning" role="alert">
                                      <strong>Line' . ($row - 1) . ' :</strong> WARNING Le BL num = ' . $value_numBL . ' n\'existe pas pour la facture num = ' . $value_numFacture . '.
                                    </div>
                                ';
                                $warning_bl_not_found_content = ' <span style="color:orange"> WARNING Le BL num = ' . $value_numBL . ' n\'existe pas pour la facture num = ' . $value_numFacture . '. </span>';
                            }
                        }

                     //  if (file_exists($pdf_ar_fileName)) {
                     //      array_push($files_array, $pdf_ar_fileName);
                     //  }

//                        $total_files++;
                        try {
                            $pdf->setFiles($files_array);
                            $pdf->concat();
                            $pdf->Output(__DIR__ . '/' . $dossier_job . '/' . $value_numFacture . '.pdf', 'F');
                            $ok++;
                            $content .= ' <br/><span style="color:green"> <strong>Line' . ($row - 1) . ' :</strong> MSE de la  facture ' . $value_numFacture . ' est bien génèré.</span>  '.$warning_bl_not_found_content.'';
                            $contentHTML .= '
                                <div class="alert alert-success" role="alert">
                                      <strong>Line' . ($row - 1) . ' :</strong> MSE de la  facture ' . $value_numFacture . ' est bien génèré.
                                    '.$warning_bl_not_found.'
                                    </div>
                                ';
                        } catch (Exception $e) {
                            $nook++;
                            $content .= ' <br/><span style="color:red"> <strong>Line' . ($row - 1) . ' :</strong> MSE Erreur -  facture ' . $value_numFacture . '.</span>';
                            $contentHTML .= '
                                <div class="alert alert-danger" role="alert">
                                      <strong>Line' . ($row - 1) . ' :</strong> MSE  Erreur -  facture ' . $value_numFacture . ' .
                                    </div>
                                ';
                        }

                    }

//                    echo $content;

                    // $jobs = simplexml_load_file('ar_gene/data_xml.xml');
                    // $job = $jobs->addChild('job');
                    // $job->addAttribute('id', $dossier_job);
                    // $job->addChild('total', $total_files);
                    // $job->addChild('time', $time_elapsed_secs = microtime(true) - $start);
                    // $job->addChild('user', $EMAIL_USER_SESSION);
                    // file_put_contents('ar_gene/data_xml.xml', $jobs->asXML());

                    $total = $highestRow - 1;
                    $user_einvoicetrack = decode($_SESSION['user_einvoicetrack']);
                    $time_elapsed_secs = microtime(true) - $start;
//                    $sql = " INSERT INTO `ar_list`(`NOM_`,`TOTAL_`, `USER_`, `TEMP_`) VALUES
//							('$dossier_job','$total','$EMAIL_USER_SESSION','$time_elapsed_secs')";
//                    mysqli_query($ma_connexion, $sql);


                    $res_text = "$total lignes traitées : $ok / $total Nouveaux  MSE, $nook / $total Erreurs";
                    $sql = " INSERT INTO `action`(`USER`, `TITRE`, `DESCRIPTION`) VALUES
							('$user_einvoicetrack','MSE','Importation des MSE - <b> $res_text </b>')";
                    mysqli_query($ma_connexion, $sql);

                    $content = "<h3> $total lignes traitées </h3>  <h5 style='color:green'>  - $ok / $total Nouveaux  MSE </h5> <h5 style='color:red'> - $nook / $total Erreurs </h5>  $content";
                    $contentHTML = "<h3> $total lignes traitées </h3>  <h5 style='color:green'>  - $ok / $total Nouveaux  MSE </h5> <h5 style='color:red'> - $nook / $total Erreurs </h5>  $contentHTML";
//                    echo $content;
                    echo $contentHTML;
                    $log_file = fopen(__DIR__ . '/' . $dossier_job . '/log_file.html', "w");
                    fwrite($log_file, $content);
                    fclose($log_file);


                    $url = 'https://www.einvoicetrack.com/phpMailer/index.php';
                    $data = array('to' => $EMAIL_USER_SESSION, 'name' => $NOM_USER_SESSION, 'html' => $content);

                    $options = array(
                        'http' => array(
                            'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                            'method' => 'POST',
                            'content' => http_build_query($data)
                        )
                    );
                    $context = stream_context_create($options);
                    $result = file_get_contents($url, false, $context);
                    if ($result === FALSE) {
                        echo '
							<script>
									$.ajax({
									url : "https://www.einvoicetrack.com/phpMailer/index.php",
									type : "POST",
									data : {"to" : "' . $EMAIL_USER_SESSION . '","name":"' . $NOM_USER_SESSION . '", "html" : "' . $content . '"},
									success : function(code_html, statut){
										alert("ok");
									}
								});
								
							</script>';

                    }

                    echo '
					
					<a href="' . $dossier_job . '/' . $foler_zip . '.zip' . '" download style="text-decoration: underline;">
					 Télécharger le fichier .zip
					</a>
					
					';
                    // echo $dossier_job.'/'.$foler_zip.'.zip' ;
                    // Get real path for our folder
                    $rootPath = realpath($dossier_job);

                    // Initialize archive object
                    $zip = new ZipArchive();
                    $zip->open($dossier_job . '/' . $foler_zip . '.zip', ZipArchive::CREATE | ZipArchive::OVERWRITE);

                    // Create recursive directory iterator
                    /** @var SplFileInfo[] $files */
                    $files = new RecursiveIteratorIterator(
                        new RecursiveDirectoryIterator($rootPath),
                        RecursiveIteratorIterator::LEAVES_ONLY
                    );

                    foreach ($files as $name => $file) {
                        // Skip directories (they would be added automatically)
                        if (!$file->isDir()) {
                            // Get real and relative path for current file
                            $filePath = $file->getRealPath();
                            $relativePath = substr($filePath, strlen($rootPath) + 1);

                            // Add current file to archive
                            $zip->addFile($filePath, $relativePath);
                        }
                    }

                    // Zip archive will be created only after closing object
                    $zip->close();

                    // echo $dossier_job.'.zip';


                } else {
                    echo '
					<div class="alert alert-danger" role="alert">
						le  fichier   <strong>  ' . $temp_name . ' </strong> est incompatible 
						</div>
					';
                }
            }


        }
    } else {
        echo "session request err";

    }
} else {
    echo "session err";

}


// $end 	= date_create(); 
// $diff  	= date_diff( $start, $end );


// $content = ob_get_clean();
// try
// {
// ob_end_clean();
// $output_file = 'ar_gene/ar'.strtotime(date('Y-m-d H:i:s')).'.pdf';
// $html2pdf = new HTML2PDF('P', 'A4', 'fr', true, 'UTF-8',array(18, 10, 18, 10));
// $html2pdf->setDefaultFont("Arial");
// $html2pdf->pdf->SetProtection(array('print','copy'));
// $html2pdf->writeHTML($content);
// $html2pdf->Output(__DIR__ . '/'.$output_file, 'F');

// echo $output_file ;

// }
// catch(HTML2PDF_exception $e) {
// echo $e;
// exit;
// }


?>