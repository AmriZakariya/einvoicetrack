<?php
ob_start();
session_start();

include 'connexion.php';


if(isset($_SESSION['user_einvoicetrack']) )
{
	header('Location: index');
}


$emailErr = "";
$flag = 1;



function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>eInvoiceTrack</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
	
	
	<!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <!-- iCheck -->
    <script src="js/plugins/iCheck/icheck.min.js"></script>
	
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

    <script>
        $(document).ready(function(){
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
		
    </script>
	
	
	<style>
	.required{
		color : red ; 
		float: left;
	}
	</style>

</head>





<body class="gray-bg">





<?php


if( isset($_POST['submit']) )
{
	if (isset($_POST['email']))
	{
		
		
		
		$EMAIL_USER=mysqli_real_escape_string($ma_connexion,$_POST['email']);
		if (empty($EMAIL_USER)) {
			$emailErr = "l'email est obligatoire";
			$flag = 0 ; 
		  } else {
			$EMAIL_USER = test_input($EMAIL_USER);
			if (!filter_var($EMAIL_USER, FILTER_VALIDATE_EMAIL)) {
			  $emailErr = "Email invalide"; 
			  $flag = 0 ; 
			}
		}
		
		
		if( $flag == 1 ) 
		{
			
			$SQL="SELECT  `NOM_USER`,`CODE_USER`
					  FROM `user` 
					  WHERE EMAIL_USER = '$EMAIL_USER'
					  AND actif= 1";
					  // echo $SQL ;
				$query=mysqli_query($ma_connexion,$SQL);
				if(mysqli_num_rows($query) == 1)
				{
					while($row=mysqli_fetch_assoc($query))
					{	
							$CODE_USER = encode2($row['CODE_USER']);
							$NOM_USER = $row['NOM_USER'];
							$flag =   encode(strtotime(date('Y-m-d H:i:s')));
							
							
							echo '
							<script>
									$.ajax({
									url : "https://www.einvoicetrack.com/phpMailer/reset.php",
									type : "POST",
									data : {"name":"'.$NOM_USER.'", "to" : "'.$EMAIL_USER.'","user" : "'.$CODE_USER.'","flag" : "'.$flag.'"},
									success : function(code_html, statut){
									},
								});
								
								
								Swal.fire({
								  type: "success",
								  title: "Veuillez vérifier votre boîte de réception ",
								  showConfirmButton: false,
								  timer: 3000
								})
								
							</script>';
							
							// echo '
							
							// $.ajax({
									// url : "https://www.einvoicetrack.com/phpMailer/reset.php",
									// type : "POST",
									// cache: true,
									// global: false,
									// data : {"name":"'.$NOM_USER.'", "to" : "'.$EMAIL_USER.'","user" : "'.$CODE_USER.'","flag" : "'.$flag.'"},
									// success : function(code_html, statut){
									// },
								// });
								
							
							
							// ';
							
							// header( "refresh:3;url=login" );
					}
				}
				else 
				{
						echo '
						<script>
											
							
							Swal.fire({
							  type: "error",
							  title: "Utilisateur n\'existe pas ou non actif ",
							  showConfirmButton: false,
							  timer: 2000
							})
							
						</script> ';
						// echo '<br/>'. mysqli_error($ma_connexion);
						$BigErr = "informations erronées -";
				}
				
				

				
		}
		else 
		{
				echo '
				<script>
									
					
					Swal.fire({
					  type: "error",
					  title: "informations erronées ",
					  showConfirmButton: false,
					  timer: 2000
					})
					
				</script> ';
				// echo '<br/>'. mysqli_error($ma_connexion);
				$BigErr = "informations erronées -";
		}
		
	}
	else 
	{
		echo '
				<script>
									
					
					Swal.fire({
					  type: "error",
					  title: "informations erronées ",
					  showConfirmButton: false,
					  timer: 2000
					})
					
				</script> ';
				// echo '<br/>'. mysqli_error($ma_connexion);
				$BigErr = "informations erronées -";
	}	
}
	
?>

     <div class="passwordBox animated fadeInDown">
        <div class="row">

            <div class="col-md-12">
				<a class="btn btn-success center m-b" href="login"><i class="fa fa-arrow-left"></i>&nbsp; Retour</a>

                <div class="ibox-content">

                    <h2 class="font-bold">Mot de passe oublié</h2>

                    <p>
                        Entrez votre adresse e-mail pour recevoir un lien de réinitialisation dans votre courrier électronique.
                    </p>

                    <div class="row">

                        <div class="col-lg-12">
                            <form class="m-t" method="POST">
                                <div class="form-group">
									<span class="label label-info float-left">Email  </span>  <span class="required">* <?php echo $emailErr;?> </span>
                                    <input type="email" name="email" class="form-control" placeholder="adresse e-mail" required>
                                </div>

                                <button type="submit" name="submit" class="btn btn-primary block full-width m-b">Envoyer un nouveau mot de passe</button>
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-6">
                eInvoiceTrack
            </div>
            <div class="col-md-6 text-right">
               <small>© 2019</small>
            </div>
        </div>
    </div>

    
</body>

</html>




<?php


ob_end_flush();
?>

