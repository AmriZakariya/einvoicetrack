<?php
ob_start();
session_start();
include 'connexion.php';


if(isset($_SESSION['user_einvoicetrack']) )
{
	header('Location: index');
}


$currentlogin = '';
if(isset($_GET['token']))
{
	$token = decode($_GET['token']);
	$sql= "SELECT EMAIL_USER
			FROM `user` WHERE CODE_USER = '$token' ; "; 
	$query=mysqli_query($ma_connexion,$sql) ;
	
	// echo $sql ;

	if(mysqli_num_rows($query) == 1)
	{
		while($row = mysqli_fetch_assoc($query))
		{
			
			$currentlogin = $row['EMAIL_USER'];
		}
	}
	
}

?>	
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>eInvoieTrack</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
	
	
	
	<style>
	.required{
		color : red ; 
		float: left;
	}
	
	p{
		
		font-size : 150%;
		
	}
	</style>


</head>

<body class="gray-bg">

    <div class="middle-box text-center  animated fadeInDown">
        <div>
            <div>

                <h1 class="logo-name">eIT</h1>

            </div>
            <h3>Bienvenue sur eInvoieTrack</h3>
            <p>Solution web pour suivi des factures et liasses clients.
                <!--Continually expanded and constantly improved Inspinia Admin Them (IN+)-->
            </p>
			<h3>Authentification</h3>
            <form class="m-t" method="POST" >
                <div class="form-group">
					<span class="label label-info float-left">login  </span> <span class="required">* </span>
                    <input type="email" class="form-control" value="<?php echo $currentlogin ;?>" name="login" placeholder="login" required="">
                </div>
                <div class="form-group">
					<span class="label label-info float-left">Mot de passe  </span> <span class="required">* </span>
                    <input type="password" class="form-control" name="password" placeholder="Mot de passe" required="">
                </div>
                <button type="submit" name="submit" class="btn btn-primary block full-width m-b">Login</button>

                <a href="reset_password"><p>Mot de passe oublié?</p></a>
                <p class="text-muted text-center"><p>Vous n'avez pas de compte?</p></p>
<!--                <a class="btn btn-lg btn-secondary " href="register">Inscription</a>-->
            </form>
            <p class="m-t"> <p>eInvoiceTrack</p> </p>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>


</body>

</html>



<?php
if(!empty($_POST) OR !empty($_FILES))
{

    $_SESSION['sauvegarde'] = $_POST ;

    $_SESSION['sauvegardeFILES'] = $_FILES ;

    

    $fichierActuel = $_SERVER['PHP_SELF'] ;

    if(!empty($_SERVER['QUERY_STRING']))

    {

        $fichierActuel .= '?' . $_SERVER['QUERY_STRING'] ;

    }

    

    header('Location: ' . $fichierActuel);

    exit;

}


if(isset($_SESSION['sauvegarde']))

{

    $_POST = $_SESSION['sauvegarde'] ;

    $_FILES = $_SESSION['sauvegardeFILES'] ;

    

    unset($_SESSION['sauvegarde'], $_SESSION['sauvegardeFILES']);

}



if(isset($_POST['submit']))
{	

	$login =  mysqli_real_escape_string($ma_connexion,$_POST["login"]) ;
	// $mdp =  $_POST["password"] ;
	$mdp =  mysqli_real_escape_string($ma_connexion,$_POST["password"]) ;
	
		
	
		
			
	$sql= "SELECT CODE_USER,actif,ROLE_USER
			FROM `user` WHERE EMAIL_USER = '$login' AND MDP_USER = '$mdp'  ; "; 
	$query=mysqli_query($ma_connexion,$sql) ;

	if(mysqli_num_rows($query) == 1)
	{
		while($row = mysqli_fetch_assoc($query))
		{
			
			if( $row['actif'] == 0 ) 
			{
				echo '
					<script>
						Swal.fire({
						  type: "error",
						  title: "Votre compte n\'est pas activé",
						  showConfirmButton: false,
						  timer: 3000
						})
					</script>';
				
			}
			else 
			{
					$_SESSION['user_einvoicetrack'] =  encode($row['CODE_USER']) ;	
					$_SESSION['role'] = 'undifined' ; 
					if($row['ROLE_USER'] == 0 )
						$_SESSION['role'] =  'superadmin' ;
					else if($row['ROLE_USER'] == 1 )
						$_SESSION['role'] =  'user' ;	
					else if($row['ROLE_USER'] == 2 )
						$_SESSION['role'] =  'admin' ;
				
				header('Location: index');
			}

		}
	}
	else{
		
		
				echo '
					<script>
						Swal.fire({
						  type: "error",
						  title: "Votre login ou mot de passe est incorrect",
						  showConfirmButton: false,
						  timer: 3000
						})
					</script>';
	}
}
	
ob_end_flush();
?>