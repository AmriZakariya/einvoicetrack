CREATE DATABASE IF NOT EXISTS `invoicetrack`;

USE `invoicetrack`;

SET foreign_key_checks = 0;

DROP TABLE IF EXISTS `action`;

CREATE TABLE `action` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER` int(11) NOT NULL,
  `TITRE` varchar(200) DEFAULT NULL,
  `DESCRIPTION` text,
  `DATE` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  KEY `USER` (`USER`),
  CONSTRAINT `fk_action_user` FOREIGN KEY (`USER`) REFERENCES `user` (`CODE_USER`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

INSERT INTO `action` VALUES (1,1,"Gestion des clients","Ajout de client numéro : <b> AE175778 </b>","2019-07-11 23:19:09"),
(2,1,"Gestion des bons de commande","Ajout du bon de commande numéro : <b> 01 </b>","2019-07-11 23:19:37"),
(3,1,"Gestion des bons de livraison","Ajout du bon de livraison numéro : <b> 01 </b>","2019-07-11 23:20:26"),
(4,1,"Gestion des factures","Ajout de la facture numéro : <b> Maroc </b>","2019-07-11 23:21:08"),
(5,1,"Gestion des clients","Ajout de client numéro : <b> AE175778 </b>","2019-07-11 23:23:22"),
(6,1,"Gestion des factures","Ajout de la facture numéro : <b> EF01 </b>","2019-07-11 23:23:47"),
(7,1,"Gestion des clients","Ajout de client numéro : <b> AE175778 </b>","2019-07-11 23:29:41"),
(8,1,"Gestion des factures","Ajout de la facture numéro : <b> F01 </b>","2019-07-11 23:30:03"),
(9,1,"Gestion des factures","Suppression de la facture numero : <b> EF01 </b>","2019-07-11 23:30:16"),
(10,1,"Gestion des factures","Modification de la facture numéro : <b> F01 </b>","2019-07-11 23:31:43"),
(11,1,"Gestion des Accusés de réception","Modification template","2019-07-15 20:18:03"),
(12,1,"Gestion des Accusés de réception","Modification template","2019-07-15 20:18:08"),
(13,25,"Gestion des Accusés de réception","Importation des Accusés de réception - <b> 1 lignes traitées : 1 / 1 Nouveaux  ARs, 0 / 1 Erreurs </b>","2019-07-15 20:27:29"),
(14,25,"Gestion des Accusés de réception","Importation des Accusés de réception - <b> 406 lignes traitées : 406 / 406 Nouveaux  ARs, 0 / 406 Erreurs </b>","2019-07-15 21:52:07"),
(15,25,"Gestion des Accusés de réception","Importation des Accusés de réception - <b> 1176 lignes traitées : 1176 / 1176 Nouveaux  ARs, 0 / 1176 Erreurs </b>","2019-07-16 09:45:13");


DROP TABLE IF EXISTS `ar_list`;

CREATE TABLE `ar_list` (
  `CODE_` int(11) NOT NULL AUTO_INCREMENT,
  `NOM_` varchar(200) DEFAULT NULL,
  `DATE_` datetime DEFAULT CURRENT_TIMESTAMP,
  `TOTAL_` int(11) DEFAULT NULL,
  `USER_` varchar(200) DEFAULT NULL,
  `TEMP_` float DEFAULT NULL,
  PRIMARY KEY (`CODE_`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO `ar_list` VALUES (1,"ar_gene/AR 2019-07-15 19-27-28 1178899385","2019-07-15 20:27:29",1,"zack00.el@gmail.com","1.1769"),
(2,"ar_gene/AR 2019-07-15 20-51-55 325885354","2019-07-15 21:52:07",406,"zack00.el@gmail.com","11.8984"),
(3,"ar_gene/AR 2019-07-16 08-44-29 1938726525","2019-07-16 09:45:13",1176,"zack00.el@gmail.com","44.1956");


DROP TABLE IF EXISTS `ar_template`;

CREATE TABLE `ar_template` (
  `CODE` int(11) NOT NULL AUTO_INCREMENT,
  `DATE` datetime DEFAULT CURRENT_TIMESTAMP,
  `HTML` longtext NOT NULL,
  `user` int(11) DEFAULT NULL,
  PRIMARY KEY (`CODE`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

INSERT INTO `ar_template` VALUES (1,"2019-06-26 12:14:45","<div style=\"width: 25%; text-align: left;\">\t\r\n\t\t\t\t\t\t\t\t<img style=\"width: 100%;\" src=\"logo.png\" alt=\"Logo\">\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<div style=\"width: 100%; text-align: right; margin-top:-90px\">\t\r\n\t\t\t\t\t\t\t\t<b>Témara</b> le  <b> [Date aujourd\'hui]</b><br><br>\r\n\t\t\t\t\t\t\t\t<b>[Code client]<br><br>\r\n\t\t\t\t\t\t\t\t<b>[Nom client]</b><br><br>\r\n\t\t\t\t\t\t\t\t<b>[Adresse]</b><br><br>\r\n\t\t\t\t\t\t\t\t<b>[Ville]</b><br><br>\r\n\t\t\t\t\t\t\t</div>\r\n\r\n\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t<br>\t\r\n\t\t\t\t\t\t\t<br>\t\r\n\t\t\t\t\t\t\t<b><u>Objet</u>: Accusé réception des factures</b><br>\r\n\t\t\t\t\t\t\t<br>\r\n\t\t\t\t\t\t\t<br>\r\n\t\t\t\t\t\t\tMessieurs,<br>\r\n\t\t\t\t\t\t\t <br>\r\n\t\t\t\t\t\t\tPar la présente, vous accusez de réception des factures :<br>\r\n\t\t\t\t\t\t\t <br>\r\n\t\t\t\t\t\t\tN° : <b>[Num facture] </b> <br>\r\n\t\t\t\t\t\t\t <br>\r\n\t\t\t\t\t\t\tDate comptabilisation : <b> [Date  comptabilisation] </b> <br>\r\n\t\t\t\t\t\t\t <br>\r\n\t\t\t\t\t\t\tDate réception : <b> __/__/____ </b> <br>\r\n\t\t\t\t\t\t\t <br>\r\n\t\t\t\t\t\t\tMotif de rejet :   <br>\r\n\t\t\t\t\t\t\t<br>\r\n\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t<table cellspacing=\"0\" style=\"width: 100%; text-align: center;margin-left:10px\" font-size: 14px\" class=\"motifs\">\r\n\t\t\t\t\t\t\t\t<tr style=\" height : 10px;\">\r\n\t\t\t\t\t\t\t\t\t<td style=\"width: 50%; text-align: center; \">\r\n\t\t\t\t\t\t\t\t\t\t<table cellspacing=\"0\" style=\"width: 100%; text-align: right; font-size: 11pt;\" border-collapse=\"collapse\">\r\n\t\t\t\t\t\t\t\t\t\t\t<tr >\r\n\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width:20%;text-align: center;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<div style=\"width:10px;height:10px;border:2px solid #000;\"></div> \r\n\t\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width:80%;text-align:left; \">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<b>Manque bon de commande   </b>  \r\n\t\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t\t\t</table>\r\n\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t<td style=\"width: 50%; text-align: center; \">\r\n\t\t\t\t\t\t\t\t\t\t<table cellspacing=\"0\" style=\"width: 100%; text-align: right; font-size: 11pt;\" border-collapse=\"collapse\">\r\n\t\t\t\t\t\t\t\t\t\t\t<tr >\r\n\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width:20%;text-align: center;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<div style=\"width:10px;height:10px;border:2px solid #000;\"></div> \r\n\t\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width:80%;text-align:left; \">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<b>Facture erronée( en double. date …)    </b>  \r\n\t\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t\t\t</table>\r\n\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t<tr><td colspan=\"2\"></td></tr>\r\n\t\t\t\t\t\t\t\t\t<tr><td colspan=\"2\"></td></tr>\r\n\t\t\t\t\t\t\t\t\t<tr><td colspan=\"2\"></td></tr>\r\n\t\t\t\t\t\t\t\t\t<tr><td colspan=\"2\"></td></tr>\r\n\t\t\t\t\t\t\t\t\t<tr><td colspan=\"2\"></td></tr>\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t<td style=\"width: 50%;\">\r\n\t\t\t\t\t\t\t\t\t\t<table cellspacing=\"0\" style=\"width: 100%; text-align: right; font-size: 11pt;\" border-collapse=\"collapse\">\r\n\t\t\t\t\t\t\t\t\t\t\t<tr >\r\n\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width:20%;text-align: center;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<div style=\"width:10px;height:10px;border:2px solid #000;\"></div> \r\n\t\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width:80%;text-align:left; \">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<b>Liasse incomplète     </b>  \r\n\t\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t\t\t</table>\r\n\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t<td style=\"width: 50%;\">\r\n\t\t\t\t\t\t\t\t\t\t<table cellspacing=\"0\" style=\"width: 100%; text-align: right; font-size: 11pt;\" cellspacing=\"0\">\r\n\t\t\t\t\t\t\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width:20%;text-align: center;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<div style=\"width:10px;height:10px;border:2px solid #000;\"></div> \r\n\t\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width:80%;text-align:left; \">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<b>Litige    </b>  \r\n\t\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t\t\t</table>\r\n\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t<tr><td colspan=\"2\"></td></tr>\r\n\t\t\t\t\t\t\t\t\t\t<tr><td colspan=\"2\"></td></tr>\r\n\t\t\t\t\t\t\t\t\t\t<tr><td colspan=\"2\"></td></tr>\r\n\t\t\t\t\t\t\t\t\t\t<tr><td colspan=\"2\"></td></tr>\r\n\t\t\t\t\t\t\t\t\t\t<tr><td colspan=\"2\"></td></tr>\r\n\t\t\t\t\t\t\t\t<tr>\t\r\n\t\t\t\t\t\t\t\t\t<td style=\"width: 50%;\">\r\n\t\t\t\t\t\t\t\t\t\t<table cellspacing=\"0\" style=\"width: 100%; text-align: right; font-size: 11pt;\" border-collapse=\"collapse\">\r\n\t\t\t\t\t\t\t\t\t\t\t<tr >\r\n\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width:20%;text-align: center;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<div style=\"width:10px;height:10px;border:2px solid #000;\"></div> \r\n\t\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width:80%;text-align:left; \">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<b>Manque bon de livraison     </b>  \r\n\t\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t\t\t</table>\r\n\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t<td style=\"width: 50%;\">\r\n\t\t\t\t\t\t\t\t\t\t<table cellspacing=\"0\" style=\"width: 100%; text-align: right; font-size: 11pt;\" cellspacing=\"0\">\r\n\t\t\t\t\t\t\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width:20%;text-align: center;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<div style=\"width:10px;height:10px;border:2px solid #000;\"></div> \r\n\t\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width:80%;text-align:left; \">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<b>Refus    </b>  \r\n\t\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t\t\t</table>\r\n\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t<tr><td colspan=\"2\"></td></tr>\r\n\t\t\t\t\t\t\t\t\t\t<tr><td colspan=\"2\"></td></tr>\r\n\t\t\t\t\t\t\t\t\t\t<tr><td colspan=\"2\"></td></tr>\r\n\t\t\t\t\t\t\t\t\t\t<tr><td colspan=\"2\"></td></tr>\r\n\t\t\t\t\t\t\t\t\t\t<tr><td colspan=\"2\"></td></tr>\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t<tr>\t\r\n\t\t\t\t\t\t\t\t\t<td style=\"width: 50%;\">\r\n\t\t\t\t\t\t\t\t\t\t<table cellspacing=\"0\" style=\"width: 100%; text-align: right; font-size: 11pt;\" border-collapse=\"collapse\">\r\n\t\t\t\t\t\t\t\t\t\t\t<tr >\r\n\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width:20%;text-align: center;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<div style=\"width:10px;height:10px;border:2px solid #000;\"></div> \r\n\t\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width:80%;text-align:left; \">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<b>Montant Incorrect (HT, TVA et TTC)     </b>  \r\n\t\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t\t\t</table>\r\n\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t<td style=\"width: 50%;\">\r\n\t\t\t\t\t\t\t\t\t\t<table cellspacing=\"0\" style=\"width: 100%; text-align: right; font-size: 11pt;\" cellspacing=\"0\">\r\n\t\t\t\t\t\t\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width:20%;text-align: center;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<div style=\"width:10px;height:10px;border:2px solid #000;\"></div> \r\n\t\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width:80%;text-align:left; \">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<b>Mentions obligatoires (ICE,RC, CNSS, Patente, IF. Raison sociale)   </b>  \r\n\t\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t\t\t</table>\r\n\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t</table>\r\n\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t<br>\r\n\t\t\t\t\t\t\t<br>\r\n\t\t\t\t\t\t\t<br>\r\n\t\t\t\t\t\t\t<br>\r\n\t\t\t\t\t\t\t<br>\r\n\t\t\t\t\t\t\t<p >\r\n\t\t\t\t\t\t\t\tNous vous en remercions et vous prions d’agréer, messieurs, l’assurance de nos saluations distinguées.\t\r\n\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t\t<br>\r\n\t\t\t\t\t\t\tCachet et signature ",NULL),
(14,"2019-07-15 20:18:03","",1),
(15,"2019-07-15 20:18:08","<div style=\"width: 25%; text-align: left;\">\t\n\t\t\t\t\t\t\t\t<img style=\"width: 100%;\" src=\"logo.png\" alt=\"Logo\">\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div style=\"width: 100%; text-align: right; margin-top:-90px\">\t\n\t\t\t\t\t\t\t\t<b>Témara</b> le  <b> [Date aujourd\'hui]</b><br><br>\n\t\t\t\t\t\t\t\t<b>[Code client]<br><br>\n\t\t\t\t\t\t\t\t<b>[Nom client]</b><br><br>\n\t\t\t\t\t\t\t\t<b>[Adresse]</b><br><br>\n\t\t\t\t\t\t\t\t<b>[Ville]</b><br><br>\n\t\t\t\t\t\t\t</b></div><b>\n\n\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t<br>\t\n\t\t\t\t\t\t\t<br>\t\n\t\t\t\t\t\t\t<b><u>Objet</u>: Accusé réception des factures</b><br>\n\t\t\t\t\t\t\t<br>\n\t\t\t\t\t\t\t<br>\n\t\t\t\t\t\t\tMessieurs,<br>\n\t\t\t\t\t\t\t <br>\n\t\t\t\t\t\t\tPar la présente, vous accusez de réception des factures :<br>\n\t\t\t\t\t\t\t <br>\n\t\t\t\t\t\t\tN° : <b>[Num facture] </b> <br>\n\t\t\t\t\t\t\t <br>\n\t\t\t\t\t\t\tDate comptabilisation : <b> [Date  comptabilisation] </b> <br>\n\t\t\t\t\t\t\t <br>\n\t\t\t\t\t\t\tDate réception : <b> __/__/____ </b> <br>\n\t\t\t\t\t\t\t <br>\n\t\t\t\t\t\t\tMotif de rejet :   <br>\n\t\t\t\t\t\t\t<br>\n\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t<table cellspacing=\"0\" style=\"width: 100%; text-align: center;margin-left:10px\" font-size:=\"\" 14px\"=\"\" class=\"motifs\">\n\t\t\t\t\t\t\t\t<tbody><tr style=\" height : 10px;\">\n\t\t\t\t\t\t\t\t\t<td style=\"width: 50%; text-align: center; \">\n\t\t\t\t\t\t\t\t\t\t<table cellspacing=\"0\" style=\"width: 100%; text-align: right; font-size: 11pt;\" border-collapse=\"collapse\">\n\t\t\t\t\t\t\t\t\t\t\t<tbody><tr>\n\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width:20%;text-align: center;\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t<div style=\"width:10px;height:10px;border:2px solid #000;\"></div> \n\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width:80%;text-align:left; \">\n\t\t\t\t\t\t\t\t\t\t\t\t\t<b>Manque bon de commande   </b>  \n\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t\t</tbody></table>\n\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t<td style=\"width: 50%; text-align: center; \">\n\t\t\t\t\t\t\t\t\t\t<table cellspacing=\"0\" style=\"width: 100%; text-align: right; font-size: 11pt;\" border-collapse=\"collapse\">\n\t\t\t\t\t\t\t\t\t\t\t<tbody><tr>\n\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width:20%;text-align: center;\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t<div style=\"width:10px;height:10px;border:2px solid #000;\"></div> \n\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width:80%;text-align:left; \">\n\t\t\t\t\t\t\t\t\t\t\t\t\t<b>Facture erronée( en double. date …)    </b>  \n\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t\t</tbody></table>\n\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t<tr><td colspan=\"2\"></td></tr>\n\t\t\t\t\t\t\t\t\t<tr><td colspan=\"2\"></td></tr>\n\t\t\t\t\t\t\t\t\t<tr><td colspan=\"2\"></td></tr>\n\t\t\t\t\t\t\t\t\t<tr><td colspan=\"2\"></td></tr>\n\t\t\t\t\t\t\t\t\t<tr><td colspan=\"2\"></td></tr>\n\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t<tr>\n\t\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t<td style=\"width: 50%;\">\n\t\t\t\t\t\t\t\t\t\t<table cellspacing=\"0\" style=\"width: 100%; text-align: right; font-size: 11pt;\" border-collapse=\"collapse\">\n\t\t\t\t\t\t\t\t\t\t\t<tbody><tr>\n\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width:20%;text-align: center;\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t<div style=\"width:10px;height:10px;border:2px solid #000;\"></div> \n\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width:80%;text-align:left; \">\n\t\t\t\t\t\t\t\t\t\t\t\t\t<b>Liasse incomplète     </b>  \n\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t\t</tbody></table>\n\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t<td style=\"width: 50%;\">\n\t\t\t\t\t\t\t\t\t\t<table cellspacing=\"0\" style=\"width: 100%; text-align: right; font-size: 11pt;\">\n\t\t\t\t\t\t\t\t\t\t\t<tbody><tr>\n\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width:20%;text-align: center;\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t<div style=\"width:10px;height:10px;border:2px solid #000;\"></div> \n\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width:80%;text-align:left; \">\n\t\t\t\t\t\t\t\t\t\t\t\t\t<b>Litige    </b>  \n\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t\t</tbody></table>\n\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t<tr><td colspan=\"2\"></td></tr>\n\t\t\t\t\t\t\t\t\t\t<tr><td colspan=\"2\"></td></tr>\n\t\t\t\t\t\t\t\t\t\t<tr><td colspan=\"2\"></td></tr>\n\t\t\t\t\t\t\t\t\t\t<tr><td colspan=\"2\"></td></tr>\n\t\t\t\t\t\t\t\t\t\t<tr><td colspan=\"2\"></td></tr>\n\t\t\t\t\t\t\t\t<tr>\t\n\t\t\t\t\t\t\t\t\t<td style=\"width: 50%;\">\n\t\t\t\t\t\t\t\t\t\t<table cellspacing=\"0\" style=\"width: 100%; text-align: right; font-size: 11pt;\" border-collapse=\"collapse\">\n\t\t\t\t\t\t\t\t\t\t\t<tbody><tr>\n\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width:20%;text-align: center;\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t<div style=\"width:10px;height:10px;border:2px solid #000;\"></div> \n\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width:80%;text-align:left; \">\n\t\t\t\t\t\t\t\t\t\t\t\t\t<b>Manque bon de livraison     </b>  \n\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t\t</tbody></table>\n\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t<td style=\"width: 50%;\">\n\t\t\t\t\t\t\t\t\t\t<table cellspacing=\"0\" style=\"width: 100%; text-align: right; font-size: 11pt;\">\n\t\t\t\t\t\t\t\t\t\t\t<tbody><tr>\n\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width:20%;text-align: center;\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t<div style=\"width:10px;height:10px;border:2px solid #000;\"></div> \n\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width:80%;text-align:left; \">\n\t\t\t\t\t\t\t\t\t\t\t\t\t<b>Refus    </b>  \n\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t\t</tbody></table>\n\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t<tr><td colspan=\"2\"></td></tr>\n\t\t\t\t\t\t\t\t\t\t<tr><td colspan=\"2\"></td></tr>\n\t\t\t\t\t\t\t\t\t\t<tr><td colspan=\"2\"></td></tr>\n\t\t\t\t\t\t\t\t\t\t<tr><td colspan=\"2\"></td></tr>\n\t\t\t\t\t\t\t\t\t\t<tr><td colspan=\"2\"></td></tr>\n\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t<tr>\t\n\t\t\t\t\t\t\t\t\t<td style=\"width: 50%;\">\n\t\t\t\t\t\t\t\t\t\t<table cellspacing=\"0\" style=\"width: 100%; text-align: right; font-size: 11pt;\" border-collapse=\"collapse\">\n\t\t\t\t\t\t\t\t\t\t\t<tbody><tr>\n\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width:20%;text-align: center;\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t<div style=\"width:10px;height:10px;border:2px solid #000;\"></div> \n\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width:80%;text-align:left; \">\n\t\t\t\t\t\t\t\t\t\t\t\t\t<b>Montant Incorrect (HT, TVA et TTC)     </b>  \n\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t\t</tbody></table>\n\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t<td style=\"width: 50%;\">\n\t\t\t\t\t\t\t\t\t\t<table cellspacing=\"0\" style=\"width: 100%; text-align: right; font-size: 11pt;\">\n\t\t\t\t\t\t\t\t\t\t\t<tbody><tr>\n\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width:20%;text-align: center;\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t<div style=\"width:10px;height:10px;border:2px solid #000;\"></div> \n\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t\t<td style=\"width:80%;text-align:left; \">\n\t\t\t\t\t\t\t\t\t\t\t\t\t<b>Mentions obligatoires (ICE,RC, CNSS, Patente, IF. Raison sociale)   </b>  \n\t\t\t\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\t\t</tbody></table>\n\t\t\t\t\t\t\t\t\t</td>\n\t\t\t\t\t\t\t\t</tr>\n\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t</tbody></table>\n\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t<br>\n\t\t\t\t\t\t\t<br>\n\t\t\t\t\t\t\t<br>\n\t\t\t\t\t\t\t<br>\n\t\t\t\t\t\t\t<br>\n\t\t\t\t\t\t\t<p>\n\t\t\t\t\t\t\t\tNous vous en remercions et vous prions d’agréer, messieurs, l’assurance de nos saluations distinguées.\t\n\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t\t\t<br>\n\t\t\t\t\t\t\tCachet et signature </b>",1);


DROP TABLE IF EXISTS `bc`;

CREATE TABLE `bc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `NUMERO_CLIENT` varchar(20) DEFAULT NULL,
  `NOM_CLIENT` varchar(120) DEFAULT NULL,
  `BC` varchar(60) DEFAULT NULL,
  `DATE_EDITION` datetime DEFAULT NULL,
  `DATE_NUMERISATION` datetime DEFAULT NULL,
  `pdf` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

INSERT INTO `bc` VALUES (12,"AE175778",NULL,01,"2019-07-11 00:00:00","2019-07-11 00:00:00",1);


DROP TABLE IF EXISTS `bl`;

CREATE TABLE `bl` (
  `BL` varchar(60) NOT NULL,
  `NUMERO_CLIENT` varchar(20) DEFAULT NULL,
  `NOM_CLIENT` varchar(120) DEFAULT NULL,
  `BC` varchar(60) DEFAULT NULL,
  `DATE_LIVRAISON` datetime DEFAULT NULL,
  `DATE_NUMERISATION` datetime DEFAULT NULL,
  `pdf` int(1) DEFAULT '0',
  PRIMARY KEY (`BL`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `bl` VALUES (01,"AE175778",NULL,01,"2019-07-11 00:00:00","2019-07-11 00:00:00",1);


DROP TABLE IF EXISTS `client`;

CREATE TABLE `client` (
  `NUMERO_CLIENT` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NOM_CLIENT` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `TELE1` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `TELE2` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ADRESSE1` text COLLATE utf8mb4_unicode_ci,
  `SECTEUR1` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `VILLE` int(11) DEFAULT NULL,
  `Latitude` varchar(22) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Longitude` varchar(22) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ADRESSE2` text COLLATE utf8mb4_unicode_ci,
  `SECTEUR2` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `VILLE2` int(11) DEFAULT NULL,
  `Latitude2` varchar(22) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Longitude2` varchar(22) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DESCRIPTION` text COLLATE utf8mb4_unicode_ci,
  `CODE_ENTREPRISE` int(11) DEFAULT NULL,
  `ETAT` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`NUMERO_CLIENT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `client` VALUES ("AE175778","ZAKARIYA EL AMRI","",605,"MEDIA.MAROC","",196,"33.9241","-7.0051","","",NULL,NULL,NULL,"",NULL,1);


DROP TABLE IF EXISTS `client_history`;

CREATE TABLE `client_history` (
  `NUMERO_CLIENT` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NOM_CLIENT` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `TELE1` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `TELE2` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ADRESSE1` text COLLATE utf8mb4_unicode_ci,
  `SECTEUR1` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `VILLE` int(11) DEFAULT NULL,
  `Latitude` varchar(22) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Longitude` varchar(22) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ADRESSE2` text COLLATE utf8mb4_unicode_ci,
  `SECTEUR2` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `VILLE2` int(11) DEFAULT NULL,
  `Latitude2` varchar(22) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Longitude2` varchar(22) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DESCRIPTION` text COLLATE utf8mb4_unicode_ci,
  `CODE_ENTREPRISE` int(11) DEFAULT NULL,
  `DATE_DELETE` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



DROP TABLE IF EXISTS `facture`;

CREATE TABLE `facture` (
  `NUM_FACTURE` varchar(60) NOT NULL,
  `NUMERO_CLIENT` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `DATE_COMPTABILISATION` date DEFAULT NULL,
  `BC` varchar(60) DEFAULT NULL,
  `BL` varchar(60) DEFAULT NULL,
  `STATUS` int(11) DEFAULT '1',
  `DATE_CREATION` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `SOURCE` int(1) NOT NULL DEFAULT '1' COMMENT '1=> import ||creation 2=> X4',
  `pdf` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`NUM_FACTURE`),
  KEY `NUMERO_CLIENT` (`NUMERO_CLIENT`),
  KEY `STATUS` (`STATUS`),
  CONSTRAINT `fk_facture_status` FOREIGN KEY (`STATUS`) REFERENCES `status` (`CODE_STATUS`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `facture` VALUES ("F01","AE175778","2019-07-03",01,01,1,"2019-07-11 23:30:03",1,0);


DROP TABLE IF EXISTS `facture_history`;

CREATE TABLE `facture_history` (
  `NUM_FACTURE` varchar(60) NOT NULL,
  `NUMERO_CLIENT` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `DATE_COMPTABILISATION` date DEFAULT NULL,
  `BC` varchar(60) DEFAULT NULL,
  `BL` varchar(60) DEFAULT NULL,
  `STATUS` int(11) DEFAULT '1',
  `DATE_CREATION` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `SOURCE` int(1) NOT NULL DEFAULT '1' COMMENT '1=> import ||creation 2=> X4'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



DROP TABLE IF EXISTS `facture_status`;

CREATE TABLE `facture_status` (
  `CODE_` int(11) NOT NULL AUTO_INCREMENT,
  `NUM_FACTURE` varchar(60) NOT NULL,
  `CODE_STATUS` int(11) NOT NULL,
  `DATE` datetime DEFAULT CURRENT_TIMESTAMP,
  `motif` int(11) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`CODE_`),
  KEY `fk_facturestatus_facture` (`NUM_FACTURE`),
  CONSTRAINT `fk_facturestatus_facture` FOREIGN KEY (`NUM_FACTURE`) REFERENCES `facture` (`NUM_FACTURE`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=519 DEFAULT CHARSET=latin1;

INSERT INTO `facture_status` VALUES (517,"F01",1,"2019-07-03 00:00:00",1,""),
(518,"F01",1,"2019-07-03 00:00:00",1,"");


DROP TABLE IF EXISTS `module`;

CREATE TABLE `module` (
  `CODE` int(11) NOT NULL AUTO_INCREMENT,
  `NOM` varchar(60) NOT NULL,
  `DESCRIPTION` text NOT NULL,
  `icon` varchar(100) NOT NULL,
  PRIMARY KEY (`CODE`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

INSERT INTO `module` VALUES (1,"Client","client","fa fa-user-circle-o"),
(2,"Facture","facture","fa fa-file"),
(3,"Bon de commande","bc","fa fa-file"),
(4,"Bon de livraison","bl","fa fa-file"),
(5,"Accusé de réception","ar","fa fa-file"),
(6,"Tableau de board","index","fa fa-tachometer"),
(7,"Rapport","graph","fa fa-th-large"),
(8,"Documents","file_manager","fa fa-folder"),
(9,"Dossier accusé de réception","ar_list","fa fa-folder");


DROP TABLE IF EXISTS `motif`;

CREATE TABLE `motif` (
  `CODE_MOTIF` int(11) NOT NULL AUTO_INCREMENT,
  `NOM_MOTIF` varchar(200) NOT NULL,
  `DESCRIPTION_MOTIF` text,
  PRIMARY KEY (`CODE_MOTIF`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

INSERT INTO `motif` VALUES (1,"Manque du bon de commande",NULL),
(2,"Liasse incomplète  ",NULL),
(3," Manque de bon de livraison",NULL),
(4,"Montant Incorrect (HT, TVA et TTC)  ",NULL),
(5,"Facture erronée( en double. date …)  ",NULL),
(6," Mentions obligatoires (ICE,RC, CNSS, Patente, IF)",NULL),
(7,"Litige",NULL),
(8,"Refus",NULL),
(9,"Mentions obligatoires(ICE,RC, CNSS, Patente, IF.Raison sociale)",NULL);


DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `CODE_ROLE` int(11) NOT NULL AUTO_INCREMENT,
  `NOM_ROLE` varchar(200) NOT NULL,
  `DESCRIPTION_ROLE` text,
  PRIMARY KEY (`CODE_ROLE`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO `role` VALUES (1,"Admin",NULL),
(2,"User",NULL),
(3,"Super_admin",NULL);


DROP TABLE IF EXISTS `status`;

CREATE TABLE `status` (
  `CODE_STATUS` int(11) NOT NULL AUTO_INCREMENT,
  `NOM_STATUS` varchar(200) NOT NULL,
  `DESCRIPTION_STATUS` text,
  PRIMARY KEY (`CODE_STATUS`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO `status` VALUES (1,"Reçue",NULL),
(2,"Distribuée",NULL),
(3,"Rejetée",NULL),
(4,"En cours",NULL);


DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `CODE_USER` int(11) NOT NULL AUTO_INCREMENT,
  `NOM_USER` varchar(200) NOT NULL,
  `PRENOM_USER` varchar(200) DEFAULT NULL,
  `EMAIL_USER` varchar(255) NOT NULL,
  `MDP_USER` varchar(255) NOT NULL,
  `CIVILITE_USER` varchar(3) DEFAULT NULL,
  `CODE_ENTREPRISE` int(11) NOT NULL,
  `ROLE_USER` int(11) NOT NULL COMMENT '0=skcconseil 1 = user , 2 = ADMIN',
  `actif` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`CODE_USER`),
  UNIQUE KEY `EMAIL_USER` (`EMAIL_USER`),
  KEY `CODE_ENTREPRISE` (`CODE_ENTREPRISE`),
  KEY `ROLE_USER` (`ROLE_USER`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

INSERT INTO `user` VALUES (1,"skcconseil",NULL,"skcconseil@einvoicetrack.com","skcconseil","Mr",1,0,1),
(13,"rizki","","rizki.akerkaou@gmail.com","azerazer","Mr",1,2,1),
(14,"ayoub","","barouchayoub3@gmail.com","azerazer","Mr",1,1,1),
(24,"HAJAR SK","","skir.hajar@gmail.com","a015642ed","Mme",1,1,0),
(25,"devzakariya","","zack00.el@gmail.com","azerazer","Mr",1,2,1),
(26,"z","","zakariya.etudes@gmail.com","dba335102","Mr",1,1,1);


DROP TABLE IF EXISTS `user_module`;

CREATE TABLE `user_module` (
  `CODE_USER` int(11) NOT NULL,
  `CODE_MODULE` int(11) NOT NULL,
  PRIMARY KEY (`CODE_USER`,`CODE_MODULE`),
  KEY `CODE_USER` (`CODE_USER`,`CODE_MODULE`),
  CONSTRAINT `fk_module_client` FOREIGN KEY (`CODE_USER`) REFERENCES `user` (`CODE_USER`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `user_module` VALUES (13,1),
(14,8),
(24,1),
(25,1),
(25,2),
(25,3),
(25,4),
(25,5),
(25,6),
(25,7),
(25,8),
(25,9);


DROP TABLE IF EXISTS `ville`;

CREATE TABLE `ville` (
  `CODE_VILLE` int(11) NOT NULL AUTO_INCREMENT,
  `NOM_VILLE` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`CODE_VILLE`)
) ENGINE=InnoDB AUTO_INCREMENT=405 DEFAULT CHARSET=latin1;

INSERT INTO `ville` VALUES (1,"Aïn Harrouda"),
(2,"Ben Yakhlef"),
(3,"Bouskoura"),
(4,"Casablanca"),
(5,"Médiouna"),
(6,"Mohammédia"),
(7,"Tit Mellil"),
(9,"Bejaâd"),
(10,"Ben Ahmed"),
(11,"Benslimane"),
(12,"Berrechid"),
(13,"Boujniba"),
(14,"Boulanouare"),
(15,"Bouznika"),
(16,"Deroua"),
(17,"El Borouj"),
(18,"El Gara"),
(19,"Guisser"),
(20,"Hattane"),
(21,"Khouribga"),
(22,"Loulad"),
(23,"Oued Zem"),
(24,"Oulad Abbou"),
(25,"Oulad H\'Riz Sahel"),
(26,"Oulad M\'rah"),
(27,"Oulad Saïd"),
(28,"Oulad Sidi Ben Daoud"),
(29,"Ras El Aïn"),
(30,"Settat"),
(31,"Sidi Rahhal Chataï"),
(32,"Soualem"),
(33,"Azemmour"),
(34,"Bir Jdid"),
(35,"Bouguedra"),
(36,"Echemmaia"),
(37,"El Jadida"),
(38,"Hrara"),
(39,"Ighoud"),
(40,"Jamâat Shaim"),
(41,"Jorf Lasfar"),
(42,"Khemis Zemamra"),
(43,"Laaounate"),
(44,"Moulay Abdallah"),
(45,"Oualidia"),
(46,"Oulad Amrane"),
(47,"Oulad Frej"),
(48,"Oulad Ghadbane"),
(49,"Safi"),
(50,"Sebt El Maârif"),
(51,"Sebt Gzoula"),
(52,"Sidi Ahmed"),
(53,"Sidi Ali Ban Hamdouche"),
(54,"Sidi Bennour"),
(55,"Sidi Bouzid"),
(56,"Sidi Smaïl"),
(57,"Youssoufia"),
(58,"Fès"),
(59,"Aïn Cheggag"),
(60,"Bhalil"),
(61,"Boulemane"),
(62,"El Menzel"),
(63,"Guigou"),
(64,"Imouzzer Kandar"),
(65,"Imouzzer Marmoucha"),
(66,"Missour"),
(67,"Moulay Yaâcoub"),
(68,"Ouled Tayeb"),
(69,"Outat El Haj"),
(70,"Ribate El Kheir"),
(71,"Séfrou"),
(72,"Skhinate"),
(73,"Tafajight"),
(74,"Arbaoua"),
(75,"Aïn Dorij"),
(76,"Dar Gueddari"),
(77,"Had Kourt"),
(78,"Jorf El Melha"),
(79,"Kénitra"),
(80,"Khenichet"),
(81,"Lalla Mimouna"),
(82,"Mechra Bel Ksiri"),
(83,"Mehdia"),
(84,"Moulay Bousselham"),
(85,"Sidi Allal Tazi"),
(86,"Sidi Kacem"),
(87,"Sidi Slimane"),
(88,"Sidi Taibi"),
(89,"Sidi Yahya El Gharb"),
(90,"Souk El Arbaa"),
(91,"Akka"),
(92,"Assa"),
(93,"Bouizakarne"),
(94,"El Ouatia"),
(95,"Es-Semara"),
(96,"Fam El Hisn"),
(97,"Foum Zguid"),
(98,"Guelmim"),
(99,"Taghjijt"),
(100,"Tan-Tan"),
(101,"Tata"),
(102,"Zag"),
(103,"Marrakech"),
(104,"Ait Daoud"),
(115,"Amizmiz"),
(116,"Assahrij"),
(117,"Aït Ourir"),
(118,"Ben Guerir"),
(119,"Chichaoua"),
(120,"El Hanchane"),
(121,"El Kelaâ des Sraghna"),
(122,"Essaouira"),
(123,"Fraïta"),
(124,"Ghmate"),
(125,"Ighounane"),
(126,"Imintanoute"),
(127,"Kattara"),
(128,"Lalla Takerkoust"),
(129,"Loudaya"),
(130,"Lâattaouia"),
(131,"Moulay Brahim"),
(132,"Mzouda"),
(133,"Ounagha"),
(134,"Sid L\'Mokhtar"),
(135,"Sid Zouin"),
(136,"Sidi Abdallah Ghiat"),
(137,"Sidi Bou Othmane"),
(138,"Sidi Rahhal"),
(139,"Skhour Rehamna"),
(140,"Smimou"),
(141,"Tafetachte"),
(142,"Tahannaout"),
(143,"Talmest"),
(144,"Tamallalt"),
(145,"Tamanar"),
(146,"Tamansourt"),
(147,"Tameslouht"),
(148,"Tanalt"),
(149,"Zeubelemok"),
(150,"Meknès‎"),
(151,"Khénifra"),
(152,"Agourai"),
(153,"Ain Taoujdate"),
(154,"MyAliCherif"),
(155,"Rissani"),
(156,"Amalou Ighriben"),
(157,"Aoufous"),
(158,"Arfoud"),
(159,"Azrou"),
(160,"Aïn Jemaa"),
(161,"Aïn Karma"),
(162,"Aïn Leuh"),
(163,"Aït Boubidmane"),
(164,"Aït Ishaq"),
(165,"Boudnib"),
(166,"Boufakrane"),
(167,"Boumia"),
(168,"El Hajeb"),
(169,"Elkbab"),
(170,"Er-Rich"),
(171,"Errachidia"),
(172,"Gardmit"),
(173,"Goulmima"),
(174,"Gourrama"),
(175,"Had Bouhssoussen"),
(176,"Haj Kaddour"),
(177,"Ifrane"),
(178,"Itzer"),
(179,"Jorf"),
(180,"Kehf Nsour"),
(181,"Kerrouchen"),
(182,"M\'haya"),
(183,"M\'rirt"),
(184,"Midelt"),
(185,"Moulay Ali Cherif"),
(186,"Moulay Bouazza"),
(187,"Moulay Idriss Zerhoun"),
(188,"Moussaoua"),
(189,"N\'Zalat Bni Amar"),
(190,"Ouaoumana"),
(191,"Oued Ifrane"),
(192,"Sabaa Aiyoun"),
(193,"Sebt Jahjouh"),
(194,"Sidi Addi"),
(195,"Tichoute"),
(196,"Sala Al Jadida"),
(197,"Tighza"),
(198,"Timahdite"),
(199,"Tinejdad"),
(200,"Tizguite"),
(201,"Toulal"),
(202,"Tounfite"),
(203,"Zaouia d\'Ifrane"),
(204,"Zaïda"),
(205,"Ahfir"),
(206,"Aklim"),
(207,"Al Aroui"),
(208,"Aïn Bni Mathar"),
(209,"Aïn Erreggada"),
(210,"Ben Taïeb"),
(211,"Berkane"),
(212,"Bni Ansar"),
(213,"Bni Chiker"),
(214,"Bni Drar"),
(215,"Bni Tadjite"),
(216,"Bouanane"),
(217,"Bouarfa"),
(218,"Bouhdila"),
(219,"Dar El Kebdani"),
(220,"Debdou"),
(221,"Douar Kannine"),
(222,"Driouch"),
(223,"El Aïoun Sidi Mellouk"),
(224,"Farkhana"),
(225,"Figuig"),
(226,"Ihddaden"),
(227,"Jaâdar"),
(228,"Jerada"),
(229,"Kariat Arekmane"),
(230,"Kassita"),
(231,"Kerouna"),
(232,"Laâtamna"),
(233,"Madagh"),
(234,"Midar"),
(235,"Nador"),
(236,"Naima"),
(237,"Oued Heimer"),
(238,"Oujda"),
(239,"Ras El Ma"),
(240,"Saïdia"),
(241,"Selouane"),
(242,"Sidi Boubker"),
(243,"Sidi Slimane Echcharaa"),
(244,"Talsint"),
(245,"Taourirt"),
(246,"Tendrara"),
(247,"Tiztoutine"),
(248,"Touima"),
(249,"Touissit"),
(250,"Zaïo"),
(251,"Zeghanghane"),
(252,"Rabat"),
(253,"Salé"),
(254,"Ain El Aouda"),
(255,"Harhoura"),
(256,"Khémisset"),
(257,"Oulmès"),
(258,"Rommani"),
(259,"Sidi Allal El Bahraoui"),
(260,"Sidi Bouknadel"),
(261,"Skhirat"),
(262,"Tamesna"),
(263,"Témara"),
(264,"Tiddas"),
(265,"Tiflet"),
(266,"Touarga"),
(267,"Agadir"),
(268,"Agdz"),
(269,"Agni Izimmer"),
(270,"Aït Melloul"),
(271,"Alnif"),
(272,"Anzi"),
(273,"Aoulouz"),
(274,"Aourir"),
(275,"Arazane"),
(276,"Aït Baha"),
(277,"Aït Iaâza"),
(278,"Aït Yalla"),
(279,"Ben Sergao"),
(280,"Biougra"),
(281,"Boumalne-Dadès"),
(282,"Dcheira El Jihadia"),
(283,"Drargua"),
(284,"El Guerdane"),
(285,"Harte Lyamine"),
(286,"Ida Ougnidif"),
(287,"Ifri"),
(288,"Igdamen"),
(289,"Ighil n\'Oumgoun"),
(290,"Imassine"),
(291,"Inezgane"),
(292,"Irherm"),
(293,"Kelaat-M\'Gouna"),
(294,"Lakhsas"),
(295,"Lakhsass"),
(296,"Lqliâa"),
(297,"M\'semrir"),
(298,"Massa (Maroc)"),
(299,"Megousse"),
(300,"Ouarzazate"),
(301,"Oulad Berhil"),
(302,"Oulad Teïma"),
(303,"Sarghine"),
(304,"Sidi Ifni"),
(305,"Skoura"),
(306,"Tabounte"),
(307,"Tafraout"),
(308,"Taghzout"),
(309,"Tagzen"),
(310,"Taliouine"),
(311,"Tamegroute"),
(312,"Tamraght"),
(313,"Tanoumrite Nkob Zagora"),
(314,"Taourirt ait zaghar"),
(315,"Taroudant"),
(316,"Temsia"),
(317,"Tifnit"),
(318,"Tisgdal"),
(319,"Tiznit"),
(320,"Toundoute"),
(321,"Zagora"),
(322,"Afourar"),
(323,"Aghbala"),
(324,"Azilal"),
(325,"Aït Majden"),
(326,"Beni Ayat"),
(327,"Béni Mellal"),
(328,"Bin elouidane"),
(329,"Bradia"),
(330,"Bzou"),
(331,"Dar Oulad Zidouh"),
(332,"Demnate"),
(333,"Dra\'a"),
(334,"El Ksiba"),
(335,"Foum Jamaa"),
(336,"Fquih Ben Salah"),
(337,"Kasba Tadla"),
(338,"Ouaouizeght"),
(339,"Oulad Ayad"),
(340,"Oulad M\'Barek"),
(341,"Oulad Yaich"),
(342,"Sidi Jaber"),
(343,"Souk Sebt Oulad Nemma"),
(344,"Zaouïat Cheikh"),
(345,"Tanger‎"),
(346,"Tétouan‎"),
(347,"Akchour"),
(348,"Assilah"),
(349,"Bab Berred"),
(350,"Bab Taza"),
(351,"Brikcha"),
(352,"Chefchaouen"),
(353,"Dar Bni Karrich"),
(354,"Dar Chaoui"),
(355,"Fnideq"),
(356,"Gueznaia"),
(357,"Jebha"),
(358,"Karia"),
(359,"Khémis Sahel"),
(360,"Ksar El Kébir"),
(361,"Larache"),
(362,"M\'diq"),
(363,"Martil"),
(364,"Moqrisset"),
(365,"Oued Laou"),
(366,"Oued Rmel"),
(367,"Ouezzane"),
(368,"Point Cires"),
(369,"Sidi Lyamani"),
(370,"Sidi Mohamed ben Abdallah el-Raisuni"),
(371,"Zinat"),
(372,"Ajdir‎"),
(373,"Aknoul‎"),
(374,"Al Hoceïma‎"),
(375,"Aït Hichem‎"),
(376,"Bni Bouayach‎"),
(377,"Bni Hadifa‎"),
(378,"Ghafsai‎"),
(379,"Guercif‎"),
(380,"Imzouren‎"),
(381,"Inahnahen‎"),
(382,"Issaguen (Ketama)‎"),
(383,"Karia (El Jadida)‎"),
(384,"Karia Ba Mohamed‎"),
(385,"Oued Amlil‎"),
(386,"Oulad Zbair‎"),
(387,"Tahla‎"),
(388,"Tala Tazegwaght‎"),
(389,"Tamassint‎"),
(390,"Taounate‎"),
(391,"Targuist‎"),
(392,"Taza‎"),
(393,"Taïnaste‎"),
(394,"Thar Es-Souk‎"),
(395,"Tissa‎"),
(396,"Tizi Ouasli‎"),
(397,"Laayoune‎"),
(398,"El Marsa‎"),
(399,"Tarfaya‎"),
(400,"Boujdour‎"),
(401,"Awsard"),
(402,"Oued-Eddahab "),
(403,"Stehat"),
(404,"Aït Attab");


SET foreign_key_checks = 1;
