<?php
ob_start();
session_start();
include 'connexion.php';


if(isset($_SESSION['user_einvoicetrack']))
{
	
	$current_user = decode($_SESSION['user_einvoicetrack']) ; 					
	$SQL="SELECT  `NOM_USER`, `PRENOM_USER`, `EMAIL_USER`,`CIVILITE_USER`,
			`CODE_ENTREPRISE`, `ROLE_USER`
		  FROM `user` 
		  WHERE CODE_USER = $current_user
		  AND actif= 1";
	$query=mysqli_query($ma_connexion,$SQL);
	if(mysqli_num_rows($query) == 1)
	{
		while($row=mysqli_fetch_assoc($query))
		{	
				$NOM_USER = $row['CIVILITE_USER'].' ' .$row['NOM_USER'];
		}
	}
	else 
	{
		header('Location: login');
	}
}
else 
{
	header('Location: login');
}
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title> eInvoiceTrack</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- c3 Charts -->
    <link href="css/plugins/c3/c3.min.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
	
	
	
	<link href="css/plugins/select2/select2.min.css" rel="stylesheet">
	
	
    <link href="css/plugins/datapicker/datepicker3.css" rel="stylesheet">
	
	<!-- Date range picker -->
     <link href="css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
	 

    <style>
	@media print{
  .c3 path, .c3 line {    
    fill: none; 
    stroke: #000;
} 
    </style>


</head>

<body>
    

    <div id="wrapper">

    
		<?php
			include 'includes/nav.php';
		?>	

        <div id="page-wrapper" class="gray-bg dashbard-1">
		
				<?php
					include 'includes/header.php';
				?>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
              
            </div>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight">
		 <div class="row">
                <div class="col-lg-6">
                    <div class="ibox ">
                        <div class="ibox-title">
                           
							
								
								
								<label class="h6 font-weight-bold">Volume des factures rejetées par mois / [Année]</label>
								<select id="factures_rejetes_date" class="sc_select2"   required>
									<option value="2016">2016</option>
									<option value="2017">2017</option>
									<option value="2018">2018</option>
									<option value="2019" selected>2019</option>
									<option value="2020">2020</option>
									<option value="2021">2021</option>
									<option value="2020">2020</option>
									<option value="2022">2022</option>
									<option value="2023">2023</option>
								</select>
							
								
							
							
							
							
							
							<button class="btn btn-success btn-rounded pull-right " type="button" onclick="printDiv('factures_rejetes')"  ><i class="fa fa-print"></i>&nbsp;&nbsp;<span class="bold">Imprimer</span></button>

                        </div>
                        <div class="ibox-content">
                            <div>
                                <div id="factures_rejetes"></div>
                            </div>
                        </div>
                    </div>
                </div>
				
				<div class="col-lg-6">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <label class="h6 font-weight-bold">Répartition des rejets par motifs /[Année]</label>
								<select id="factures_motifs_date" class="sc_select2"   required>
									<option value="2016">2016</option>
									<option value="2017">2017</option>
									<option value="2018">2018</option>
									<option value="2019" selected>2019</option>
									<option value="2020">2020</option>
									<option value="2021">2021</option>
									<option value="2020">2020</option>
									<option value="2022">2022</option>
									<option value="2023">2023</option>
								</select>

							<button class="btn btn-success btn-rounded pull-right " type="button" onclick="printDiv('factures_motifs')"  ><i class="fa fa-print"></i>&nbsp;&nbsp;<span class="bold">Imprimer</span></button>


                        </div>
                        <div class="ibox-content">
                            <div>
                                <div id="factures_motifs"></div>
                            </div>
                        </div>
                    </div>
                </div>
		</div>


		 <div class="row">
                <div class="col-lg-6">
                    <div class="ibox ">
                        <div class="ibox-title input-group ">
                           

							 <label class="h6 font-weight-bold">Distribution des factures par statut / [Période encours ]</label>
							<div class="form-group" id="factures_status_picker">
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									<input id="factures_status_value"  type="text" class="form-control" readonly style="background-color: #f3f3f4; max-width: 80px; cursor:pointer">
								</div>
                            </div>
							
							<button id="factures_status_date" style="margin-top: 4px; margin-left: 5px;" class="btn btn-primary btn-circle btn-sm" type="button"><i class="fa fa-refresh"></i>
							</button>
							 <!--
								<div id="factures_status_date" class="form-control" style="max-width:45%">
									<i class="fa fa-calendar"></i>
									<span></span> <b class="caret"></b>
								</div>
								 -->
								 
								 
								

							<button class="btn btn-success btn-rounded pull-right " type="button" onclick="printDiv('factures_status')"  ><i class="fa fa-print"></i>&nbsp;&nbsp;<span class="bold">Imprimer</span></button>


                        </div>
                        <div class="ibox-content">
						
						<div class="tabs-container">
							<ul class="nav nav-tabs" role="tablist">
								<li><a class="nav-link active" data-toggle="tab" href="#tab-1"><i class="fa fa-bar-chart-o"></i> </a></li>
								<li><a class="nav-link" data-toggle="tab" href="#tab-2"><i class="fa fa-bar-chart-o"></i></a></li>
							</ul>
							<div class="tab-content">
								<div role="tabpanel" id="tab-1" class="tab-pane active">
									<div class="panel-body">
										
										<div id="factures_status"></div>
									
									</div>
								</div>
								<div role="tabpanel" id="tab-2" class="tab-pane">
									<div class="panel-body">
									
										<div id="chartContainer" style="height: 370px; max-width: 920px; margin: 0px auto;"></div>
									
									</div>
								</div>
							</div>


						</div>
 
                        </div>
                    </div>
                    
                </div>
                <div class="col-lg-6">
                   <div class="ibox ">
                        <div class="ibox-title">
							<label class="h6 font-weight-bold">Nbr de facture client par région desservie / [Période encours ]</label>
							<div class="form-group" id="factures_region_picker">
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									<input id="factures_region_value"  type="text" class="form-control" readonly style="background-color: #f3f3f4; max-width: 80px; cursor:pointer">
									<button id="factures_region_date" style="margin-top: 4px; margin-left: 5px;" class="btn btn-primary btn-circle btn-sm" type="button"><i class="fa fa-refresh"></i>
									</button>
								</div>
                            </div>
							<button class="btn btn-success btn-rounded pull-right " type="button" onclick="printDiv('factures_region')"  ><i class="fa fa-print"></i>&nbsp;&nbsp;<span class="bold">Imprimer</span></button>


                        </div>
                        <div class="ibox-content" >
						 <div>
                                <div id="factures_region"></div>
                            </div>
						</div>
                    </div>
                </div>
            </div>
			
		
		
         
        </div>
       <?php
				include 'includes/footer.php';
			?>	

        </div>
        </div>



    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>

    <!-- d3 and c3 charts -->
    <script src="js/plugins/d3/d3.min.js"></script>
    <script src="js/plugins/c3/c3.min.js"></script>
	
	
	
	 <!-- Maps -->
	<link rel="stylesheet" type="text/css" href="https://js.api.here.com/v3/3.0/mapsjs-ui.css?dp-version=1549984893" />
	<script type="text/javascript" src="https://js.api.here.com/v3/3.0/mapsjs-core.js"></script>
	<script type="text/javascript" src="https://js.api.here.com/v3/3.0/mapsjs-service.js"></script>
	<script type="text/javascript" src="https://js.api.here.com/v3/3.0/mapsjs-ui.js"></script>
	<script type="text/javascript" src="https://js.api.here.com/v3/3.0/mapsjs-mapevents.js"></script>
	
	
	 <!-- Data picker -->
   <script src="js/plugins/datapicker/bootstrap-datepicker.js"></script>

	  
	 <!-- Date range use moment.js same as full calendar plugin -->
    <script src="js/plugins/fullcalendar/moment.min.js"></script>
	

	 <!-- Date range picker -->
    <script src="js/plugins/daterangepicker/daterangepicker.js"></script>
	
	
	
    <!-- Select2 -->
    <script src="js/plugins/select2/select2.full.min.js"></script>
	<script src="js/i18n/fr.js"></script>
	
	<script src="canvasjs.min.js"></script>
	


	<script>
$(document).ready(function () {
	
	
		$.fn.select2.defaults.set('language', 'fr');
	
		$(".sc_select2").select2({
			tags: true,
			language: "fr",
			 width: '15%',
		});


		function printDiv(divName) {
			 var printContents = document.getElementById(divName).innerHTML;
			 var originalContents = document.body.innerHTML;

			 document.body.innerHTML = printContents;

			 window.print();

			 document.body.innerHTML = originalContents;
		}
		
		function getRandomColor() {
		  var letters = '0123456789ABCDEF';
		  var color = '#';
		  for (var i = 0; i < 6; i++) {
			color += letters[Math.floor(Math.random() * 16)];
		  }
		  return color;
		}

		var svgMarkup = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="42" height="42" viewBox="0 0 263.335 263.335" style="enable-background:new 0 0 263.335 263.335;" xml:space="preserve">'
				+'<g>'
				+'	<g xmlns="http://www.w3.org/2000/svg">'
				+'		<path d="M40.479,159.021c21.032,39.992,49.879,74.22,85.732,101.756c0.656,0.747,1.473,1.382,2.394,1.839   c0.838-0.396,1.57-0.962,2.178-1.647c80.218-61.433,95.861-125.824,96.44-128.34c2.366-9.017,3.57-18.055,3.57-26.864    C237.389,47.429,189.957,0,131.665,0C73.369,0,25.946,47.424,25.946,105.723c0,8.636,1.148,17.469,3.412,26.28" fill="${COLOR}"/>'
				+'	<text x="80" y="130" font-family="sans-serif" font-size="5em" fill="white">${TEXT}</text>'
				+'	</g>'
				+'</g></svg>';
		
		function addMarkerToGroup(group, coordinate, html,text) {
			  let iconmarker = new H.map.Icon(svgMarkup.replace('${COLOR}','#1c84c6' ).replace('${TEXT}', text));
			  let marker = new H.map.Marker(coordinate,{icon: iconmarker});
			  marker.setData(html);
			  group.addObject(marker);
		}

		var gps_data = null ; 
		function showmapGPS()
		{
			
			$('#factures_region').html('<div id="map_gps" style="width: 100%; height: 320px; background: grey" />');	
			// $('#drag_gps').show();	

			

			var platform = new H.service.Platform({
			  app_id: 'devportal-demo-20180625',
			  app_code: '9v2BkviRwi9Ot26kp2IysQ',
			  useHTTPS: true
			});
			var pixelRatio = window.devicePixelRatio || 1;
			var defaultLayers = platform.createDefaultLayers({
			  tileSize: pixelRatio === 1 ? 256 : 512,
			  ppi: pixelRatio === 1 ? undefined : 320
			});

			var map = new H.Map(document.getElementById('map_gps'),
			  defaultLayers.normal.map,{
			  center: {lat:30.6230, lng: -8.0836},
			  zoom: 5,
			  pixelRatio: pixelRatio
			});

			var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));

			var ui = H.ui.UI.createDefault(map, defaultLayers);
			
			var group = new H.map.Group();

			map.addObject(group);

			group.addEventListener('tap', function (evt) {
				var bubble =  new H.ui.InfoBubble(evt.target.getPosition(), {
				  content: evt.target.getData()
				});
				ui.addBubble(bubble);
			}, false);

			if(gps_data)
			jQuery.each(gps_data, function(i, val) {
				addMarkerToGroup(group, {lat:val.lat, lng:val.lng},val.html,val.text);
			});
		}
	
	

		/* FACTURE REJETS */
		factures_rejetes('2019');
		function factures_rejetes(annee)
		{
			$.ajax(
			{
				type : 'POST',
				url : 'get/get_chart_data.php',
				data: "key=factures_rejetes&annee="+annee,
				success : function(data)
				{
					// console.log(data);
					info_html = JSON.parse(data);
					var chart = c3.generate({
						bindto: '#factures_rejetes',
						 data: {
							json: info_html,
							keys: {
							  x: 'mois',
							  value: ['total']
							},
							type: 'bar',
							colors: {
								total: "blue"
								// total: function(d) {
									// return getRandomColor();
								// }
							},
						  },
						  axis: {
								x:{
									type: 'category',
									tick: {
									  culling: false,
									  rotate: 0
									},
									label: {
										text: 'mois',
										position: 'inner-bottom'
									}
								},
								y:{
									label: {
										text: 'volume',
										position: 'outer-middle'
									}
								},
							}
						
					  });
				}
			});
			
		}
		

		$("#factures_rejetes_date").change(function()
		{
			factures_rejetes($(this).val());
		});
			
		/* END FACTURE REJETS */
		
		
		
		/* FACTURE MOTIF */
		factures_motifs("2019");
		function factures_motifs(annee)
		{
			$.ajax(
				{
				type : 'POST',
				url : 'get/get_chart_data.php',
				data: "key=factures_motifs&annee="+annee,
				success : function(data)
				{
					info_html = JSON.parse(data);
					// console.log(info_html);
					var data = {};
					var motifs = [];
					info_html.forEach(function(e) {
						motifs.push(e.motif);
						data[e.motif] = e.total;
					}) 
					var chart = c3.generate({
						bindto: '#factures_motifs',
						data: {
							json: [ data ],
							keys: {
								value: motifs,
							},
							type:'pie'
						 },
						pie: {
							expand: false,
							label: {
								format: function (value, ratio, id) {
									// return (id.replace(/ .*/,''));
									return d3.format("%")(ratio);
								}
							}
						},
						tooltip: {
								format: {
									value: function (value, ratio, id) {
										var format = id === 'data1' ? d3.format(',') : d3.format('');
										return format(value);
									}
								}
							},
						legend: {
							position: 'right'
						}
					});
				}
			});
			
		}
		
		$("#factures_motifs_date").change(function()
		{
			factures_motifs($(this).val());
		});
		/* END FACTURE MOTIF */
		
		
		
		/* FACTURE STATUS */
	
		factures_status(moment().format('MM'),moment().format('YYYY'));
		factures_status2(moment().format('MM'),moment().format('YYYY'));
		function factures_status(mois,annee)
		{
			$.ajax(
				{
				type : 'POST',
				url : 'get/get_chart_data.php',
				data: "key=factures_status&mois="+mois+"&annee="+annee,
				success : function(data)
				{
					// console.log(data);
					info_html = JSON.parse(data);
					var data = {};
					var status = [];
					info_html.forEach(function(e) {
						status.push(e.status);
						data[e.status] = e.total;
					}) 
					var chart = c3.generate({
						bindto: '#factures_status',
						data: {
							json: [ data ],
							keys: {
								value: status,
							},
							type:'pie'
						 },
						pie: {
							expand: false,
							label: {
								format: function (value, ratio, id) {
									// return (id.replace(/ .*/,''));
									return d3.format("%")(ratio);
								}
							}
						},
						tooltip: {
								format: {
									value: function (value, ratio, id) {
										var format = id === 'data1' ? d3.format(',') : d3.format('');
										return format(value);
									}
								}
							},
						legend: {
							position: 'right'
						}
						
					})
				}
			});
			
		}
		function factures_status2(mois,annee)
		{
			$.ajax(
			{
				type : 'POST',
				url : 'get/get_chart_data.php',
				data: "key=factures_status&mois="+mois+"&annee="+annee,
				success : function(data)
				{
					info_html = JSON.parse(data);
					// console.log(info_html);
					var dataPoints = [];
					for (var i = 0; i < info_html.length; i++) {
						dataPoints.push({
							label: info_html[i].status,
							y: info_html[i].total
						});
					}
					
					
					var chart = new CanvasJS.Chart("chartContainer", {
						exportEnabled: true,
						animationEnabled: true,
						title:{
							text: "Distribution des factures par statut"
						},
						legend:{
							cursor: "pointer",
							itemclick: explodePie
						},
						data: [{
							type: "pie",
							startAngle: 240,
							percentFormatString: "#0.##",
							toolTipContent: "{label} (#percent%)",
							showInLegend: "true",
							legendText: "{label} (#percent%)",
							indexLabel: "{label} ({y})",
							dataPoints: dataPoints
						}]
					});
					chart.render();
					
					
				}
			});	
			
		}
		
		$(document).on('click', '#factures_status_date', function(){ 
			res = $('#factures_status_value').val().split('-');
			factures_status(res[0],res[1]);
			factures_status2(res[0],res[1]);
		
		})
		$('#factures_status_picker .input-group.date').datepicker({
			format: "mm-yyyy",
			startView: "months",  
			minViewMode: "months",
			onSelect: function(dateText, inst) {
				alert(dateText);
			}
		})
		
		$('#factures_status_value').val(moment().format('MM-YYYY'));
		// $("#factures_status_date").change(function()
		// {
			// alert($(this).val());
		// });
		
		
		// $('#factures_status_date span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

		// $('#factures_status_date').daterangepicker({
			// language:'fr',
			// format: 'YYYY-MM-DD',
			// startDate: moment().subtract(29, 'days'),
			// dateLimit: { days: 360 },
			// showDropdowns: true,
			// showWeekNumbers: true,
			// timePicker: false,
			// timePickerIncrement: 1,
			// timePicker12Hour: true,
			// ranges: {
				// 'Les 30 derniers jours': [moment().subtract(29, 'days'), moment()],
				// 'Ce mois-ci': [moment().startOf('month'), moment().endOf('month')],
				// 'Le mois dernier': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			// },
			// opens: 'right',
			// drops: 'down',
			// buttonClasses: ['btn', 'btn-sm'],
			// applyClass: 'btn-primary',
			// cancelClass: 'btn-default',
			// separator: ' to ',
			// locale: {
				// applyLabel: 'Enregistrer',
				// cancelLabel: 'Annuler',
				// fromLabel: 'À partir',
				// toLabel: 'jusqu\'à',
				// firstDay: 1,
				// customRangeLabel: 'Personnalisé',
			// }
		// }, function(start, end, label) {
			// start_date_1 = start.format('YYYY-MM-DD');
			// end_date_1 = end.format('YYYY-MM-DD');
			// $('#factures_status_date span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
		// });
		
		
		
		/* END FACTURE STATUS */
		
		
		
		/* FACTURE REGION */

		factures_region(moment().format('MM'),moment().format('YYYY'));
		function factures_region(mois,annee)
		{
			$.ajax(
			{
				type : 'POST',
				url : 'get/get_chart_data.php',
				data: "key=factures_region&mois="+mois+"&annee="+annee,
				success : function(data)
				{
					// console.log(data);
					gps_data = JSON.parse(data) ; 
					showmapGPS();

				}
			});
			
		}
		
		$(document).on('click', '#factures_region_date', function(){ 
			res = $('#factures_region_value').val().split('-');
			factures_region(res[0],res[1]);
		
		})
		$('#factures_region_picker .input-group.date').datepicker({
			format: "mm-yyyy",
			startView: "months",  
			minViewMode: "months",
			onSelect: function(dateText, inst) {
				alert(dateText);
			}
		})
		
		$('#factures_region_value').val(moment().format('MM-YYYY'));
		
		
		

			
			
			
			
			  
});
		
		
		
		

    </script>
	
	
<script>


function explodePie (e) {
	if(typeof (e.dataSeries.dataPoints[e.dataPointIndex].exploded) === "undefined" || !e.dataSeries.dataPoints[e.dataPointIndex].exploded) {
		e.dataSeries.dataPoints[e.dataPointIndex].exploded = true;
	} else {
		e.dataSeries.dataPoints[e.dataPointIndex].exploded = false;
	}
	e.chart.render();

}
</script>

</body>

</html>
