<?php
ob_start();
session_start();

include 'connexion.php';


if(isset($_SESSION['user_einvoicetrack']) )
{
	header('Location: index');
}


$mdpErr = "";
$flag = 1;



function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>eInvoiceTrack</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="css/plugins/iCheck/custom.css" rel="stylesheet">
     <!-- Text spinners style -->
    <link href="css/plugins/textSpinners/spinners.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
	
	
	<!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <!-- iCheck -->
    <script src="js/plugins/iCheck/icheck.min.js"></script>
	
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	
	 <!-- Custom and plugin javascript -->
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>

    <!-- Password meter -->
    <script src="js/plugins/pwstrength/pwstrength-bootstrap.min.js"></script>
    <script src="js/plugins/pwstrength/zxcvbn.js"></script>

    <script>
        $(document).ready(function(){
            var options1 = {};
				options1.ui = {
					container: "#pwd-container1",
					showVerdictsInsideProgressBar: true,
					viewports: {
						progress: ".pwstrength_viewport_progress"
					}
				};
				options1.common = {
					debug: false,
				};
				$('.example1').pwstrength(options1);
        });
		
    </script>
	
	
	<style>
	.required{
		color : red ; 
		float: left;
	}
	</style>

</head>





<body class="gray-bg">



<?php


 

if(isset($_GET['token']) && isset($_GET['token']) )
{
	

	$token = $_GET['token'];
	$login = decode2($_GET['token']);
	$flag_time = date('Y-m-d H:i:s', decode($_GET['flag']));
	
	

	if((time()-(60*60*24)) < strtotime($flag_time))
	{
		
		if( isset($_POST['submit']) )
		{
			if (isset($_POST['password1']) && isset($_POST['password1_confirm']) )
			{
				
				$mdp =  mysqli_real_escape_string($ma_connexion,$_POST["password1"]) ;
				if (empty($mdp)) {
					$mdpErr = "le mot de passe est obligatoire";
					$flag = 0 ; 
				  } else {
					$mdp = test_input($mdp);
					if (strlen($mdp) < 6) {
					  $mdpErr = "Le mot de passe doit contenir au moins 6 caractères!"; 
					  $flag = 0 ; 
					}
				}
				
				
				$mdp_confirm =  test_input(mysqli_real_escape_string($ma_connexion,$_POST["password1_confirm"])) ;
				
				if( $mdp != $mdp_confirm )
				{
					$mdpErr = "Les mots de passes saisis ne sont pas identiques "   ;
					$flag = 0 ;
				}
				
				
				echo $mdp ; 
				
				if( $flag == 1 ) 
				{
					$sqlUpdate= "Update  user set MDP_USER = '$mdp' WHERE CODE_USER = $login "; 

					if (mysqli_query($ma_connexion, $sqlUpdate)) {
						
						echo '
								<script>
									Swal.fire({
									  type: "success",
									  title: "Votre mot de passe a bien été modifié",
									  showConfirmButton: false,
									  timer: 3000
									})
								</script>
							';
							header( "refresh:3;url=login?token=$token" );
						
					}
						

						
				}
				else 
				{
						// echo '
						// <script>
											
							
							// Swal.fire({
							  // type: "error",
							  // title: "Utilisateur existe déjà ou informations erronées ",
							  // showConfirmButton: false,
							  // timer: 2000
							// })
							
						// </script> ';
						// echo '<br/>'. mysqli_error($ma_connexion);
						$BigErr = "informations erronées -";
				}
				
			}
			else 
			{
				$BigErr = "informations erronées -";
			}	
		}
	
		?>
		
		<div class="passwordBox animated fadeInDown">
			<div class="row">

				<div class="col-md-12">
					<div class="ibox-content">

						<h2 class="font-bold">Mot de passe oublié</h2>

						<p>
							Entrez votre adresse e-mail pour recevoir un lien de réinitialisation dans votre courrier électronique.
						</p>

						<div class="row">

							<div class="col-lg-12">
								<form class="m-t" method="POST">
									<div class="row" id="pwd-container1">
										<div class="col-sm-12">
											<div class="form-group">
												<span class="label label-info float-left">Nouveau de passe  </span>  <span class="required">* <?php echo $mdpErr;?> </span>
												<input type="password" class="form-control example1" id="password1" name="password1" placeholder="**********">
											</div>
											<div class="form-group">
												<div class="pwstrength_viewport_progress"></div>
											</div>
										</div>
									</div>
									
									<div class="form-group">
										<span class="label label-info float-left">Confirmer mot de passe  </span>  <span class="required">* </span>
										<input type="password" class="form-control" id="password1_confirm" name="password1_confirm" placeholder="Password">
									</div>
										
										 

									<button type="submit" name="submit" class="btn btn-primary block full-width m-b">Enregistrer</button>

								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<hr/>
			<div class="row">
				<div class="col-md-6">
					eInvoiceTrack
				</div>
				<div class="col-md-6 text-right">
				   <small>© 2019</small>
				</div>
			</div>
		</div>
		
		<?php
		
	}
	else 
	{
		
		?>
		
		
		<?php
	}
}





 ?>


    
</body>

</html>




<?php





ob_end_flush();
?>

