<?php
ob_start();
session_start();
include 'connexion.php';

$folder_name = 'tmp';
$match = '';
if(isset($_SESSION['user_einvoicetrack']))
{
	
	$current_user = decode($_SESSION['user_einvoicetrack']) ; 					
	$SQL="SELECT  `NOM_USER`, `PRENOM_USER`, `EMAIL_USER`,`CIVILITE_USER`,
			`CODE_ENTREPRISE`, `ROLE_USER`
		  FROM `user` 
		  WHERE CODE_USER = $current_user
		  AND actif= 1";
	$query=mysqli_query($ma_connexion,$SQL);
	if(mysqli_num_rows($query) == 1)
	{
		while($row=mysqli_fetch_assoc($query))
		{	
				$NOM_USER = $row['CIVILITE_USER'].' ' .$row['NOM_USER'];
		}
	}
	else 
	{
		header('Location: login');
	}
	if( $_SESSION['role'] != 'superadmin' )
	{
		$SQL="SELECT 1
				FROM  user_module um
				WHERE  um.CODE_USER ='".decode($_SESSION['user_einvoicetrack'])."' 
				AND um.CODE_MODULE = 5"
		;
		$query=mysqli_query($ma_connexion,$SQL);
			
		if(mysqli_num_rows($query) == 0)
		{
			
			header('Location: users');
		}
	}
	
	if(isset($_GET['target']))
	{
		if($_GET['target'] == 'tmp')
			$folder_name =  'tmp';
		else if($_GET['target'] == 'einvoicetrack')
			$folder_name =  'einvoicetrack'; 
		
	}
	
	if(isset($_GET['match']))
	{
		$match =  $_GET['match'] ;

		
	}
		
	
		// $i = 0 ; 
												

		// $SQL="SHOW COLUMNS FROM client";
		// $query=mysqli_query($ma_connexion,$SQL);
		// while($row=mysqli_fetch_assoc($query))
		// {	
				// echo '
					// <a class="toggle-vis" data-column="'.$i++.'">'.$row['Field'].'</a> -
					// ';
		// }
}
else 
{
	header('Location: login');
}

?>
<!DOCTYPE html>
<html lang="fr">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>eInvoiceTrack</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
	
	<link href="css/plugins/dropzone/basic.css" rel="stylesheet">
    <link href="css/plugins/dropzone/dropzone.css" rel="stylesheet">
	
	    <link href="css/plugins/summernote/summernote-bs4.css" rel="stylesheet">

	 
 <!-- Bootstrap Tour -->
    <link href="css/plugins/bootstrapTour/bootstrap-tour.min.css" rel="stylesheet">



	<style>
	.note-editor{
		    width: 100%;
	}
	#summernote{
		    width: 100%;
	}
	</style>
</head>

<body class="">

    <div id="wrapper">

    
		<?php
			include 'includes/nav.php';
		?>	

        <div id="page-wrapper" class="gray-bg">
		   <?php
				include 'includes/header.php';
			?>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Mise sous enveloppe</h2>
				</div>
				<div class="col-sm-8">
				<div class="title-action">
				 <a href="#" class="btn btn-primary startTour"><i class="fa fa-play"></i> Démo</a>
				</div>
			</div>
            </div>
        <div class="wrapper wrapper-content">
            <div class="row">
			
			
			
			
                <div class="col-lg-4">
                    <div class="ibox" id="ibox_import">
						 <div class="ibox-title" style="background-color: #24c6c8; color: white;">
						 <h5> <i class="fa fa-file"></i> Importer liste des factures</h5>
						 
							</div>
							<div class="ibox-content">
									<div class="sk-spinner sk-spinner-wave " >
										<div class="sk-rect1"></div>
										<div class="sk-rect2"></div>
										<div class="sk-rect3"></div>
										<div class="sk-rect4"></div>
										<div class="sk-rect5"></div>
									</div>
									
								<form method="get" action="Fichier facture MSE.xlsx">
									<div class="alert alert-warning">
										Merci de respecter l'ordonnancement et les libellés 
										des colonnes du modéle d'import
										</div>
									
									<button class="btn btn-primary btn-rounded btn-block"  id="tour_download" type="submit"><i class="fa fa-download"></i> Télécharger le modèle d'import</button>
								</form>	
								<br/>
								<br/>	
								<form action="mse_worker.php" class="dropzone a" id="dropzoneForm"  id="tour_import">
									
									
								</form>
								<br/>
								<button class="btn btn-success btn-rounded btn-block" id="importer"><i class="fa fa-gear"></i> Traiter</button>
								
								<div class="logs"
                                     style="overflow-y: scroll; max-height:400px;  margin-top: 10px; margin-bottom:10px;">
								
								</div>
							</div>
						   
						</div>
                </div>
				
				<div class="col-lg-8" >
                <div class="ibox ">

                    <div class="ibox-content">
					<?php
					if( $_SESSION['role'] ==  'superadmin' ||  $_SESSION['role'] ==  'admin' )
					{
						?>
                    <div id="tour_template_btn">
								<button target="summernote" value="[Code client]"class="addResource btn badge badge-success text-light" > [Code client]</button>
								<button target="summernote" value="[Nom client]"class="addResource btn badge badge-success text-light" > [Nom client]</button>
								<button target="summernote" value="[Adresse]"class="addResource btn badge badge-success text-light" > [Adresse]</button>
								<button target="summernote" value="[Ville]"class="addResource btn badge badge-success text-light" > [Ville]</button>
								<button target="summernote" value="[Num facture]"class="addResource btn badge badge-success text-light" > [Num facture]</button>
								<button target="summernote" value="[Date  comptabilisation]"class="addResource btn badge badge-success text-light" > [Date  comptabilisation]</button>
								<button target="summernote" value="[Date aujourd'hui]"class="addResource btn badge badge-primary text-light" > [Date aujourd'hui]</button>
								<button class="btn badge badge-dark text-light pull-right" style="margin-left:2px ; background-color: #101010;"id="reset" ><i class="fa fa-refresh"></i> </button> &nbsp;
								<button class="btn badge badge-info text-light pull-right" style="margin-left:2px"id="enregistrer" >Enregistrer </button> &nbsp;
								<button class="btn badge badge-danger text-light pull-right" style="marin-left:10px"id="vider" >Vider </button> &nbsp;
					</div>


						<input type="hidden" name="isAdminToken" id="isAdminToken" value="X9823i9?;zz0">
					<?php
					}
					?>
                        <div id="tour_template" >
							<div id="summernote" >

								<?php

									$SQL="SELECT  HTML
										  FROM `ar_template`
										  ORDER BY CODE DESC LIMIT 1 " ;
									$query=mysqli_query($ma_connexion,$SQL);

									while($row=mysqli_fetch_assoc($query))
									{
											echo $row['HTML'];
									}



								?>

							</div>
                        </div>

                    </div>
                </div>
            </div>
               
                
				
				
               
                </div>
                </div>
				<?php
						include 'includes/footer.php';
					?>	

        </div>
            </div>


    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>
	
	<script src="js/plugins/pdfjs/pdf.js"></script>
	
	
	
    <!-- DROPZONE -->
    <script src="js/plugins/dropzone/dropzone.js"></script>
	
	  <!-- Bootstrap Tour -->
    <script src="js/plugins/bootstrapTour/bootstrap-tour.min.js"></script>

	

	 <script id="script">
	 


        $(document).ready(function () {
				 
	 
	 var tour = new Tour({
            steps: [
                {
                    element: "#ibox_import",
                    title: "Ajouter client",
                    content: "Vous pouvez ajouter un client manuellement <br/> Les champs marqués par ( <span style='color:red'>*</span> ) sont obligatoires",
                    placement: "top",
                    backdrop: true,
                    backdropContainer: '#wrapper',
                    onShown: function (tour){
                        $('body').addClass('tour-open')
                    },
                    onHidden: function (tour){
                        $('body').removeClass('tour-close')
                    }
                },
                {
                    element: "#tour_download",
                    title: "Ajouter client",
                    content: "Vous pouvez ajouter un client manuellement <br/> Les champs marqués par ( <span style='color:red'>*</span> ) sont obligatoires",
                    placement: "top",
                    backdrop: true,
                    backdropContainer: '#wrapper',
                    onShown: function (tour){
                        $('body').addClass('tour-open')
                    },
                    onHidden: function (tour){
                        $('body').removeClass('tour-close')
                    }
                },
                {
                    element: "#tour_import",
                    title: "Ajouter client",
                    content: "Vous pouvez ajouter un client manuellement <br/> Les champs marqués par ( <span style='color:red'>*</span> ) sont obligatoires",
                    placement: "top",
                    backdrop: true,
                    backdropContainer: '#wrapper',
                    onShown: function (tour){
                        $('body').addClass('tour-open')
                    },
                    onHidden: function (tour){
                        $('body').removeClass('tour-close')
                    }
                },
                {
                    element: "#tour_template_btn",
                    title: "Ajouter client",
                    content: "Vous pouvez ajouter un client manuellement <br/> Les champs marqués par ( <span style='color:red'>*</span> ) sont obligatoires",
                    placement: "top",
                    backdrop: true,
                    backdropContainer: '#wrapper',
                    onShown: function (tour){
                        $('body').addClass('tour-open')
                    },
                    onHidden: function (tour){
                        $('body').removeClass('tour-close')
                    }
                },
                {
                    element: "#tour_template",
                    title: "Ajouter client",
                    content: "Vous pouvez ajouter un client manuellement <br/> Les champs marqués par ( <span style='color:red'>*</span> ) sont obligatoires",
                    placement: "top",
                    backdrop: true,
                    backdropContainer: '#wrapper',
                    onShown: function (tour){
                        $('body').addClass('tour-open')
                    },
                    onHidden: function (tour){
                        $('body').removeClass('tour-close')
                    }
                },
                
            ],
			smartPlacement: true,
			  keyboard: true,
			  storage: false,
			  debug: true,
			  labels: {
				end: 'Terminer',
				next: 'Suivant &raquo;',
				prev: '&laquo; Prev'
			  },
			  });

        // Initialize the tour
        tour.init();

         $('.startTour').click(function(){
            tour.restart();

            // Start the tour
            // tour.start();
        });
	});
	 // showpdf("ar.pdf");
        function showpdf(url)
		{
			

			var pdfDoc = null,
					pageNum = 1,
					pageRendering = false,
					pageNumPending = null,
					scale = 1.5,
					zoomRange = 0.25,
					canvas = document.getElementById('the-canvas'),
					ctx = canvas.getContext('2d');

			/**
			 * Get page info from document, resize canvas accordingly, and render page.
			 * @param num Page number.
			 */
			function renderPage(num, scale) {
				pageRendering = true;
				// Using promise to fetch the page
				pdfDoc.getPage(num).then(function(page) {
					var viewport = page.getViewport(scale);
					canvas.height = viewport.height;
					canvas.width = viewport.width;

					// Render PDF page into canvas context
					var renderContext = {
						canvasContext: ctx,
						viewport: viewport
					};
					var renderTask = page.render(renderContext);

					// Wait for rendering to finish
					renderTask.promise.then(function () {
						pageRendering = false;
						if (pageNumPending !== null) {
							// New page rendering is pending
							renderPage(pageNumPending);
							pageNumPending = null;
						}
					});
				});

				// Update page counters
				document.getElementById('page_num').value = num;
			}

			/**
			 * If another page rendering in progress, waits until the rendering is
			 * finised. Otherwise, executes rendering immediately.
			 */
			function queueRenderPage(num) {
				if (pageRendering) {
					pageNumPending = num;
				} else {
					renderPage(num,scale);
				}
			}

			/**
			 * Displays previous page.
			 */
			function onPrevPage() {
				if (pageNum <= 1) {
					return;
				}
				pageNum--;
				var scale = pdfDoc.scale;
				queueRenderPage(pageNum, scale);
			}
			document.getElementById('prev').addEventListener('click', onPrevPage);

			/**
			 * Displays next page.
			 */
			function onNextPage() {
				if (pageNum >= pdfDoc.numPages) {
					return;
				}
				pageNum++;
				var scale = pdfDoc.scale;
				queueRenderPage(pageNum, scale);
			}
			document.getElementById('next').addEventListener('click', onNextPage);

			/**
			 * Zoom in page.
			 */
			function onZoomIn() {
				if (scale >= pdfDoc.scale) {
					return;
				}
				scale += zoomRange;
				var num = pageNum;
				renderPage(num, scale)
			}
			document.getElementById('zoomin').addEventListener('click', onZoomIn);

			/**
			 * Zoom out page.
			 */
			function onZoomOut() {
				if (scale >= pdfDoc.scale) {
					return;
				}
				scale -= zoomRange;
				var num = pageNum;
				queueRenderPage(num, scale);
			}
			document.getElementById('zoomout').addEventListener('click', onZoomOut);

			/**
			 * Zoom fit page.
			 */
			function onZoomFit() {
				if (scale >= pdfDoc.scale) {
					return;
				}
				scale = 1;
				var num = pageNum;
				queueRenderPage(num, scale);
			}
			document.getElementById('zoomfit').addEventListener('click', onZoomFit);


			/**
			 * Asynchronously downloads PDF.
			 */
			PDFJS.getDocument(url).then(function (pdfDoc_) {
				pdfDoc = pdfDoc_;
				var documentPagesNumber = pdfDoc.numPages;
				document.getElementById('page_count').textContent = '/ ' + documentPagesNumber;

				$('#page_num').on('change', function() {
					var pageNumber = Number($(this).val());

					if(pageNumber > 0 && pageNumber <= documentPagesNumber) {
						queueRenderPage(pageNumber, scale);
					}

				});

				// Initial/first page rendering
				renderPage(pageNum, scale);
			});
		}
		 // Dropzone.autoDiscover = false;
		var  start_time = null;
		
		function millisToMinutesAndSeconds(millis) {
		  var minutes = Math.floor(millis / 60000);
		  var seconds = ((millis % 60000) / 1000).toFixed(0);
		  return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
		}

		
		
		Dropzone.autoDiscover = false;
		
		var myDropzoneTheFirst = new Dropzone(
				//id of drop zone element 1
				'#dropzoneForm', { 
					autoProcessQueue: false,
					acceptedFiles:".xlsx,xls",
					addRemoveLinks: true,
                    timeoutSeconds: 0,
					dictDefaultMessage: "<strong>Déposer les fichiers ici ou cliquer pour importer. </strong></br> ",
					init: function(){
						let myDropzone = this;
						$(document).on('click', '#importer', function(e)
						{ 
							// $("body").html( $('.summernote').summernote('code'));
							myDropzone.processQueue();
						});
						 this.on("success", function(file, response) {
							// console.log(response);
							 var _this = this;
							 _this.removeAllFiles();
							 $(".logs").html(response);
							 console.log(response);
							 // $("body").html(response);

							 $(".logs").prepend('<span class="badge badge-warning">Import dans '+ millisToMinutesAndSeconds((new Date().getTime() - start_time))+' minutes  </span>');
							 $('#ibox_import').children('.ibox-content').toggleClass('sk-loading');
						})
						 this.on('error', function(file, response) {
							$(file.previewElement).find('.dz-error-message').text(response);
							console.log(response);
						});
						 this.on('sending', function(file, xhr, formData){
						   // alert('ok');
                             xhr.ontimeout = (() => {
                                 /*Execute on case of timeout only*/
                                 console.log('Server Timeout this.on sending ' )
                             });

						    formData.append('html', $('#summernote').summernote('code'));
							start_time = new Date().getTime() ; 
							$('#ibox_import').children('.ibox-content').toggleClass('sk-loading');
						});
					},
				}
			);
		
    </script>
	
	   <!-- SUMMERNOTE -->
    <script src="js/plugins/summernote/summernote-bs4.js"></script>
	
	 <!-- Sweet alert -->
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	

    <script>
        $(document).ready(function(){

            // $('.summernote').summernote();

			 $('#summernote').summernote({
				placeholder: 'AR Template',
				tabsize: 2,
				disableDragAndDrop: true,
				height: 800,
				toolbar: [
				  ['img', ['picture']],
				  ['style', ['style', 'addclass', 'clear']],
				  ['fontstyle', ['bold', 'italic', 'ul', 'ol', 'link', 'paragraph']],
				  ['fontstyleextra', ['strikethrough', 'underline', 'hr', 'color', 'superscript', 'subscript']]
					]
			  });
			  
			  if(!$('#isAdminToken').val())
				  $('#summernote').summernote('disable');

				  


       });
	   
	   
	   $('.addResource').on('click', function() {
		  
		var text = '<b>'+$(this).val()+' </b>';
		$('#summernote').summernote('editor.pasteHTML', text);

	});
	   $('#vider').on('click', function() {
			$('#summernote').summernote('code','')
		});
	   $('#enregistrer').on('click', function() {
			
			 $.ajax({
					url : 'set/enregistrer_template.php',               
					type:   'POST',    
					data: 'html='+$('#summernote').summernote('code'),
					success: function(date){
						// $(".logs").html(date);
						Swal.fire(
						  'Modèl AR modifié avec succes  !',
						  'la template est à jour!.',
						  'success'
						)
						
					}
			   });
		
		});
	   $('#reset').on('click', function() {
			 $.ajax({
					url : 'get/get_template.php',               
					type:   'POST',    
					success: function(date){
						$('#summernote').summernote('code',date)
						
					}
			   });
			
		
		});
		
		
		
		
		
		
		
    </script>


	

</body>

</html>
