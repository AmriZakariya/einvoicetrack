<?php
require 'vendor/autoload.php';
use Dompdf\Dompdf;

// instantiate and use the dompdf class
$dompdf = new Dompdf();
$dompdf->loadHtml('
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<style>
  body { font-family: DejaVu Sans, sans-serif; }
</style>
<title>č s š Š</title>
</head>
<body>
<h3 style="text-align: right; ">Témara le &lt;&lt;auj&gt;&gt;</h3>
<h3 style="font-family: &quot;open sans&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; color: rgb(103, 106, 108); text-align: right;">&lt;&lt;code client&gt;&gt;</h3>
<h3 style="text-align: right; ">&lt;&lt;Nom client&gt;&gt;</h3>
<h3 style="text-align: right; ">&lt;&lt;Adresse&gt;&gt;</h3>
<h3 style="font-family: &quot;open sans&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; color: rgb(103, 106, 108); text-align: right;">&lt;&lt;Ville&gt;&gt;</h3>
<h3><br></h3>
<h3>Objet: Accusé réception des factures</h3>
<p>
    <br>
</p>
<h3>Messieurs,</h3>
<h3>Par la présente, vous accusez de réception des factures :</h3>
<h3>N° : &lt;&lt;Num facture&gt;&gt;</h3>
<h3>Date comptabilisation : &lt;&lt;Date comptabilisation&gt;&gt;</h3>
<h3>Date réception : __/__/____</h3>
<h3>Motif de rejet :</h3>
<p>
    <br>
</p>
<p>
    <br>
</p>
<table class="table table-bordered">
    <tbody>
        <tr>
            <td>
                <h3 style="font-family: &quot;open sans&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; color: rgb(103, 106, 108);">&nbsp; Manque bon de commande</h3></td>
            <td>
                <h3 style="font-family: &quot;open sans&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; color: rgb(103, 106, 108);"> Facture erronée( en double. date …)</h3></td>
        </tr>
        <tr>
            <td>
                <h3 style="font-family: &quot;open sans&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; color: rgb(103, 106, 108);">&nbsp; Liasse incomplète</h3></td>
            <td>
                <h3 style="font-family: &quot;open sans&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; color: rgb(103, 106, 108);">&nbsp; Litige</h3></td>
        </tr>
        <tr>
            <td>
                <h3 style="font-family: &quot;open sans&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; color: rgb(103, 106, 108);">&nbsp; Manque bon de livraison</h3></td>
            <td>
                <h3 style="font-family: &quot;open sans&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; color: rgb(103, 106, 108);">&nbsp; Refus</h3></td>
        </tr>
        <tr>
            <td>
                <h3 style="font-family: &quot;open sans&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; color: rgb(103, 106, 108);">&nbsp; Montant Incorrect (HT, TVA et TTC)</h3></td>
            <td>
                <h3 style="font-family: &quot;open sans&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; color: rgb(103, 106, 108);">&nbsp; Mentions obligatoires (ICE,RC, CNSS, Patente, IF. Raison sociale)</h3></td>
        </tr>
    </tbody>
</table>
<p>
    <br>
</p>
<h3>Nous vous en remercions et vous prions d’agréer, messieurs, l’assurance de nos saluations distinguées.<br></h3>
<h3>Cachet et signature</h3>
<ul>
</ul>
</body>
</html>
');

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'portrait');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
$dompdf->stream();

?>