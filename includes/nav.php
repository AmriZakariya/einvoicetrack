<nav class="navbar-default navbar-static-side scroll" role="navigation"  >
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu" >
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <img alt="image" class="rounded-circle" src="img/profile_small.jpg"/>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="block m-t-xs font-bold"><?php echo $NOM_USER ; ?></span>
                        <span class="text-muted text-xs block"><?php echo $_SESSION['role'] ; ?> <b class="caret"></b></span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a class="dropdown-item" href="profile">Profil</a></li>
                    </ul>
                </div>m
                <div class="logo-element">
                    eIT
                </div>
            </li>
            <?php
            $SQL2="SELECT m.NOM,m.DESCRIPTION,m.icon
									FROM module m , user_module um
									WHERE m.CODE = um.CODE_MODULE
									AND um.CODE_USER ='".decode($_SESSION['user_einvoicetrack'])."'  "
            ;
            // echo $SQL2 ;
            $query2=mysqli_query($ma_connexion,$SQL2);

            while($row2=mysqli_fetch_assoc($query2))
            {

                ?>

                <li class="<?php echo $row2['DESCRIPTION'] ?>"><a href="<?php echo $row2['DESCRIPTION'] ?>"><i class="<?php echo $row2['icon'] ?>"></i> <span class="nav-label"><?php echo $row2['NOM'] ?></span></a></li>
                <?php
            }





            ?>






            <?php
            if( $_SESSION['role'] ==  'superadmin')
            {
                echo '
							
							
								<li class="index"><a href="index"><i class="fa fa-tachometer"></i> <span class="nav-label">Tableau de Board</span></a></li>
								<li class="client"><a href="client"><i class="fa fa-user-circle-o"></i> <span class="nav-label">Client</span></a></li>
								<li class="facture"><a href="facture"><i class="fa fa-file"></i> <span class="nav-label">Factures clients</span></a></li>
								<li class="file_manager"><a href="file_manager"><i class="fa fa-folder"></i> <span class="nav-label"> Documents</span></a></li>
								<li class="graph"><a href="graph"><i class="fa fa-file"></i> <span class="nav-label"> Rapports</span></a></li>
								<li  >
								<a href="ar"><i class="fa fa-file"></i> <span class="nav-label"> Accusé de réception</span> <span class="fa arrow fa-arrow-right"></span></a>
									<ul class="nav nav-second-level collapse">
										<li class="ar">  <a href="ar"><i class="fa fa-file"></i> <span class="nav-label"> Accusé de réception</span></a></li>
										<li class="ar_list"> <a href="ar_list"><i class="fa fa-folder"></i> <span class="nav-label"> Dossier accusé de réception</span></a></li>
									</ul>
								</li>
							<!--	<li class="bc"><a href="bc"><i class="fa fa-file"></i> <span class="nav-label">Bon de commande</span></a></li> -->
								<li class="bl"><a href="bl"><i class="fa fa-file"></i> <span class="nav-label">Bon de livraison</span></a></li>
								<li class="mse"><a href="mse"><i class="fa fa-envelope"></i> <span class="nav-label">Mise sous enveloppe</span></a></li>
								<li class="drive"><a href="drive"><i class="fa fa-hdd-o"></i> <span class="nav-label">Drive</span></a></li>

							
							
							';
                echo'

							<li  class="users">
								<a href="users"><i class="fa fa-users"></i> <span class="nav-label"> Utilisateurs</span> <span class="fa arrow"></span></a>
									<ul class="nav nav-second-level collapse">
										<li class="users">  <a href="users"><i class="fa fa-users"></i> <span class="nav-label"> Utilisateurs </span></a></li>
										<li class="user_add"> <a href="user_add"><i class="fa fa-user-plus"></i> <span class="nav-label"> Ajouter utilisatur </span></a></li>
									</ul>
								</li>	
								
								<li class="restore"><a href="restore"><i class="fa fa-refresh"></i> <span class="nav-label">Restore</span></a></li>
								
							';
            }
            else
            {
                echo'
										<li class="users">
											<a href="users"><i class="fa fa-users"></i> <span class="nav-label"> Utilisateurs</span></a>
										</li>
									';


            }
            ?>
        </ul>

    </div>
</nav>