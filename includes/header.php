<?php

//ini_set('max_execution_time', 100000);
//set_time_limit(100000);
//?>

<div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <form role="search" class="navbar-form-custom" >
                <div class="form-group">
                    <input type="text" placeholder="Recherche ..." class="form-control" name="top-search" id="top-search">
                </div>
            </form>
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li style="padding: 20px">
                    <span class="m-r-sm text-muted welcome-message">Bienvenue sur eInvoiceTrack </span>
                </li>

                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell"></i>  <span class="label label-primary"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts" style="width: 700px">
                        <div class="inspinia-timeline" style="overflow-y: scroll; max-height:600px;  margin-top: 10px; margin-bottom:10px; background-color: #fafafa;">
                            <?php
                            $date_end = date('Y-m-d');
                            $date_start = date('Y-m-d', strtotime($date_end." -1 month"));

                            $SQL="SELECT a.ID,a.TITRE, a.DESCRIPTION, a.DATE,u.NOM_USER,u.CIVILITE_USER
										FROM action a,user u
										WHERE a.USER = u.CODE_USER
										AND DATE(a.DATE) >= '$date_start' AND '$date_end'
										ORDER BY a.ID DESC 
LIMIT 5";
                            // echo $SQL ;
                            $query=mysqli_query($ma_connexion,$SQL);
                            while($row=mysqli_fetch_assoc($query))
                            {
                                ?>

                                <div class="timeline-item" onclick="navigateProfile()">
                                    <div class="row">
                                        <div class="col-3 date">
                                            <i class="fa fa-briefcase"></i>
                                            <?php echo '<label style=" font-size: 13px;" class="label label-success "> '.$row['CIVILITE_USER'].' '.$row['NOM_USER'].'</label>'; ?>
                                            <?php //echo $row['CIVILITE_USER'] == 'Mr' ? '<i class="fa fa-male red"></i> Mr'.$row['NOM_USER'] : '<i class="fa fa-female"></i> Mme'.$row['NOM_USER'] ?>
                                            <br/>
                                            <?php echo '<label style=" font-size: 13px;" class="label label-secondary "> '.$row['DATE'].'</label>'; ?>
                                        </div>
                                        <div class="col-8 content no-top-border" style="line-height: 20px;overflow: hidden;">
                                            <p class="m-b-xs"><strong><?php echo '<label style=" font-size: 13px;"class="label label-info "> '.$row['TITRE'].'</label>';?></strong></p>
                                             <p ><?php echo $row['DESCRIPTION'] ; ?>.</p>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>


                        </div>

                    </ul>
                </li>


                <li>
                    <a class="deconnexion">
                        <i class="fa fa-sign-out"></i> Déconnexion
                    </a>
                </li>
                <li>
                    <a class="right-sidebar-toggle">
                        <i class="fa fa-tasks"></i>
                    </a>
                </li>
            </ul>

        </nav>
        </div>


<script>
    function navigateProfile() {
        window.location.href = "./profile";
    }

</script>