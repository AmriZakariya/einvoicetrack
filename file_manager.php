<?php
ob_start();
session_start();
include 'connexion.php';

$folder_name = 'tmp';
$match = '';
if(isset($_SESSION['user_einvoicetrack']))
{

    $current_user = decode($_SESSION['user_einvoicetrack']) ;
    $SQL="SELECT  `NOM_USER`, `PRENOM_USER`, `EMAIL_USER`,`CIVILITE_USER`,
			`CODE_ENTREPRISE`, `ROLE_USER`
		  FROM `user` 
		  WHERE CODE_USER = $current_user
		  AND actif= 1";
    $query=mysqli_query($ma_connexion,$SQL);
    if(mysqli_num_rows($query) == 1)
    {
        while($row=mysqli_fetch_assoc($query))
        {
            $NOM_USER = $row['CIVILITE_USER'].' ' .$row['NOM_USER'];
        }
    }
    else
    {
        header('Location: login');
    }

    if( $_SESSION['role'] != 'superadmin' )
    {
        $SQL="SELECT 1
				FROM  user_module um
				WHERE  um.CODE_USER ='".decode($_SESSION['user_einvoicetrack'])."' 
				AND um.CODE_MODULE = 8"
        ;
        $query=mysqli_query($ma_connexion,$SQL);

        if(mysqli_num_rows($query) == 0)
        {

            header('Location: users');
        }
    }
    if(isset($_GET['target']))
    {
        if($_GET['target'] == 'tmp')
            $folder_name =  'tmp';
        else if($_GET['target'] == 'einvoicetrack')
            $folder_name =  'einvoicetrack';
        else if($_GET['target'] == 'dossier_bc')
            $folder_name =  'dossier_bc';
        else if($_GET['target'] == 'dossier_bl')
            $folder_name =  'dossier_bl';
        else if($_GET['target'] == 'mse_gene')
            $folder_name =  'mse_gene';
        else
            $folder_name =  $_GET['target'] ;


    }

    if(isset($_GET['match']))
    {
        $match =  $_GET['match'] ;


    }


    // $i = 0 ;


    // $SQL="SHOW COLUMNS FROM client";
    // $query=mysqli_query($ma_connexion,$SQL);
    // while($row=mysqli_fetch_assoc($query))
    // {
    // echo '
    // <a class="toggle-vis" data-column="'.$i++.'">'.$row['Field'].'</a> -
    // ';
    // }
}
else
{
    header('Location: login');
}
?>
<!DOCTYPE html>
<html lang="fr">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>eInvoiceTrack</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <!-- Bootstrap Tour -->
    <link href="css/plugins/bootstrapTour/bootstrap-tour.min.css" rel="stylesheet">



    <style>

        .myInputautocomplete-list {
            /*the container must be positioned relative:*/
            position: relative;
            display: inline-block;
        }
        .autocomplete {
            /*the container must be positioned relative:*/
            position: relative;
            display: inline-block;
        }


        .autocomplete-items {
            position: absolute;
            border: 1px solid #d4d4d4;
            border-bottom: none;
            border-top: none;
            z-index: 99;
            /*position the autocomplete items to be the same width as the container:*/
            top: 100%;
            left: 0;
            right: 0;
            width: 30%;

            overflow-y: scroll; max-height:300px;
        }
        .autocomplete-items div {
            padding: 10px;
            cursor: pointer;
            background-color: #fff;
            border-bottom: 1px solid #d4d4d4;
        }
        .autocomplete-items div:hover {
            /*when hovering an item:*/
            background-color: #e9e9e9;
        }
        .autocomplete-active {
            /*when navigating through the items using the arrow keys:*/
            background-color: DodgerBlue !important;
            color: #ffffff;
        }
    </style>



</head>

<body class="">

<div id="wrapper">


    <?php
    include 'includes/nav.php';
    ?>

    <div id="page-wrapper" class="gray-bg">
        <?php
        include 'includes/header.php';
        ?>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <h2>Documents</h2>

            </div>
            <div class="col-sm-8">
                <div class="title-action">
                    <a href="#" class="btn btn-primary startTour"><i class="fa fa-play"></i> Démo</a>
                </div>
            </div>
        </div>
        <div class="wrapper wrapper-content">
            <div class="row">
                <div class="col-lg-3" >
                    <div class="ibox " id="tour_dossier">
                        <div class="ibox-content">
                            <div class="file-manager" >
                                <h5>Dossiers</h5>
                                <ul class="folder-list" style="padding: 0">
                                    <li <?php if($folder_name == 'tmp') echo 'style="background-color: #e1e1e2"' ; ?>><a href="file_manager?target=tmp"><i class="fa fa-folder"></i> tmp</a></li>
                                    <li <?php if($folder_name == 'einvoicetrack') echo 'style="background-color: #e1e1e2"' ; ?>><a href="file_manager?target=einvoicetrack"><i class="fa fa-folder"></i> einvoicetrack</a></li>
                                    <li <?php if($folder_name == 'dossier_bc') echo 'style="background-color: #e1e1e2"' ; ?>><a href="file_manager?target=dossier_bc"><i class="fa fa-folder"></i> Bon de commande</a></li>
                                    <li <?php if($folder_name == 'dossier_bl') echo 'style="background-color: #e1e1e2"' ; ?>><a href="file_manager?target=dossier_bl"><i class="fa fa-folder"></i> Bon de livraison</a></li>
                                    <li <?php if($folder_name == 'mse_gene') echo 'style="background-color: #e1e1e2"' ; ?>><a href="file_manager?target=mse_gene"><i class="fa fa-folder"></i> MSE</a></li>

                                    <ul class="folder-list inside" style="padding: 0 ; margin-left: 10px;">

                                        <?php
                                        $files = scandir("mse_gene");
                                        if(false !== $files)
                                        {

                                            foreach($files as $file)
                                            {

                                                if('.' !=  $file && '..' != $file )
                                                {
                                                    if($folder_name == 'mse_gene') {
                                                        echo '  <li  style="background-color: #e1e1e2" > ' ;

                                                    }
                                                    else{
                                                        echo  '  <li>  ' ;

                                                    }


                                                    echo '
            			                          
            			                                <a href="file_manager?target=mse_gene/'.$file.'"><i class="fa fa-folder"></i> '.$file.' </a>
            			                            </li>
    
            								    ' ;
                                                }
                                            }
                                        }



                                        ?>

                                    </ul>
                                </ul>

                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 animated fadeInRight" >
                    <form method="GET" action="file_manager" >
                        <div class="input-group  " id="tour_search">
                            <input type="hidden" name="target" value="<?php echo $folder_name ;?>">
                            <input type="text" name="match" autocomplete="off" placeholder="Rechercher ici..." value="" maxlength="70" class="searchinput" name="search" id="myInput" style="width:24%">
                            <button type="submit"  class="btn btn-primary">Rechercher!</button>
                            <span class="input-group-append">
						</span>
                        </div>
                    </form>
                    <br>

                    <div class="row" id="tour_list">
                        <div class="col-lg-12">
                            <?php
                            $files = scandir($folder_name);
                            if(false !== $files)
                            {
                                foreach($files as $file)
                                {
                                    if('.' !=  $file && '..' != $file && pathinfo($file)['extension'] == 'pdf' )
                                    {
                                        // unlink($folder_name.DIRECTORY_SEPARATOR.$file);
                                        if ( $match != '' )
                                        {
                                            if(strpos($file, $match) !== false)
                                            {
                                                echo '
										<div >
												 <div class="file-box">
													<a class="add_preview"   name="'.$folder_name.'/'.$file.'">
														<div class="file">
															<span class="corner"></span>

															<div class="icon">
																<i class="fa fa-file-pdf-o"></i>
															</div>
															<div class="file-name">
																'.$file.'
																<br/>
																<small>Dernière modification: '.date("F d Y H:i:s.",filemtime($folder_name.'/'.$file)).'</small>
																
															</div>
															<a download="'.$folder_name.'/'.$file.'" href="'.$folder_name.'/'.$file.'" title="'.$file.'">
																<img src="dw.png" class="img-fluid d-block " style="width:20px ; margin-left: 45%;" alt="'.$file.'">
															</a>
														</div>
													</a>
												</div>
												</div>
												
												';

                                            }

                                        }
                                        else
                                        {

                                            echo '
										<div >
										 <div class="file-box">
											<a class="add_preview"   name="'.$folder_name.'/'.$file.'">
												<div class="file">
													<span class="corner"></span>

													<div class="icon">
														<i class="fa fa-file-pdf-o"></i>
													</div>
													<div class="file-name">
														'.$file.'
														<br/>
														<small>Dernière modification: '.date("F d Y H:i:s.",filemtime($folder_name.'/'.$file)).'</small>
														
														

													</div>
													<a download="'.$folder_name.'/'.$file.'" href="'.$folder_name.'/'.$file.'" title="'.$file.'">
															<img src="dw.png" class="img-fluid d-block " style="width:20px ; margin-left: 45%;" alt="'.$file.'">
														</a>
												</div>
											</a>
										</div>
										</div>
										
										
										
										';


                                        }

                                    }

                                }
                            }

                            ?>
                        </div>
                        <div class="col-lg-12" id="preview_div">
                            <div class="text-center pdf-toolbar">

                                <div class="btn-group">
                                    <button id="prev" class="btn btn-white"><i class="fa fa-long-arrow-left"></i> <span class="d-none d-sm-inline">Previous</span></button>
                                    <button id="next" class="btn btn-white"><i class="fa fa-long-arrow-right"></i> <span class="d-none d-sm-inline">Next</span></button>
                                    <button id="zoomin" class="btn btn-white"><i class="fa fa-search-minus"></i> <span class="d-none d-sm-inline">Zoom In</span></button>
                                    <button id="zoomout" class="btn btn-white"><i class="fa fa-search-plus"></i> <span class="d-none d-sm-inline">Zoom Out</span> </button>
                                    <button id="zoomfit" class="btn btn-white"> 100%</button>
                                    <span class="btn btn-white hidden-xs">Page: </span>

                                    <div class="input-group">
                                        <input type="text" class="form-control" id="page_num">

                                        <div class="input-group-append">
                                            <button type="button" class="btn btn-white" id="page_count">/ 22</button>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="text-center m-t-md">
                                <canvas id="the-canvas" class="pdfcanvas border-left-right border-top-bottom b-r-md"></canvas>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <?php
        include 'includes/footer.php';
        ?>

    </div>
</div>


<!-- Mainly scripts -->
<script src="js/jquery-3.1.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="js/inspinia.js"></script>
<script src="js/plugins/pace/pace.min.js"></script>

<script src="js/plugins/pdfjs/pdf.js"></script>



<!-- Bootstrap Tour -->
<script src="js/plugins/bootstrapTour/bootstrap-tour.min.js"></script>

<script id="script">



    function autocomplete(inp, arr) {
        // alert('ok');
        /*the autocomplete function takes two arguments,
        the text field element and an array of possible autocompleted values:*/
        var currentFocus;
        /*execute a function when someone writes in the text field:*/
        inp.addEventListener("input", function(e) {
            var a, b, i, val = this.value,posiindex;
            /*close any already open lists of autocompleted values*/
            closeAllLists();
            if (!val) { return false;}
            currentFocus = -1;
            /*create a DIV element that will contain the items (values):*/
            a = document.createElement("DIV");
            a.setAttribute("id", this.id + "autocomplete-list");
            a.setAttribute("class", "autocomplete-items");
            /*append the DIV element as a child of the autocomplete container:*/
            this.parentNode.appendChild(a);
            /*for each item in the array...*/
            for (i = 0; i < arr.length; i++) {
                /*check if the item starts with the same letters as the text field value:*/
                if ( (posiindex = (arr[i].toUpperCase().indexOf(val.toUpperCase()))) !== -1) {
                    /*create a DIV element for each matching element:*/
                    b = document.createElement("DIV");
                    /*make the matching letters bold:*/
                    b.innerHTML =  arr[i].substr(0, posiindex);
                    b.innerHTML += "<strong>" + arr[i].substr(posiindex, val.length) + "</strong>";
                    b.innerHTML += arr[i].substr(val.length);
                    /*insert a input field that will hold the current array item's value:*/
                    b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                    /*execute a function when someone clicks on the item value (DIV element):*/
                    b.addEventListener("click", function(e) {
                        /*insert the value for the autocomplete text field:*/
                        inp.value = this.getElementsByTagName("input")[0].value;
                        /*close the list of autocompleted values,
                        (or any other open lists of autocompleted values:*/
                        closeAllLists();
                    });
                    a.appendChild(b);
                }
            }
        });
        /*execute a function presses a key on the keyboard:*/
        inp.addEventListener("keydown", function(e) {
            var x = document.getElementById(this.id + "autocomplete-list");
            if (x) x = x.getElementsByTagName("div");
            if (e.keyCode == 40) {
                /*If the arrow DOWN key is pressed,
                increase the currentFocus variable:*/
                currentFocus++;
                /*and and make the current item more visible:*/
                addActive(x);
            } else if (e.keyCode == 38) { //up
                /*If the arrow UP key is pressed,
                decrease the currentFocus variable:*/
                currentFocus--;
                /*and and make the current item more visible:*/
                addActive(x);
            } else if (e.keyCode == 13) {
                /*If the ENTER key is pressed, prevent the form from being submitted,*/
                e.preventDefault();
                if (currentFocus > -1) {
                    /*and simulate a click on the "active" item:*/
                    if (x) x[currentFocus].click();
                }
            }
        });
        function addActive(x) {
            /*a function to classify an item as "active":*/
            if (!x) return false;
            /*start by removing the "active" class on all items:*/
            removeActive(x);
            if (currentFocus >= x.length) currentFocus = 0;
            if (currentFocus < 0) currentFocus = (x.length - 1);
            /*add class "autocomplete-active":*/
            x[currentFocus].classList.add("autocomplete-active");
        }
        function removeActive(x) {
            /*a function to remove the "active" class from all autocomplete items:*/
            for (var i = 0; i < x.length; i++) {
                x[i].classList.remove("autocomplete-active");
            }
        }
        function closeAllLists(elmnt) {
            /*close all autocomplete lists in the document,
            except the one passed as an argument:*/
            var x = document.getElementsByClassName("autocomplete-items");
            for (var i = 0; i < x.length; i++) {
                if (elmnt != x[i] && elmnt != inp) {
                    x[i].parentNode.removeChild(x[i]);
                }
            }
        }
        /*execute a function when someone clicks in the document:*/
        document.addEventListener("click", function (e) {
            closeAllLists(e.target);
        });
    }


    var countries ;
    $(document).ready(function(){

        jQuery(document).ready(function() {
            var dataString = "folder_name=<?php echo $folder_name ?>" ;
            // alert(dataString);
            $.ajax
            ({
                type: "POST",
                url: "get/get_list_dossier_search.php",
                data: dataString,
                cache: false,
                success: function(html)
                {
                    // console.log(html);
                    // console.log(JSON.parse(html));
                    countries = JSON.parse(html);
                    console.log(countries);
                    autocomplete(document.getElementById("myInput"), countries);

                }
            });
        })


        var tour = new Tour({
            steps: [
                {
                    element: "#tour_dossier",
                    title: "Ajouter client",
                    content: "Vous pouvez ajouter un client manuellement <br/> Les champs marqués par ( <span style='color:red'>*</span> ) sont obligatoires",
                    placement: "top",
                    backdrop: true,
                    backdropContainer: '#wrapper',
                    onShown: function (tour){
                        $('body').addClass('tour-open')
                    },
                    onHidden: function (tour){
                        $('body').removeClass('tour-close')
                    }
                },
                {
                    element: "#tour_search",
                    title: "Importer client",
                    content: "Vous pouvez importer des clients ici",
                    placement: "top",
                    backdrop: true,
                    backdropContainer: '#wrapper',
                    onShown: function (tour){
                        $('body').addClass('tour-open')
                    },
                    onHidden: function (tour){
                        $('body').removeClass('tour-close')
                    }
                },
                {
                    element: "#tour_list",
                    title: "Rechercher client",
                    content: "Vous pouvez r des clients ici",
                    placement: "top",
                    backdrop: true,
                    backdropContainer: '#wrapper',
                    onShown: function (tour){
                        $('body').addClass('tour-open')
                    },
                    onHidden: function (tour){
                        $('body').removeClass('tour-close')
                    }
                },

            ],
            smartPlacement: true,
            keyboard: true,
            storage: false,
            debug: true,
            labels: {
                end: 'Terminer',
                next: 'Suivant &raquo;',
                prev: '&laquo; Prev'
            },
        });

        // Initialize the tour
        tour.init();

        $('.startTour').click(function(){
            tour.restart();

            // Start the tour
            // tour.start();
        })
    });


    $('.add_preview').on('click', function() {

        $("#preview_div").insertAfter($(this).parent().parent());
        showpdf($(this).attr("name"));

    });
    function showpdf(url)
    {


        var pdfDoc = null,
            pageNum = 1,
            pageRendering = false,
            pageNumPending = null,
            scale = 1.5,
            zoomRange = 0.25,
            canvas = document.getElementById('the-canvas'),
            ctx = canvas.getContext('2d');

        /**
         * Get page info from document, resize canvas accordingly, and render page.
         * @param num Page number.
         */
        function renderPage(num, scale) {
            pageRendering = true;
            // Using promise to fetch the page
            pdfDoc.getPage(num).then(function(page) {
                var viewport = page.getViewport(scale);
                canvas.height = viewport.height;
                canvas.width = viewport.width;

                // Render PDF page into canvas context
                var renderContext = {
                    canvasContext: ctx,
                    viewport: viewport
                };
                var renderTask = page.render(renderContext);

                // Wait for rendering to finish
                renderTask.promise.then(function () {
                    pageRendering = false;
                    if (pageNumPending !== null) {
                        // New page rendering is pending
                        renderPage(pageNumPending);
                        pageNumPending = null;
                    }
                });
            });

            // Update page counters
            document.getElementById('page_num').value = num;
        }

        /**
         * If another page rendering in progress, waits until the rendering is
         * finised. Otherwise, executes rendering immediately.
         */
        function queueRenderPage(num) {
            if (pageRendering) {
                pageNumPending = num;
            } else {
                renderPage(num,scale);
            }
        }

        /**
         * Displays previous page.
         */
        function onPrevPage() {
            if (pageNum <= 1) {
                return;
            }
            pageNum--;
            var scale = pdfDoc.scale;
            queueRenderPage(pageNum, scale);
        }
        document.getElementById('prev').addEventListener('click', onPrevPage);

        /**
         * Displays next page.
         */
        function onNextPage() {
            if (pageNum >= pdfDoc.numPages) {
                return;
            }
            pageNum++;
            var scale = pdfDoc.scale;
            queueRenderPage(pageNum, scale);
        }
        document.getElementById('next').addEventListener('click', onNextPage);

        /**
         * Zoom in page.
         */
        function onZoomIn() {
            if (scale >= pdfDoc.scale) {
                return;
            }
            scale += zoomRange;
            var num = pageNum;
            renderPage(num, scale)
        }
        document.getElementById('zoomin').addEventListener('click', onZoomIn);

        /**
         * Zoom out page.
         */
        function onZoomOut() {
            if (scale >= pdfDoc.scale) {
                return;
            }
            scale -= zoomRange;
            var num = pageNum;
            queueRenderPage(num, scale);
        }
        document.getElementById('zoomout').addEventListener('click', onZoomOut);

        /**
         * Zoom fit page.
         */
        function onZoomFit() {
            if (scale >= pdfDoc.scale) {
                return;
            }
            scale = 1;
            var num = pageNum;
            queueRenderPage(num, scale);
        }
        document.getElementById('zoomfit').addEventListener('click', onZoomFit);


        /**
         * Asynchronously downloads PDF.
         */
        PDFJS.getDocument(url).then(function (pdfDoc_) {
            pdfDoc = pdfDoc_;
            var documentPagesNumber = pdfDoc.numPages;
            document.getElementById('page_count').textContent = '/ ' + documentPagesNumber;

            $('#page_num').on('change', function() {
                var pageNumber = Number($(this).val());

                if(pageNumber > 0 && pageNumber <= documentPagesNumber) {
                    queueRenderPage(pageNumber, scale);
                }

            });

            // Initial/first page rendering
            renderPage(pageNum, scale);
        });
    }


</script>


</body>

</html>
