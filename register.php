<?php
ob_start();
session_start();
include 'connexion.php';


use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require 'vendor/autoload.php';




$name = "$NOM_USER";
$mdp = "$MDP_USER";
$to = "zakariya.etudes@gmail.com";
$user = "$user_current";



//Create an instance; passing `true` enables exceptions
$mail = new PHPMailer(true);



try {
    //Server settings
    $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
    $mail->isSMTP();                                            //Send using SMTP
    $mail->Host       = 'mail.einvoicetrack.net';                     //Set the SMTP server to send through
    $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
    $mail->Username   = 'support@einvoicetrack.net';                     //SMTP username
    $mail->Password   = 'D~1tW5RO!+uN';                               //SMTP password
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
    $mail->Port       = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

    //    $mail->SMTPAuth   = true;
    //    $mail->SMTPSecure = "tls";

    //    $mail->SMTPOptions = array(
    //        'ssl' => array(
    //            'verify_peer' => false,
    //            'verify_peer_name' => false,
    //            'allow_self_signed' => true
    //        )
    //    );
    //Recipients
    $mail->setFrom('support@einvoicetrack.net', 'eInvoiceTrack');

    $mail->addAddress($to);



    $mail->addAttachment('einvoicetrack.png');



    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Inscription eInvoiceTrack';
    $mail->Body = '
    		 <html>
    		   <head>
    			  <style>
    				 .banner-color {
    				 background-color: #eb681f;
    				 }
    				 .title-color {
    				 color: #1eb092;
    				 }
    				 .button-color {
    				     background-color: #1eb092;
    				     color: #ececec;
    				 }
    				 @media screen and (min-width: 500px) {
    				 .banner-color {
    				 background-color: #1eb092;
    				 }
    				 .title-color {
    				 color: #eb681f;
    				 }
    				 .button-color {
    				 background-color: #eb681f;
    				 }
    			  </style>
    		   </head>
    		   <body>
    			  <div style="background-color:#ececec;padding:0;margin:0 auto;font-weight:200;width:100%!important">
    				 <table align="center" border="0" cellspacing="0" cellpadding="0" style="table-layout:fixed;font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
    					<tbody>
    					   <tr>
    						  <td align="center">
    							 <center style="width:100%">
    								<table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" style="margin:0 auto;max-width:512px;font-weight:200;width:inherit;font-family:Helvetica,Arial,sans-serif" width="512">
    								   <tbody>
    									  <tr>
    										 <td bgcolor="#F3F3F3" width="100%" style="background-color:#f3f3f3;padding:12px;border-bottom:1px solid #ececec">
    											<table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;width:100%!important;font-family:Helvetica,Arial,sans-serif;min-width:100%!important" width="100%">
    											   <tbody>
    												  <tr>
    													 <td align="left" valign="middle" width="50%"><span style="margin:0;color:#4c4c4c;white-space:normal;display:inline-block;text-decoration:none;font-size:12px;line-height:20px">eInvoiceTrack</span></td>
    													 <td valign="middle" width="50%" align="right" style="padding:0 0 0 10px"><span style="margin:0;color:#4c4c4c;white-space:normal;display:inline-block;text-decoration:none;font-size:12px;line-height:20px">'.date('Y-m-d H:i:s').'</span></td>
    													 <td width="1">&nbsp;</td>
    												  </tr>
    											   </tbody>
    											</table>
    										 </td>
    									  </tr>
    									  <tr>
    										 <td align="left">
    											<table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
    											   <tbody>
    												  <tr>
    													 <td width="100%">
    														<table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
    														   <tbody>
    															  <tr>
    																 <td align="center" bgcolor="#8BC34A" style="padding:20px 48px;color:#ffffff" class="banner-color">
    																	<table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
    																	   <tbody>
    																		  <tr>
    																			 <td align="center" width="100%">
    																				<h1 style="padding:0;margin:0;color:#ffffff;font-weight:500;font-size:20px;line-height:24px">Bienvenue chez eInvoiceTrack</h1>
    																			 </td>
    																		  </tr>
    																	   </tbody>
    																	</table>
    																 </td>
    															  </tr>
    									
    															<tr>
    																 <td align="center" style="padding:20px 0 10px 0">
    																	<table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
    																	   <tbody>
    																		  <tr>
    																			 <td align="center" width="100%" style="padding: 0 15px;text-align: justify;color: rgb(76, 76, 76);font-size: 12px;line-height: 18px;">
    																				<h3 style="font-weight: 600; padding: 0px; margin: 0px; font-size: 16px; line-height: 24px; text-align: justify ;" class="title-color">Bonjour '.$name.',</h3>
    																				<p style="margin: 20px 0 30px 0;font-size: 15px;text-align: justify ;">Bienvenue sur la plateforme eInvoiceTrack, où vous pouvez suivre en temps réel la distribution de vos factures clients ainsi gérer électroniquement tous vos liasses clients.
     
    																				Vos  informations de connexion temporaires sont ci-dessous:<br><br>  Identifiant: '.$to.' <br>  Votre mot de passe est :  <b> '.$mdp.'</b><br><br>  <br>&nbsp;&nbsp;&nbsp; Valider votre inscription sur le lien suivant : https://einvoicetrack.net/validation?token='.$user.' <br><br> Note: Nous vous recommandons d\'ajouter le lien d\'accès de eInvoicetrack en raccourci afin de faciliter l\'accès à votre compte.Nos meilleures salutation</p>
    																				<div style="font-weight: 200; text-align: justify ; margin: 25px;"><a  style="background-color: #1eb092; color : #ececec ; padding:0.6em 1em;border-radius:600px;font-size:14px;text-decoration:none;font-weight:bold; cursor:pointer" class="button-color" href="https://einvoicetrack.net/login" target="_blanc" >eInvoiceTrack</a></div>
    																			 </td>
    																		  </tr>
    																	   </tbody>
    																	</table>
    																 </td>
    															  </tr>
    															  <tr>
    															  </tr>
    															  <tr>
    															  </tr>
    														   </tbody>
    														</table>
    													 </td>
    												  </tr>
    											   </tbody>
    											</table>
    										 </td>
    									  </tr>
    									  <tr>
    										 <td align="left">
    											<table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" style="padding:0 24px;color:#999999;font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
    											   <tbody>
    												  <tr>
    													 <td align="center" width="100%">
    														<table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
    														   <tbody>
    															  <tr>
    																 <td align="center" valign="middle" width="100%" style="border-top:1px solid #d9d9d9;padding:12px 0px 20px 0px;text-align:center;color:#4c4c4c;font-weight:200;font-size:12px;line-height:18px">Merci,
    																	<br><b> Service client eInvoiceTrack </b>
    																 </td>
    															  </tr>
    														   </tbody>
    														</table>
    													 </td>
    												  </tr>
    												  <tr>
    													 <td align="center" width="100%">
    														<table border="0" cellspacing="0" cellpadding="0" style="font-weight:200;font-family:Helvetica,Arial,sans-serif" width="100%">
    														   <tbody>
    															  <tr>
    																 <td align="center" style="padding:0 0 8px 0" width="100%"></td>
    															  </tr>
    														   </tbody>
    														</table>
    													 </td>
    												  </tr>
    											   </tbody>
    											</table>
    										 </td>
    									  </tr>
    								   </tbody>
    								</table>
    							 </center>
    						  </td>
    					   </tr>
    					</tbody>
    				 </table>
    			  </div>
    		   </body>
    		</html>';
    $mail->AltBody = 'Bienvenue chez eInvoiceTrack Votre mot de pass :'.$mdp;


    $mail->send();
    // echo 'Message has been sent';
} catch (Exception $e) {
    //echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
}


?>

