<?php
ob_start();
session_start();
include 'connexion.php';


if(isset($_SESSION['user_einvoicetrack']))
{
	
	$current_user = decode($_SESSION['user_einvoicetrack']) ; 					
	$SQL="SELECT  `NOM_USER`, `PRENOM_USER`, `EMAIL_USER`,`CIVILITE_USER`,
			`CODE_ENTREPRISE`, `ROLE_USER`
		  FROM `user` 
		  WHERE CODE_USER = $current_user
		  AND actif= 1";
	$query=mysqli_query($ma_connexion,$SQL);
	if(mysqli_num_rows($query) == 1)
	{
		while($row=mysqli_fetch_assoc($query))
		{	
				$NOM_USER = $row['CIVILITE_USER'].' ' .$row['NOM_USER'];
		}
		
	}
	else 
	{
		header('Location: login');
	}
	
	if( $_SESSION['role'] != 'superadmin' )
	{
		$SQL="SELECT 1
				FROM  user_module um
				WHERE  um.CODE_USER ='".decode($_SESSION['user_einvoicetrack'])."' 
				AND um.CODE_MODULE = 1
				 "
		;
		$query=mysqli_query($ma_connexion,$SQL);
			
		if(mysqli_num_rows($query) == 0)
		{
			
			header('Location: users');
		}
	}
	
	
		// $i = 0 ; 
												

		// $SQL="SHOW COLUMNS FROM client";
		// $query=mysqli_query($ma_connexion,$SQL);
		// while($row=mysqli_fetch_assoc($query))
		// {	
				// echo '
					// <a class="toggle-vis" data-column="'.$i++.'">'.$row['Field'].'</a> -
					// ';
		// }
}
else 
{
	header('Location: login');
}
?>
<!DOCTYPE html>
<html lang="fr">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>eInvoiceTrack</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/plugins/dropzone/basic.css" rel="stylesheet">
    <link href="css/plugins/dropzone/dropzone.css" rel="stylesheet">
    <link href="css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
	
	 <link href="css/plugins/dataTables/datatables.min.css" rel="stylesheet">

	
	<link href="css/plugins/select2/select2.min.css" rel="stylesheet">
	
	
	<!-- Bootstrap Tour -->
    <link href="css/plugins/bootstrapTour/bootstrap-tour.min.css" rel="stylesheet">

	
	
	
	
	
	
	

<style>

    .wizard-big.wizard > .content {
        min-height: 320px;
        height: 500px;
    }

.select2 {
	width:100%!important;
	}
	
	.DETAILL
	{
		text-align: center; 
		vertical-align: middle;
	}
	.NOM
	{
		font-weight: bold;
		text-decoration: underline;
		cursor : pointer ; 
	}
	#liste_table tr
	{
		 cursor : pointer ; 
	}
	
	.popover{
    max-width: 100%; /* Max Width of the popover (depending on the container!) */
}
	
	
	
</style>
	
</head>

<body class="">

    <div id="wrapper">

    
		<?php
			include 'includes/nav.php';
		?>	

        <div id="page-wrapper" class="gray-bg">
		   <?php
				include 'includes/header.php';
			?>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>List des clients</h2> 
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="index"> Accueil </a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong></strong>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-8">
                    <div class="title-action">
					 <a href="#" class="btn btn-primary startTour"><i class="fa fa-play"></i> Démo</a>
                    </div>
                </div>
            </div>

           <div class="wrapper wrapper-content  animated fadeInRight">
            <div class="row" id="sortable-view">
			
			<?php
				if( $_SESSION['role'] ==  'superadmin' ||  $_SESSION['role'] ==  'admin' )
				{
					
														
					?>
			<div class="col-md-9 dragme" id="add_client_panel">
                    <div class="ibox helloagain">
					 <div class="ibox-title" style="background-color: #24c6c8; color: white;">
					 <h5> <i class="fa fa-user-plus"></i> Ajouter client </h5>
					  <div class="ibox-tools" >
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up" style="color: white;"></i>
                                </a>
                                

                            </div>
                        </div>
                        <div class="ibox-content" id="tores_add_pan">
                        
	
						</div>
                       
					</div>
			</div>
				
			<div class="col-md-3 dragme" id="import_client_panel">
                    <div class="ibox" id="ibox_import">
					 <div class="ibox-title" style="background-color: #24c6c8; color: white;">
					 <h5> <i class="fa fa-file"></i> Importer fichier client </h5>
					  <div class="ibox-tools" >
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up" style="color: white;"></i>
                                </a>
								
								<a class="close-link">'
									<i class="fa fa-times"  style="color: white;"></i>'
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
								<div class="sk-spinner sk-spinner-wave " >
                                    <div class="sk-rect1"></div>
                                    <div class="sk-rect2"></div>
                                    <div class="sk-rect3"></div>
                                    <div class="sk-rect4"></div>
                                    <div class="sk-rect5"></div>
                                </div>
							<form method="get" action="eInvoiceTrack_Client.xlsx">
								<div class="alert alert-warning">
                                	Merci de respecter l'ordonnancement et les libellés 
									des colonnes du modéle d'import
									</div>
								<button class="btn btn-primary btn-rounded btn-block" type="submit"><i class="fa fa-download"></i>Télécharger le modèle d'import</button>	
							</form>	
							<br/>
							<br/>
							<form action="set/import_client.php" class="dropzone" id="dropzoneForm" enctype="multipart/form-data">
								
								
							</form>
							<br/>
							<button class="btn btn-success btn-rounded btn-block" id="importer"><i class="fa fa-gear"></i> Traiter</button>
							
							<div class="logs" style="overflow-y: scroll; max-height:400px;  margin-top: 10px; margin-bottom:10px;">
							
							</div>
						</div>
                       
					</div>
			</div>
			
			<?php
						

						}							
					?>
				
			<div class="col-md-12 dragme" id="recherche_panel">
                    <div class="ibox ">
					 <div class="ibox-title" style="background-color: #24c6c8; color: white;">
							<h5> <i class="fa fa-search"></i> Recherche </h5>
							 <div class="ibox-tools" >
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up" style="color: white;"></i>
                                </a>
                                
                               
                            </div>
                        </div>
                        <div class="ibox-content">
                        <form id="search_form" method="POST">
									<div class="row">
										<div class="col-md-3">
											<span class="badge badge-success">Nom client</span>
											<select class="sc_select2 form-control"  name="NOM_CLIENT[]" id="NOM_CLIENT" data-placeholder="Entrer nom client"  multiple="multiple">
												
											</select>
										</div>
										
										<div class="col-md-3">
											<span class="badge badge-success">Code client</span> 
											<select class="sc_select2 form-control" name="NUMERO_CLIENT[]"  id="NUMERO_CLIENT" data-placeholder="Entrer numero client" multiple="multiple">
												
											</select>
										</div>
										<div class="col-md-3">
											<span class="badge badge-success">Télephone</span> 
											<select class="sc_select2 form-control" name="TELE_CLIENT[]"  id="TELE_CLIENT" data-placeholder="Entrer télephone client" multiple="multiple">
												
											</select>
										</div>
										<div class="col-md-3">
											<span class="badge badge-success">Adresse</span> 
											<select class="sc_select2 form-control" name="ADRESSE_CLIENT[]"  id="ADRESSE_CLIENT" data-placeholder="Entrer adresse client" multiple="multiple">
												
											</select>
										</div>
										<div class="col-md-3">
											<span class="badge badge-success">Ville</span> 
											<select class="sc_select2 form-control" name="VILLE_CLIENT[]"  id="VILLE_CLIENT" data-placeholder="Sélectionner ville client" multiple="multiple">
												<?php
														$query = " SELECT DISTINCT `VILLE` FROM `client`  ORDER by `VILLE`
														 ";
														$result = mysqli_query($ma_connexion, $query); 
													   while(($row = mysqli_fetch_array($result)) == true )  
														{ 										
															
//															$CODE_VILLE = encode($row['CODE_VILLE']) ;
															$NOM_VILLE = $row['VILLE'] ;
															echo ' <option value="'.$NOM_VILLE.'">'.$NOM_VILLE.'</option>' ;
															 
														}
													?>
											</select>
										</div>
										
										<div class="col-md-3">
											<span class="badge badge-success">Secteur</span> 
											<select class="sc_select2 form-control" name="SECTEUR_CLIENT[]"  id="SECTEUR_CLIENT" data-placeholder="Entrer secteur client" multiple="multiple">
												
											</select>
										</div>

										<div class="col-md-3">
											<span class="badge badge-success">Contact</span>
											<select class="sc_select2 form-control" name="CONTACT_CLIENT[]"  id="CONTACT_CLIENT" data-placeholder="Entrer contact client" multiple="multiple">

											</select>
										</div>
										<div class="col-md-3">
											<span class="badge badge-success">Type client</span>
											<select class="sc_select2 form-control" name="TYPE_CLIENT[]"  id="TYPE_CLIENT" data-placeholder="Entrer type client" multiple="multiple">

											</select>
										</div>

										<div class="col-md-3">
											<span class="badge badge-success">Nature client</span>
											<select class="sc_select2 form-control" name="NATURE_CLIENT[]"  id="NATURE_CLIENT" data-placeholder="Entrer nature client" multiple="multiple">

											</select>
										</div>

									</div>
									<br/>
									<div class="row">
										<div class="col-md-12">
											<button type="button" class="btn btn-success btn-rounded pull-right" id="rechercher" > <i class="fa fa-search"></i> Rechercher</button>
											<button type="button" class="btn btn-danger btn-rounded pull-right" id="vider" style="margin-right: 4px;"> <i class="fa fa-eraser"></i> Vider</button>
										</div>
									</div>
								</form>
	
						</div>
                       
					</div>
			</div>
			<br/>
				
			
			<div class="col-lg-12 dragme" id="res_search">
				<div class="ibox">
					<div class="ibox-title" style="background-color: #24c6c8; color: white;">
					<h5> <i class="fa fa-list"></i> Resultat Recherche</h5>
					 <div class="ibox-tools" >
							<a class="collapse-link">
								<i class="fa fa-chevron-up" style="color: white;"></i>
							</a>
						
						   
						</div>
					</div>
					<div class="ibox-content">
						<div>
						
						
						
							<a class="toggle-vis badge badge-success text-light" data-column="1">Nom client</a> 
							<a class="toggle-vis badge badge-success text-light" data-column="2">Code client</a> 
							<a class="toggle-vis badge badge-success text-light" data-column="3">N° Tél1</a> 
							<a class="toggle-vis badge " data-column="4">N° Tél2</a> 
							<a class="toggle-vis badge " data-column="5">Adresse facturation</a>
							<a class="toggle-vis badge " data-column="6">Secteur facturation</a>
							<a class="toggle-vis badge " data-column="7">Adresse supplémentaire </a>
							<a class="toggle-vis badge " data-column="8">Secteur supplémentaire </a>
							<a class="toggle-vis badge badge-success text-light" data-column="9">Ville facturation</a>
							<a class="toggle-vis badge " data-column="10">GPS</a>
							<a class="toggle-vis badge " data-column="11">Déscription</a>
							<a class="toggle-vis badge " data-column="12">Contact </a>
							<a class="toggle-vis badge " data-column="13">Type client</a>
							<a class="toggle-vis badge " data-column="14">Nature client</a>
						</div>

						
						<hr/>
						<?php
			if( $_SESSION['role'] ==  'superadmin' ||  $_SESSION['role'] ==  'admin' )
			{
				
													
				?>
						<button class="btn btn-danger delete_multiple" ><i class="glyphicon glyphicon-trash"></i>  <i class="fa fa-delete "></i> Supprimer</button>	
<?php
			}
													
				?>			
						<div class="table-responsive">
							<div class="sk-spinner sk-spinner-wave" style="display: none">
								<div class="sk-rect1"></div>
								<div class="sk-rect2"></div>
								<div class="sk-rect3"></div>
								<div class="sk-rect4"></div>
								<div class="sk-rect5"></div>
							</div>
							<table class="table table-bordered table-striped"  style="width : 100%" id="liste_table">
								<thead>
								  <tr>
									<th ></th>
									<th >Nom client</th>
									<th >Code client</th>
									<th >N° Tél1</th>
									<th >N° Tél2</th>
									<th >Adresse facturation</th>
									<th >Secteur facturation</th>
									<th >Adresse supplémentaire</th>
									<th >Secteur supplémentaire</th>
									<th >Ville facturation</th>
									<th >GPS</th>
									<th >Déscription</th>
									<th >Contact</th>
									<th >Type client</th>
									<th >Nature client</th>
									<th style="width:20%"></th>
								  </tr>
								</thead>
								<tbody>
								
								</tbody>
							</table>
							
							
						</div>

					</div>
				</div>
			</div>
				
			<div class="col-lg-4 dragme" style="display:none" id="drag_gps">
				<div class="ibox ">
				 <div class="ibox-title" style="background-color: #24c6c8; color: white;">
						<h5> <i class="fa fa-map-marker "></i> GPS </h5>
						 <div class="ibox-tools" >
							<a class="collapse-link">
								<i class="fa fa-chevron-up" style="color: white;"></i>
							</a>
							
						</div>
					</div>
					<div class="ibox-content" id="gps_list">
					</div>
				</div>
				
			</div>
			
			<div class="col-lg-4 dragme"  style="display:none" id="drag_edit">
				<form id="edit_form" method="POST"  >				
					<div class="ibox">
						<div class="ibox-title">
							<h5>Modifier <span class="label label-primary"></span> </h5>
						</div>

						<div class="ibox-content" id="detail_list">
						</div>
					</div>
				<form>
			</div>
			
			<div class="col-lg-4 dragme"  style="display:none" id="drag_detail">
				<div class="ibox">
					<div class="ibox-title">
						<h5>Détail client <span class="label label-primary"></span> </h5>
					</div>
					<div class="ibox-content" id="detail_list_info">
					
						
					</div>
					
					
				</div>
			</div>
				


            </div>
        </div>
           <?php
			include 'includes/footer.php';
		?>	

        </div>
        </div>





	
    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>

    <!-- jquery UI -->
    <script src="js/plugins/jquery-ui/jquery-ui.min.js"></script>

    <!-- Touch Punch - Touch Event Support for jQuery UI -->
    <script src="js/plugins/touchpunch/jquery.ui.touch-punch.min.js"></script>

	
    <!-- Select2 -->
    <script src="js/plugins/select2/select2.full.min.js"></script>
	<script src="js/i18n/fr.js"></script>
	
	
	<!-- datatable -->
	<script src="js/plugins/dataTables/datatables.min.js"></script>
    <script src="js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>


	<!-- Steps -->
	<link href="css/plugins/steps/jquery.steps.css" rel="stylesheet">
    <script src="js/plugins/steps/jquery.steps.min.js"></script>
    <script src="js/plugins/steps/jquery.steps.fix.js"></script>

    <!-- Jquery Validate -->
    <script src="js/plugins/validate/jquery.validate.min.js"></script>
    <script src="js/plugins/validate/messages_fr.js"></script>
	
	
	 <!-- Maps -->
	<link rel="stylesheet" type="text/css" href="https://js.api.here.com/v3/3.0/mapsjs-ui.css?dp-version=1549984893" />
	<script type="text/javascript" src="https://js.api.here.com/v3/3.0/mapsjs-core.js"></script>
	<script type="text/javascript" src="https://js.api.here.com/v3/3.0/mapsjs-service.js"></script>
	<script type="text/javascript" src="https://js.api.here.com/v3/3.0/mapsjs-ui.js"></script>
	<script type="text/javascript" src="https://js.api.here.com/v3/3.0/mapsjs-mapevents.js"></script>
	
	 <!-- Sweet alert -->
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	
	
	 <!-- DROPZONE -->
    <script src="js/plugins/dropzone/dropzone.js"></script>
	
	<!-- datatables checkboxes	  -->
	<link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/css/dataTables.checkboxes.css" rel="stylesheet" />
	<script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/js/dataTables.checkboxes.min.js"></script>


	   <!-- Bootstrap Tour -->
    <script src="js/plugins/bootstrapTour/bootstrap-tour.min.js"></script>

<script>

$(document).ready(function(){
	
	var tour = new Tour({
            steps: [
                {
                    element: "#add_client_panel",
                    title: "Ajouter client",
                    content: "Vous pouvez ajouter un client manuellement <br/> Les champs marqués par ( <span style='color:red'>*</span> ) sont obligatoires",
                    placement: "top",
                    backdrop: true,
                    backdropContainer: '#wrapper',
                    onShown: function (tour){
                        $('body').addClass('tour-open')
                    },
                    onHidden: function (tour){
                        $('body').removeClass('tour-close')
                    }
                },
                {
                    element: "#import_client_panel",
                    title: "Importer client",
                    content: "Vous pouvez importer des clients ici",
                    placement: "top",
                    backdrop: true,
                    backdropContainer: '#wrapper',
                    onShown: function (tour){
                        $('body').addClass('tour-open')
                    },
                    onHidden: function (tour){
                        $('body').removeClass('tour-close')
                    }
                },
                {
                    element: "#recherche_panel",
                    title: "Rechercher client",
                    content: "Vous pouvez r des clients ici",
                    placement: "top",
                    backdrop: true,
                    backdropContainer: '#wrapper',
                    onShown: function (tour){
                        $('body').addClass('tour-open')
                    },
                    onHidden: function (tour){
                        $('body').removeClass('tour-close')
                    }
                },
                {
                    element: "#res_search",
                    title: "Resultat recherche client",
                    content: "Vous pouvez voir detail/modifier ou suprimer des clients ici",
                    placement: "top",
                    backdrop: true,
                    backdropContainer: '#wrapper',
                    onShown: function (tour){
                        $('body').addClass('tour-open')
                    },
                    onHidden: function (tour){
                        $('body').removeClass('tour-close')
                    }
					
                }
                
            ],
			smartPlacement: true,
			  keyboard: true,
			  storage: false,
			  debug: true,
			  labels: {
				end: 'Terminer',
				next: 'Suivant &raquo;',
				prev: '&laquo; Prev'
			  },
			  });

        // Initialize the tour
        tour.init();

         $('.startTour').click(function(){
            tour.restart();

            // Start the tour
            // tour.start();
        })
	
		WinMove();
		
		
	
		$.fn.select2.defaults.set('language', 'fr');
	
		$(".sc_select2").select2({
			allowClear: true,
			tags: true,
			language: "fr",
			 width: '100%',
			 
		});
		
		var table =   $('#liste_table').DataTable({	
				responsive: true,
				"language":{
						"sProcessing":     "Traitement en cours...",
						"sSearch":         "Rechercher un client &nbsp;:",
						"sLengthMenu":     "Afficher _MENU_  client",
						"sInfo":           "Affichage des client _START_ &agrave; _END_ sur _TOTAL_ clients",
						"sInfoEmpty":      "Affichage du client 0 &agrave; 0 sur 0 clients",
						"sInfoFiltered":   "(filtr&eacute; de _MAX_ clients au total)",
						"sInfoPostFix":    "",
						"sLoadingRecords": "Chargement en cours...",
						"sZeroRecords":    "Aucuns virements &agrave; afficher",
						"sEmptyTable":     "Aucunes donn&eacute;e disponible dans le tableau",
						"oPaginate": {
							"sFirst":      "Premier",
							"sPrevious":   "Pr&eacute;c&eacute;dent",
							"sNext":       "Suivant",
							"sLast":       "Dernier"
						},
						"oAria": {
							"sSortAscending":  ": activer pour trier la colonne par ordre croissant",
							"sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
						}
				},
				dom: '<"html5buttons"B>lTfgitp',
				buttons: [
					{ 
						extend: 'copy',
						title: 'eInvoiceTrack',
						exportOptions: {
							columns:  ':visible:not(:last-child)'
						}
					},
					{
						extend: 'csv',
						title: 'eInvoiceTrack',
						exportOptions: {
							columns:  ':visible:not(:last-child)'
						}
					},
					{
						extend: 'excel',
						title: 'eInvoiceTrack',
						exportOptions: {
							columns:  ':visible:not(:last-child)'
						}
					},

					{extend: 'print',
					 customize: function (win){
							$(win.document.body).addClass('white-bg');
							$(win.document.body).css('font-size', '10px');

							$(win.document.body).find('table')
									.addClass('compact')
									.css('font-size', 'inherit');
						}
					}
				],
				"columns": [
					{ "data": "check_box"},
					{ "data": "NOM",className: "NOM"},
					{ "data": "NUMERO",className: "NUMERO"},
					{ "data": "TEL1",className: "TEL1"},
					{ "data": "TEL2",className: "TEL2", "visible": false },
					{ "data": "ADRESSE1",className: "ADRESSE1", "visible": false },
					{ "data": "SECTEUR1",className: "SECTEUR1", "visible": false },
					{ "data": "ADRESSE2",className: "ADRESSE2", "visible": false },
					{ "data": "SECTEUR2",className: "SECTEUR2", "visible": false },
					{ "data": "VILLE",className: "VILLE"},
					{ "data": "GPS",className: "GPS", "visible": false },
					{ "data": "DESCRIPTION",className: "DESCRIPTION", "visible": false },
                    { "data": "CONTACT",className: "CONTACT", "visible": false },
                    { "data": "TYPE_CLIENT",className: "TYPE_CLIENT", "visible": false },
                    { "data": "NATURE_CLIENT",className: "NATURE_CLIENT", "visible": false },
					{ "data": "DETAILL",className: "DETAILL"}
				 ],
			    'columnDefs': [
				 {
					'targets': 0,
					'checkboxes': {
					   'selectRow': true,
					   'selectCallback': function(nodes, selected){
								// console.log(nodes);
								
						        
					   }
					},
				 }
			  ],
			  'select': 'multi',
			  'order': [[1, 'asc']]
			});	
			
		  
			
			
			function getRandomColor() {
			  var letters = '0123456789ABCDEF';
			  var color = '#';
			  for (var i = 0; i < 6; i++) {
				color += letters[Math.floor(Math.random() * 16)];
			  }
			  return color;
			}

			var svgMarkup = '<svg xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="42" height="42" viewBox="0 0 263.335 263.335" style="enable-background:new 0 0 263.335 263.335;" xml:space="preserve">'
					+'<g>'
					+'	<g xmlns="https://www.w3.org/2000/svg">'
					+'		<path d="M40.479,159.021c21.032,39.992,49.879,74.22,85.732,101.756c0.656,0.747,1.473,1.382,2.394,1.839   c0.838-0.396,1.57-0.962,2.178-1.647c80.218-61.433,95.861-125.824,96.44-128.34c2.366-9.017,3.57-18.055,3.57-26.864    C237.389,47.429,189.957,0,131.665,0C73.369,0,25.946,47.424,25.946,105.723c0,8.636,1.148,17.469,3.412,26.28" fill="${COLOR}"/>'
					+'	<text x="80" y="130" font-family="sans-serif" font-size="5em" fill="white">${TEXT}</text>'
					+'	</g>'
					+'</g></svg>';
			
			// function addMarkerToGroup(group, coordinate, html,text) {
			// 	  let iconmarker = new H.map.Icon(svgMarkup.replace('${COLOR}','#1c84c6' ).replace('${TEXT}', text));
			// 	  let marker = new H.map.Marker(coordinate,{icon: iconmarker});
			// 	  marker.setData(html);
			// 	  group.addObject(marker);
			// }

			var gps_data = null ; 
			function showmapGPS()
			{
				
				$('#gps_list').html('<div id="map_gps" style="width: 100%; height: 700px; background: grey" />');	
				$('#drag_gps').show();	

				

				var platform = new H.service.Platform({
				  app_id: 'devportal-demo-20180625',
				  app_code: '9v2BkviRwi9Ot26kp2IysQ',
				  useHTTPS: true
				});
				var pixelRatio = window.devicePixelRatio || 1;
				var defaultLayers = platform.createDefaultLayers({
				  tileSize: pixelRatio === 1 ? 256 : 512,
				  ppi: pixelRatio === 1 ? undefined : 320
				});

				var map = new H.Map(document.getElementById('map_gps'),
				  defaultLayers.normal.map,{
				  center: {lat:33.916422, lng: -6.946514},
				  zoom: 6,
				  pixelRatio: pixelRatio
				});

				var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));

				var ui = H.ui.UI.createDefault(map, defaultLayers);
				
				var group = new H.map.Group();

				if(gps_data){
                    jQuery.each(gps_data, function(i, val) {
                        // addMarkerToGroup(group, {lat:val.lat, lng:val.lng},val.html,val.text);
                        console.log(val);

                        // var pngIcon = new H.map.Icon("https://cdn0.iconfinder.com/data/icons/daily-boxes/150/phone-box-32.png");
                        var pngIcon = new H.map.Icon("https://cdn1.iconfinder.com/data/icons/user-avatar-20/64/06-student-256.png", {size: {w: 40, h: 40}});
                        let marker = new H.map.Marker({lat:val.lat, lng:val.lng}, {
                            icon: pngIcon
                        });
                        marker.setData(val.html);
                        group.addObject(marker);
                        // map.addObject(marker);


                    });
                }

                group.addEventListener('tap', function (evt) {
                    var bubble =  new H.ui.InfoBubble(evt.target.getPosition(), {
                        content: evt.target.getData()
                    });
                    ui.addBubble(bubble);
                }, false);

                console.log(group);

                map.addObject(group);


            }
			
			
			
			
			$(document).on('click', '#rechercher', function(){ 	
				$.ajax(
				{
					type : 'post',
					url : 'get/get_client_search.php',
					data: { 
							"NOM_CLIENT" : $('#NOM_CLIENT').val(),
							"NUMERO_CLIENT" : $('#NUMERO_CLIENT').val(),
							"TELE_CLIENT" : $('#TELE_CLIENT').val(),
							"ADRESSE_CLIENT" : $('#ADRESSE_CLIENT').val(),
							"VILLE_CLIENT" : $('#VILLE_CLIENT').val(),
							"SECTEUR_CLIENT" : $('#SECTEUR_CLIENT').val()
							},
					success : function(data)
					{
						console.log(data);
						info_html = JSON.parse(data);
						datatable = JSON.parse(info_html.datatable);
						table.clear();
						table.rows.add(datatable).draw();
						
						
						// $('#gps_list').html('<div class="ibox ">'
												// +' <div class="ibox-title" style="background-color: #24c6c8; color: white;">'
												// +'		<h5> <i class="fa fa-map-marker "></i> GPS </h5>'
												// +'		 <div class="ibox-tools" >'
												// +'			<a class="collapse-link">'
												// +'				<i class="fa fa-chevron-up" style="color: white;"></i>'
												// +'			</a>'
												// +'			<a class="close-link">'
												// +'				<i class="fa fa-times"  style="color: white;"></i>'
												// +'			</a>'
												// +'		</div>'
												// +'	</div>'
												// +'	<div class="ibox-content">'
												// +'		<div id="map_gps" style="width: 100%; height: 700px; background: grey" />'
												// +'	</div>'
												// +'</div>');	

						gps_data = JSON.parse(info_html.gps_data) ;
						console.log(gps_data)
						showmapGPS();
						
						
						

					},
					complete : function(data)
					{
						$(".sk-spinner").css("display","none");
						$("#res_search").attr("class","col-lg-8");
					},
					beforeSend : function(data)
					{
						$(".sk-spinner").css("display","block ");
						// alert(data);
						// do something, not critical.
					},
					error: function (jqXHR, exception) {
						var msg = '';
						if (jqXHR.status === 0) {
							msg = 'Not connect.\n Verify Network.';
						} else if (jqXHR.status == 404) {
							msg = 'Requested page not found. [404]';
						} else if (jqXHR.status == 500) {
							msg = 'Internal Server Error [500].';
						} else if (exception === 'parsererror') {
							msg = 'Requested JSON parse failed.';
						} else if (exception === 'timeout') {
							msg = 'Time out error.';
						} else if (exception === 'abort') {
							msg = 'Ajax request aborted.';
						} else {
							msg = 'Uncaught Error.\n' + jqXHR.responseText;
						}
						// alert(msg);
					}
				});

			});
			
			
			
			
			
			
			
			 $('a.toggle-vis').on( 'click', function (e) {
				e.preventDefault();
		 
				var column = table.column( $(this).attr('data-column') );
		 
				column.visible( ! column.visible() );
				
				$(this).toggleClass( "badge-success text-light" );
			} );
			
			
			
			$(document).on('click', '.delete', function(e)
			{ 
				e.stopPropagation();
					
				var this_ = $(this) ; 
						Swal.fire({
						  title: 'Supprimer client',
						  text: "Êtes-vous sûr de continuer?!",
						  type: 'warning',
						  showCancelButton: true,
						  confirmButtonColor: '#3085d6',
						  cancelButtonColor: '#d33',
						  confirmButtonText: 'Oui, Supprimer!',
						  cancelButtonText: "Non, Annuler!",  
						}).then((result) => {
						  if (result.value) {
									var dataString = "current_delete="+ this_.val(); 
									$.ajax({
									type: "POST",
									url: "set/edit_client.php",
									data: dataString,
									cache: true,
									success: function(html){
											// this_.parents("tr").find('.label-primary').parent().html('<span class="label label-danger">supprimé </span>');
											// this_.parents("tr").find('.prixhistoryedit').html('<span class="label label-danger">');
											// this_.parent().html('<span class="label label-danger">');
											console.log(html);
											table.row(this_.parents('tr')).remove().draw(false);
											 $("#detail_list").html('')
												Swal.fire(
												  'Client supprimé !',
												  '',
												  'success'
												)
												
												$("#res_search").attr("class","col-lg-12");
												$("#drag_gps").hide();
												$("#drag_detail").hide();
												$("#drag_edit").hide();
													
											// console.log(html);
											} 
									
											
									});
							  }else 
							  {
								  Swal.fire(
									  'annulé!',
									  'Action annulée!!',
									  'error'
									)
								  
							  }
							})
				

			});
	
			$(document).on('click', '.delete_multiple', function(e)
			{ 
				e.stopPropagation();
				
				let rows = $( table.$('input[type="checkbox"]').map(function () {
						  return $(this).prop("checked") ? $(this).closest('tr') : null ;					  
				}));
				
				// keys.each(function(k,v){
					// console.log(v);
				// })

					
				var this_ = $(this) ; 
						Swal.fire({
						  title: 'Supprimer clients',
						  text: "Êtes-vous sûr de continuer?!",
						  type: 'warning',
						  showCancelButton: true,
						  confirmButtonColor: '#3085d6',
						  cancelButtonColor: '#d33',
						  confirmButtonText: 'Oui, Supprimer!',
						  cancelButtonText: "Non, Annuler!",  
						}).then((result) => {
						  if (result.value) {
							  

								$.ajax(
								{
									type : 'POST',
									url : 'set/edit_client.php',
									data: { 
											"delete_code" : table.$('input[type="checkbox"]').map(function () {
																	  return $(this).prop("checked") ? $(this).closest('tr').find('.delete').val() : null;
															}).get(),
											},
									success : function(data)
									{
										console.log(data);
										// $(".logs_man").html(data);
										rows.each(function(k,v){
											table.row(v).remove().draw(false);
										})
										

									},
									complete : function(data)
									{
										$("#res_search").attr("class","col-lg-12");
										$("#drag_gps").hide();
										$("#drag_detail").hide();
										$("#drag_edit").hide();
										// $(".sk-spinner").css("display","none");
										// $("#res_search").attr("class","col-lg-8");
										// $('#ibox_result').children('.ibox-content').toggleClass('sk-loading');
									},
									beforeSend : function(data)
									{
										// $('#ibox_result').children('.ibox-content').toggleClass('sk-loading');
										// $(".sk-spinner").css("display","block ");
										// alert(data);
										// do something, not critical.
									},
									error: function (jqXHR, exception) {
										var msg = '';
										if (jqXHR.status === 0) {
											msg = 'Not connect.\n Verify Network.';
										} else if (jqXHR.status == 404) {
											msg = 'Requested page not found. [404]';
										} else if (jqXHR.status == 500) {
											msg = 'Internal Server Error [500].';
										} else if (exception === 'parsererror') {
											msg = 'Requested JSON parse failed.';
										} else if (exception === 'timeout') {
											msg = 'Time out error.';
										} else if (exception === 'abort') {
											msg = 'Ajax request aborted.';
										} else {
											msg = 'Uncaught Error.\n' + jqXHR.responseText;
										}
										// alert(msg);
									}
								});
							  
								// var rows = $( table.$('input[type="checkbox"]').map(function () {
								  // return $(this).prop("checked") ? $(this).closest('tr') : null;
								// } ) );
								
								
								// function show(values) {
									// console.log(values);  
								 // }
								 // rows.forEach(show[0].cells[4].find('.delete').val()); 
								
								// console.log(rows[0].cells[4].find('.delete').val());
									// var dataString = "current_delete="+ this_.val(); 
									// $.ajax({
										// type: "POST",
										// url: "set/edit_client.php",
										// data: dataString,
										// cache: true,
										// success: function(html){
											
											// console.log(html);
										// }
											
									// });
							  }else 
							  {
								  Swal.fire(
									  'annulé!',
									  'Action annulée!!',
									  'error'
									)
								  
							  }
							})
				

			});

			
			var current_tr = null ; 
			var current_tr_row_Node = null ;
			$(document).on('click', '.edit', function(e){
				current_tr = $(this);

                console.log("current_tr = ",current_tr);
                current_tr_row_Node = table.row(current_tr.parents('tr')) ;


                e.stopPropagation();
					
					var dataString = '__UI__='+$(this).val();
					$.ajax({
						type: "POST",
						url: "get/get_client_detail.php",
						data: dataString,
						cache: true,
						success: function(html){
								$("#res_search").attr("class","col-lg-8");
								
								$("#detail_list").html(html);
								$("#drag_gps").hide();
								$("#drag_detail").hide();
								$("#drag_edit").show();
							}
						});	
					
			});
			
			
			
			
			
			$(document).on('click', '.detail', function(e){
					e.stopPropagation();
					
					var dataString = '__UI__='+$(this).val();
					$.ajax({
						type: "POST",
						url: "get/get_client_info.php",
						data: dataString,
						cache: true,
						success: function(html){
							
								$.ajax({
									type: "POST",
									url: "get/get_client_gps.php",
									data: dataString,
									cache: true,
									success: function(data){										
											$("#res_search").attr("class","col-lg-8");
											$("#detail_list_info").html(html);
											showmapDetail(JSON.parse(data));
											$("#drag_gps").hide();
											$("#drag_edit").hide();
											$("#drag_detail").show();
											
											
											
										}
									});	
								
							
							}
						});	
					
			});
			
			

			$(document).on('click', '#enregistrer_edit', function(){ 	
			var this_ = $(this) ; 
				var dataString = $( "#edit_form" ).serialize()+"&current_list="+ $(this).val(); 
				$.ajax
					({
						type: "POST",
						url: "set/edit_client.php",
						data: dataString ,
						cache: false,
						success: function(html)
						{
							console.log(html);
							if (html.trim() == 1 )
								{
							
									Swal.fire(
									  'Modification effectuée!',
									  '',
									  'success'
									)


									// alert(this_.val());
									$.ajax(
									{
										type : 'POST',
										url : 'get/get_client_search_unique.php',
										data: 'current_client=' +  this_.val(),
										success : function(data)
										{
											// console.log("res = ",data);
											info_html = JSON.parse(data);
											// table.rows.add(info_html).draw();


                                            current_tr_row_Node.remove().draw(false);
                                            current_tr_row_Node = table.rows.add(info_html).draw()
                                                .node();

                                            // $( rowNode )
                                            //     .css( 'color', 'red' )
                                            //     .animate( { color: 'black' } );
										}
										
									});
									
									
									
								
								}
								else 
								{
									Swal.fire(
									  'Erreur!',
									  'Code client déjà existant !',
									  'error'
									)
									
								}


						}
					});

			});
			
	
		
			$(document).on('click', '#vider', function(){ 	
				$('.sc_select2').val(null).trigger('change');
			});
			
			$(document).on('click', '#vider_edit', function(){ 	
				// $("#res_search").attr("class","col-lg-12");
				$("#detail_list").html('');
				$("#drag_edit").hide();
			});
			
			$("#drag_gps").on( "sortupdate", function( event, ui ) {
				showmapGPS();
			});
		

	
	
});
</script>


 <script>
 
	$(document).ready(function(){
			var group = new H.map.Group();
		function setUpClickListener(map) {
				map.addEventListener('tap', function (evt) {
				var coord = map.screenToGeo(evt.currentPointer.viewportX,
						evt.currentPointer.viewportY);
						
						
				map.removeObject(group);
                var pngIcon = new H.map.Icon("https://cdn1.iconfinder.com/data/icons/user-avatar-20/64/06-student-256.png", {size: {w: 40, h: 40}});
                let marker = new H.map.Marker({lat:coord.lat.toFixed(4), lng:coord.lng.toFixed(4)}, {
                    icon: pngIcon
                });

				group = new H.map.Group();
				 group.addObject(marker);
				map.addObject(group);


				$("#add_gps_lat").val(coord.lat.toFixed(4));
				$("#add_gps_long").val(coord.lng.toFixed(4));
				  
		  });
		}

		function showmap()
		{	
			// Step 1: initialize communication with the platform
			var platform = new H.service.Platform({
			 'app_id': 'xvY0IBhmajdYValFmgLI',
			  'app_code': 'RBdh5mLMXnjhhSvvL3DX5g',
			  useHTTPS: true
			});
			var pixelRatio = window.devicePixelRatio || 1;
			var defaultLayers = platform.createDefaultLayers({
			  tileSize: pixelRatio === 1 ? 256 : 512,
			  ppi: pixelRatio === 1 ? undefined : 320
			});

			// Step 2: initialize a map
			var map = new H.Map(document.getElementById('map'), defaultLayers.normal.map, {
			  // initial center and zoom level of the map
			  center: new H.geo.Point(33.916422, -6.946514),
			  zoom: 13
			});

			// Step 3: make the map interactive
			// MapEvents enables the event system
			// Behavior implements default interactions for pan/zoom (also on mobile touch environments)
			var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));

			// Step 4: Create the default UI:
			var ui = H.ui.UI.createDefault(map, defaultLayers, 'en-US');
			
			
			

			map.addObject(group);

			// map.addObject(londonMarker);


			setUpClickListener(map);
			
			//addDraggableMarker(map, behavior);

			
				
		}
		
		
		
		var group2 = new H.map.Group();
		function setUpClickListener2(map) {
				map.addEventListener('tap', function (evt) {
				var coord = map.screenToGeo(evt.currentPointer.viewportX,
						evt.currentPointer.viewportY);
						
						
				map.removeObject(group2);
                var pngIcon = new H.map.Icon("https://cdn1.iconfinder.com/data/icons/user-avatar-20/64/06-student-256.png", {size: {w: 40, h: 40}});
                let marker = new H.map.Marker({lat:coord.lat.toFixed(4), lng:coord.lng.toFixed(4)}, {
                    icon: pngIcon
                });
				group2 = new H.map.Group();
				 group2.addObject(marker);
				map.addObject(group2);


				$("#add_gps_lat2").val(coord.lat.toFixed(4));
				$("#add_gps_long2").val(coord.lng.toFixed(4));
				  
		  });
		}

		function showmap2()
		{	
			// Step 1: initialize communication with the platform
			var platform = new H.service.Platform({
					app_id: 'devportal-demo-20180625',
				  app_code: '9v2BkviRwi9Ot26kp2IysQ',
				  useHTTPS: true
			});
			var pixelRatio = window.devicePixelRatio || 1;
			var defaultLayers = platform.createDefaultLayers({
			  tileSize: pixelRatio === 1 ? 256 : 512,
			  ppi: pixelRatio === 1 ? undefined : 320
			});

			// Step 2: initialize a map
			var map = new H.Map(document.getElementById('map2'), defaultLayers.normal.map, {
			  // initial center and zoom level of the map
			  center: new H.geo.Point(33.916422, -6.946514),
			  zoom: 13
			});

			// Step 3: make the map interactive
			// MapEvents enables the event system
			// Behavior implements default interactions for pan/zoom (also on mobile touch environments)
			var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));

			// Step 4: Create the default UI:
			var ui = H.ui.UI.createDefault(map, defaultLayers, 'en-US');
			
			
			

			map.addObject(group2);

			// map.addObject(londonMarker);


			setUpClickListener2(map);
			
			//addDraggableMarker(map, behavior);

			
				
		}
		

		
		function steps_maj()
		{
			
			 $.ajax({
				 async: false,
				url : 'get/get_client_steps.php',               
				type:   'POST',    
				success: function(html){
					$("#tores_add_pan").html(html);
					
					 $("#add_form").steps({
						bodyTag: "fieldset",
						onStepChanging: function (event, currentIndex, newIndex)
						{
							resizeJquerySteps();
							// if (currentIndex > newIndex)
							// {
								// return true;
							// }
							indicekey = 1 ; 
							if( currentIndex == 0 ) 
							{
								$.ajax
								({
									async: false,
									type: "POST",
									url: "get/uniquetest.php",
									data: 'NUMERO_CLIENT='+ $('#add_numero').val() ,
									cache: false,
									success: function(html)
									{
										console.log(html);
										if (html.trim() == '0')
										{
											// console.log('hh');

											Swal.fire(
											  'Attention...',
											  'Ce code existe deja 22 !',
											  'warning'
											)
											indicekey = 0 ; 
											return false ; 
											
										}
										
										
										
									}
									
								});	
								
							}

							if( indicekey )
							{
								
								var form = $(this);

								if (currentIndex < newIndex)
								{
									$(".body:eq(" + newIndex + ") label.error", form).remove();
									$(".body:eq(" + newIndex + ") .error", form).removeClass("error");
								}

								form.validate().settings.ignore = ":disabled,:hidden";
								// console.log(form.valid());
								return form.valid();
							}
							
						},
						onStepChanged: function (event, currentIndex, priorIndex)
						{
							resizeJquerySteps();
							showmap();
							
							if (currentIndex === 3)
							{
								$(this).steps("next");
							}

							// Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
							if (currentIndex === 3 && priorIndex === 4)
							{
								$(this).steps("previous");
							}
							
							showmap2();
						},
						onFinishing: function (event, currentIndex)
						{
							resizeJquerySteps();
							var form = $(this);

							
							form.validate().settings.ignore = ":disabled";

							// Start validation; Prevent form submission if false
							return form.valid();
						},
						onFinished: function (event, currentIndex)
						{
							// alert('finished');
							$.ajax
							({
								type: "POST",
								url: "set/add_client.php",
								data: $( "#add_form" ).serialize(),
								cache: false,
								success: function(html)
								{
									console.log(html);
									if (html.trim() == 1 )
										{
									
											Swal.fire(
											  'Client ajouté avec succès!',
											  '',
											  'success'
											)
											
											$( "#add_form" ).html('');
											steps_maj();
										}
										else 
										{
											Swal.fire(
											  'Erreur!',
											  'Code client déjà existant !',
											  'error'
											)
											
										}


								}
							});
							
						}
					}).validate({
								errorPlacement: function (error, element)
								{
									element.before(error);
								},
						});
					
				}
		   });
		
			
	
		}
		
		steps_maj();
          
	});
				
				
				
		
	   
	   
    </script>



    <script>
        
		var  start_time = null;

		Dropzone.options.dropzoneForm = {
		  autoProcessQueue: false,
		  acceptedFiles:".xlsx,xls",
		  addRemoveLinks: true,
			dictDefaultMessage: "<strong>Déposer les fichiers ici ou cliquer pour importer. </strong></br> ",
		  init: function(){
		   var submitButton = document.querySelector('#importer');
		   myDropzone = this;
		   submitButton.addEventListener("click", function(){
				myDropzone.processQueue();
				
		   });
		    this.on("success", function(file, response) {
                console.log(response);
				 var _this = this;
				 _this.removeAllFiles();
				 $(".logs").html(response);
				 $(".logs").prepend('<span class="badge badge-warning">Import dans '+  (new Date().getTime() - start_time)+' ms </span>');
				 $('#ibox_import').children('.ibox-content').toggleClass('sk-loading');
            })
		   this.on('error', function(file, response) {
				$(file.previewElement).find('.dz-error-message').text(response);
				console.log(response);
			});
		   this.on('sending', function() {
			    start_time = new Date().getTime() ; 
				$('#ibox_import').children('.ibox-content').toggleClass('sk-loading');
			});
		  },
		 };
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 function showmapDetail(data)
			{
				
				$('#detail_list_info').prepend('<div id="map_detail" style="width: 100%; height: 300px; background: grey" /> <br/>');	

				var platform = new H.service.Platform({
				  app_id: '9Kunpn4peSnjFg0VeSH0',
				  app_code: '0G8hoE85eFFIF5u-6CUYzA',
				  useHTTPS: true
				  });
				var pixelRatio = window.devicePixelRatio || 1;
				var defaultLayers = platform.createDefaultLayers({
				  tileSize: pixelRatio === 1 ? 256 : 512,
				  ppi: pixelRatio === 1 ? undefined : 320
				});

				var map = new H.Map(document.getElementById('map_detail'),
				  defaultLayers.normal.map,{
				  center: {lat:data.Latitude, lng: data.Longitude},
				  zoom: 6,
				  pixelRatio: pixelRatio
				});

				var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));

				var ui = H.ui.UI.createDefault(map, defaultLayers);
                var pngIcon = new H.map.Icon("https://cdn1.iconfinder.com/data/icons/user-avatar-20/64/06-student-256.png", {size: {w: 40, h: 40}});
                let marker = new H.map.Marker({lat:data.Latitude, lng:data.Longitude}, {
                    icon: pngIcon
                });
				map.addObject(marker);
				
			
			}
    </script>
	

</body>

</html>
