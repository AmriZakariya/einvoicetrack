<?php
ob_start();
session_start();
include 'connexion.php';


if(isset($_SESSION['user_einvoicetrack']))
{
	
	$current_user = decode($_SESSION['user_einvoicetrack']) ; 					
	$SQL="SELECT  `NOM_USER`, `PRENOM_USER`, `EMAIL_USER`,`CIVILITE_USER`,
			`CODE_ENTREPRISE`, `ROLE_USER`
		  FROM `user` 
		  WHERE CODE_USER = $current_user
		  AND actif= 1";
	$query=mysqli_query($ma_connexion,$SQL);
	if(mysqli_num_rows($query) == 1)
	{
		while($row=mysqli_fetch_assoc($query))
		{	
				$NOM_USER = $row['CIVILITE_USER'].' ' .$row['NOM_USER'];
		}
	}
	else 
	{
		header('Location: login');
	}
	
	if( $_SESSION['role'] != 'superadmin' )
	{
		$SQL="SELECT 1
				FROM  user_module um
				WHERE  um.CODE_USER ='".decode($_SESSION['user_einvoicetrack'])."' 
				AND um.CODE_MODULE = 4"
		;
		$query=mysqli_query($ma_connexion,$SQL);
			
		if(mysqli_num_rows($query) == 0)
		{
			
			header('Location: users');
		}
	}
	
	
		// $i = 0 ; 
												

		// $SQL="SHOW COLUMNS FROM client";
		// $query=mysqli_query($ma_connexion,$SQL);
		// while($row=mysqli_fetch_assoc($query))
		// {	
				// echo '
					// <a class="toggle-vis" data-column="'.$i++.'">'.$row['Field'].'</a> -
					// ';
		// }
}
else 
{
	header('Location: login');
}
?>
<!DOCTYPE html>
<html lang="fr">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>eInvoiceTrack</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/plugins/dropzone/basic.css" rel="stylesheet">
    <link href="css/plugins/dropzone/dropzone.css" rel="stylesheet">
    <link href="css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
	
	 <link href="css/plugins/dataTables/datatables.min.css" rel="stylesheet">

	
	<link href="css/plugins/select2/select2.min.css" rel="stylesheet">
	
	
	<!-- Date range picker -->
     <link href="css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
	 

	
	
	

	
	
	
	
	
	

<style>

.select2 {
	width:100%!important;
	}
	
	.DETAILL
	{
		text-align: center; 
		vertical-align: middle;
	}
	.NOM
	{
		font-weight: bold;
		text-decoration: underline;
		cursor : pointer ; 
	}
	#liste_table tr
	{
		 cursor : pointer ; 
	}
	
	a.active{
		    background-color: #24c6c8;
			color: black;
	}
	
}



	
	
	
</style>
	
</head>

<body class="">

    <div id="wrapper">

    
		<?php
			include 'includes/nav.php';
		?>	

        <div id="page-wrapper" class="gray-bg">
		   <?php
				include 'includes/header.php';
			?>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4"> 
                    <h2>Bon de livraison</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="index"> Accueil </a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong></strong>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-8">
                    <div class="title-action">
                        <a href="#" class="btn btn-primary startTour"><i class="fa fa-play"></i> Démo </a>
                    </div>
                    <div class="title-action">
                        <button href="#" class="btn btn-warning syncData"  id="load2" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Synchronisation">
                            <i class="fa fa-refresh" id="refresh_icon"></i>
                            <img  src="loading_gif.gif" alt="loading_gif" id="loading_gif" style="width:32px;height:32px;display:none">
                            Synchroniser
                        </button>

<!--                        <button type="button" class="btn btn-primary btn-lg syncData" id="load2" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Processing Order">Submit Order-->
<!--                            <i class="fa fa-refresh"></i>-->
<!--                        </button>-->
                    </div>


                </div>
            </div>

           <div class="wrapper wrapper-content  animated fadeInRight">
            <div class="row" id="sortable-view">
			
			<div class="col-lg-12 dragme"  >
				<div class="ibox">
					
					
					
				</div>
			</div>
			
			<?php
				if( $_SESSION['role'] ==  'superadmin' ||  $_SESSION['role'] ==  'admin' )
				{
					
														
					?>
			<div class="col-md-6 dragme" id="add_client_panel">
				<div class="ibox ">
				 <div class="ibox-title" style="background-color: #24c6c8; color: white;">
				 <h5> <i class="fa fa-file"></i> Ajouter un bon de livraison </h5>
				  <div class="ibox-tools" >
							<a class="collapse-link">
								<i class="fa fa-chevron-up" style="color: white;"></i>
							</a>
							
							<a class="close-link">'
									<i class="fa fa-times"  style="color: white;"></i>'
                                </a>
						</div>
					</div>
					<div class="ibox-content">
					<form id="add_form" method="POST" >
					<!-- 
						<p>
							<button id="btn-add-tab" type="button" class="btn btn-primary pull-right"> <i class="fa fa-plus"></i>&nbsp;Ajouter Facture</button>
						</p>-->
						<!-- Nav tabs -->
							
								<div class="row">
									<div class="col-lg-6">
										<div class="form-group">
											<span class="badge badge-success">N° BL</span><span style="color:red">*</span> 
											<input name="add_bl"  type="text" class="form-control required" required>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<span class="badge badge-success">Client</span><span style="color:red">*</span> 
											<select class="sc_select2_simple form-control required" name="add_client"  data-placeholder="Selectionner client"  required>
												<?php
														$query = " SELECT NUMERO_CLIENT,NOM_CLIENT 
																	FROM client WHERE ETAT = 1 
														 ";
														$result = mysqli_query($ma_connexion, $query); 
													   while(($row = mysqli_fetch_array($result)) == true )  
														{ 										
															
															$CODE_CLIENT = urlencode($row['NUMERO_CLIENT']) ;
															$NUMERO_CLIENT = $row['NUMERO_CLIENT'] ;
															$NOM_CLIENT = $row['NOM_CLIENT'] ;
															echo ' <option value="'.$CODE_CLIENT.'">'.$NUMERO_CLIENT.'--'.$NOM_CLIENT.'</option>' ;
															 
														}
													?>
											</select>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<span class="badge badge-success">Responsable facturation</span><span style="color:red"></span>
											<input name="add_bc"  type="text" class="form-control " >
										</div>
									</div>
									
									<div class="col-lg-6">
										<div class="form-group">
											<span class="badge badge-success">Date livraison</span><span style="color:red">*</span> 
											<input name="add_date_livraison" type="date"  value="<?php echo date('Y-m-d'); ?>" class="form-control  required" required>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<span class="badge badge-success">Date numérisation </span><span style="color:red"></span> 
											<input name="add_date_numerisation" type="date"  value="<?php echo date('Y-m-d'); ?>" class="form-control  " >
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<span class="badge badge-success">Fichier joint </span><span style="color:red"></span> 
											<input type="file" name="add_pdf" class="form-control"  accept="application/pdf" id="add_file_pdf" >
										</div>
									</div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <span class="badge badge-success">Type de vente</span><span style="color:red"></span>
                                            <input name="add_TYPE_VENTE"  type="text" class="form-control " >
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <span class="badge badge-success">Type client</span><span style="color:red"></span>
                                            <input name="add_TYPE_CLIENT"  type="text" class="form-control " >
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <span class="badge badge-success">BL Manuel</span><span style="color:red"></span>
                                            <input name="add_BL_MANUEL"  type="text" class="form-control " >
                                        </div>
                                    </div>


                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <span class="badge badge-success">Date document</span><span style="color:red"></span>
                                            <input name="add_DATE_DOCUMENT" type="date"  value="<?php echo date('Y-m-d'); ?>" class="form-control  " >
                                        </div>
                                    </div>
								</div>
							
							<div class="logs_man" style="overflow-y: scroll; max-height:400px;  margin-top: 10px; margin-bottom:10px;">
							
							</div>
						<br/>
						<div class="row">
							<div class="col-md-12">
								<button type="button" class="btn btn-success btn-rounded pull-right" id="ajouter" > <i class="fa fa-save"></i>&nbsp; Enregistrer</button>
							</div>
						</div>
					</form>
					
					</div>
				   
				</div>
			</div>
		
			
			
			<div class="col-md-6 dragme" id="import_facture_panel_X4">
                    <div class="ibox" id="ibox_import_X4">
					 <div class="ibox-title" style="background-color: #24c6c8; color: white;">
					 <h5> <i class="fa fa-file"></i> Importer fichier de bons de livraisons </h5>
					  <div class="ibox-tools" >
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up" style="color: white;"></i>
                                </a>
								
								<a class="close-link">'
									<i class="fa fa-times"  style="color: white;"></i>'
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
								<div class="sk-spinner sk-spinner-wave " >
                                    <div class="sk-rect1"></div>
                                    <div class="sk-rect2"></div>
                                    <div class="sk-rect3"></div>
                                    <div class="sk-rect4"></div>
                                    <div class="sk-rect5"></div>
                                </div>
								
							<form method="get" action="eInvoiceTrack_Bon_de_Livraison.xlsx">
							<div class="alert alert-warning">
                                	Merci de respecter l'ordonnancement et les libellés 
									des colonnes du modéle d'import
									</div>
								<button class="btn btn-primary btn-rounded btn-block" type="submit"><i class="fa fa-download"></i> Télécharger le modèle d'import</button>	
							</form>	
							<br/>
							<br/>	
							<form action="set/importer_bl.php" class="dropzone" id="dropzoneForm2" >
								
								
							</form>
							<br/>
							<button class="btn btn-success btn-rounded btn-block" id="importer_X4"><i class="fa fa-gear"></i> Traiter</button>
							
							<div class="logs_X4" style="overflow-y: scroll; max-height:400px;  margin-top: 10px; margin-bottom:10px;">
							
							</div>
						</div>
                       
					</div>
			</div>
			
			
			<?php
						

						}							
					?>
			
				
			<div class="col-md-12 dragme">
                    <div class="ibox " id="ibox_recherche">
					 <div class="ibox-title" style="background-color: #24c6c8; color: white;">
							<h5> <i class="fa fa-search"></i> Recherche </h5>
							 <div class="ibox-tools" >
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up" style="color: white;"></i>
                                </a>
                               
                            </div>
                        </div>
                        <div class="ibox-content">
                        <form id="search_form" method="POST">
									<div class="row">
									
										
										<div class="col-md-6">
											<span class="badge badge-success">Date de livraison</span> 
											 <div id="reportrange1" class="form-control">
												<i class="fa fa-calendar"></i>
												<span></span> <b class="caret"></b>
											</div>
										</div>
										
										
										<div class="col-md-3">
											<span class="badge badge-success">N° BL</span> 
											<select class="sc_select2 form-control" name="NUM_BL[]"  id="NUM_BL" data-placeholder="Entrer N° BL" multiple="multiple">
												
											</select>
										</div>
										
										<div class="col-md-3">
											<span class="badge badge-success">Nom Client</span>
											<select class="sc_select2 form-control"  name="NOM_CLIENT[]" id="NOM_CLIENT" data-placeholder="Entrer nom client"  multiple="multiple">
												<?php
															$query = " SELECT NOM_CLIENT 
																		FROM client WHERE ETAT = 1 
															 ";
															$result = mysqli_query($ma_connexion, $query); 
														   while(($row = mysqli_fetch_array($result)) == true )  
															{ 										
																
																$NOM_CLIENT = $row['NOM_CLIENT'] ;
																echo ' <option value="'.$NOM_CLIENT.'">'.$NOM_CLIENT.'</option>' ;
																 
															}
														?>						
											</select>
										</div>
										
										
										
										
									
										<div class="col-md-6">
											<span class="badge badge-success">Date de numérisation</span> 
											 <div id="reportrange2" class="form-control">
												<i class="fa fa-calendar"></i>
												<span></span> <b class="caret"></b>
											</div>
										</div>
										
										
										<div class="col-md-3">
											<span class="badge badge-success">Code client</span> 
											<select class="sc_select2 form-control" name="NUMERO_CLIENT[]"  id="NUMERO_CLIENT" data-placeholder="Entrer code client" multiple="multiple">
												<?php
														$query = " SELECT NUMERO_CLIENT 
																	FROM client WHERE ETAT = 1 
														 ";
														$result = mysqli_query($ma_connexion, $query); 
													   while(($row = mysqli_fetch_array($result)) == true )  
														{ 										
															
															$NUMERO_CLIENT = $row['NUMERO_CLIENT'] ;
															echo ' <option value="'.$NUMERO_CLIENT.'">'.$NUMERO_CLIENT.'</option>' ;
															 
														}
													?>
											</select>
										</div>
										
										<div class="col-md-3">
											<span class="badge badge-success">Responsable facturation</span>
											<select class="sc_select2 form-control" name="BC[]"  id="BC" data-placeholder="Entrer Responsable facturation" multiple="multiple">
												
											</select>
										</div>

                                        <div class="col-md-3">
                                            <span class="badge badge-success">Type de vente:</span>
                                            <select class="sc_select2_no form-control" name="TYPE_VENTE[]"  id="TYPE_VENTE" data-placeholder="Sélectionner le type de vente" multiple="multiple">
                                                <?php
//                                                $query = " SELECT CODE_STATUS , NOM_STATUS
//																	FROM status
//														 ";
//                                                $result = mysqli_query($ma_connexion, $query);
//                                                while(($row = mysqli_fetch_array($result)) == true )
//                                                {
//
//                                                    $CODE_STATUS = encode($row['CODE_STATUS']) ;
//                                                    $NOM_STATUS = $row['NOM_STATUS'] ;
//                                                    echo ' <option value="'.$CODE_STATUS.'">'.$NOM_STATUS.'</option>' ;
//
//                                                }
                                                ?>
                                            </select>
                                        </div>

                                        <div class="col-md-3">
                                            <span class="badge badge-success">Type client:</span>
                                            <select class="sc_select2_no form-control" name="TYPE_CLIENT[]"  id="TYPE_CLIENT" data-placeholder="Sélectionner le type de client" multiple="multiple">
                                                <?php
//                                                $query = " SELECT CODE_STATUS , NOM_STATUS
//																	FROM status
//														 ";
//                                                $result = mysqli_query($ma_connexion, $query);
//                                                while(($row = mysqli_fetch_array($result)) == true )
//                                                {
//
//                                                    $CODE_STATUS = encode($row['CODE_STATUS']) ;
//                                                    $NOM_STATUS = $row['NOM_STATUS'] ;
//                                                    echo ' <option value="'.$CODE_STATUS.'">'.$NOM_STATUS.'</option>' ;
//
//                                                }
                                                ?>
                                            </select>
                                        </div>



                                        <div class="col-md-3">
                                            <span class="badge badge-success">N° BL Manuel</span>
                                            <select class="sc_select2 form-control" name="BL_MANUEL[]"  id="BL_MANUEL" data-placeholder="Entrer N° BL Manuel" multiple="multiple">

                                            </select>
                                        </div>


                                        <div class="col-md-3">
                                            <span class="badge badge-success">Date document</span>
                                            <select class="sc_select2 form-control" name="DATE_DOCUMENT[]"  id="DATE_DOCUMENT" data-placeholder="Entrer Date document" multiple="multiple">

                                            </select>
                                        </div>



                                    </div>
									<br/>
									<div class="row">
										<div class="col-md-12">
											<button type="button" class="btn btn-success btn-rounded pull-right" id="rechercher" > <i class="fa fa-search"></i> Rechercher</button>
											<button type="button" class="btn btn-danger btn-rounded pull-right" id="vider" style="margin-right: 4px;"> <i class="fa fa-eraser"></i> Vider</button>
										</div>
									</div>
								</form>
	
						</div>
                       
					</div>
                </div>
				
			<div class="col-lg-12 dragme"  style="display:none" id="drag_detail">
					<div class="ibox">
						<div class="ibox-title"  style="background-color: #24c6c8; color: white;">
							<h5> Détail Bon de livraison </h5>
							 <div class="ibox-tools" >
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up" style="color: white;"></i>
                                </a>
                                <a class="collapse-close-tabs">
                                    <i class="fa fa-times" style="color: white;"></i>
                                </a>
                               
                            </div>
						</div>
						
						
						<div class="ibox-content" >
								<div class="text-center pdf-toolbar">

										<div class="btn-group">
											<button id="prev" class="btn btn-white"><i class="fa fa-long-arrow-left"></i> <span class="d-none d-sm-inline">Previous</span></button>
											<button id="next" class="btn btn-white"><i class="fa fa-long-arrow-right"></i> <span class="d-none d-sm-inline">Next</span></button>
											<button id="zoomin" class="btn btn-white"><i class="fa fa-search-minus"></i> <span class="d-none d-sm-inline">Zoom In</span></button>
											<button id="zoomout" class="btn btn-white"><i class="fa fa-search-plus"></i> <span class="d-none d-sm-inline">Zoom Out</span> </button>
											<button id="zoomfit" class="btn btn-white"> 100%</button>
											<span class="btn btn-white hidden-xs">Page: </span>

										<div class="input-group">
											<input type="text" class="form-control" id="page_num">

											<div class="input-group-append">
												<button type="button" class="btn btn-white" id="page_count">/ 22</button>
											</div>
										</div>

											</div>
									</div>

									<div class="text-center m-t-md">
										<canvas id="the-canvas" class="pdfcanvas border-left-right border-top-bottom b-r-md"></canvas>
									</div>	
									
									
									<div id="detail_list_info">
									</div>	
									
									
							
						</div>
						
						
					</div>
				</div>
			 <div class="col-lg-12 dragme" id="res_search">
                    <div class="ibox" id="ibox_result">
                        <div class="ibox-title" style="background-color: #24c6c8; color: white;">
						<h5> <i class="fa fa-list"></i> Resultat Recherche</h5>
						 <div class="ibox-tools" >
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up" style="color: white;"></i>
                                </a>
                               
                            </div>
                        </div>
                        <div class="ibox-content">
							<div class="sk-spinner sk-spinner-wave " >
                                    <div class="sk-rect1"></div>
                                    <div class="sk-rect2"></div>
                                    <div class="sk-rect3"></div>
                                    <div class="sk-rect4"></div>
                                    <div class="sk-rect5"></div>
                                </div>
								
							
							<hr/>
							<?php
				if( $_SESSION['role'] ==  'superadmin' ||  $_SESSION['role'] ==  'admin' )
				{
					
														
					?>
							<button class="btn btn-danger delete_multiple" ><i class="glyphicon glyphicon-trash"></i> Supprimer</button>
<?php
				}
														
					?>							
							<button class="btn btn-dark pdf_multiple" > <i class="fa fa-file-pdf-o "></i> Télécharger PJ</button>	
	
							<div class="table-responsive">
								
								<table class="table table-bordered table-striped" cellspacing="0" width="100%" id="liste_table">
									<thead>
									  <tr>
										<th ></th>
										<th >N° BL</th>
										<th >Nom client</th>
										<th >Code client</th>
										<th >Responsable facturation</th>
										<th >Date de livraison </th>
										<th >Date de numérisation </th>
										<th >Type de vente </th>
										<th >Type client </th>
										<th >BL Manuel </th>
										<th >Date document </th>





										<th style="width:20%"></th>
									  </tr>
									</thead>
									<tbody>
									
									</tbody>
								</table>
								
								
							</div>

                        </div>
                    </div>
                </div>
				
				<div class="col-lg-4 dragme"  style="display:none" id="drag_edit">
					<form id="edit_form" method="POST"  >				
						<div class="ibox">
							<div class="ibox-title"  style="background-color: #24c6c8; color: white;">
								<h5>Modifier</h5>
								 <div class="ibox-tools" >
									<a class="collapse-link">
										<i class="fa fa-chevron-up" style="color: white;"></i>
									</a>
									<a class="collapse-close-tabs">
										<i class="fa fa-times" style="color: white;"></i>
									</a>
								   
								</div>
							</div>

							<div class="ibox-content" id="detail_list">
							
							</div>
						</div>
					<form>
				</div>
				
				
				
				
				
			
				
				
				
               
				
                
                
            

            </div>
        </div>
           <?php
			include 'includes/footer.php';
		?>	

        </div>
        </div>







    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>

    <!-- jquery UI -->
    <script src="js/plugins/jquery-ui/jquery-ui.min.js"></script>

    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>

    <!-- Touch Punch - Touch Event Support for jQuery UI -->
    <script src="js/plugins/touchpunch/jquery.ui.touch-punch.min.js"></script>

	
    <!-- Select2 -->
    <script src="js/plugins/select2/select2.full.min.js"></script>
	<script src="js/i18n/fr.js"></script>
	
	
	<!-- datatable -->
	<script src="js/plugins/dataTables/datatables.min.js"></script>
    <script src="js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>


	<!-- Steps -->
	<link href="css/plugins/steps/jquery.steps.css" rel="stylesheet">
    <script src="js/plugins/steps/jquery.steps.min.js"></script>
    <script src="js/plugins/steps/jquery.steps.fix.js"></script>

    <!-- Jquery Validate -->
    <script src="js/plugins/validate/jquery.validate.min.js"></script>
    <script src="js/plugins/validate/messages_fr.js"></script>
	
	
	
	 <!-- Sweet alert -->
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	
	
	
    <!-- Jasny -->
    <script src="js/plugins/jasny/jasny-bootstrap.min.js"></script>

    <!-- DROPZONE -->
    <script src="js/plugins/dropzone/dropzone.js"></script>

    
	 <!-- Date range use moment.js same as full calendar plugin -->
    <script src="js/plugins/fullcalendar/moment.min.js"></script>

	 <!-- Date range picker -->
    <script src="js/plugins/daterangepicker/daterangepicker.js"></script>

	
	<link type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/awesome-bootstrap-checkbox/0.3.7/awesome-bootstrap-checkbox.css" rel="stylesheet" />
	<link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/css/dataTables.checkboxes.css" rel="stylesheet" />
	<script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/js/dataTables.checkboxes.min.js"></script>
	
	
	
	<script src="js/plugins/pdfjs/pdf.js"></script>

<script>

 var start_date_1 = null ;
        var end_date_1 = null ;

 var start_date_2 = null ;
        var end_date_2 = null ;

$(document).ready(function(){

    $('#loading_gif').hide();

    $('.startTour').click(function(){
        // tour.restart();

    })
    $('.syncData').click(function(){

        var $this = $(this);

        // $this.button('loading');
        $('#loading_gif').show();
        $('#refresh_icon').hide();

        $.ajax({
            url : 'set/edit_bl.php',
            type:   'POST',
            data: {
                "synchronisation" : "true"
            },
            success: function(response){
                // $this.button('reset');
                $('#loading_gif').hide();
                $('#refresh_icon').show();

                console.log(response);
                // window.location = response;

                Swal.fire(
                    'la synchronisation terminée',
                    '',
                    'success'

                )
            },
            error: function (jqXHR, exception) {
                // $this.button('reset');
                $('#loading_gif').hide();
                $('#refresh_icon').show();

                var msg = '';
                if (jqXHR.status === 0) {
                    msg = 'Not connect.\n Verify Network.';
                } else if (jqXHR.status == 404) {
                    msg = 'Requested page not found. [404]';
                } else if (jqXHR.status == 500) {
                    msg = 'Internal Server Error [500].';
                } else if (exception === 'parsererror') {
                    msg = 'Requested JSON parse failed.';
                } else if (exception === 'timeout') {
                    msg = 'Time out error.';
                } else if (exception === 'abort') {
                    msg = 'Ajax request aborted.';
                } else {
                    msg = 'Uncaught Error.\n' + jqXHR.responseText;
                }
                // alert(msg);
            }
        });
    })
		WinMove();
	
		$.fn.select2.defaults.set('language', 'fr');
	
		$(".sc_select2").select2({
			allowClear: true,
			tags: true,
			language: "fr",
			 width: '100%',
			 
		});
		$(".sc_select2_no").select2({
			allowClear: true,
			language: "fr",
			 width: '100%',
			 
		});
		$(".sc_select2_simple").select2({
			language: "fr",
			 width: '100%'
			 
		});
		
		var table =   $('#liste_table').DataTable({	
				responsive: true,
				"language":{
						"sProcessing":     "Traitement en cours...",
						"sSearch":         "Rechercher une bon de livraison &nbsp;:",
						"sLengthMenu":     "Afficher _MENU_  bon de livraison",
						"sInfo":           "Affichage des bon de livraison _START_ &agrave; _END_ sur _TOTAL_ bons de livraisons",
						"sInfoEmpty":      "Affichage du bon de livraison 0 &agrave; 0 sur 0 bons de livraisons",
						"sInfoFiltered":   "(filtr&eacute; de _MAX_ bons de livraisons au total)",
						"sInfoPostFix":    "",
						"sLoadingRecords": "Chargement en cours...",
						"sZeroRecords":    "Aucuns virements &agrave; afficher",
						"sEmptyTable":     "Aucunes donn&eacute;e disponible dans le tableau",
						"oPaginate": {
							"sFirst":      "Premier",
							"sPrevious":   "Pr&eacute;c&eacute;dent",
							"sNext":       "Suivant",
							"sLast":       "Dernier"
						},
						"oAria": {
							"sSortAscending":  ": activer pour trier la colonne par ordre croissant",
							"sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
						}
				},
				dom: '<"html5buttons"B>lTfgitp',
				buttons: [
					{ 
						extend: 'copy',
						title: 'eInvoiceTrack',
						exportOptions: {
							columns:  ':visible:not(:last-child)'
						}
					},
					{
						extend: 'csv',
						title: 'eInvoiceTrack',
						exportOptions: {
							columns:  ':visible:not(:last-child)'
						}
					},
					{
						extend: 'excel',
						title: 'eInvoiceTrack',
						exportOptions: {
							columns:  ':visible:not(:last-child)'
						}
					},

					{extend: 'print',
					 customize: function (win){
							$(win.document.body).addClass('white-bg');
							$(win.document.body).css('font-size', '10px');

							$(win.document.body).find('table')
									.addClass('compact')
									.css('font-size', 'inherit');
						}
					}
				],
				"columns": [
					{ "data": "check_box"},
					{ "data": "BL",className: "BL"},
					{ "data": "NOM_CLIENT",className: "NOM_CLIENT"},
					{ "data": "CODE_CLIENT",className: "CODE_CLIENT"},
					{ "data": "BC",className: "BC"},
					{ "data": "DATE_LIVRAISON",className: "DATE_LIVRAISON"},
					{ "data": "DATE_NUMERISATION",className: "DATE_NUMERISATION"},
					{ "data": "TYPE_VENTE",className: "TYPE_VENTE"},
					{ "data": "TYPE_CLIENT",className: "TYPE_CLIENT"},
					{ "data": "BL_MANUEL",className: "BL_MANUEL"},
					{ "data": "DATE_DOCUMENT",className: "DATE_DOCUMENT"},
					{ "data": "DETAILL",className: "DETAILL"},




					
					
				 ],
			    'columnDefs': [
					 {
						'targets': 0,
						'render': function(data, type, row, meta){
						   if(type === 'display'){
							  data = '<div class="checkbox"><input type="checkbox" class="dt-checkboxes"><label></label></div>';
						   }

						   return data;
						},
						'checkboxes': {
						   'selectRow': true,
						   'selectAllRender': '<div class="checkbox"><input type="checkbox" class="dt-checkboxes"><label></label></div>'
						}
					 }
				  ],
				  'select': 'multi',
				  'order': [[1, 'asc']]
			});	
					
			
			$(document).on('click', '#rechercher', function(){ 	
				$.ajax(
				{
					type : 'POST',
					url : 'get/get_bl_search.php',
					data: { 
							"NUM_BL" : $('#NUM_BL').val(),
							"NOM_CLIENT" : $('#NOM_CLIENT').val(),
							"NUMERO_CLIENT" : $('#NUMERO_CLIENT').val(),
							"BC" : $('#BC').val(),
							"DATE_LIVRAISON_START" : start_date_1,
							"DATE_LIVRAISON_END" : end_date_1,
							"DATE_NUMERISATION_START" : start_date_2,
							"DATE_NUMERISATION_END" : end_date_2,
							"TYPE_VENTE" : $('#TYPE_VENTE').val(),
							"TYPE_CLIENT" : $('#TYPE_CLIENT').val(),
							"BL_MANUEL" : $('#BL_MANUEL').val(),
							"DATE_DOCUMENT" : $('#DATE_DOCUMENT').val()
                    },
					success : function(data)
					{
						
						console.log(data);
						datatable = JSON.parse(data);
						table.clear();
						table.rows.add(datatable).draw();
						$("#drag_detail").hide();
						$("#drag_edit").hide();
						$("#res_search").attr("class","col-lg-12");
						
						
					},
					complete : function(data)
					{
						$('#ibox_result').children('.ibox-content').toggleClass('sk-loading');
					},
					beforeSend : function(data)
					{
						$('#ibox_result').children('.ibox-content').toggleClass('sk-loading');
					},
					error: function (jqXHR, exception) {
						var msg = '';
						if (jqXHR.status === 0) {
							msg = 'Not connect.\n Verify Network.';
						} else if (jqXHR.status == 404) {
							msg = 'Requested page not found. [404]';
						} else if (jqXHR.status == 500) {
							msg = 'Internal Server Error [500].';
						} else if (exception === 'parsererror') {
							msg = 'Requested JSON parse failed.';
						} else if (exception === 'timeout') {
							msg = 'Time out error.';
						} else if (exception === 'abort') {
							msg = 'Ajax request aborted.';
						} else {
							msg = 'Uncaught Error.\n' + jqXHR.responseText;
						}
					}
				});

			});
			
			
			
			$(document).on('click', '#ajouter', function(){ 
				let req = 1 ;	
				$('#add_form :input').css("border-color","");	
				$('#add_form [required]').css("border-color","");	
				$('#add_form [required]').each(function () {
					if ( !$(this).val())
					{
						// console.log($(this));
						$(this).css("border-color","#FF0000");
						// $('#select2-'+$(this)+'-container').parent().css('background-color', 'red');
						req = 0 ; 
					}
				});
				if(req)
				{
					
					var file_data = $('#add_file_pdf').prop('files')[0];   
					var form_data = new FormData();                  
					form_data.append('file', file_data);	
					form_data.append('add_bl', $('input[name="add_bl"]').val());	
					form_data.append('add_client', $('select[name="add_client"] option:selected').val());	
					form_data.append('add_bc', $('input[name="add_bc"]').val());	
					form_data.append('add_date_livraison', $('input[name="add_date_livraison"]').val());	
					form_data.append('add_date_numerisation', $('input[name="add_date_numerisation"]').val());

					form_data.append('add_TYPE_VENTE', $('input[name="add_TYPE_VENTE"]').val());
					form_data.append('add_TYPE_CLIENT', $('input[name="add_TYPE_CLIENT"]').val());
					form_data.append('add_BL_MANUEL', $('input[name="add_BL_MANUEL"]').val());
					form_data.append('add_DATE_DOCUMENT', $('input[name="add_DATE_DOCUMENT"]').val());

					
					$.ajax({
						url: 'set/add_bl.php', 
						dataType: 'text',  
						cache: false,
						contentType: false,
						processData: false,
						data: form_data,                         
						type: 'POST',					
						success : function(data)
						{
							// console.log(data);
							$('#add_form')[0].reset();
							$(".logs_man").append(data);

						},
						complete : function(data)
						{
							// $(".sk-spinner").css("display","none");
							// $("#res_search").attr("class","col-lg-8");
							// $('#ibox_result').children('.ibox-content').toggleClass('sk-loading');
						},
						beforeSend : function(data)
						{
							// $('#ibox_result').children('.ibox-content').toggleClass('sk-loading');
							// $(".sk-spinner").css("display","block ");
							// alert(data);
							// do something, not critical.
						},
						error: function (jqXHR, exception) {
							var msg = '';
							if (jqXHR.status === 0) {
								msg = 'Not connect.\n Verify Network.';
							} else if (jqXHR.status == 404) {
								msg = 'Requested page not found. [404]';
							} else if (jqXHR.status == 500) {
								msg = 'Internal Server Error [500].';
							} else if (exception === 'parsererror') {
								msg = 'Requested JSON parse failed.';
							} else if (exception === 'timeout') {
								msg = 'Time out error.';
							} else if (exception === 'abort') {
								msg = 'Ajax request aborted.';
							} else {
								msg = 'Uncaught Error.\n' + jqXHR.responseText;
							}
							// alert(msg);
						}
					});
					
				}
				
				
				
				

			});
			
			
			
			
			
			
			
			 $('a.toggle-vis').on( 'click', function (e) {
				e.preventDefault();
		 
				var column = table.column( $(this).attr('data-column') );
		 
				column.visible( ! column.visible() );
				
				$(this).toggleClass( "badge-success text-light" );
			} );
			
			
			
			 $('a.collapse-close-tabs').on( 'click', function (e) {
				$("#res_search").attr("class","col-lg-12");
				//$("#detail_list").html(html);
				$(this).closest('.dragme').hide();
			} );
			
			
			
			$(document).on('click', '.delete', function(e)
			{ 
				e.stopPropagation();
					
				var this_ = $(this) ; 
						Swal.fire({
						  title: 'Supprimer Bon de livraison',
						  text: "Êtes-vous sûr de continuer?!",
						  type: 'warning',
						  showCancelButton: true,
						  confirmButtonColor: '#3085d6',
						  cancelButtonColor: '#d33',
						  confirmButtonText: 'Oui, Supprimer!',
						  cancelButtonText: "Non, Annuler!",  
						}).then((result) => {
						  if (result.value) {
									var dataString = "current_delete="+ this_.val(); 
									$.ajax({
									type: "POST",
									url: "set/edit_bl.php",
									data: dataString,
									cache: true,
									success: function(html){
											// this_.parents("tr").find('.label-primary').parent().html('<span class="label label-danger">supprimé </span>');
											// this_.parents("tr").find('.prixhistoryedit').html('<span class="label label-danger">');
											// this_.parent().html('<span class="label label-danger">');
												console.log(html);
												table.row(this_.parents('tr')).remove().draw(false);
												$("#detail_list").html('')
													Swal.fire(
													  'BL supprimé !',
													  '',
													  'success'
													)
											} 
											
									});
							  }else 
							  {
								  Swal.fire(
									  'annulé!',
									  'Action annulée!!',
									  'error'
									)
								  
							  }
							})
				

			});

			$(document).on('click', '.delete_multiple', function(e)
			{ 
				e.stopPropagation();
				
				let rows = $( table.$('input[type="checkbox"]').map(function () {
						  return $(this).prop("checked") ? $(this).closest('tr') : null ;					  
				}));
				
				// keys.each(function(k,v){
					// console.log(v);
				// })

					
				var this_ = $(this) ; 
						Swal.fire({
						  title: 'Supprimer BL',
						  text: "Êtes-vous sûr de continuer ?!",
						  type: 'warning',
						  showCancelButton: true,
						  confirmButtonColor: '#3085d6',
						  cancelButtonColor: '#d33',
						  confirmButtonText: 'Oui, Supprimer!',
						  cancelButtonText: "Non, Annuler!",  
						}).then((result) => {
						  if (result.value) {
							  

								$.ajax(
								{
									type : 'POST',
									url : 'set/edit_bl.php',
									data: { 
											"delete_code" : table.$('input[type="checkbox"]').map(function () {
																	  return $(this).prop("checked") ? $(this).closest('tr').find('.delete').val() : null;
															}).get(),
											},
									success : function(data)
									{
										console.log(data);
										// $(".logs_man").html(data);
										rows.each(function(k,v){
											table.row(v).remove().draw(false);
										})
										

									},
									complete : function(data)
									{
										// $(".sk-spinner").css("display","none");
										// $("#res_search").attr("class","col-lg-8");
										// $('#ibox_result').children('.ibox-content').toggleClass('sk-loading');
									},
									beforeSend : function(data)
									{
										// $('#ibox_result').children('.ibox-content').toggleClass('sk-loading');
										// $(".sk-spinner").css("display","block ");
										// alert(data);
										// do something, not critical.
									},
									error: function (jqXHR, exception) {
										var msg = '';
										if (jqXHR.status === 0) {
											msg = 'Not connect.\n Verify Network.';
										} else if (jqXHR.status == 404) {
											msg = 'Requested page not found. [404]';
										} else if (jqXHR.status == 500) {
											msg = 'Internal Server Error [500].';
										} else if (exception === 'parsererror') {
											msg = 'Requested JSON parse failed.';
										} else if (exception === 'timeout') {
											msg = 'Time out error.';
										} else if (exception === 'abort') {
											msg = 'Ajax request aborted.';
										} else {
											msg = 'Uncaught Error.\n' + jqXHR.responseText;
										}
										// alert(msg);
									}
								});
							  
								// var rows = $( table.$('input[type="checkbox"]').map(function () {
								  // return $(this).prop("checked") ? $(this).closest('tr') : null;
								// } ) );
								
								
								// function show(values) {
									// console.log(values);  
								 // }
								 // rows.forEach(show[0].cells[4].find('.delete').val()); 
								
								// console.log(rows[0].cells[4].find('.delete').val());
									// var dataString = "current_delete="+ this_.val(); 
									// $.ajax({
										// type: "POST",
										// url: "set/edit_client.php",
										// data: dataString,
										// cache: true,
										// success: function(html){
											
											// console.log(html);
										// }
											
									// });
							  }else 
							  {
								  Swal.fire(
									  'annulé!',
									  'Action annulée!!',
									  'error'
									)
								  
							  }
							})
				

			});
			
			$(document).on('click', '.pdf_multiple', function(e)
			{ 
				e.stopPropagation();
				
				 $.ajax({
						url : 'set/edit_bl.php',               
						type:   'POST',    
						data: { 
								"pdf_code" : table.$('input[type="checkbox"]').map(function () {
													  return $(this).prop("checked") ? $(this).closest('tr').find('.pdf_dwn').val() : null;
											}).get(),
							},
						success: function(response){
							console.log(response);
						    window.location = response;
						}
				   });
			});	
			
			
			$(document).on('click', '.pdf_dwn', function(e)
			{ 
				e.stopPropagation();
				 $.ajax({
						url : 'set/edit_bl.php',               
						type:   'POST',    
						data: 'pdf_dwn='+$(this).val(),
						success: function(file_path){
							file_path = file_path.trim();
							console.log(file_path);
							  // window.location.href = response;
							  var a = document.createElement('A');
								a.href = file_path;
								a.download = file_path.substr(file_path.lastIndexOf('/') + 1);
								document.body.appendChild(a);
								a.click();
								document.body.removeChild(a);
						}
				   });
			});
			
			var current_tr = null ; 
			$(document).on('click', '.edit', function(e){
				current_tr = $(this);
					e.stopPropagation();
					
					var dataString = '__UI__='+$(this).val();
					$.ajax({
						type: "POST",
						url: "get/get_bl_detail.php",
						data: dataString,
						cache: true,
						success: function(html){
								$("#res_search").attr("class","col-lg-8");
								$("#detail_list").html(html);
								$("#drag_edit").show();
								
							}
						});	
					
			});
			
			
			
			
			
			$(document).on('click', '.detail', function(e){
					e.stopPropagation();
					let url = './dossier_bl/'+$(this).val()+'.pdf';
					var dataString = '__UI__='+$(this).val();
					$.ajax({
						async:false ,
						type: "POST",
						url: "get/get_bl_info.php",
						data: dataString,
						cache: true,
						success: function(html){
							
								//$("#res_search").attr("class","col-lg-8");
								$("#detail_list_info").html(html);
								//$("#drag_edit").hide();
								$("#drag_detail").show();
								showpdf(url);
								
							
							}
						});	
					
			});
			
			
			
			
			

			$(document).on('click', '#enregistrer_edit', function(){ 

			
			var this_ = $(this) ; 
			let req = 1 ;	
			$('#edit_form :input').css("border-color","");	
			$('#edit_form [required]').css("border-color","");	
			$('#edit_form [required]').each(function () {
				if ( !$(this).val())
				{
					// console.log($(this));
					$(this).css("border-color","#FF0000");
					// $('#select2-'+$(this)+'-container').parent().css('background-color', 'red');
					req = 0 ; 
				}
			});
			
			// alert(req);
			if(req)
			{
			
				var file_data = $('#edit_file_pdf').prop('files')[0];   
				var form_data = new FormData();                  
				form_data.append('current_list', $(this).val());	
				form_data.append('file', file_data);	
				form_data.append('edit_numero', $('input[name="edit_numero"]').val());	
				form_data.append('edit_bc', $('input[name="edit_bc"]').val());	
				form_data.append('edit_date_livraison', $('input[name="edit_date_livraison"]').val());	
				form_data.append('edit_date_numerisation', $('input[name="edit_date_numerisation"]').val());	
				form_data.append('edit_TYPE_VENTE', $('input[name="edit_TYPE_VENTE"]').val());
				form_data.append('edit_TYPE_CLIENT', $('input[name="edit_TYPE_CLIENT"]').val());
				form_data.append('edit_BL_MANUEL', $('input[name="edit_BL_MANUEL"]').val());
				form_data.append('edit_DATE_DOCUMENT', $('input[name="edit_DATE_DOCUMENT"]').val());


			
				// var dataString = $( "#edit_form" ).serialize()+"&current_list="+ $(this).val(); 
				$.ajax({
						url: 'set/edit_bl.php', 
						dataType: 'text',  
						cache: false,
						contentType: false,
						processData: false,
						data: form_data,                         
						type: 'POST',					
						success : function(html)
						{
							console.log(html);
							
							if(html.trim() == '1' )
							{								
                                Swal.fire(
                                  'Bon de livraison modifié avec succès!',
                                  '',
                                  'success'

                                )

                                $("#drag_edit").hide();
                                        $("#rechercher").trigger("click");
                                $("#res_search").attr("class","col-lg-12");

                                }
							else 
							{
								Swal.fire(
								  'Attention!',
								  html,
								  'warining'

								)
								
								
							}
									
									
									
							


						}
					});
					
				}

			});
			
			
	
		
			$(document).on('click', '#vider', function(){ 	
				$('.sc_select2').val(null).trigger('change');
				$('.sc_select2_no').val(null).trigger('change');
				$('.sc_select2_simple').val(null).trigger('change');
				$('#reportrange1 span').html('');
				$('#reportrange2 span').html('');

                start_date_1 = null ;
                end_date_1 = null ;

                start_date_2 = null ;
                end_date_2 = null ;

			});
			
		
	
	
});

         // Dropzone.autoDiscover = false;
		var  start_time = null;
		
		
		Dropzone.autoDiscover = false;
		

		var myDropzoneTheSecond = new Dropzone(
				//id of drop zone element 1
				'#dropzoneForm2', { 
					autoProcessQueue: false,
					acceptedFiles:".xlsx,xls",
					addRemoveLinks: true,
					dictDefaultMessage: "<strong>Déposer les fichiers ici ou cliquer pour importer. </strong></br> ",
					init: function(){
						 let myDropzone = this;
						$(document).on('click', '#importer_X4', function(e)
						{ 
							myDropzone.processQueue();
						});
						 this.on("success", function(file, response) {
							// console.log(response);
							 var _this = this;
							 _this.removeAllFiles();
							 $(".logs_X4").html(response);
							 $(".logs_X4").prepend('<span class="badge badge-warning">Import dans '+  (new Date().getTime() - start_time)+' ms </span>');
							 $('#ibox_import_X4').children('.ibox-content').toggleClass('sk-loading');
						})
						 this.on('error', function(file, response) {
							$(file.previewElement).find('.dz-error-message').text(response);
							// console.log(response);
						});
						 this.on('sending', function() {
							start_time = new Date().getTime() ; 
							$('#ibox_import_X4').children('.ibox-content').toggleClass('sk-loading');
						});
					},
				}
			);
	
	
		 
		var button='<button class="close" type="button" title="Remove this page">×</button>';
		var tabID = 1;
		function resetTab(){
			var tabs=$("#tab-list li:not(:first)");
			var len=1
			$(tabs).each(function(k,v){
				len++;
				$(this).find('a').html('Tab ' + len + button);
			})
			tabID--;
		}

		$(document).ready(function() {
			$('#btn-add-tab').click(function() {
				tabID++;
				$('#tab-list').append($('<li><a href="#tab' + tabID + '" role="tab" data-toggle="tab" style="color:black"><span>Facture ' + tabID + '</span><button class="close" type="button" title="Remove this page">×</button></a></li>'));
				
				$.ajax(
				{
					async: false,
					type : 'post',
					url : 'get/get_add_facture_pane.php',
					success : function(data)
					{
						$('#tab-content').append($('<div class="tab-pane " id="tab' + tabID + '" style="margin-top:18px">'+data+'</div>'));
						$(".sc_select2_simple").select2({
							language: "fr",
							 width: '100%'
							 
						});	
					}
					
				});

				$(".edit").click(editHandler);
				// $("a").parent().removeClass("active");
				// $("a[href$='#tab"+tabID+"']").parent().addClass("active");
				$("a[href$='#tab"+tabID+"']").tab('show');
			});
			
			$('#tab-list').on('click', '.close', function() {
				var tabID = $(this).parents('a').attr('href');
				$(this).parents('li').remove();
				$(tabID).remove();

				//display first tab
				var tabFirst = $('#tab-list a:first');
				resetTab();
				tabFirst.tab('show');
			});

			var list = document.getElementById("tab-list");
		});

		var editHandler = function() {
		  var t = $(this);
		  t.css("visibility", "hidden");
		  $(this).prev().attr("contenteditable", "true").focusout(function() {
			$(this).removeAttr("contenteditable").off("focusout");
			t.css("visibility", "visible");
		  });
		};

		$(".edit").click(editHandler);
		
		
		
		moment.locale('fr'); 
		
		
            // $('#reportrange1 span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

            $('#reportrange1').daterangepicker({
				language:'fr',
                format: 'YYYY-MM-DD',
                dateLimit: { days: 360 },
                showDropdowns: true,
                showWeekNumbers: true,
                timePicker: true,
                timePickerIncrement: 1,
                timePicker12Hour: true,
                ranges: {
                    'Aujourd\'hui': [moment(), moment()],
                    'Hier': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Les 7 derniers jours': [moment().subtract(6, 'days'), moment()],
                    'Les 30 derniers jours': [moment().subtract(29, 'days'), moment()],
                    'Ce mois-ci': [moment().startOf('month'), moment().endOf('month')],
                    'Le mois dernier': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                opens: 'right',
                drops: 'down',
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-primary',
                cancelClass: 'btn-default',
                separator: ' to ',
                locale: {
                    applyLabel: 'Enregistrer',
                    cancelLabel: 'Annuler',
                    fromLabel: 'À partir',
                    toLabel: 'jusqu\'à',
                    firstDay: 1,
					customRangeLabel: 'Personnalisé',
                }
            }, function(start, end, label) {
				start_date_1 = start.format('YYYY-MM-DD');
				end_date_1 = end.format('YYYY-MM-DD');
                // console.log(start.format('YYYY-MM-DD'), end.format('YYYY-MM-DD'));
                $('#reportrange1 span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            });
			
			
			
        $('#reportrange1 span').html('Selectionner la date de livraison ');
		$('#reportrange2 span').html('Selectionner la date de numérisation ');
            $('#reportrange2').daterangepicker({
				language:'fr',
                format: 'YYYY-MM-DD',
                dateLimit: { days: 360 },
                showDropdowns: true,
                showWeekNumbers: true,
                timePicker: true,
                timePickerIncrement: 1,
                timePicker12Hour: true,
                ranges: {
                    'Aujourd\'hui': [moment(), moment()],
                    'Hier': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Les 7 derniers jours': [moment().subtract(6, 'days'), moment()],
                    'Les 30 derniers jours': [moment().subtract(29, 'days'), moment()],
                    'Ce mois-ci': [moment().startOf('month'), moment().endOf('month')],
                    'Le mois dernier': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                opens: 'right',
                drops: 'down',
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-primary',
                cancelClass: 'btn-default',
                separator: ' to ',
                locale: {
                    applyLabel: 'Enregistrer',
                    cancelLabel: 'Annuler',
                    fromLabel: 'À partir',
                    toLabel: 'jusqu\'à',
                    firstDay: 1,
					customRangeLabel: 'Personnalisé',
                }
            }, function(start, end, label) {
				start_date_2 = start.format('YYYY-MM-DD');
				end_date_2 = end.format('YYYY-MM-DD');
				// alert('ok');
				
                // console.log(start.format('YYYY-MM-DD'), end.format('YYYY-MM-DD'));
                $('#reportrange2 span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            });
		
		
		$("#add_statut").change(function()
		{
			$("#add_date_distribution").hide();
			$("#add_motif").hide();
			$("#add_description").hide();
			$('#add_file_pdf').removeAttr('required');

			if ( $(this).val() == 2 ||  $(this).val() == 3 ||$(this).val() == 4  ) 
			{
				$("#add_date_distribution").show();
			}
			if ( $(this).val() == 3 ) 
			{
				$("#add_motif").show();
				$("#add_description").show();
			}
			if ( $(this).val() == 2 || $(this).val() == 3 ) 
			{
				$("#add_file_pdf").prop('required',true);
			}
		});

		$(document).on('change', '#edit_statut', function(){ 	
				
				$("#edit_date_distribution").hide();
				$("#edit_motif").hide();
				$("#edit_description").hide();
				$('#edit_file_pdf').removeAttr('required');
				
				if ( $(this).val() == 2 ||  $(this).val() == 3 ||$(this).val() == 4 ) 
				{
					$("#edit_date_distribution").show();
				}
				if ( $(this).val() == 3 ) 
				{
					$("#edit_motif").show();
					$("#edit_description").show();
				}
				if ( $(this).val() == 2 || $(this).val() == 3 ) 
				{
					$("#edit_file_pdf").prop('required',true);
				}

			});
			
		
    </script>
	
	 <script id="script">
        function showpdf(url)
		{
			

			var pdfDoc = null,
					pageNum = 1,
					pageRendering = false,
					pageNumPending = null,
					scale = 1.5,
					zoomRange = 0.25,
					canvas = document.getElementById('the-canvas'),
					ctx = canvas.getContext('2d');

			/**
			 * Get page info from document, resize canvas accordingly, and render page.
			 * @param num Page number.
			 */
			function renderPage(num, scale) {
				pageRendering = true;
				// Using promise to fetch the page
				pdfDoc.getPage(num).then(function(page) {
					var viewport = page.getViewport(scale);
					canvas.height = viewport.height;
					canvas.width = viewport.width;

					// Render PDF page into canvas context
					var renderContext = {
						canvasContext: ctx,
						viewport: viewport
					};
					var renderTask = page.render(renderContext);

					// Wait for rendering to finish
					renderTask.promise.then(function () {
						pageRendering = false;
						if (pageNumPending !== null) {
							// New page rendering is pending
							renderPage(pageNumPending);
							pageNumPending = null;
						}
					});
				});

				// Update page counters
				document.getElementById('page_num').value = num;
			}

			/**
			 * If another page rendering in progress, waits until the rendering is
			 * finised. Otherwise, executes rendering immediately.
			 */
			function queueRenderPage(num) {
				if (pageRendering) {
					pageNumPending = num;
				} else {
					renderPage(num,scale);
				}
			}

			/**
			 * Displays previous page.
			 */
			function onPrevPage() {
				if (pageNum <= 1) {
					return;
				}
				pageNum--;
				var scale = pdfDoc.scale;
				queueRenderPage(pageNum, scale);
			}
			document.getElementById('prev').addEventListener('click', onPrevPage);

			/**
			 * Displays next page.
			 */
			function onNextPage() {
				if (pageNum >= pdfDoc.numPages) {
					return;
				}
				pageNum++;
				var scale = pdfDoc.scale;
				queueRenderPage(pageNum, scale);
			}
			document.getElementById('next').addEventListener('click', onNextPage);

			/**
			 * Zoom in page.
			 */
			function onZoomIn() {
				if (scale >= pdfDoc.scale) {
					return;
				}
				scale += zoomRange;
				var num = pageNum;
				renderPage(num, scale)
			}
			document.getElementById('zoomin').addEventListener('click', onZoomIn);

			/**
			 * Zoom out page.
			 */
			function onZoomOut() {
				if (scale >= pdfDoc.scale) {
					return;
				}
				scale -= zoomRange;
				var num = pageNum;
				queueRenderPage(num, scale);
			}
			document.getElementById('zoomout').addEventListener('click', onZoomOut);

			/**
			 * Zoom fit page.
			 */
			function onZoomFit() {
				if (scale >= pdfDoc.scale) {
					return;
				}
				scale = 1;
				var num = pageNum;
				queueRenderPage(num, scale);
			}
			document.getElementById('zoomfit').addEventListener('click', onZoomFit);


			/**
			 * Asynchronously downloads PDF.
			 */
			PDFJS.getDocument(url).then(function (pdfDoc_) {
				pdfDoc = pdfDoc_;
				var documentPagesNumber = pdfDoc.numPages;
				document.getElementById('page_count').textContent = '/ ' + documentPagesNumber;

				$('#page_num').on('change', function() {
					var pageNumber = Number($(this).val());

					if(pageNumber > 0 && pageNumber <= documentPagesNumber) {
						queueRenderPage(pageNumber, scale);
					}

				});

				// Initial/first page rendering
				renderPage(pageNum, scale);
			});
		}
    </script>
	

</body>

</html>
