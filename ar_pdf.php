<?php 
include 'connexion.php';


$num_facture = 'num_facture';		
$code_client = 'code_client';		
$nom_client = 'nom_client';		
$date_comptabilisationt = 'date_comptabilisationt';		
$adresse = 'adresse';		
$ville = 'ville';		


ob_start();
?>
<page style="font-size: 12pt" backimgy="bottom">
    <page_footer>
        <hr />
        <p>eInvoiceTrack	&reg;,  le <?php echo date("d/m/y"); ?></p>
    </page_footer>
	
	<div style="width: 25%; text-align: left;">	
		<img style="width: 100%;" src="logo.png" alt="Logo">
	</div>
	<div style="width: 100%; text-align: right; margin-top:-80px">	
		<b>  Témara </b> le  <b><?php echo date("d/m/y"); ?> </b> <br> <br>
		<b> <?php echo $code_client ?> </b> <br><br>
		<b> <?php echo $nom_client ?> </b><br><br>
		<b> <?php echo $adresse ?> </b><br><br>
		<b> <?php echo $ville ?> </b><br><br>
	</div>


	<br>	
	<br>	
	<br>	
	<br>	
	<b><u>Objet</u>: &laquo;Accusé réception des factures&raquo;</b><br>
	<br>
	<br>
	<br>
	Messieurs,<br>
	 <br>
	Par la présente, vous accussez réception des factures :<br>
	 <br>
	N° : <b> <?php echo $num_facture ?></b> <br>
	 <br>
	Date comptabilisation : <b> <?php echo $date_comptabilisationt ?></b> <br>
	 <br>
	Date réception : <b> __/__/____ </b> <br>
	 <br>
	Motif de rejet :   <br>
	<br>
	<br>
	
	
	<div style="width:10px;height:10px;border:2px solid #000;margin-left:35px"></div> 
	<b style="margin-top:-18px;margin-left:62px">Manque bon de commande   </b>  
	<br>
	<br>
	<div style="width:10px;height:10px;border:2px solid #000;margin-left:35px"></div> 
	<b style="margin-top:-18px;margin-left:62px">Liasse incomplète    </b>  
	<br>
	<br>
	<div style="width:10px;height:10px;border:2px solid #000;margin-left:35px"></div> 
	<b style="margin-top:-18px;margin-left:62px">Manque bon de livraison     </b>  
	<br>
	<br>
	<div style="width:10px;height:10px;border:2px solid #000;margin-left:35px"></div> 
	<b style="margin-top:-18px;margin-left:62px">Montant Incorrect (HT, TVA et TTC)     </b>  
	<br>
	<br>
	
	
	<div style=" margin-top:-136px;margin-left : 290px" >
		<div style="width:10px;height:10px;border:2px solid #000;margin-left:60px"></div> 
		<b style="margin-top:-18px;margin-left:87px">Facture erronée( en double. date ...)    </b> 
		<br>
		<br>
		<div style="width:10px;height:10px;border:2px solid #000;margin-left:60px"></div> 
		<b style="margin-top:-18px;margin-left:87px">Litige     </b> 
		<br>
		<br>
		<div style="width:10px;height:10px;border:2px solid #000;margin-left:60px"></div> 
		<b style="margin-top:-18px;margin-left:87px">Refus  </b> 
		<br>
		<br>
		<div style="width:10px;height:10px;border:2px solid #000;margin-left:60px"></div> 
		<b style="margin-top:-18px;margin-left:87px">Mentions obligatoires  </b> 
		<br>
		<br>
	
	</div> 

	
	<br>
	<br>
	<br>
	<br>
	<br>
	Nous vous en remercions et vous prions d’agrer, messieurs, l’assurance de nos saluations distinguées.	<br>
		<br>
	Cachet et signature 

</page>
<?php 
    $content = ob_get_clean();
    require_once dirname(__FILE__).'/html2pdf/vendor/autoload.php';
    use Spipu\Html2Pdf\Html2Pdf;
    use Spipu\Html2Pdf\Exception\Html2PdfException;
    use Spipu\Html2Pdf\Exception\ExceptionFormatter;
    try
    {
		ob_end_clean();
		
        $html2pdf = new HTML2PDF('P', 'A4', 'fr', true, 'UTF-8',array(18, 10, 18, 10));
        $html2pdf->setDefaultFont("Arial");
		$html2pdf->pdf->SetProtection(array('print','copy'));
        $html2pdf->writeHTML($content);
		$html2pdf->Output('example_006.pdf', 'I');

    }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }
?>