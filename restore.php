<?php
ob_start();
session_start();
include 'connexion.php';

$folder_name = '';
$script_job = '';
$match = '';
if(isset($_SESSION['user_einvoicetrack']))
{
	
	$current_user = decode($_SESSION['user_einvoicetrack']) ; 					
	$SQL="SELECT  `NOM_USER`, `PRENOM_USER`, `EMAIL_USER`,`CIVILITE_USER`,
			`CODE_ENTREPRISE`, `ROLE_USER`
		  FROM `user` 
		  WHERE CODE_USER = $current_user
		  AND actif= 1";
	$query=mysqli_query($ma_connexion,$SQL);
	if(mysqli_num_rows($query) == 1)
	{
		while($row=mysqli_fetch_assoc($query))
		{	
				$NOM_USER = $row['CIVILITE_USER'].' ' .$row['NOM_USER'];
		}
	}
	else 
	{
		header('Location: login');
	}
	
	if( $_SESSION['role'] != 'superadmin' )
	{

		header('Location: users');
		
	}
	if(isset($_GET['target']))
	{
		$script_job = 'ERROR' ;
		$folder_name = 'backup/'.$_GET['target'] ;
		$files = scandir($folder_name);
		if(false !== $files)
		{
			foreach($files as $file)
			{
				if(!is_dir($file) )
				{
					$file_parts = pathinfo($file);
					if($file_parts['extension'] == "sql" ) 
						$script_job = $file ;  	
					
				}
		   
			}
		}
		
		// $folder_name_norm = $_GET['target'] ;
		
	}
	
	if(isset($_GET['match']))
	{
		$match =  $_GET['match'] ;

		
	}
		
	
		// $i = 0 ; 
												

		// $SQL="SHOW COLUMNS FROM client";
		// $query=mysqli_query($ma_connexion,$SQL);
		// while($row=mysqli_fetch_assoc($query))
		// {	
				// echo '
					// <a class="toggle-vis" data-column="'.$i++.'">'.$row['Field'].'</a> -
					// ';
		// }
}
else 
{
	header('Location: login');
}
?>
<!DOCTYPE html>
<html lang="fr">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>eInvoiceTrack</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
	
	<!-- Bootstrap Tour -->
    <link href="css/plugins/bootstrapTour/bootstrap-tour.min.css" rel="stylesheet">

	
	<link href="jquery-ui.css" rel="stylesheet">
	
	
	 <!-- Ladda style -->
    <link href="css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">

<style>

.myInputautocomplete-list {
  /*the container must be positioned relative:*/
  position: relative;
  display: inline-block;
}
.autocomplete {
  /*the container must be positioned relative:*/
  position: relative;
  display: inline-block;
}


.autocomplete-items {
  position: absolute;
  border: 1px solid #d4d4d4;
  border-bottom: none;
  border-top: none;
  z-index: 99;
  /*position the autocomplete items to be the same width as the container:*/
  top: 100%;
  left: 0;
  right: 0;
  width: 30%;

  overflow-y: scroll; max-height:300px; 
}
.autocomplete-items div {
  padding: 10px;
  cursor: pointer;
  background-color: #fff; 
  border-bottom: 1px solid #d4d4d4; 
}
.autocomplete-items div:hover {
  /*when hovering an item:*/
  background-color: #e9e9e9; 
}
.autocomplete-active {
  /*when navigating through the items using the arrow keys:*/
  background-color: DodgerBlue !important; 
  color: #ffffff; 
}
</style>
	
	

</head>

<body class="">

    <div id="wrapper">

    
		<?php
			include 'includes/nav.php';
		?>	

        <div id="page-wrapper" class="gray-bg">
		   <?php
				include 'includes/header.php';
			?>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2> <i class="fa fa-refresh"></i> Restauration </h2>
                    
                </div>	
				<div class="col-sm-8">
                    <div class="title-action">
					 <a href="#" class="btn btn-primary startTour"><i class="fa fa-play"></i> Démo</a>
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content">
            <div class="row">
                <div class="col-lg-3" >
                    <div class="ibox " id="tour_dossier">
                        <div class="ibox-content">
                            <div class="file-manager" >
                                <h5>Dossiers</h5>
                                <ul class="folder-list" style="padding: 0">
                                  <?php
								
										// $files = scandir('ar_gene');
										// if(false !== $files)
										// {
											// foreach($files as $file)
											// {
												// if(is_dir($file) )
												// {
													
													// echo'	
													// <li><a href="ar_list?target='.$file.'"><i class="fa fa-folder"></i> '.$file.'</a></li>
														// ';
													
												// }
										   
											// }
										// }
										
										$dir = new DirectoryIterator('backup');
										foreach ($dir as $fileinfo) {
											if ($fileinfo->isDir() && !$fileinfo->isDot()) {
												// echo $fileinfo->getFilename().'<br>';
												
													echo'	
													<li'; if($folder_name == 'backup/'.$fileinfo->getFilename()) echo 'style="background-color: #e1e1e2"' ; echo' ><a href="restore?target='.$fileinfo->getFilename().'"><i class="fa fa-folder"></i> '.$fileinfo->getFilename().'</a></li>
														';
													
											}
										}
									
									?>
                                </ul>
                              
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 animated fadeInRight" >
				
					
                    <div class="row" id="tour_list">
					<?php
						if($folder_name != '' ) 
						{
						echo '<input type="hidden" name="isAdminToken" id="isAdminToken" value="iuqsdCsrf">' ; 

						?>		
						
						
                       
						
                        <div class="col-lg-2" style="    background-color: white; overflow-y: scroll; max-height:500px;  margin-top: 10px; margin-bottom:10px;">
							<h3> <i class="fa fa-folder" aria-hidden="true"></i> tmp</h3>
							<div id="container_tmp"></div>
						</div>
                        <div class="col-lg-3" style="    background-color: white; overflow-y: scroll; max-height:500px;  margin-top: 10px; margin-bottom:10px;">
							<h3> <i class="fa fa-folder" aria-hidden="true"></i> einvoicetrack</h3>
							<div id="container_einvoicetrack"></div>
						</div>
                        <div class="col-lg-3" style="    background-color: white; overflow-y: scroll; max-height:500px;  margin-top: 10px; margin-bottom:10px;">
							<h3> <i class="fa fa-folder" aria-hidden="true"></i> einvoicetrack_backup</h3>
							<div id="container_einvoicetrack_backup"></div>
						</div>
                        <div class="col-lg-2" style="    background-color: white; overflow-y: scroll; max-height:500px;  margin-top: 10px; margin-bottom:10px;">
							<h3> <i class="fa fa-folder" aria-hidden="true"></i> dossier_bc</h3>
							<div id="container_dossier_bc"></div>
						</div>
                        <div class="col-lg-2" style="    background-color: white; overflow-y: scroll; max-height:500px;  margin-top: 10px; margin-bottom:10px;">
							<h3> <i class="fa fa-folder" aria-hidden="true"></i> dossier_bl</h3>
							<div id="container_dossier_bl"></div>
						</div>
						
						 <div class="col-lg-12" >
							<div class="d-flex justify-content-center">
								<button type="button" class="btn btn-primary" id="restore_btn"> <i class="fa fa-refresh"></i> Restaurer </button>

							</div>

						</div>
						
					<?php
						}
						?>
					</div>
                        
				</div>

			</div>
		</div>
				<?php
						include 'includes/footer.php';
					?>	

        </div>
            </div>


    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>
	
	<script src="js/plugins/pdfjs/pdf.js"></script>
	
	

	   <!-- Bootstrap Tour -->
    <script src="js/plugins/bootstrapTour/bootstrap-tour.min.js"></script>
	
	<script src="jquery-ui.js"></script>

		<script src="jquery.easing.js" type="text/javascript"></script>
		<script src="jqueryFileTree.js" type="text/javascript"></script>
		<link href="jqueryFileTree.css" rel="stylesheet" type="text/css" media="screen" />
		
    <!-- Ladda -->
    <script src="js/plugins/ladda/spin.min.js"></script>
    <script src="js/plugins/ladda/ladda.min.js"></script>
    <script src="js/plugins/ladda/ladda.jquery.min.js"></script>



	 <!-- Sweet alert -->
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	
	 <script id="script">
	 
	
$(document).ready( function() {
   if($('#isAdminToken').val())
   {
	  
	$('#container_tmp').fileTree({ root: "/<?php echo $folder_name ?>/tmp/", script: 'connectors/jqueryFileTree.php' }, function(file) { 
		file = file.substring(1);
		var a = document.createElement('A');
		a.href = file;
		a.download = file.substr(file.lastIndexOf('/') + 1);
		document.body.appendChild(a);
		a.click();
		document.body.removeChild(a);
	});
	$('#container_einvoicetrack').fileTree({ root: "/<?php echo $folder_name ?>/einvoicetrack/", script: 'connectors/jqueryFileTree.php' }, function(file) { 
		file = file.substring(1);
var a = document.createElement('A');
		a.href = file;
		a.download = file.substr(file.lastIndexOf('/') + 1);
		document.body.appendChild(a);
		a.click();
		document.body.removeChild(a);
	});
	$('#container_einvoicetrack_backup').fileTree({ root: "/<?php echo $folder_name ?>/einvoicetrack_backup/", script: 'connectors/jqueryFileTree.php' }, function(file) { 
		file = file.substring(1);
var a = document.createElement('A');
		a.href = file;
		a.download = file.substr(file.lastIndexOf('/') + 1);
		document.body.appendChild(a);
		a.click();
		document.body.removeChild(a);
	});
	$('#container_dossier_bc').fileTree({ root: "/<?php echo $folder_name ?>/dossier_bc/", script: 'connectors/jqueryFileTree.php' }, function(file) { 
		file = file.substring(1);
var a = document.createElement('A');
		a.href = file;
		a.download = file.substr(file.lastIndexOf('/') + 1);
		document.body.appendChild(a);
		a.click();
		document.body.removeChild(a);
	});
	$('#container_dossier_bl').fileTree({ root: "/<?php echo $folder_name ?>/dossier_bl/", script: 'connectors/jqueryFileTree.php' }, function(file) { 
		file = file.substring(1);
		var a = document.createElement('A');
		a.href = file;
		a.download = file.substr(file.lastIndexOf('/') + 1);
		document.body.appendChild(a);
		a.click();
		document.body.removeChild(a);
	});
	
	
   }
});


	var countries ;  
$(document).ready(function(){
	
	
	
	
		var tour = new Tour({
            steps: [
                {
                    element: "#tour_dossier",
                    title: "Ajouter client",
                    content: "Vous pouvez ajouter un client manuellement <br/> Les champs marqués par ( <span style='color:red'>*</span> ) sont obligatoires",
                    placement: "top",
                    backdrop: true,
                    backdropContainer: '#wrapper',
                    onShown: function (tour){
                        $('body').addClass('tour-open')
                    },
                    onHidden: function (tour){
                        $('body').removeClass('tour-close')
                    }
                },
                {
                    element: "#tour_search",
                    title: "Importer client",
                    content: "Vous pouvez importer des clients ici",
                    placement: "top",
                    backdrop: true,
                    backdropContainer: '#wrapper',
                    onShown: function (tour){
                        $('body').addClass('tour-open')
                    },
                    onHidden: function (tour){
                        $('body').removeClass('tour-close')
                    }
                },
                {
                    element: "#tour_list",
                    title: "Rechercher client",
                    content: "Vous pouvez r des clients ici",
                    placement: "top",
                    backdrop: true,
                    backdropContainer: '#wrapper',
                    onShown: function (tour){
                        $('body').addClass('tour-open')
                    },
                    onHidden: function (tour){
                        $('body').removeClass('tour-close')
                    }
                },
                
            ],
			smartPlacement: true,
			  keyboard: true,
			  storage: false,
			  debug: true,
			  labels: {
				end: 'Terminer',
				next: 'Suivant &raquo;',
				prev: '&laquo; Prev'
			  },	
			  });

        // Initialize the tour
        tour.init();

         $('.startTour').click(function(){
            tour.restart();

            // Start the tour
            // tour.start();
        })
});


	 $('.add_preview').on('click', function() {
		  
		$("#preview_div").insertAfter($(this).parent().parent());
		showpdf($(this).attr("name"));

	});
        function showpdf(url)
		{
			

			var pdfDoc = null,
					pageNum = 1,
					pageRendering = false,
					pageNumPending = null,
					scale = 1.5,
					zoomRange = 0.25,
					canvas = document.getElementById('the-canvas'),
					ctx = canvas.getContext('2d');

			/**
			 * Get page info from document, resize canvas accordingly, and render page.
			 * @param num Page number.
			 */
			function renderPage(num, scale) {
				pageRendering = true;
				// Using promise to fetch the page
				pdfDoc.getPage(num).then(function(page) {
					var viewport = page.getViewport(scale);
					canvas.height = viewport.height;
					canvas.width = viewport.width;

					// Render PDF page into canvas context
					var renderContext = {
						canvasContext: ctx,
						viewport: viewport
					};
					var renderTask = page.render(renderContext);

					// Wait for rendering to finish
					renderTask.promise.then(function () {
						pageRendering = false;
						if (pageNumPending !== null) {
							// New page rendering is pending
							renderPage(pageNumPending);
							pageNumPending = null;
						}
					});
				});

				// Update page counters
				document.getElementById('page_num').value = num;
			}

			/**
			 * If another page rendering in progress, waits until the rendering is
			 * finised. Otherwise, executes rendering immediately.
			 */
			function queueRenderPage(num) {
				if (pageRendering) {
					pageNumPending = num;
				} else {
					renderPage(num,scale);
				}
			}

			/**
			 * Displays previous page.
			 */
			function onPrevPage() {
				if (pageNum <= 1) {
					return;
				}
				pageNum--;
				var scale = pdfDoc.scale;
				queueRenderPage(pageNum, scale);
			}
			document.getElementById('prev').addEventListener('click', onPrevPage);

			/**
			 * Displays next page.
			 */
			function onNextPage() {
				if (pageNum >= pdfDoc.numPages) {
					return;
				}
				pageNum++;
				var scale = pdfDoc.scale;
				queueRenderPage(pageNum, scale);
			}
			document.getElementById('next').addEventListener('click', onNextPage);

			/**
			 * Zoom in page.
			 */
			function onZoomIn() {
				if (scale >= pdfDoc.scale) {
					return;
				}
				scale += zoomRange;
				var num = pageNum;
				renderPage(num, scale)
			}
			document.getElementById('zoomin').addEventListener('click', onZoomIn);

			/**
			 * Zoom out page.
			 */
			function onZoomOut() {
				if (scale >= pdfDoc.scale) {
					return;
				}
				scale -= zoomRange;
				var num = pageNum;
				queueRenderPage(num, scale);
			}
			document.getElementById('zoomout').addEventListener('click', onZoomOut);

			/**
			 * Zoom fit page.
			 */
			function onZoomFit() {
				if (scale >= pdfDoc.scale) {
					return;
				}
				scale = 1;
				var num = pageNum;
				queueRenderPage(num, scale);
			}
			document.getElementById('zoomfit').addEventListener('click', onZoomFit);


			/**
			 * Asynchronously downloads PDF.
			 */
			PDFJS.getDocument(url).then(function (pdfDoc_) {
				pdfDoc = pdfDoc_;
				var documentPagesNumber = pdfDoc.numPages;
				document.getElementById('page_count').textContent = '/ ' + documentPagesNumber;

				$('#page_num').on('change', function() {
					var pageNumber = Number($(this).val());

					if(pageNumber > 0 && pageNumber <= documentPagesNumber) {
						queueRenderPage(pageNumber, scale);
					}

				});

				// Initial/first page rendering
				renderPage(pageNum, scale);
			});
		}
		
		
		$(document).on('click', '#restore_btn', function(e){
					e.stopPropagation();
					$("#myModal").modal({backdrop: 'static', keyboard: false})  
					
			});
			
			$(document).ready(function (){

        


        var l = $( '.restor_button' ).ladda();

        l.click(function(){
            // Start loading
			$("#annuler_restore").prop("disabled",true);
            l.ladda( 'start' );
			var dataString = "dossier_job=<?php echo $folder_name ?>"+"&mdp="+$( "#password1" ).val()+"&script_job=<?php echo $script_job ?>" ; 
			$.ajax({
					url : 'restore_job.php',               
					type: 'POST',    
					data: dataString,
					success: function(log){
						console.log(log);
						$("#log_res").html(log);
						l.ladda('stop');
						("#annuler_restore").prop("disabled",false);
						("#annuler_restore").attr("disabled",false);
						
					}
			   });

        });

    });
		
		
    </script>

	
	
	
 <div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered">
	<div class="modal-content animated bounceInRight">
		<form method="POST" >
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<i class="fa fa-refresh modal-icon"></i>
				<h4 class="modal-title">Restaurer la version</h4>
				<h3 class="modal-title modal-version"><?php echo $folder_name ;	?></h3>
			</div>
			<div class="modal-body" >
				<div id="log_res">
					
				</div>
				<div class="form-group">
					<span class="label label-info float-left">Veuillez saisir le mot de passe </span>  <span class="required">* </span>
					<input type="password" class="form-control" id="password1" name="password1" placeholder="**********">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-white" id="annuler_restore" data-dismiss="modal">Annuler</button>
				<button class="ladda-button btn btn-primary restor_button" type="button" name="dossier_job" data-style="expand-right">Restaurer</button>
			</div>
		</form>
	</div>
	</div>
</div>
</body>

</html>

