<?php
ob_start();
session_start();
include 'connexion.php';

$folder_name = 'tmp';
$match = '';
if(isset($_SESSION['user_einvoicetrack']))
{
	
	$current_user = decode($_SESSION['user_einvoicetrack']) ; 					
	$SQL="SELECT  `NOM_USER`, `PRENOM_USER`, `EMAIL_USER`,`CIVILITE_USER`,
			`CODE_ENTREPRISE`, `ROLE_USER`
		  FROM `user` 
		  WHERE CODE_USER = $current_user
		  AND actif= 1";
	$query=mysqli_query($ma_connexion,$SQL);
	if(mysqli_num_rows($query) == 1)
	{
		while($row=mysqli_fetch_assoc($query))
		{	
				$NOM_USER = $row['CIVILITE_USER'].' ' .$row['NOM_USER'];
				$EMAIL_USER = $row['EMAIL_USER'] ;
			
		}
	}
	else 
	{
		header('Location: login');
	}
	
	
	if(isset($_GET['target']))
	{
		if($_GET['target'] == 'tmp')
			$folder_name =  'tmp';
		else if($_GET['target'] == 'einvoicetrack')
			$folder_name =  'einvoicetrack'; 
		
	}
	
	if(isset($_GET['match']))
	{
		$match =  $_GET['match'] ;

		
	}
		
	
		// $i = 0 ; 
												

		// $SQL="SHOW COLUMNS FROM client";
		// $query=mysqli_query($ma_connexion,$SQL);
		// while($row=mysqli_fetch_assoc($query))
		// {	
				// echo '
					// <a class="toggle-vis" data-column="'.$i++.'">'.$row['Field'].'</a> -
					// ';
		// }
}
else 
{
	header('Location: login');
}

?>
<!DOCTYPE html>
<html lang="fr">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>eInvoiceTrack</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
	<link href="css/plugins/dropzone/basic.css" rel="stylesheet">
    <link href="css/plugins/dropzone/dropzone.css" rel="stylesheet">
    <link href="css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
	
	 <link href="css/plugins/dataTables/datatables.min.css" rel="stylesheet">

	
	<link href="css/plugins/select2/select2.min.css" rel="stylesheet">
	
	
	<!-- Date range picker -->
     <link href="css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
	 

	
	
	
	

<style>

.select2 {
	width:100%!important;
	}

.timeline-item {
	width:100%!important;
	}

timeline-item.label{
	
	 font-size: 13px;
}
	
	
</style>
	
</head>

<body class="">

    <div id="wrapper">

    
		<?php
			include 'includes/nav.php';
		?>	

        <div id="page-wrapper" class="gray-bg">
		   <?php
				include 'includes/header.php';
			?>
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-9">
                    <h2>Profil</h2>
					
				</div>
            </div>
               
        <div class="wrapper wrapper-content">
            <div class="row">
			
			
			
			
               
                <div class="col-lg-4">
                    <div class="ibox" id="ibox_import">
						 <div class="ibox-title" style="background-color: #24c6c8; color: white;">
						 <h5> <i class="fa fa-file"></i> Mes informations</h5>
						 
						</div>
							<div class="ibox-content">
								<form class="m-t" method="POST">
									<div class="col-md-12">
										<div class="form-group">
											<span class="badge badge-success">Nom</span>
											<div class="alert alert-primary">
												<?php echo $NOM_USER ; ?>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<span class="badge badge-success">Email</span>
											<div class="alert alert-primary">
												<?php echo $EMAIL_USER ; ?>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<span class="badge badge-success">Mot de passe</span>
											 <button type="submit" name="submit" class="btn btn-primary block full-width m-b"> <i class="fa fa-key" aria-hidden="true"></i>  Envoyer un e-mail de rénitialisation</button>
									
										</div>
									</div>

								</form>
							</div>
						   
					</div>
                </div>
				
				 <div class="col-lg-8">
                <div class="ibox"  id="ibox_result">
                    <div class="ibox-title" style="background-color: #24c6c8; color: white;">
                        <h5> <i class="fa fa-tasks"></i> Actions</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#" class="dropdown-item">Config option 1</a>
                                </li>
                                <li><a href="#" class="dropdown-item">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>

                    <div class="ibox-content ">
					<div class="sk-spinner sk-spinner-wave " >
                                    <div class="sk-rect1"></div>
                                    <div class="sk-rect2"></div>
                                    <div class="sk-rect3"></div>
                                    <div class="sk-rect4"></div>
                                    <div class="sk-rect5"></div>
                                </div>
									<?php
									if( $_SESSION['role'] ==  'superadmin' ||  $_SESSION['role'] ==  'admin' )
									{									
										?>
							<div class="row">
								<div class="col-md-12">
									<span class="badge badge-success">Utilisateur</span>
									<select class="sc_select2 form-control"  name="USERS_SEARCH[]" id="USERS_SEARCH" data-placeholder="Entrer nom Utilisateur"  multiple="multiple">
										<?php
													$query = " SELECT CODE_USER,NOM_USER 
																FROM user
																
													 ";
													$result = mysqli_query($ma_connexion, $query); 
												   while(($row = mysqli_fetch_array($result)) == true )  
													{ 										
	
														echo ' <option value="'.$row['CODE_USER'].'" selected>'.$row['NOM_USER'].'</option>' ;
														 
													}
												?>						
									</select>
								</div>	
								<div class="col-md-12">
									<span class="badge badge-success">Date</span> 
									 <div id="reportrange1" class="form-control">
										<i class="fa fa-calendar"></i>
										<span></span> <b class="caret"></b>
									</div>
								</div>	
									
								<div class="col-md-12">
									<br/>
									<button id="rechercher" class="btn btn-primary pull-right btn-lg" type="button"><i class="fa fa-refresh"></i>
									</button>
								</div>								
							</div>		
							<hr size="5">	
<?php
									}									
										?>							
							
							<div class="inspinia-timeline" style="overflow-y: scroll; max-height:600px;  margin-top: 10px; margin-bottom:10px; background-color: #fafafa;">
								<?php 
								$date_end = date('Y-m-d');
								$date_start = date('Y-m-d', strtotime($date_end." -1 month"));
								
								 $SQL="SELECT a.ID,a.TITRE, a.DESCRIPTION, a.DATE,u.NOM_USER,u.CIVILITE_USER
										FROM action a,user u
										WHERE a.USER = u.CODE_USER
										AND DATE(a.DATE) >= '$date_start' AND '$date_end'
										ORDER BY a.ID DESC  ";
										// echo $SQL ; 
									$query=mysqli_query($ma_connexion,$SQL);
									while($row=mysqli_fetch_assoc($query))
									{	
								?>	

									 <div class="timeline-item" >
										<div class="row">
											<div class="col-3 date">
												<i class="fa fa-briefcase"></i>
												<?php echo '<label style=" font-size: 13px;" class="label label-success "> '.$row['CIVILITE_USER'].' '.$row['NOM_USER'].'</label>'; ?>
												<?php //echo $row['CIVILITE_USER'] == 'Mr' ? '<i class="fa fa-male red"></i> Mr'.$row['NOM_USER'] : '<i class="fa fa-female"></i> Mme'.$row['NOM_USER'] ?>
												<br/>
												<?php echo '<label style=" font-size: 13px;" class="label label-secondary "> '.$row['DATE'].'</label>'; ?>
											</div>
											<div class="col-8 content no-top-border">
												<p class="m-b-xs"><strong><?php echo '<label style=" font-size: 13px;"class="label label-info "> '.$row['TITRE'].'</label>';?></strong></p>

												<p><?php echo $row['DESCRIPTION'] ; ?>.</p>
											</div>
										</div>
									</div>
								<?php 
								}
								?>
							   
								
							</div>
                    </div>
                </div>
            </div>
				
				
				
				
               
                </div>
                </div>
				<?php
						include 'includes/footer.php';
					?>	

        </div>
            </div>


    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>

	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

 <!-- Date range use moment.js same as full calendar plugin -->
    <script src="js/plugins/fullcalendar/moment.min.js"></script>

	 <!-- Date range picker -->
    <script src="js/plugins/daterangepicker/daterangepicker.js"></script>

	
    <!-- Select2 -->
    <script src="js/plugins/select2/select2.full.min.js"></script>
	<script src="js/i18n/fr.js"></script>
	
	

<script>

$(document).ready(function(){
		
		$.fn.select2.defaults.set('language', 'fr');
		$(".sc_select2").select2({
			allowClear: true,
			tags: true,
			language: "fr",
			 width: '100%',
		});	
		
		 var start_date_1 = null ;
        var end_date_1 = null ;
		
		$('#reportrange1 span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

		$('#reportrange1').daterangepicker({
				language:'fr',
                format: 'YYYY-MM-DD',
				startDate: moment().subtract(29, 'days'),
                endDate: moment(),
                dateLimit: { days: 360 },
                showDropdowns: true,
                showWeekNumbers: true,
                timePicker: true,
                timePickerIncrement: 1,
                timePicker12Hour: true,
                ranges: {
                    'Aujourd\'hui': [moment(), moment()],
                    'Hier': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Les 7 derniers jours': [moment().subtract(6, 'days'), moment()],
                    'Les 30 derniers jours': [moment().subtract(29, 'days'), moment()],
                    'Ce mois-ci': [moment().startOf('month'), moment().endOf('month')],
                    'Le mois dernier': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                opens: 'right',
                drops: 'down',
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-primary',
                cancelClass: 'btn-default',
                separator: ' to ',
                locale: {
                    applyLabel: 'Enregistrer',
                    cancelLabel: 'Annuler',
                    fromLabel: 'À partir',
                    toLabel: 'jusqu\'à',
                    firstDay: 1,
					customRangeLabel: 'Personnalisé',
                }
            }, function(start, end, label) {
				start_date_1 = start.format('YYYY-MM-DD');
				end_date_1 = end.format('YYYY-MM-DD');
                // console.log(start.format('YYYY-MM-DD'), end.format('YYYY-MM-DD'));
                $('#reportrange1 span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            });
			
			
			
			
			$(document).on('click', '#rechercher', function(){ 	
				$.ajax(
				{
					type : 'POST',
					url : 'get/get_profile_action_search.php',
					data: { 
							"USERS_SEARCH" : $('#USERS_SEARCH').val(),
							"DATE_SEARCH_START" : start_date_1,
							"DATE_SEARCH_END" : end_date_1
							},
					success : function(data)
					{

						$(".inspinia-timeline").html(data);
						
						
					},
					complete : function(data)
					{
						$('#ibox_result').children('.ibox-content').toggleClass('sk-loading');
					},
					beforeSend : function(data)
					{
						$('#ibox_result').children('.ibox-content').toggleClass('sk-loading');
					},
					error: function (jqXHR, exception) {
						var msg = '';
						if (jqXHR.status === 0) {
							msg = 'Not connect.\n Verify Network.';
						} else if (jqXHR.status == 404) {
							msg = 'Requested page not found. [404]';
						} else if (jqXHR.status == 500) {
							msg = 'Internal Server Error [500].';
						} else if (exception === 'parsererror') {
							msg = 'Requested JSON parse failed.';
						} else if (exception === 'timeout') {
							msg = 'Time out error.';
						} else if (exception === 'abort') {
							msg = 'Ajax request aborted.';
						} else {
							msg = 'Uncaught Error.\n' + jqXHR.responseText;
						}
					}
				});

			});
			
})
</script>
</body>

</html>





<?php


if( isset($_POST['submit']) )
{
			
		$CODE_USER = encode2($current_user);
		$flag =   encode(strtotime(date('Y-m-d H:i:s')));
		
		
		echo '
		<script>
				$.ajax({
				url : "https://www.einvoicetrack.com/phpMailer/reset.php",
				type : "POST",
				data : {"name":"'.$NOM_USER.'", "to" : "'.$EMAIL_USER.'","user" : "'.$CODE_USER.'","flag" : "'.$flag.'"},
				success : function(code_html, statut){
				},
			});
			
			
			Swal.fire({
			  type: "success",
			  title: "Veuillez vérifier votre boîte de réception ",
			  showConfirmButton: false,
			  timer: 3000
			})
			
		</script>';
		// echo '<script language="Javascript">document.location.replace("'.$_SERVER['SCRIPT_NAME']."?".$_SERVER['QUERY_STRING'].'"); </script>';  
			
							
	
}
	
?>

